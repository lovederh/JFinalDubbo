package edu.dsideal.campus.framework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dsideal-lee on 2016/12/9.
 */
public class RegexTest {
    public static void main(String[] args) {

        String content = " and editType ='天气,气象,预报,天气预报,气象预报'>";
        String regex = "editType='([^']*)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(content);
        if (m.find())
        {
            System.out.println(m.group(1));
        }
    }
}
