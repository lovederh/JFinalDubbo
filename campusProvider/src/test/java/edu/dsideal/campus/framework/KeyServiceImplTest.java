package edu.dsideal.campus.framework;

import com.xiaoleilu.hutool.util.DateUtil;
import edu.dsideal.campus.framework.impl.KeyServiceImpl;
import edu.dsideal.campus.framework.impl.KeyServiceImpl;

import java.math.BigDecimal;

/**
 * Created by dsideal-lee on 2016/10/31.
 */
public class KeyServiceImplTest extends Thread{
    public static void main(String[] args) {
        StringBuilder keySb = new StringBuilder();
        char[] c = "233".toCharArray();
        //得到当前树的级数
        int curLevel =  Integer.parseInt("2");
        //得到当前级的结构位数
        int level = Integer.parseInt(c[curLevel-1]+"");
        for (int i = 0; i < level-1; i++) {
            keySb.append("0");
        }
        keySb.append("1");
        System.out.println(keySb.toString());
        System.out.println(new BigDecimal("233").add(BigDecimal.ONE));
    }

    @Override
    public void run() {
        //开始字符串
        String beginWith = DateUtil.today();
        //存储日期流水的StringBuilder
        StringBuilder sb = new StringBuilder(beginWith);
        KeyServiceImpl.createFlowNum(sb, "1", 5, beginWith);
        System.out.println(sb.toString());
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
