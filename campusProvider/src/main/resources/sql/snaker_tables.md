
DROP TABLE IF EXISTS `wf_cc_order`;
CREATE TABLE `wf_cc_order` (
  `order_Id` varchar(50)  COMMENT '流程实例ID',
  `actor_Id` varchar(200)  COMMENT '参与者ID',
  `creator` varchar(200)  COMMENT '发起人',
  `create_Time` varchar(50)  COMMENT '抄送时间',
  `finish_Time` varchar(50)  COMMENT '完成时间',
  `status` int(2)  COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抄送实例表';


DROP TABLE IF EXISTS `wf_hist_task`;
CREATE TABLE `wf_hist_task` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(50)  COMMENT '流程实例ID',
  `task_Name` varchar(100)  COMMENT '任务名称',
  `display_Name` varchar(100)  COMMENT '任务显示名称',
  `task_Type` int(2)  COMMENT '任务类型',
  `perform_Type` int(2)  COMMENT '参与类型',
  `task_State` int(2)  COMMENT '任务状态',
  `operator` varchar(200)  COMMENT '任务处理人',
  `create_Time` varchar(50)  COMMENT '任务创建时间',
  `finish_Time` varchar(50)  COMMENT '任务完成时间',
  `expire_Time` varchar(50)  COMMENT '任务期望完成时间',
  `action_Url` varchar(200)  COMMENT '任务处理url',
  `parent_Task_Id` varchar(50)  COMMENT '父任务ID',
  `variable` varchar(5000) COMMENT '附属变量json存储',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='历史任务表';


DROP TABLE IF EXISTS `wf_hist_task_actor`;
CREATE TABLE `wf_hist_task_actor` (
  `task_Id` varchar(50)  COMMENT '任务ID',
  `actor_Id` varchar(200)  COMMENT '参与者ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='历史任务参与者表';


DROP TABLE IF EXISTS `wf_order`;
CREATE TABLE `wf_order` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `parent_Id` varchar(50)  COMMENT '父流程ID',
  `process_Id` varchar(50)  COMMENT '流程定义ID',
  `creator` varchar(200)  COMMENT '发起人',
  `create_Time` varchar(50)  COMMENT '发起时间',
  `expire_Time` varchar(50)  COMMENT '期望完成时间',
  `last_Update_Time` varchar(50)  COMMENT '上次更新时间',
  `last_Updator` varchar(200)  COMMENT '上次更新人',
  `priority` int(2)  COMMENT '优先级',
  `parent_Node_Name` varchar(100)  COMMENT '父流程依赖的节点名称',
  `order_No` varchar(100)  COMMENT '流程实例编号',
  `variable` varchar(5000) COMMENT '附属变量json存储',
  `version` int  COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程实例表';


DROP TABLE IF EXISTS `wf_surrogate`;
CREATE TABLE `wf_surrogate` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `process_Name` varchar(100)  COMMENT '流程名称',
  `operator` varchar(200)  COMMENT '授权人',
  `surrogate` varchar(200)  COMMENT '代理人',
  `odate` varchar(64)  COMMENT '操作时间',
  `sdate` varchar(64)  COMMENT '开始时间',
  `edate` varchar(64)  COMMENT '结束时间',
  `state` int(2)  COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='委托代理表';


DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(50)  COMMENT '流程实例ID',
  `task_Name` varchar(100)  COMMENT '任务名称',
  `display_Name` varchar(100)  COMMENT '任务显示名称',
  `task_Type` int(2)  COMMENT '任务类型',
  `perform_Type` int(2)  COMMENT '参与类型',
  `operator` varchar(200)  COMMENT '任务处理人',
  `create_Time` varchar(50)  COMMENT '任务创建时间',
  `finish_Time` varchar(50)  COMMENT '任务完成时间',
  `expire_Time` varchar(50)  COMMENT '任务期望完成时间',
  `action_Url` varchar(200)  COMMENT '任务处理的url',
  `parent_Task_Id` varchar(50)  COMMENT '父任务ID',
  `variable` varchar(5000) COMMENT '附属变量json存储',
  `version` int  COMMENT '版本',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务表';


DROP TABLE IF EXISTS `wf_task_actor`;
CREATE TABLE `wf_task_actor` (
  `task_Id` varchar(50)  COMMENT '任务ID',
  `actor_Id` varchar(200)  COMMENT '参与者ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务参与者表';


DROP TABLE IF EXISTS `wf_hist_order`;
CREATE TABLE `wf_hist_order` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `process_Id` varchar(50) COMMENT '流程定义ID',
  `order_State` int(2)  COMMENT '状态',
  `creator` varchar(200) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50)  COMMENT '发起时间',
  `end_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `priority` int(2) DEFAULT NULL COMMENT '优先级',
  `parent_Id` varchar(50) DEFAULT NULL COMMENT '父流程ID',
  `order_No` varchar(100) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(5000) COMMENT '附属变量json存储',
  `orderTitle` varchar(150) COMMENT '工单主题',
  domain varchar(100) COMMENT '企业域',
  delFlag varchar(2) COMMENT '逻辑删除标识(0未删除;1已删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='历史流程实例表';


DROP TABLE IF EXISTS `wf_process`;
CREATE TABLE `wf_process` (
  `id` varchar(50) NOT NULL COMMENT '主键ID',
  `name` varchar(100)  COMMENT '流程名称',
  `display_Name` varchar(100)  COMMENT '流程显示名称',
  `type` varchar(100)  COMMENT '流程类型',
  `instance_Url` varchar(200)  COMMENT '实例url',
  `state` int(2)  COMMENT '流程是否可用',
  `content` longblob COMMENT '流程模型定义',
  `version` int  COMMENT '版本',
  `create_Time` varchar(50)  COMMENT '创建时间',
  `creator` varchar(200)  COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程定义表';



-- 流程信息配置
DROP TABLE IF EXISTS `wf_processSet`;
CREATE TABLE `wf_processSet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processName` varchar(100) DEFAULT NULL COMMENT '流程编号(英文名)',
  `processDisplay` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `processType` varchar(10) DEFAULT NULL COMMENT '流程类型',
    `processImg` varchar(200) DEFAULT NULL COMMENT '流程图片',
  `customType` varchar(10) COMMENT '流程定制类型',
    `customVersion` int COMMENT '流程定制版本号',
  `isDeploy` varchar(2) COMMENT '是否已安装',
  `processId` varchar(50) COMMENT '安装后流程表ID',
  `deployFilePath` varchar(200) COMMENT '安装流程时使用文件路径',
  `deployFile` varchar(100) COMMENT '安装流程时使用文件',
  `isMultiForm` varchar(2) COMMENT '是否多任务多表单(常N)',
  `formType` varchar(10) COMMENT '流程启动表单类型',
    `formName` varchar(100) COMMENT '流程启动表单编码',
    `formValue` varchar(50) COMMENT '流程启动表单值',
  `dataTable` varchar(50) COMMENT '流程启动使用数据表',
  `hasCcOperator` varchar(2) COMMENT '流程启动是否启用抄送',
  `expireDay` varchar(10) COMMENT '工单时限天数(只支持整数)',
      `interceptor` varchar(255) COMMENT '流程处理拦截器',
  domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程信息配置';



-- 流程与任务节点配置表
DROP TABLE IF EXISTS `wf_processTaskSet`;
CREATE TABLE `wf_processTaskSet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50)  COMMENT '流程配置表ID',
  `processId` varchar(50)  COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
  `processVersion` int COMMENT '流程版本号_del',
  `taskName` varchar(100)  COMMENT '任务节点英文名',
    `taskDisplay` varchar(100)  COMMENT '任务节点英文名',
  `taskType` varchar(20) COMMENT '任务节点类型',
  `isStartTask` varchar(2) COMMENT '是否为启动环节',
  `formType` varchar(10) COMMENT '任务节点表单类型',
    `formName` varchar(100) COMMENT '任务节点表单编码',
    `formValue` varchar(50) COMMENT '任务节点表单值',
    `dataTable` varchar(50) COMMENT '任务节点使用数据表',
    `hasApproval` varchar(2) COMMENT '是否包含审批域_del',
    `hasCcOperator` varchar(2) COMMENT '是否包含抄送域_del',
    domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程与任务节点配置';



-- 流程与任务操作人配置表
DROP TABLE IF EXISTS `wf_processTaskActorSet`;
CREATE TABLE `wf_processTaskActorSet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50)  COMMENT '流程配置表ID',
  `processTaskSetId` varchar(50) COMMENT '流程任务配置ID',
  `processId` varchar(50)  COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
  `taskName` varchar(100)  COMMENT '任务节点英文名',
  `actorType` varchar(10) COMMENT '任务操作人类型',
  `actorValue` varchar(200) COMMENT '任务操作人值',
  domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程与任务操作人配置';

-- 当前环节前往表(即下一环节集合, 与当前环节1对多)
DROP TABLE IF EXISTS `wf_processTaskTo`;
CREATE TABLE `wf_processTaskTo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50)  COMMENT '流程配置表ID',
  `processTaskSetId` varchar(50) COMMENT '流程任务配置ID',
  `processId` varchar(50)  COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
  `taskName` varchar(100)  COMMENT '任务节点英文名',
  `toTaskName` varchar(100)  COMMENT '前往的任务节点名',
    domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='当前环节前往表';



-- 流程内决策节点来源环节表
DROP TABLE IF EXISTS `wf_processDecisionFrom`;
CREATE TABLE `wf_processDecisionFrom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50) DEFAULT NULL COMMENT '流程配置表ID',
  `processId` varchar(50) DEFAULT NULL COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
   `decisionNodeId` varchar(50) DEFAULT NULL COMMENT '决策节点总表ID',
   `fromTaskName` varchar(100) DEFAULT NULL COMMENT '来源环节英文名称',
   `fromTaskDisplay` varchar(100) DEFAULT NULL COMMENT '来源环节名称',
   domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程内决策节点来源环节表';


-- 流程内决策节点总表
DROP TABLE IF EXISTS `wf_processDecisionNode`;
CREATE TABLE `wf_processDecisionNode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50) DEFAULT NULL COMMENT '流程配置表ID',
  `processId` varchar(50) DEFAULT NULL COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
  `decisionNodeName` varchar(100) DEFAULT NULL COMMENT '决策节点英文名称',
  `decisionNodeDisplay` varchar(100) DEFAULT NULL COMMENT '决策节点显示名称',
  domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程内决策节点总表';


-- 流程内决策条件配置表
DROP TABLE IF EXISTS `wf_processDecisions`;
CREATE TABLE `wf_processDecisions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `processSetId` varchar(50) DEFAULT NULL COMMENT '流程配置表ID',
  `processId` varchar(50) DEFAULT NULL COMMENT '流程ID',
  `processName` varchar(100)  COMMENT '流程英文名',
  `decisionNodeId` varchar(50) DEFAULT NULL COMMENT '决策节点总表ID',
  `toTransition` varchar(100) DEFAULT NULL COMMENT '目标指向字符串',
  `toTransitionDisplay` varchar(100) DEFAULT NULL COMMENT '目标指向汉语提示',
  `toTaskName` varchar(100) DEFAULT NULL COMMENT '决策目标环节英文名称_del',
  `toTaskDisplay` varchar(100) DEFAULT NULL COMMENT '决策目标环节名称_del',
  `priorityLevel` varchar(10) DEFAULT NULL COMMENT '优先级别',
        `decisions` varchar(1000) DEFAULT NULL COMMENT '决策公式供Fel使用',
         `decisionsJson` varchar(2000) DEFAULT NULL COMMENT '决策公式Json字符串',
        `rolesLimit` varchar(500) DEFAULT NULL COMMENT '角色限制',
        `orgsLimit` varchar(500) DEFAULT NULL COMMENT '部门限制',
    `field1` varchar(50) DEFAULT NULL COMMENT '变量1对应字段',
    `field2` varchar(50) DEFAULT NULL COMMENT '变量2对应字段',
    `field3` varchar(50) DEFAULT NULL COMMENT '变量3对应字段',
    `field4` varchar(50) DEFAULT NULL COMMENT '变量4对应字段',
    `field5` varchar(50) DEFAULT NULL COMMENT '变量5对应字段',
    `field6` varchar(50) DEFAULT NULL COMMENT '变量6对应字段',
    `field7` varchar(50) DEFAULT NULL COMMENT '变量7对应字段',
    `field8` varchar(50) DEFAULT NULL COMMENT '变量8对应字段',
    `field9` varchar(50) DEFAULT NULL COMMENT '变量9对应字段',
    `field10` varchar(50) DEFAULT NULL COMMENT '变量10对应字段',
    `field11` varchar(50) DEFAULT NULL COMMENT '变量11对应字段',
    `field12` varchar(50) DEFAULT NULL COMMENT '变量12对应字段',
    `field13` varchar(50) DEFAULT NULL COMMENT '变量13对应字段',
    `field14` varchar(50) DEFAULT NULL COMMENT '变量14对应字段',
    `field15` varchar(50) DEFAULT NULL COMMENT '变量15对应字段',
    `field16` varchar(50) DEFAULT NULL COMMENT '变量16对应字段',
    `field17` varchar(50) DEFAULT NULL COMMENT '变量17对应字段',
    `field18` varchar(50) DEFAULT NULL COMMENT '变量18对应字段',
    `field19` varchar(50) DEFAULT NULL COMMENT '变量19对应字段',
    `field20` varchar(50) DEFAULT NULL COMMENT '变量20对应字段',
    domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程内决策条件配置表';





-- 任务节点信息表(任务节点处理副表)
DROP TABLE IF EXISTS `wf_hist_taskInfo`;
CREATE TABLE `wf_hist_taskInfo` (
    `id`  int(11) NOT NULL AUTO_INCREMENT ,
      `infoName` varchar(100) COMMENT '内容名称(模板/草稿)',
      `infoType` varchar(2) COMMENT '内容类型(1工单数据/-2模板/-1草稿)',
      `infoDraftUsed` varchar(2) COMMENT '草稿内容是否派发过',
    `processId` varchar(50) DEFAULT NULL COMMENT '流程ID',
    `orderId`  varchar(50)  COMMENT '工单ID' ,
    `orderTitle` varchar(150) COMMENT '工单主题(草稿时有效)',
    `taskId`  varchar(50)  COMMENT '任务节点ID' ,
    `taskName`  varchar(100)  COMMENT '任务节点英文名' ,
    `taskType` varchar(10) COMMENT '任务节点类型',
    `operator`  varchar(200)  COMMENT '操作人' ,
    `operateTime`  varchar(100) COMMENT '操作时间' ,
       `isMultiForm` varchar(2) COMMENT '是否多任务多表单',
       `isStartTask` varchar(2) COMMENT '是否为启动环节',
       `formType` varchar(10) COMMENT '任务节点表单类型',
       `formName` varchar(100) COMMENT '任务节点表单编码',
       `formValue` varchar(50) COMMENT '任务节点表单值',
       `dataTable` varchar(50) COMMENT '任务节点使用数据表',
       `dataIdValue` varchar(50) COMMENT '业务数据ID',
       `hasApproval` varchar(2) COMMENT '是否包含审批域_del',
       `hasCcOperator` varchar(2) COMMENT '是否包含抄送域',
       `approvalResult` varchar(10) COMMENT '审核结果',
       `approvalRemark` varchar(500) COMMENT '节点备注',
       `ccOperators` varchar(400) COMMENT '抄送人',
       domain varchar(100) COMMENT '企业域',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务节点信息副表';


-- 任务节点处理者表
DROP TABLE IF EXISTS `wf_hist_taskInfo_actors`;
CREATE TABLE `wf_hist_taskInfo_actors` (
    `id`  int(11) NOT NULL AUTO_INCREMENT ,
      `infoType` varchar(2) COMMENT '内容类型(1工单数据/-2模板/-1草稿)',
      `taskInfoId` varchar(2) COMMENT 'taskInfo数据ID',
    `processId` varchar(50) DEFAULT NULL COMMENT '流程ID',
    `orderId`  varchar(50)  COMMENT '工单ID' ,
    `taskName`  varchar(100)  COMMENT '任务节点英文名' ,
       `isCcActors` varchar(2) COMMENT '是否为抄送的处理者',
    `actorType` varchar(10) COMMENT '任务操作人类型',
    `actorValue` varchar(200) COMMENT '任务操作人值',
    domain varchar(100) COMMENT '企业域',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务节点处理者表';



-- 用户自定制表单表
DROP TABLE IF EXISTS `t_sys_userForm`;
CREATE TABLE `t_sys_userForm` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `formName` varchar(100)  COMMENT '表单编号(英文名称)',
  `formDisplay` varchar(100)  COMMENT '表单展示名称',
      `version` int COMMENT '表单版本号',
  `formType` varchar(10)  COMMENT '表单类型',
  `formUrl` varchar(200)  COMMENT '表单请求地址',
  `dataTable` varchar(50)  COMMENT '使用表名',
  `dataFields` varchar(1000)  COMMENT '表单内字段(系统表单时必须配置)',
   domain varchar(100) COMMENT '企业域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户自定制表单表';