create
===
create table t_sys_dict(
	id int primary key auto_increment comment '主键',
	dictId varchar(100) comment '字典编码',
	dictName varchar(100) comment '字典名称',
	pdictId varchar(100) comment '父字典编码',
	dictType varchar(100) comment '字典大类',
	realValue varchar(200) comment '字典真实值',
	orderNo int comment '排序号'
)character set utf8 collate utf8_general_ci;