create
===
create table t_sys_menu(
	id int primary key auto_increment,
	menuId varchar(30) comment '菜单ID',
	menuName varchar(100) comment '菜单名',
	menuUrl varchar(200) comment '菜单url',
	pd varchar(30) comment '父菜单ID',
	businessType varchar(8) comment '业务类型',
	isEnabled varchar(8) comment '是否启用',
	relatedBo varchar(100) comment '关联业务模型',
	orderNo int comment '排序号',
	menuLevel int comment '菜单级别',
	isLeaf varchar(8) comment '是否叶子节点'
)character set utf8 collate utf8_general_ci;

update
===
alter table t_sys_menu change pmenuId pId varchar(64) not null;
update t_sys_columns set colName = 'pId' where colName = 'pmenuId';
update t_sys_boconfig set colName = 'pId' where colName = 'pmenuId';

alter table t_sys_menu change menuLevel level varchar(64) not null;
update t_sys_columns set colName = 'level' where colName = 'menuLevel';
update t_sys_boconfig set colName = 'level' where colName = 'menuLevel';
