package edu.dsideal.campus.config;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;

import java.util.List;

/**
 * Created by dsideal-lee on 2017/2/21.
 */
public class PersonAsyncAszjcTask implements Runnable{
    @Override
    public void run() {
        System.out.println("开始同步鞍山职教城数据！");

        List<Record> oracleUser = Db.use("Oracle").find(
                "select ts.login_name as loginId, upper(ts.login_pwd) as passwd," +
                "       tr.xm as userName, ts.school_id as orgId, 'aszjc' as domain," +
                "       tr.sfzjh as cardNo, '1' as isUsed, '22' as userType " +
                "  from t_sys_loginperson ts left join t_rs_employee tr on ts.person_id = tr.jg_id " +
                " where ts.b_use = 1 and tr.b_use = 1 "
        );
        Db.update("truncate table t_temp_user");

        if(oracleUser!= null && !oracleUser.isEmpty()){
            Db.batchSave("t_temp_user", oracleUser, oracleUser.size());
        }

        //先同步删除鞍山职教城那边删除的账号
        Db.update(  "delete from t_sys_user " +
                    "where domain='aszjc' and loginId<>'sa' " +
                    "and loginId not in(select loginId from t_temp_user)"
        );


        //插入新的用户数据
        Db.update(  "insert into t_sys_user(loginId, passwd, userName, orgId, domain, cardNo, isUsed, userType) " +
                    "select loginId, passwd, userName,orgId, domain, cardNo, isUsed, userType " +
                    "from t_temp_user " +
                    "where loginId not in (select loginId from t_sys_user) "
        );

        //插入权限数据
        Db.update(  "insert into t_sys_user_role(roleid, loginId, r_r)" +
                    "(select '10102', loginId, '1' " +
                    "from t_sys_user where loginId not in(select loginId from t_sys_user_role))"
        );

        System.out.println("同步鞍山程序结束！");
    }
}
