package edu.dsideal.campus.config;

import com.alibaba.druid.util.JdbcConstants;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
//import edu.dsideal.campus.framework.model.FrameworkMappingKit;
import edu.dsideal.campus.framework.plugin.spring.SpringPlugin;

public class DemoProviderApp {

	public static void start() throws InterruptedException {
		// 读取配置文件
		Prop p = PropKit.use("jdbc.properties", "utf-8");

		// 配置Druid数据库连接池插件
		DruidPlugin dp = new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p
				.get("password").trim());

		WallFilter wall = new WallFilter();
		wall.setDbType(JdbcConstants.MYSQL);
		dp.addFilter(wall);

		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);

		arp.setDialect(new MysqlDialect());
		arp.setShowSql(p.getBoolean("devmode", false));
		arp.setDevMode(p.getBoolean("devmode", false));
//		FrameworkMappingKit.mapping(arp);
		// 配置Spring插件
		SpringPlugin sp = new SpringPlugin();

		// 手动启动各插件
		dp.start();
		arp.start();
		sp.start();
		System.out.println("Demo provider for Dubbo启动完成。");
		
		// 没有这一句，启动到这服务就退出了
		Thread.currentThread().join();
	}
}
