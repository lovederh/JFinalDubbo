package edu.dsideal.campus.config;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.util.JdbcConstants;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.Const;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import edu.dsideal.campus.framework.model.FrameworkMappingKit;
import edu.dsideal.campus.framework.plugin.beetlsql.BeetlSqlPlugin;
import edu.dsideal.campus.framework.plugin.spring.SpringPlugin;
import edu.dsideal.campus.framework.cache.FrameworkCacheManager;
import edu.dsideal.campus.framework.config.FrameworkProviderConfig;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;
import org.beetl.sql.ext.jfinal.Trans;
import org.snaker.jfinal.plugin.SnakerPlugin;
import edu.dsideal.campus.snaker.cache.SnakerCacheManager;

import java.util.List;

public class MainConfig extends JFinalConfig {
    private GroupTemplate groupTemplate;

    public GroupTemplate getGroupTemplate() {
        return groupTemplate;
    }

    public void setGroupTemplate(GroupTemplate groupTemplate) {
        this.groupTemplate = groupTemplate;
    }

    @Override
    public void configConstant(Constants me) {
        loadPropertyFile("jdbc.properties");
        me.setDevMode(getPropertyToBoolean("devmode", false));
        JFinal3BeetlRenderFactory rf = new JFinal3BeetlRenderFactory();
        rf.config();
        me.setRenderFactory(rf);
        GroupTemplate gt = rf.groupTemplate;
        me.setEncoding("UTF-8");
        //默认10M,此处设置为最大1000M
        me.setMaxPostSize(100 * Const.DEFAULT_MAX_POST_SIZE);
    }

    @Override
    public void configHandler(Handlers me) {
        // 声明Druid监控页面URL
        me.add(new DruidStatViewHandler("/druid"));
    }

    @Override
    public void configInterceptor(Interceptors me) {
        me.addGlobalActionInterceptor(new Trans());
    }

    private void configOracle(Plugins me){

        DruidPlugin dsOracle = new DruidPlugin(getProperty("oracleJdbcUrl"),
                getProperty("oracleUser"), getProperty("oraclePwd").trim());
        dsOracle.setValidationQuery("select 1 from dual");
        StatFilter stat = new StatFilter();
        stat.setMergeSql(true);
        dsOracle.addFilter(stat);

        WallFilter wall = new WallFilter();
        wall.setDbType(JdbcConstants.ORACLE);
        dsOracle.addFilter(wall);
        ActiveRecordPlugin arp = new ActiveRecordPlugin("Oracle", dsOracle);
        arp.setShowSql(true);
        arp.setDialect(new OracleDialect());

        //设置大小写不敏感工厂
        arp.setContainerFactory(new CaseInsensitiveContainerFactory());
        dsOracle.start();
        arp.start();
        //设置 定时任务同步鞍山程序
        Cron4jPlugin cp = new Cron4jPlugin();
        //每天凌晨2:15分, 启动用户同步线程
        cp.addTask("15 2 * * *", new PersonAsyncAszjcTask());
        //cp.addTask("17 * * * *", new PersonAsyncAszjcTask());

        me.add(cp);
        me.add(arp);
        me.add(dsOracle);
    }
    private void configMySql(Plugins me){
        // 配置Druid数据库连接池插件
        DruidPlugin dp = new DruidPlugin(getProperty("jdbcUrl"),
                getProperty("user"), getProperty("password").trim());
        dp.setMaxActive(Integer.parseInt(getProperty("maxActive")))
                .setInitialSize(getPropertyToInt("initialSize"))
                .setMaxWait(getPropertyToInt("maxWait"))
                .setMinIdle(getPropertyToInt("minIdle"));
        dp.setMaxPoolPreparedStatementPerConnectionSize(getPropertyToInt("maxPoolPreparedStatementPerConnectionSize"));
        dp.setTimeBetweenEvictionRunsMillis(getPropertyToInt("timeBetweenEvictionRunsMillis"));
        dp.setMinEvictableIdleTimeMillis(getPropertyToInt("minEvictableIdleTimeMillis"));
        dp.setRemoveAbandonedTimeoutMillis(getPropertyToInt("removeAbandonedTimeout"));
        dp.setRemoveAbandoned(getPropertyToBoolean("removeAbandoned"));
        StatFilter stat = new StatFilter();
        stat.setMergeSql(true);
        dp.addFilter(stat);

        WallFilter wall = new WallFilter();
        wall.setDbType(JdbcConstants.MYSQL);

        dp.addFilter(wall);

        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);

        arp.setShowSql(true);
        arp.setDialect(new MysqlDialect());
        //数据库和bean映射
        //FrameworkMappingKit.mapping(arp);
        FrameworkMappingKit.mapping(arp);
        //配置snaker流程引擎插件(传入数据库连接)
        SnakerPlugin snakerPlugin = new SnakerPlugin(dp, this.prop.getProperties());
        //需要手动调用start方法
        dp.start();
        arp.start();
        //snaker流程引擎插件
        snakerPlugin.start();
        me.add(dp);
        me.add(arp);
        me.add(snakerPlugin);
    }
    @Override
    public void configPlugin(Plugins me) {
        //PropKit.use("beetlSql.properties");
        configMySql(me);


        //configOracle(me);


        // 配置Spring插件
        SpringPlugin sp = new SpringPlugin();
        BeetlSqlPlugin.init();
        //配置redis缓存插件
        RedisPlugin redisPlugin = new RedisPlugin("dssm", "localhost", 6379, 100000);
        redisPlugin.getJedisPoolConfig().setMaxIdle(20);
        //配置流程使用的redis缓存
        RedisPlugin snakerRedis = new RedisPlugin("snaker", "localhost", 6379, 100000);
        snakerRedis.getJedisPoolConfig().setMaxIdle(20);
        sp.start();
        // 加入各插件到Config
        me.add(sp);
        me.add(redisPlugin);
        me.add(snakerRedis);
    }

    @Override
    public void configRoute(Routes me) {
        me.add(new FrameworkProviderConfig());
    }

    @Override
    public void configEngine(Engine engine) {
    }

    private void testOracleConn(){
        //启动时候同步一次用户
        PersonAsyncAszjcTask tr = new PersonAsyncAszjcTask();
        Thread t = new Thread(tr);
        t.start();
    }

    @Override
    public void afterJFinalStart() {

        //testOracleConn();


        //将数据库类型写入缓存
        FrameworkCacheManager.loadCache();
        //缓存所有已安装的流程配置数据
        SnakerCacheManager.loadCache();
        System.out.println("provider for Dubbo启动完成");
    }

    public static void main(String[] args) {
        JFinal.start();
	    /*try {
            DemoProviderApp.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
}
