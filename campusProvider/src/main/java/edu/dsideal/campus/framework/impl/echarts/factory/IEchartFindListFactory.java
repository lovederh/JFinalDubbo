package edu.dsideal.campus.framework.impl.echarts.factory;

import edu.dsideal.campus.framework.impl.echarts.service.IEchartFindListService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartFindListNormalService;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartFindListService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartFindListNormalService;

public class IEchartFindListFactory {
	/**
	 * 手工录入sql的查询操作处理工厂
	 * 	返回IEchartFindListService, 此接口用于对手工录入的统计sql执行查询操作, 返回Record集合
	 * @param echartRecord	传入record, 便于以后个性化处理
	 * @return
	 */
	public static IEchartFindListService createFindListService(Record echartRecord){
		IEchartFindListService iFindListService;
		// 对于sql查询前未做任何处理, 查询后的结果同样未做任何处理的实现类
		iFindListService = new EchartFindListNormalService();
		// 如果需要在查询前/后做相应处理, 则构造IEchartFindListService的实现类即可
		return iFindListService;
	}
}
