package edu.dsideal.campus.framework.impl.echarts.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;
import java.util.Map;

public interface IEchartIterateService {
	/**
	 * 根据图表类型, 确认对查询结果的遍历方式, 
	 * 遍历echartDataList, 返回关键的供option属性使用的字符串集合
	 * 可以通过实现该接口完成查询结果处理方式的扩展
	 * @param echartDataList
	 * @param echartRecord
	 * @return
	 */
	public abstract Map<String, String> createMySettingData(List<Record> echartDataList, Record echartRecord);
}
