package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;

import java.util.Map;

public class EchartPieOptionService implements IEchartOptionService {
	/**
	 * 图表类型为饼图与南丁格尔图时, 拼接option属性字符串的操作
	 * 功能: 	拼接mySettingMap中供option属性使用的关键性字符串
	 * 		最终返回的可供前端页面使用的option属性字符串
	 * 		以字符串形式返回出去
	 */
	@Override
	public String createOptionData(Record echartRecord, Map<String, String> mySettingMap) {
		// 图表标题
		String echartName = echartRecord.get("echartsName");
		// 是否饼图标识(南丁格尔图则该值为false)
		boolean pieTypeFlag = "1".equals(echartRecord.get("echartsType"));
		
		// 需要的属性配置字符串
		StringBuffer optionSb = new StringBuffer();
		optionSb.append(
					"{" +
					"	title : {" +
					"		text: '").append(echartName).append("',");
		
		// 饼图与南丁格尔图一些属性配置差异, 通过判断实现相应字符串拼接
		if(pieTypeFlag){
			optionSb.append(
					"		x:'center'"
			);
		}else{
			optionSb.append(
					"		left: 'center',top: 10"
			);
		}
		
		optionSb.append(
					"	}," +
					"	tooltip : {" +
					"		trigger: 'item'," +
					"		formatter: \"{a} <br/>{b} : {c} ({d}%)\"" +
					"	}," +
					"	legend: {" +
					"		orient: 'vertical'," +
					"		left: 'left',\n" +
					"		data: [").append(mySettingMap.get("legendData")).append("]" +
					"	}," +
					"	series : [" +
					"		{" +
					"			name: '").append(echartName).append("'," +
					"		    type: 'pie',");
		if(!pieTypeFlag){
			// 南丁格尔图标识(面积模式)
			if("area".equals(echartRecord.get("nightingaleType"))){
				optionSb.append(
					"		    roseType: 'area',"
				);
			}else{
				// 半径模式
				optionSb.append(
					"		    roseType: 'angle',"
				);
			}
		}
		
		optionSb.append(
					"		    radius : '55%'," +
					"		    center: ['50%', '60%']," +
					"		    data:[").append(mySettingMap.get("seriesData")).append("]");
		
		if(!pieTypeFlag){
			// 南丁格尔图需要排序
			optionSb.append("			.sort(function (a, b) { return a.value - b.value})");
		}
		optionSb.append(	"	,");
		
		// 背景风格不同处理
		if(pieTypeFlag){
			optionSb.append(
					"		    itemStyle: {" +
					"		     	emphasis: {" +
					"		        	shadowBlur: 10," +
					"		            shadowOffsetX: 0," +
					"		            shadowColor: 'rgba(0, 0, 0, 0.5)' " +
					"		        }" +
					"		     }"
			);
		}else{
			optionSb.append(
					"         	itemStyle: {" +
					"				normal: {" +
					"					shadowBlur: 200," +
					"					shadowColor: 'rgba(0, 0, 0, 0.5)'" +
					"				}" +
					"			}"
			);
		}
		
		optionSb.append(
					"		}" +
					"	]" +
					"}"
		);
		return optionSb.toString();
	}
}
