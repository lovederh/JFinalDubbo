package edu.dsideal.campus.framework.impl.echarts;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.impl.echarts.factory.IEchartFindListFactory;
import edu.dsideal.campus.framework.impl.echarts.factory.IEchartIterateFactory;
import edu.dsideal.campus.framework.impl.echarts.factory.IEchartOptionFactory;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartFindListService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;

import java.util.List;
import java.util.Map;

/**
 * 处理我的echarts图表的策略模式类
 * 生成option属性分为三步, 相应建立三个工厂类
 * 	(1) 手工录入的统计sql查询操作, 如果需要在查询前/后做相应处理, 则构造IEchartFindListService的实现类即可
 * 	(2) 根据图表类型, 确认对查询结果的遍历方式, 返回关键的供option属性使用的字符串, 可实现IEchartIterateService以扩展处理方式
 * 	(3) 依照图表类型, 拼接(2)中option属性使用的关键性字符串, 返回最终的可供前端页面使用的option属性字符串
 * @author zhaoyou
 */
public class MyEchartStrategy {
	// 当前图表项配置信息Record
	Record echartRecord;
	// 图表显示数据结果的处理类
	IEchartFindListService findListService = null;
	// 遍历查询结果的处理类
	IEchartIterateService iterateService = null;
	// 生成图表option属性的处理类
	IEchartOptionService optionService = null;
	
	/**
	 * 构造方法, 同时完成根据图表项Record配置信息, 确定程序执行的三个步骤中, 所需要对应实现类的实例化操作
	 * @param echartRecord
	 */
	public MyEchartStrategy(Record echartRecord){
		this.echartRecord = echartRecord;
		// 手工录入sql的查询操作处理工厂
		findListService = IEchartFindListFactory.createFindListService(echartRecord);
		// 遍历查询结果的工厂(返回关键的供option属性使用的字符串)
		iterateService = IEchartIterateFactory.createIterateService(echartRecord);
		// 依照图表类型, 生成图表option属性的工厂
		optionService = IEchartOptionFactory.createOptionService(echartRecord);
	}
	
	/**
	 * 该类中对外提供的一个方法
	 * 功能: 返回最终的可供前端页面使用的option属性字符串
	 * 外部类直接调用即可(先利用图表项配置Record实例化出来)完成该功能, 降低耦合性
	 * @return
	 */
	public String createOptionData(){
		// 第一维度取值字段
		String xAxisColumn = echartRecord.get("xAxisColumn");
		if(CommonTools.isEmpty(xAxisColumn)){
			echartRecord.set("xAxisColumn", "xAxis");
		}
		// 第二维度取值字段
		String yAxisColumn = echartRecord.get("yAxisColumn");
		if(CommonTools.isEmpty(yAxisColumn)){
			echartRecord.set("yAxisColumn", "yAxis");
		}
		// 数字数据取值字段
		String dataColumn = echartRecord.get("dataColumn");
		if(CommonTools.isEmpty(dataColumn)){
			echartRecord.set("dataColumn", "data");
		}
		
		// 执行查询返回图表项数据
		List<Record> echartDataList = findListService.doEchartsSqlFindList(echartRecord);
		// 遍历拼接属性字符串
		Map<String, String> mySettingMap = iterateService.createMySettingData(echartDataList, echartRecord);
		// 返回echarts图表属性配置字符串
		String optionData = optionService.createOptionData(echartRecord, mySettingMap);
		return optionData;
	}
}
