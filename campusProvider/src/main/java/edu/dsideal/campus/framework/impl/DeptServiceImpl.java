package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IDeptService;
import edu.dsideal.campus.framework.sql.BoSql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.jfinal.plugin.activerecord.Db.find;
import static com.jfinal.plugin.activerecord.Db.findFirst;
import static com.jfinal.plugin.activerecord.Db.update;

public class DeptServiceImpl implements IDeptService {
    /**
     * 查询所有组织机构集合
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> findAllDeptDefine(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        return findDeptByCondition(loginUserObj,true,paraMap);
    }

    /**
     * 查询指定子子机构集合
     * @param loginUserObj
     * @param paraMap
     * @param nowNodeId
     * @param nowLevel
     * @return
     */
    @Override
    public List<Record> findDeptChildren(LoginUserObj loginUserObj,Map<String, String[]> paraMap,String nowNodeId, String nowLevel){
        return findDeptByCondition(loginUserObj,false,paraMap, nowNodeId, nowLevel);
    }

    /**
     * 拼接sql, 执行查询组织机构集合操作
     * @param isFindAll     是否查询全部标识
     * @param nowNodeInfo   当前节点信息数组: 第一项为节点机构ID; 第二项为节点当前级别
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    private List<Record> findDeptByCondition(LoginUserObj loginUserObj, boolean isFindAll, Map<String, String[]> paraMap, String... nowNodeInfo){
        String boName = CommonTools.getValueFromParaMap(paraMap,"dtoName");
        String domain = CommonTools.getValueFromParaMap(paraMap,"domain");
        //获得树的配置信息
        Record boAndRuleConf = BoSql.getBoAndKeyRule(boName, domain);
        String tableName = boAndRuleConf.getStr("tableName");

        String orgName = "组织机构信息";

        // 构造根节点下的叶子节点
        StringBuilder treeSql = new StringBuilder();

        treeSql.append("select t.id as dataId,");
        treeSql.append("       t.").append(boAndRuleConf.getStr("caption")).append(" as orgName,");
        treeSql.append("       t.orgId,");
        treeSql.append("       t.pId,");
        treeSql.append("       t.level,");
        treeSql.append("       case t.isLeaf when '1' then 'true' else 'false' end as isLeaf,");
        treeSql.append("       'false' as expanded ");
        treeSql.append(" from ").append(tableName);
        treeSql.append(" t left join ").append(tableName).append("_role t1 on t.id = t1.dataId ");
        treeSql.append(" where t.delFlag ='0' and t.domain = ? ");
        //判断是否有权限控制
        if(ConstStatic.KEY_RULE_CONF_DICT[4].equals(boAndRuleConf.getStr("keyRule"))) {
            treeSql.append(" and t1.roleId in(" + loginUserObj.getRoles() + ")");
        }

        // 菜单树节点数据
        List<Record> treeData = new ArrayList<Record>();
        // 构造虚拟根节点
        if(isFindAll){
            Record root = new Record();
            root.set("orgId", "0");
            if(!"domain".equals(loginUserObj.getDomain())){
                Record domainInfo = findFirst(
                        "select s.mainTitle from t_sys_systemconfig s where s.id =( select u.domain from t_sys_user u where u.loginId=?)",
                        loginUserObj.getLoginId());
                if(domainInfo != null){
                    orgName = domainInfo.get("mainTitle");
                }
            }
            root.set("orgName", orgName);
            root.set("open", "true");
            root.set("level", "0");
            root.set("isLeaf", false);
            root.set("isParent", true);
            treeData.add(root);

        }else{
            // 向下级查询时则加入查询下级过滤条件
            String nowNodeId = nowNodeInfo[0];
            int nowLevel = Integer.parseInt(nowNodeInfo[1]);
            treeSql.append( " and t.level = ").append(nowLevel + 1).append(" and t.orgId like '").append(nowNodeId).append("%' ");
        }

        treeSql.append(" order by t.orgId, t.ord ");

        // 查询并构造树列表数据集合
        List<Record> deptData = Db.find(treeSql.toString(),domain);
        if(deptData!=null && !deptData.isEmpty()){
            treeData.addAll(deptData);
        }
        return treeData;
    }

    /**
     *  查询选择当前节点的组织机构信息
     * @param loginUserObj
     * @param orgId
     * @return
     */
    @Override
    public List<Record> queryDeptInfo(LoginUserObj loginUserObj, String orgId){
        StringBuilder deptInfoSql = new StringBuilder();
        deptInfoSql.append(
                "select o.id,       " +
                "       o.orgName,  " +
                "       o.orgId,    " +
                "       o.pId,      " +
                "       (select b.maintitle from t_sys_systemconfig b where b.id = o.domain) domain " +
                "from   t_sys_org o " +
                "left join t_sys_org_role t " +
                "       on o.id = t.dataId " +
                "where  o.domain='"+loginUserObj.getDomain()+"' and " +
                "       o.delFlag ='0' and t.roleId in(" + loginUserObj.getRoles() + ") " );
        Object[] param = new Object[0];
        // 不是根节点显示的树信息,树的根节点为'0'
        if(!"0".equals(orgId)){
            deptInfoSql.append(" and (o.orgId =? or o.pId =?) ");
            param = new Object[]{orgId,orgId};
        }
        List<Record> deptInfoData = find(deptInfoSql.toString(),param);
        return deptInfoData;
    }

    /**
     * 增加选择节点子节点机构的信息
     * @param keyId
     * @param paraMap
     * @return
     */
    @Override
    public String addDept(LoginUserObj loginUserObj, String keyId, Map<String, String[]> paraMap){
        String pId = CommonTools.getValueFromParaMap(paraMap,"pId");
        String name = CommonTools.getValueFromParaMap(paraMap,"name");
        String domain = loginUserObj.getDomain();

        if(CommonTools.isEmpty(keyId)){
            throw new RuntimeException("添加数据失败!");
        }else{
            // 1.通过pid获取组织机构级数deptLevel
            Record deptLevel = findFirst("select level from t_sys_org where delFlag ='0' and domain = ? and orgId=?",domain, pId);
            // 判断选择节点机构是否有子机构,如果没有子机构,该选择的节点的isLeaf(是否为叶子)要更改为'0'
            long isOrgChild = Db.queryLong("select count(*) from t_sys_org where domain = ? and delFlag ='0' and pid = ?",domain,pId);
            if(isOrgChild == 0){
                update("update t_sys_org set isLeaf ='0' where domain=? and delFlag ='0' and orgId = ?",domain,pId);
            }
            Record addDeptRecord = new Record().set("orgId",keyId).set("pId",pId).set("orgName",name).set("domain",domain);
            int parentLevel = 0;
            // 2.如果deptLevel不为空,则说明level有值,赋值给parentLevel,说明可以增加子节点,子节点childLevel的level等于parentLevel+1,并插入到addDeptRecord.
            if(deptLevel != null){
                parentLevel = Integer.valueOf(deptLevel.get("level"));
                int childLevel = parentLevel+1;
                addDeptRecord.set("level",childLevel);
            }else{
                // 3.如果deptLevel为空,说明该节点为根节点的一级子节点,并赋值level为2,并插入到addDeptRecord
                parentLevel = 1;
                addDeptRecord.set("level",parentLevel);
            }
            Db.save("t_sys_org",addDeptRecord);

            // 获取新增之后的Id
            Record getOrgId = Db.findFirst("select o.id from t_sys_org o where o.domain = ? and o.orgId = ?",domain,keyId );

            // 新增组织机构增加权限
            Record insertOrgRole = new Record().set("roleId",loginUserObj.getRoles()).set("dataId",getOrgId.get("id")).set("r_r","1").set("r_w","1").set("r_u","1").set("r_d","1");
            Db.save("t_sys_org_role",insertOrgRole);
        }
        return keyId;
    }

    /**
     * 删除所选择的节点
     * @param id
     * @param loginUserObj
     * @return
     */
    @Override
    public String delDept(LoginUserObj loginUserObj, String id){
        boolean delResult = false;//保存成功标记
        String delMsg = null;//操作完成提示
        String domain = loginUserObj.getDomain();
        // 查询要删除节点的PID
        Record checkDelNotePid = Db.findFirst("select pId from t_sys_org where domain =? and delFlag = '0' and orgId =? ",domain,id );
        String parentOrgId = checkDelNotePid.get("pId");
        if(CommonTools.isEmpty(id)){
            delMsg = "删除节点数据失败!";
        }else{
            StringBuilder delDeptSql = new StringBuilder();
                delDeptSql.append("update t_sys_org ");
                delDeptSql.append("set delFlag = '1' where domain ='");
                delDeptSql.append(domain);
                delDeptSql.append("' and orgId = ");
                delDeptSql.append(id);
                delDeptSql.append(" or pId like '");
                delDeptSql.append(id);
                delDeptSql.append("%'");
            Db.update(delDeptSql.toString());

            delResult = true;
            delMsg = "删除成功!";
        }
        // 通过pId查询对应的父亲的orgId,是否还有叶子节点,如果有叶子节点---isLeaf不变,没有叶子节点---isLeaf变成'1'
        long isPidChild = Db.queryLong("select count(*) from t_sys_org where domain =? and delFlag = '0' and pId =? ",domain,parentOrgId);
        if(isPidChild == 0){
            update("update t_sys_org set isLeaf = '1' where domain =? and delFlag = '0' and orgId =?", domain,parentOrgId);
        }
        return CommonTools.getMsgJson(delResult, delMsg);
    }

    /**
     *  部门调整方法
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String deptAdjustSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        List<Record> batchUpdateOrgs = new ArrayList<Record>();
        // 当前移动的部门ID
        String currentOrgId = CommonTools.getValueFromParaMap(paraMap, "currentOrgId");
        int currentOrgLen = currentOrgId.length();
        String domain = loginUserObj.getDomain();
        //找到自己的部门信息
        Record currentOrg = findFirst("select id, orgId, pId, level from t_sys_org where domain = ? and  orgId = ?",domain, currentOrgId);
        int currentOrgLv = Integer.valueOf(currentOrg.get("level"));
        //找到自己所有的子孙部门
        List<Record> currentOrgChildren = find("select id, orgId, pId, level from t_sys_org where domain = ? and pId like '"+currentOrgId+"%'",domain);
        //找到当前节点的父节点信息
        String currentParentOrgId = currentOrg.get("pId");
        if(CommonTools.isNotEmpty(currentParentOrgId) && !"0".equals(currentParentOrgId)){
            //当前父节点不为虚拟根节点, 则要进行是否改动叶子节点标识判断
            long currentParentChildrenNum = Db.queryLong("select count(*) from t_sys_org where pId = ?", currentParentOrgId);
            if(currentParentChildrenNum <= 1){
                Db.update("update t_sys_org set isLeaf = 1 where orgId = ?", currentParentOrgId);
            }
        }

        //要前往的父部门ID
        String move2ParentOrgId = CommonTools.getValueFromParaMap(paraMap, "move2ParentOrgId");
        String currentOrgChildOrgId;
        String currentOrgChildPid;
        int currentOrgChildLv;

        String move2OrgChildOrgId;
        String move2OrgChildPid;
        int move2OrgChildLv;
        int betweenLvValue;
        String move2OrgId;
        String maxChild;
        // 分两次情况---情况1:如果move2ParentOrgId为空或者为"0"的话,说明要把部门调整到根节点下面
        //          ---情况2:否则,说明要把部门调整到非根节点下面
        if("0".equals(move2ParentOrgId) || CommonTools.isEmpty(move2ParentOrgId)){
            //要前往的部门是根节点orgId最大值
            maxChild = Db.queryStr("select max(orgId) from t_sys_org where pId = '0' ");
            move2OrgId = ""+(Integer.parseInt(maxChild) + 1);
            betweenLvValue = 1 - currentOrgLv;
            // 要调整部门的level为---1,pId为---0
            currentOrg.set("pId","0");
            currentOrg.set("level","1");

            //前往的父节点为根节点, 则不做改动后父节点是否叶子判断
        }else{
            //取出前往父部门的级别
            int move2ParentOrgLv = Integer.valueOf(Db.queryStr("select level from t_sys_org where domain=? and orgId = ?",domain, move2ParentOrgId));
            maxChild = Db.queryStr("select max(orgId) from t_sys_org where domain=? and pId = ?",domain, move2ParentOrgId);
            move2OrgId = CommonTools.isEmpty(maxChild)? move2ParentOrgId+"001" : ""+(Integer.parseInt(maxChild) + 1);
            int move2OrgLv = move2ParentOrgLv + 1;
            betweenLvValue = move2OrgLv - currentOrgLv;

            currentOrg.set("pId", move2ParentOrgId);
            currentOrg.set("level", move2OrgLv + "");

            //前往的父节点为实际存在的, 那么前往的父节点是否叶子属性必定变为假
            Db.update("update t_sys_org set isLeaf = 0 where orgId = ?", move2ParentOrgId);
        }
        currentOrg.set("orgId",move2OrgId);
        batchUpdateOrgs.add(currentOrg);
        // 当前机构具有的人员
        if(null!=currentOrgChildren && !currentOrgChildren.isEmpty()){
            for(Record orgChild : currentOrgChildren){
                currentOrgChildOrgId = orgChild.get("orgId");
                currentOrgChildPid = orgChild.get("pId");
                currentOrgChildLv = Integer.parseInt(orgChild.get("level"));

                // 计算移动之后的部门ID/父ID/级别
                move2OrgChildOrgId = move2OrgId + currentOrgChildOrgId.substring(currentOrgLen);
                move2OrgChildPid = move2OrgId + currentOrgChildPid.substring(currentOrgLen);
                move2OrgChildLv = currentOrgChildLv + betweenLvValue;

                orgChild.set("orgId", move2OrgChildOrgId);
                orgChild.set("pId", move2OrgChildPid);
                orgChild.set("level", move2OrgChildLv + "");

                batchUpdateOrgs.add(orgChild);
            }
        }

        Db.batchUpdate("t_sys_org", batchUpdateOrgs, batchUpdateOrgs.size());
        return CommonTools.getMsgJson(true, "调整成功!");
    }

}
