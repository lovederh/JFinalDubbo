package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.Base64Util;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.MD5Util;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.ILoginService;
import edu.dsideal.campus.framework.service.IRoleService;
import edu.dsideal.campus.framework.sql.LoginUserSql;

import java.util.ArrayList;
import java.util.Map;

/**
 * 2016/10/31 0031
 */
public class LoginServiceImpl implements ILoginService {
    /**
     * 通过登陆人账号, 后台查询重要信息
     */
    @Override
    public Record findUserRecordByLoginId(String loginId) {
        return LoginUserSql.findUserRecordByLoginId(loginId);
    }

    /**
     * 登录时的后台校验方法, 校验项如下:
     * 1.用户账号loginId是否存在;
     * 2.用户账号是否可用(isUsed='1'可用);
     * 3.密码是否正确;
     */
    @Override
    public LoginUserObj checkLoginUser(Map<String, String[]> paraMap) {
        //输入的登陆账号
        String inputLoginId = CommonTools.getValueFromParaMap(paraMap, "loginId");
        //输入的登陆密码
        String inputPasswd = MD5Util.MD5(Base64Util.getFromBase64(CommonTools.getValueFromParaMap(paraMap, "passwd")));
        //查询人员重要信息, 为构建登陆人对象LoginUserObj做准备
        Record sysUser = LoginUserSql.findUserRecordByLoginId(inputLoginId);

        //作为返回结果的登录人对象
        LoginUserObj loginUserObj;
        if (null == sysUser) {
            loginUserObj = new LoginUserObj();
            loginUserObj.setUserStatus(false);
            loginUserObj.setUserStatusMsg("您登陆的用户, 账号或密码不正确!");
            return loginUserObj;
        }
        loginUserObj = new LoginUserObj(sysUser);
        if (!"1".equals(sysUser.get("isUsed"))) {
            loginUserObj.setUserStatus(false);
            loginUserObj.setUserStatusMsg("您登陆的用户, 账号不可用!");
            return loginUserObj;
        }
        if (inputPasswd == null || !inputPasswd.equals(sysUser.get("passwd"))) {
            loginUserObj.setUserStatus(false);
            loginUserObj.setUserStatusMsg("您登陆的用户, 账号或密码不正确!");
            return loginUserObj;
        }
        loginUserObj.setUserStatus(true);
        //设置账号权限
        IRoleService roleService = new RoleServiceImpl();
        String loginRoles = roleService.getRolesByLoginId(inputLoginId, loginUserObj.getDomain());
        loginUserObj.setRoles(loginRoles);

        return loginUserObj;
    }

    /**
     * 检查注册用户账号是否存在
     */
    public String checkRegisterLoginID(String loginId) {
        Record record = Db.findFirst("select loginId from t_sys_systemconfig where loginId=?", loginId);
        if (record == null) {
            return "";
        } else {
            return record.getStr("loginId");
        }
    }

    public String checkRegisterEmailAdd(String emailAdd) {
        Record record = Db.findFirst("select emailAdd from t_sys_systemconfig where emailAdd=?", emailAdd);
        if (record == null) {
            return "";
        } else {
            return record.getStr("emailAdd");
        }
    }

    /**
     * 保存注册信息
     */
    @Override
    public String saveRegisterValue(ArrayList<String> registerValue) {


        Record record = new Record().set("id", registerValue.get(6)).set("loginID", registerValue.get(7))
                                    .set("companyName", registerValue.get(0)).set("companyAdd", registerValue.get(1))
                                    .set("tel", registerValue.get(2)).set("corporation", registerValue.get(3))
                                    .set("faxNum", registerValue.get(4)).set("emailAdd", registerValue.get(5)).set("applyTime",registerValue.get(8));
        Db.save("t_sys_systemconfig", record);
        return CommonTools.getMsgJson(true, "注册成功!");
    }
}
