package edu.dsideal.campus.framework.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsideal-lee on 2016/12/19.
 */
public class RoleTreeFactory {
    private List<IRoleTreeInitService> roleList = new ArrayList<IRoleTreeInitService>();
    private String tableName;
    private String domain;
    private String roleId;
    public RoleTreeFactory(String tableName, String domain, String roleId){
        this.tableName = tableName;
        this.domain = domain;
        this.roleId = roleId;
    }
    public static IRoleTreeInitService initService(String treeId){
        IRoleTreeInitService roleTreeInitService = null;
        switch (treeId){
            case "js_tree_t1":
                roleTreeInitService = new OrgTreeServiceImpl();
                break;
            case "js_tree_t2":
                roleTreeInitService = new MenuTreeServiceImpl();
                break;
            case "js_tree_t3":
                roleTreeInitService = new MenuBtnTreeServiceImpl();
                break;
            case "js_tree_t4":
                roleTreeInitService = new TableColumnTreeServiceImpl();
                break;
            case "js_tree_t5":
                roleTreeInitService = new CommonTreeServiceImpl();
                break;
            default:
                roleTreeInitService = new CommonTreeServiceImpl();
                break;
        }
        return roleTreeInitService;
    }
    /**
     * 删除全部此角色的权限
     */
    public void registRoleService(){
        if("t_sys_org".equals(tableName)){
            registService(new CommonTreeServiceImpl());
            return;
        }
        roleList.add(new OrgTreeServiceImpl());
        roleList.add(new MenuTreeServiceImpl());
        roleList.add(new MenuBtnTreeServiceImpl());
        roleList.add(new TableColumnTreeServiceImpl());
    }
    /**
     * 自定义删除哪个角色的权限
     */
    public void registService(IRoleTreeInitService roleService){
        roleList.add(roleService);
    }

    /**
     * 执行删除权限
     * @throws SQLException
     */
    public void doDelRole() throws SQLException{
        for(IRoleTreeInitService service: roleList) {
            service.delRoleData(tableName,domain, roleId);
        }
    }
}
