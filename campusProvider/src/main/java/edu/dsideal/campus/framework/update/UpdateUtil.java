package edu.dsideal.campus.framework.update;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpdateUtil {
    // 配置文件名称
    private final static String XML_ROOT_PATH = "/update";
    protected static String filePath;
    // 模型
    protected String modelName;
    protected Map<String, Object> map;
    protected Record record;
    protected String loginId;
    protected UpdateModel updateModel;
    protected XmlOperator xmlOpt;

    public UpdateUtil(String modelName) {
        this.modelName = modelName;
        String xmlPath = new StringBuilder(XML_ROOT_PATH).append("/").append(modelName).append(".xml").toString();
        URL configUrl = UpdateUtil.class.getClassLoader().getResource("../"+xmlPath);
        filePath = configUrl.getPath();
    }
    public UpdateUtil(String modelName, String loginId, Record record) {
        this.modelName = modelName;
        this.loginId = loginId;
        this.record = record;
        this.map = record.getColumns();
        URL configUrl =
                UpdateUtil.class.getClassLoader().getResource(XML_ROOT_PATH + modelName + ".xml");
        filePath = configUrl.getPath();
    }

    /**
     * 构造插入语句
     *
     * @return string sql语句 insert into table () values(); insert into table_role(roleid, dataid,
     *         r_r, r_crt_time, r_crt_user) values();
     */
    public String constructInsertSql() {
        // 存储insert into table(id,name,...)
        StringBuilder insSqlkey = new StringBuilder();
        // 存储 values(...)
        StringBuilder insSqlValue = new StringBuilder();
        insSqlkey.append("insert into ").append(modelName).append(" (");
        insSqlValue.append("values(");
        String id = null;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            try {
                insSqlkey.append(key).append(",");
                insSqlValue.append("'").append(entry.getValue()).append("'").append(",");
                if ("id".equals(key)) {
                    //id 需要在构造权限的时候使用
                    id = String.valueOf(entry.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return   insSqlkey.deleteCharAt(insSqlkey.length()-1).append(") ")
                .append(insSqlValue.deleteCharAt(insSqlValue.length()-1)).append(")").append(";")
                .append("insert into ").append(modelName).append("_role")
                .append("(roleid, dataid, r_r, r_crt_time, r_crt_user )")
                .append(" values('9999','").append(id).append("',").append("'1','")
                .append(CommonTools.getCurrentTimeYmdhms()).append("','").append(loginId)
                .append("');").toString();
    }

    /**
     * 构造更新sql
     *
     * @return String sql语句 update table set xxx = 'xx', aa = 'bb' where id = '';
     */
    public String constructUpdateSql() {
        StringBuilder updSql = new StringBuilder();
        String id = null;
        updSql.append("update ").append(modelName).append(" set ");
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            try {
                if (!"id".equals(entry.getKey())) {
                    //先拼set后面的语句
                    updSql.append(entry.getKey()).append(" = '").append(entry.getValue())
                            .append("',");
                } else {
                    //如果字段是id，先将值存起来
                    id = String.valueOf(entry.getValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return updSql.deleteCharAt(updSql.length()-1).append(" where id = ")
                .append(id).append(";").toString();
    }

    /**
     * 删除表中数据
     * @param ids
     * @return String 语句 delete from table where id = '';delete from table_role where dataid = '';
     */
    public String constructDeleteSql(String[] ids) {
        StringBuilder sql = new StringBuilder();
        for (String id : ids) {
            sql.append(" delete from ").append(modelName).append(" where id = ").append(id)
                    .append(";").append(" delete from ").append(modelName).append("_role")
                    .append(" where dataid = '").append(id).append("';").toString();
        }
        return sql.toString();
    }

    /**
     * 构造重构语句
     *
     * @param sqls 可变参数
     * @return
     */
    public String constructRebuildSql(String... sqls) {
        StringBuilder sqlStr = new StringBuilder();
        for (String sql : sqls) {
            sqlStr.append(sql).append(";");
        }
        return sqlStr.toString();
    }

    /**
     * 构造xml数据
     *
     * @return String
     */
    public void writeXmlData2File() {
        // 获得xml对象
        Document xmldoc = XmlOperator.readXml(filePath);
        // 创建xml结点
        XmlOperator.createVerElement(xmldoc, updateModel);
        // 保存xml
        XmlOperator.saveXml(filePath, xmldoc);
    }

    /**
     * 解析json数据返回表对象
     *
     * @return
     */
    public List<Record> parseJson() {
        // 获得数据库version
        String dbver = getDBVersion();
        // 读json文件，拿到json数据
        String jsonStr = XmlOperator.getUpdateJsonData(dbver, filePath);
        List<Record> list = new ArrayList<Record>();
        JSONArray jsonArray = JSONArray.parseArray(jsonStr);
        //// 如果jsonArray为空
        if (jsonArray != null) {
            Record r = null;
            for (int i = 0, size = jsonArray.size(); i < size; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                r =buildRecord(String.valueOf(jsonObject.get("tableId")),
                                String.valueOf(jsonObject.get("version")),
                                String.valueOf(jsonObject.get("loginId")),
                                String.valueOf(jsonObject.get("sql")));
                // 最后一条应该是最新的
                if (i == (size - 1)) {
                    r.set("isMax", "true");
                } else {
                    r.set("isMax", "false");
                }
                list.add(r);
            }
        }
        return list;
    }

    private Record buildRecord(String tableId, String loginId, String version, String sql) {
        Record record = new Record();
        record.set("tableId", tableId);
        record.set("loginId", loginId);
        record.set("version", version);
        record.set("sql", sql);
        return record;
    }

    /**
     * 查询xml中升级信息列表
     * @return
     */
    public List<Map<String, String>> findUpdateDataList(){
        //查询需要更新的结点数据
        NodeList someNodes = XmlOperator.getUpdateNode(getDBVersion(), filePath);
        List<Map<String, String>> updateDataList = new ArrayList<Map<String, String>>();
        if(someNodes != null){
            NodeList childNodes;
            Node childNode;
            Map<String, String> dataMap;
            for(int i=0, len=someNodes.getLength(); i<len; i++) {
                childNodes = someNodes.item(i).getChildNodes();
                dataMap = new HashMap<String , String>();
                for(int j=0, childrenLen=childNodes.getLength(); j<childrenLen; j++) {
                    childNode = childNodes.item(j);
                    if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("tableId")){
                        dataMap.put("tableId", childNode.getTextContent());
                        continue;
                    }
                    if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("version")){
                        dataMap.put("version", childNode.getTextContent());
                        continue;
                    }
                    if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("loginId")){
                        dataMap.put("loginId", childNode.getTextContent());
                        continue;
                    }
                    if(childNode.getNodeType() == Node.ELEMENT_NODE && childNode.getNodeName().equals("sql")){
                        dataMap.put("sql", childNode.getTextContent());
                        continue;
                    }
                    // 最后一条应该是最新的
                    if (i == (len - 1)) {
                        dataMap.put("isMax", "true");
                    } else {
                        dataMap.put("isMax", "false");
                    }
                }
                updateDataList.add(dataMap);
            }
        }
        return updateDataList;
    }


    /**
     * 数据库当前version写入
     *
     * @param latestVersion
     */
    public void writeVersion2DB(String latestVersion) {
        Db.update(" update t_sys_update set version = ? where table_name = ?", latestVersion, modelName);
    }

    /**
     * 数据库版本号
     *
     * @return String
     */
    public String getDBVersion() {
        return Db.queryStr("select version from t_sys_update where tableName = ?", modelName);
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }


    public void setUpdateModel(UpdateModel updateModel) {
        if (updateModel != null) {
            this.updateModel = updateModel;
        }
    }

}
