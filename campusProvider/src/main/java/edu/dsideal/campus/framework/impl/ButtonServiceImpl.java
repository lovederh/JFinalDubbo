package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IButtonService;

import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/21.
 */
public class ButtonServiceImpl implements IButtonService {
    @Override
    public List<Record> getBtns(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String domain = loginUserObj.getDomain();
        String menuId = paraMap.get("menuId")[0];
        if (ConstStatic.ADMIN_DOMAIN.equals(domain)) { //超级管理员权限控制
            return Db.find(new StringBuilder("select t1.id,t1.btnName," +
                    "t1.clickFun,t1.iconClass," +
                    "t1.position,t1.isSystem," +
                    "t1.url,t1.level,t1.isLeaf," +
                    "t1.pId,t1.expanded,t1.eventType," +
                    "t.showWidth, t.showHeight " +
                    " from t_sys_menubtn t " +
                    " inner join t_sys_buttons t1 on t.btnId = t1.id ")
                    .append(" left join t_sys_menubtn_role t2 on t.id = t2.dataId ")
                    .append(" where t.menuId = ? ").toString(), menuId);
        }
        return Db.find(new StringBuilder("select t1.id,t1.btnName," +
                        "t1.clickFun,t1.iconClass," +
                        "t1.position,t1.isSystem," +
                        "t1.url,t1.level,t1.isLeaf," +
                        "t1.pId,t1.expanded,t1.eventType," +
                        " t.showWidth, t.showHeight from t_sys_menubtn t inner join t_sys_buttons t1 on t.btnId = t1.id ")
                        .append(" left join t_sys_menubtn_role t2 on t.id = t2.dataId ")
                        .append(" where t2.roleId in (").append(loginUserObj.getRoles())
                        .append(") and t.menuId = ? and t.domain in('domain','").append(domain).append("')").toString(),
                menuId);
    }
}
