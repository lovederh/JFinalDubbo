package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EchartIterateSingleService implements IEchartIterateService{
	/**
	 * 图表类型为单柱图/单折线图时, 对查询结果的遍历方式
	 * 功能: 	单次遍历, 生成单柱图/单折线图option属性中关键的
	 * 		legend中data属性, series总体属性和xAxis中data属性(横向柱图时对应yAxis中data属性)
	 * 		以map形式返回, 为最终的图表option属性拼接做准备
	 */
	@Override
	public Map<String, String> createMySettingData(List<Record> echartDataList, Record echartRecord) {
		// 第一维度取值字段
		String xAxisColumn = echartRecord.get("xAxisColumn");
		// 数字数据取值字段
		String dataColumn = echartRecord.get("dataColumn");
		// 图表标题
		String echartName = echartRecord.get("echartsName");
		// 判刑选择的是否为折线图, 进而确定系列(series)中type属性值
		String echartTypeOpt = "3".equals(echartRecord.get("echartsType"))? "type:'line',": "type:'bar',";
		
		// 导航列表数据对应的字符串(legend中data属性)
		StringBuffer legendDataSb = new StringBuffer("");
		// 第一维度提示数据对应的字符串(xAxis/yAxis中data属性)
		StringBuffer axisDataSb = new StringBuffer("");
		// 所有配置项数据对应的字符串(series属性)
		StringBuffer seriesItemSb = new StringBuffer("");
		
		// 遍历一次即可(导航列表中只有一条数据)
		legendDataSb.append("'").append(echartName).append("'");
		if(null!=echartDataList && echartDataList.size()>0){
			// 查询结果中x轴配置项对应的汉字信息
			Object xAxisStr;
			// 数字数据
			Object dataObj;
			
			// 系列中(y轴配置项series)的data属性字符串
			StringBuffer seriesDataSb = new StringBuffer("");
			for(int i=0,size=echartDataList.size(); i<size ; i++){
				Record echartData = echartDataList.get(i);
				xAxisStr = echartData.get(xAxisColumn);
				dataObj = echartData.get(dataColumn);
				
				if(i>0){
					axisDataSb.append(",");
					seriesDataSb.append(",");
				}
				axisDataSb.append("'").append(xAxisStr).append("'");
				if(dataObj!=null){
					seriesDataSb.append(dataObj);
				}else{
					seriesDataSb.append("0");
				}
			}
			// 整合一个系列(series总体属性)
			seriesItemSb.append("{").append(echartTypeOpt).append("name:'").append(echartName).append("',");
			seriesItemSb.append("data:[").append(seriesDataSb.toString()).append("]}");
		}

		Map<String, String> mySettingMap = new HashMap<String, String>();
		mySettingMap.put("legendData", legendDataSb.toString());
		mySettingMap.put("axisData", axisDataSb.toString());
		mySettingMap.put("seriesItem", seriesItemSb.toString());
		return mySettingMap;
	}
}
