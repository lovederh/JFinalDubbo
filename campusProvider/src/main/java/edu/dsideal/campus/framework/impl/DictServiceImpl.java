package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IDictService;
import edu.dsideal.campus.framework.common.JFinalHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 2016/11/4 0004
 */
public class DictServiceImpl implements IDictService {
    // 处理类使用表名
    private static final String TABLE_NAME = "t_sys_dict";
    // 表单中字段(可以不用包括id)
    private static final String TABLE_FORM_COLUMNS = "dictName,pdictId,dictType,realValue,orderNo";

    /**************************************字典初始化********************************************/
    /**
     * 初始构造字典树数据
     * @return
     */
    @Override
    public List<Record> initDictTree() {
        // 构造虚拟根节点
        Record root = new Record();
        root.set("id", "root");
        root.set("name", "根节点");
        root.set("open", "true");

        // 查询树节点使用的sql
        StringBuilder treeSql = new StringBuilder();
        treeSql.append(
            "select d.dictId as id," +
            "	    d.dictName as name," +
            "	    d.pdictId as parentId," +
            "		'false' as `open`," +
            "		d.dictType," +
            "		d.id as primaryId " +
            "  from t_sys_dict d "
        );

        // 查询并构造树列表数据集合
        List<Record> treeData = Db.find(treeSql.toString());
        treeData = treeData==null? new ArrayList<Record>() : treeData;
        treeData.add(0, root);
        return treeData;
    }

    /**************************************字典增删改查********************************************/
    /**
     * 根据字典编码, 删除当前字典及其子字典
     * @param delDictId
     */
    @Override
    public void deleteDict(String delDictId) {
        if(CommonTools.isEmpty(delDictId)){
            return;
        }
        StringBuilder deleteSql = new StringBuilder();
        deleteSql.append("delete from t_sys_dict where dictId like '").append(delDictId).append("%' ");
        Db.update(deleteSql.toString());
    }

    /**
     * 根据主键ID, 按表查询一条记录
     * @param id
     * @return
     */
    @Override
    public Record findDictById(String id) {
        if(CommonTools.isEmpty(id)){
            return null;
        }
        return Db.findById(TABLE_NAME, id);
    }

    /**
     * 保存方法
     * @return
     */
    @Override
    public String saveDict(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        boolean operateResult = false;//操作成功标记
        String operateMsg = null;//操作完成提示
        //封装要保存的菜单数据Record
        Record dict = JFinalHelper.searchRecordPutParams(TABLE_FORM_COLUMNS, paraMap, TABLE_NAME);
        if(dict != null){
            try {
                Object id = dict.get("id");
                if(id==null || CommonTools.isEmpty(String.valueOf(id))){
                    // 新增时候, 需要根据父字典编码, 利用生成规则, 自动生成字典编码
                    String genDictId = genDictIdWhenAdd(dict.get("pdictId"));
                    dict.set("dictId", genDictId);
                    dict.set("domain", loginUserObj.getDomain());
                    Db.save(TABLE_NAME, dict);
                }else{
                    Db.update(TABLE_NAME, dict);
                }
                operateMsg = "字典信息保存成功!";
                operateResult = true;
            } catch (Exception e) {
                operateMsg = e.getMessage();
            }
        }else {
            operateMsg = "保存处理异常, 数据封装错误!";
        }
        return CommonTools.getMsgJson(operateResult, operateMsg);
    }

    /**
     * 新增字典时, 根据父字典编码, 利用生成规则, 自动生成字典编码
     * 生成规则: 第一级字典, 三位字母正序排列; 其它字典, 取同级子字典最大编码+1
     */
    private String genDictIdWhenAdd(String pdictId){
        String dictId;
        // 找到最大的子级字典编码值
        String maxDictId = Db.queryStr("select max(dictId) from t_sys_dict where pdictId = ? ", pdictId);
        maxDictId = maxDictId==null? "" : maxDictId;
        int maxDictIdLen = maxDictId.length();
        if ("root".equals(pdictId)) {
            // 第一级字典, 三位字母正序排列
            if("".equals(maxDictId)){
                dictId = "AAA";
            }else{
                // 转换为char数组进行加位操作
                char[] charArray = maxDictId.toCharArray();
                char c0 = charArray[0];
                char c1 = charArray[1];
                char c2 = charArray[2];
                if("ZZZ".equals(maxDictId)){
                    throw new RuntimeException("字典编码越界!");
                }else if(c2=='Z' && c1=='Z'){
                    dictId = (++c0) + "AA";
                }else if(c2 == 'Z'){
                    dictId = c0 + "" + (++c1) + "A";
                }else{
                    dictId = c0 + "" + c1 + (++c2);
                }
            }
        }else{
            // 其它字典, 取同级子字典最大编码+1
            if("".equals(maxDictId)){
                dictId = pdictId + "001";
            }else if("999".equals(maxDictId.substring(maxDictIdLen - 3))) {
                throw new RuntimeException("字典编码越界!");
            }else{
                String dictPrefix = maxDictId.substring(0, 3);
                // 得到+1后的字典编码处理结果
                long dictSuffix = Long.parseLong(maxDictId.substring(3)) + 1;
                // 格式化处理, 并执行数字码前置位补零
                dictId = dictPrefix + String.format("%0" + (maxDictIdLen - 3) + "d", dictSuffix);
            }
        }
        return dictId;
    }

    /**************************************字典查询********************************************/
    /**
     * 模糊查询
     * 	返回满足条件的字典值ID, 是以逗号分隔的字符串
     * @param searchDictInput   查询字典信息
     * @return
     */
    @Override
    public String searchDicts(String searchDictInput){
        StringBuilder searchDictsSqlSb = new StringBuilder();
        searchDictsSqlSb.append("select id from t_sys_dict where 1=1 ");
        if(CommonTools.isNotEmpty(searchDictInput)){
            searchDictsSqlSb.append(
                " and (dictName like '%").append(searchDictInput).append("%' " +
                "   or dictType like '%").append(searchDictInput).append("%')"
            );
        }
        List<Object> searchDictList = Db.query(searchDictsSqlSb.toString());
        StringBuilder searchDictIdsSb = new StringBuilder("");
        if(searchDictList!=null && searchDictList.size()>0){
            for(int i=0,size=searchDictList.size() ; i<size ; i++){
                if(i != 0){
                    searchDictIdsSb.append(",");
                }
                searchDictIdsSb.append(searchDictList.get(i));
            }
        }
        return searchDictIdsSb.toString();
    }

    /**
     * 列表查询
     * @param pageNumber
     * @param pageSize
     * @param dictId
     * @return
     */
    @Override
    public Page<Record> findChildrenDicts(String pageNumber, String pageSize, String dictId) {
        StringBuilder sqlExcept = new StringBuilder(" from t_sys_dict ");
        if("root".equals(dictId)){
            sqlExcept.append(" where pdictId = 'root' ");
        }else{
            sqlExcept.append(" where dictId like '").append(dictId).append("%' ");
        }
        sqlExcept.append(" order by pdictId, orderNo ");
        return JFinalHelper.searchPaginate(pageNumber, pageSize, " select * ", sqlExcept.toString());
    }
}
