package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.model.DBServiceFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/12/22.
 */
public class TableColumnTreeServiceImpl implements IRoleTreeInitService {
    private final static String TABLE_NAME = "t_sys_boconfig";
    @Override
    public List<Record> getRoleTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String domain = paraMap.get("domain")[0];
        String roleId = paraMap.get("roleId")[0];
        StringBuilder sqlBuilder = new StringBuilder("select t.id as `key`, t.boName as id, " +
                "concat(t.showName, '(', t.tableName, ')') as name," +
                " '0' as pId, " +
                "'1' as level, " +
                "'false' as isLeaf, 'true' as nocheck, " +
                " 'false' as checked from t_sys_bo t where ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
        sqlBuilder.append(" union all ");
        sqlBuilder.append("select t.id as `key`, t.id, concat(t.showName, '(', t.colName, ')') as name ," +
                " t.boName as pId, '2' as level, 'true' as isLeaf, 'false' as nocheck," +
                " case when t1.id is null then 'false' else 'true' end as checked " +
                " from t_sys_boconfig t " +
                " left join t_sys_boconfig_role t1 on t.id = t1.dataId and t1.roleId = '").append(roleId)
                .append("' inner join t_sys_bo t2 on t.boName = t2.boName where ");
        //查询权限树节点
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
        sqlBuilder.append(" order by id asc ");
        return Db.find(sqlBuilder.toString());
    }
    @Override
    public boolean delRoleData(String tableName, String domain, String roleId) throws SQLException {
        return RoleTreeService.delRoleData(TABLE_NAME, domain, roleId);
    }
}
