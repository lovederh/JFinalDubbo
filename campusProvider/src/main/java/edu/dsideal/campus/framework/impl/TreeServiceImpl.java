package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.ITreeService;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.service.IRoleTreeInitService;
import edu.dsideal.campus.framework.service.RoleTreeFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/21.
 */
public class TreeServiceImpl implements ITreeService {
    @Override
    public List<Record> getTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        IRoleTreeInitService treeInitService = RoleTreeFactory.initService("common");
        return treeInitService.getRoleTreeData(paraMap, loginUserObj);
    }

    @Override
    public List<Record> initRoleTree(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        IRoleTreeInitService roleTreeInitService = RoleTreeFactory.initService(paraMap.get("paraTreeId")[0]);
        return roleTreeInitService.getRoleTreeData(paraMap, loginUserObj);
    }

    @Override
    public boolean saveTreeRole(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String boName = paraMap.get("dtoName")[0];
        String domain = paraMap.get("domain")[0];
        String[] ids = paraMap.get("ids")[0].split(",");
        String roleId = paraMap.get("roleId")[0];
        String tableName = DBServiceFactory.getDoNameByBoName(boName, domain);
        List<String> sqlList = new ArrayList<>();
        Db.update("delete from "+tableName+"_role where roleId = ?", roleId);
        for(String id :ids){
            sqlList.add("insert into "+tableName+"_role(roleId, dataId) values('"+roleId+"','"+id+"')");
        }
        return Db.batch(sqlList, sqlList.size())[0] >= 0;
    }

}
