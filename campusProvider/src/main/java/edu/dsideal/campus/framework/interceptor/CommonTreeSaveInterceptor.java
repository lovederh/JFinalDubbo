package edu.dsideal.campus.framework.interceptor;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.sql.BoSql;
import edu.dsideal.campus.framework.sql.RoleSql;

import java.sql.SQLException;

/**
 * Created by dsideal-lee on 2017/2/14.
 */
public class CommonTreeSaveInterceptor extends MetaObjectIntercept {
    @Override
    @Before(Tx.class)
    public void addAfter(BaseTable record) throws SQLException {
        LoginUserObj loginUserObj = record.get("loginUserObj");
        String dtoName = record.getStr("dtoName");
        String domain = record.getStr("domain");
        String boName = BoSql.getDoName(dtoName, domain);
        String sql = RoleSql.getInsertRoleSql(boName);
        Db.update(sql,loginUserObj.getRoles().split(",")[0], record.get("id"), CommonTools.getCurrentTimeYmdhms(), loginUserObj.getLoginId());
        String formWrapper = record.getStr("formWrapper");
        if("addChild".equals(formWrapper)){
            Db.update(new StringBuilder("update ")
                    .append(DBServiceFactory.getDoNameByBoName(record.getStr("dtoName"), record.getStr("domain")))
                    .append(" set isLeaf = 'false' where codeId = '").append(record.getStr("pId")).append("'").toString());
        }
    }
    @Override
    public void delTreeNodeAfter(Record record) throws SQLException{
        String level =record.getStr("level");
        if(!"1".equals(level)){
            StringBuilder sqlBuilder = new StringBuilder("update ")
                    .append(record.getStr("tableName"))
                    .append(" set isLeaf = 'true' , expanded = 'false' where codeId = ?");
            Db.update(sqlBuilder.toString(), record.getStr("pId"));
        }
    }
}
