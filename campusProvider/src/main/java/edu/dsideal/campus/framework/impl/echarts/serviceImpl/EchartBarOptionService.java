package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;

import java.util.Map;

public class EchartBarOptionService implements IEchartOptionService{
	/**
	 * 图表类型为柱状图时, 拼接option属性字符串的操作
	 * 功能: 	拼接mySettingMap中供option属性使用的关键性字符串
	 * 		最终返回的可供前端页面使用的option属性字符串
	 * 		以字符串形式返回出去
	 */
	@Override
	public String createOptionData(Record echartRecord, Map<String, String> mySettingMap) {
		// 图表标题
		String echartName = echartRecord.get("echartsName");
		// 确定柱状图是纵向(上下方向)标识
		String echartsType = echartRecord.get("echartsType");
		boolean top2BottomFlag = "5".equals(echartsType) || "6".equals(echartsType);
		
		// 需要的属性配置字符串
		StringBuffer optionSb = new StringBuffer();
		optionSb.append(
				"{" +
				"	title: {" +
				"		text: '").append(echartName).append("'" +
				"	}," +
				"	tooltip: {" +
				"		trigger: 'axis'," +
				"		axisPointer : {" +
				"			type : 'shadow'" +
				"		}" +
				"	}," +
				"	legend: {" +
				"		data:[").append(mySettingMap.get("legendData")).append("]" +
				"	}," +
				"	grid: {" +
				"		left: '3%'," +
				"		right: '4%'," +
				"		bottom: '3%'," +
				"		containLabel: true" +
				"	},"
		);
		
		// 柱状图是横向还是纵向体现在数据分布位置
		if(top2BottomFlag){
			optionSb.append(
				"	xAxis: {" +
				"		type: 'category'," +
				"		data: [").append(mySettingMap.get("axisData")).append("]" +
				"	}," +
				"	yAxis: [" +
				"		{" +
				"			type: 'value'" +
				"		}" +
				"	],"
			);
		}else{
			optionSb.append(
				"	yAxis: {" +
				"		type: 'category'," +
				"		data: [").append(mySettingMap.get("axisData")).append("]" +
				"	}," +
				"	xAxis: [" +
				"		{" +
				"			type: 'value'" +
				"		}" +
				"	],");
		}
		
		optionSb.append(
				"	series: [").append(mySettingMap.get("seriesItem")).append("]" +
				"}"
		);
		return optionSb.toString();
	}
}
