package edu.dsideal.campus.framework.impl.echarts;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IEchartsService;
import edu.dsideal.campus.framework.common.JFinalHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 2016/11/23 0023
 */
public class EchartsServiceImpl implements IEchartsService {
    // 表单中字段(可以不用包括id)
    private static final String TABLE_FORM_COLUMNS = "echartsName,echartsType,xAxisColumn,yAxisColumn,dataColumn,echartsSql,ord,pId,barSumFlag,remark,nightingaleType";

    /**
     * 构造图表信息树
     */
    @Override
    public List<Record> initEchartsTree(){
        // 构造虚拟根节点
        Record root = new Record();
        root.set("id", 0);
        root.set("name", "图表项维护");
        root.set("open", "true");

        StringBuilder echartsTreeSql = new StringBuilder();
        echartsTreeSql.append(
                "select e.id as id," +
                "	    e.echartsName as name," +
                "	    e.pId as parentId,"+
                "       'false' as `open` " +
                "from   t_sys_echarts e " +
                "where  e.delFlag = 0 "
        );
        // 查询并构造树列表数据集合
        List<Record> echartsData = Db.find(echartsTreeSql.toString());
        echartsData = echartsData==null? new ArrayList<Record>() : echartsData;
        echartsData.add(0, root);
        return echartsData;
    }

    /**
     * 查询选中一条的图表信息
     * @param echartId 图表ID
     * @return
     */
    @Override
    public Record findOne(String echartId) {
        return CommonTools.isEmpty(echartId)? null: Db.findById("t_sys_echarts", "id", echartId);
    }


    /**
     *  图表类型列表
     *  @return
     */
    @Override
    public List<Record> findEchartsTypeList(){
        return Db.find("select d.realValue,d.dictName from t_sys_dict d where d.dictType = 'dict_echartType' and d.pdictId <> 'root' ");
    }

    /**
     * 图表项维护信息的保存方法
     */
    @Override
    public Record saveEchartInfo(Map<String, String[]> echartMap){
        String tableName = "t_sys_echarts";
        //封装要保存的菜单数据Record
        Record echartRecord = JFinalHelper.searchRecordPutParams(TABLE_FORM_COLUMNS, echartMap, tableName);
        if(echartRecord != null){
            Object id = echartRecord.get("id");
            if(id==null || CommonTools.isEmpty(String.valueOf(id))){
                Db.save(tableName, echartRecord);
            }else{
                Db.update(tableName, echartRecord);
            }
        }else {
            throw new RuntimeException("保存处理异常, 数据封装错误!");
        }
        return echartRecord;
    }

    /**
     * 该类中对外提供的一个方法
     * 功能: 返回最终的可供前端页面使用的option属性字符串
     * 外部类直接调用即可(先利用图表项配置Record实例化出来)完成该功能, 降低耦合性
     * @return
     */
    @Override
    public String createOptionData(Record echartRecord) {
        // 再生成图表预览使用的option属性并返回
        MyEchartStrategy myEchartStrategy = new MyEchartStrategy(echartRecord);
        return myEchartStrategy.createOptionData();
    }

    /**
     * 删除选中图表
     * @param echartId
     */
    @Override
    public void deleteEcharts(String echartId) {
        if(CommonTools.isEmpty(echartId)){
            return;
        }
        StringBuilder deleteEchartsSql = new StringBuilder();
        deleteEchartsSql.append("update t_sys_echarts set delFlag='1' where id = '").append(echartId).append("' ");
        Db.update(deleteEchartsSql.toString());
    }
}
