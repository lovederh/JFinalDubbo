package edu.dsideal.campus.framework.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.interceptor.MetaObjectIntercept;
import edu.dsideal.campus.framework.service.IDoService;
import edu.dsideal.campus.framework.common.Crud;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.framework.interceptor.MetaObjectIntercept;
import edu.dsideal.campus.framework.model.ISQLService;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by dsideal-lee on 2016/10/15.
 */
public class DoServiceImpl implements IDoService {

    MetaObjectIntercept interceptor;

    @Override
    public Page<Record> show(BaseTable model, int pageNumber, int pageSize, String where, String order) {
        return Db.paginate(pageNumber, pageSize, "select *",
                new StringBuilder(" from ").append(model.getStr("dtoName")).append(where).append(order).toString());
    }

    @Override
    @Before(Tx.class)
    public boolean save(BaseTable model) throws Exception {
        return Crud.add(model);
    }

    @Override
    public boolean update(BaseTable model) throws Exception {
        return Crud.update(model);
    }

    @Override
    @Before(Tx.class)
    public boolean deleteById(BaseTable model, String[] ids) throws Exception {
        if(ids != null) {
            return Crud.deleteById(model, ids);
        }else{
            return false;
        }
    }

    @Override
    @Before(Tx.class)
    public boolean rebuild(final BaseTable model) throws Exception {
        ISQLService sqlService = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
        return Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                String dtoName = model.getStr("dtoName");
                //查询表结构中哪些字段未重构
                List<Record> needBuildColList = Db.find("select * from t_sys_columns where isBuild = '0' and tableName = ?", dtoName);
                //判断是否有需要重构的数据
                if(needBuildColList != null && needBuildColList.size() > 0) {
                    //将未生成未重构的表加入到boconfig
                    int countInst = Db.update(
                            new StringBuilder("insert into t_sys_boconfig(colName, tableName, type, colLen, isKey, showName, pId, boName)")
                                    .append(" select t.colName, t.tableName, t.type, t.colLen, t.isKey, t.showName,t1.id  as pId ,t1.boName as boName from t_sys_columns t")
                                    .append(" inner join t_sys_tables t2 on t.tableName = t2.tableName ")
                                    .append(" left join t_sys_bo t1 on t2.tableName = t1.tableName where t.isBuild = 0 and t.tableName = ?").toString(),
                            dtoName);
                    //更新实体表
                    StringBuilder crtSql = new StringBuilder("alter table ").append(dtoName);
                    for (Record r : needBuildColList) {
                        crtSql.append(" add ");
                        crtSql.append(r.getStr("colName"));
                        //判断字段类型
                        String colType = r.getStr("type");
                        sqlService.buildColConfig(crtSql, r, colType);
                    }
                    crtSql.deleteCharAt(crtSql.length() - 1);
                    int countInsSql = Db.update(crtSql.toString());
                    //修改表t_sys_columns表中的标识为已经重构
                    int countBuild = Db.update("update t_sys_columns set isBuild = '1' where tableName = ?", dtoName);
                    return countBuild >= 0 && countInsSql >= 0 && countInst >= 0;
                }
                return false;
            }
        });
    }
}
