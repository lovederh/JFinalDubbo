package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Redis;
import com.xiaoleilu.hutool.util.DateUtil;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IKeyService;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.framework.model.ISQLService;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.jfinal.plugin.activerecord.Db.findFirst;
public class KeyServiceImpl implements IKeyService {
    @Override
    public String[] getKey(Map<String, String[]> paraMap) {
        String dtoName = paraMap.get("dtoName")[0];
        //得到的key值
        String[] key = new String[2];
        //查询key的配置信息
        Record keyRecord = findFirst("select t.id id, t.keyCol,t.keyRule keyRule,t.boName boName, t.struct struct, t.levelCol levelCol, t.pidCol pidCol," +
                " t1.ruleType ruleType, t1.beginWith beginWith," +
                " t1.flowNumWidth flowNumWidth " +
                " from t_sys_keyrule t " +
                " left join t_sys_ruleconfig t1 on t.keyRule = t1.id " +
                " where t1.isUsed = 'true' and boName = ?", dtoName);
        if(keyRecord != null) {
            key[0] = keyRecord.getStr("keyCol");
            //开始字符
            String beginWith;
            //存储主键
            StringBuilder keySb;
            switch (keyRecord.getStr("ruleType")) {
                case "0":
                    //系统默认
                    key = null;
                    break;
                case "1":
                    //UUID
                    key[1] = UUID.randomUUID().toString();
                    break;
                case "2":
                    String pId = CommonTools.getValueFromParaMap(paraMap, "pId");
                    //树结构主键
                    keySb = new StringBuilder();
                    createTreeId(keySb, pId, keyRecord);
                    key[1] = keySb.toString();
                    break;
                case "3":
                    //开始日期字符串
                    beginWith = DateUtil.format(DateUtil.date(), "yyyyMMdd");
                    //存储日期流水的StringBuilder
                    keySb = new StringBuilder(beginWith);
                    //生成流水
                    createFlowNum(keySb, key[0], Integer.parseInt(keyRecord.getStr("flowNumWidth")), beginWith);
                    key[1] = keySb.toString();
                    break;
                case "4":
                    //开始字符串
                    beginWith = keyRecord.getStr("beginWith");
                    //自定义开头字母的流水
                    keySb = new StringBuilder(beginWith);
                    //生成流水
                    createFlowNum(keySb, key[0], Integer.parseInt(keyRecord.getStr("flowNumWidth")), beginWith);
                    key[1] = keySb.toString();
                    break;
            }
        }
        return key;
    }

    /**
     * 生成树主键的方法
     *
     * @param keySb     存储构造主键的StringBuilder
     * @param pId       父Id
     * @param keyRecord 主键配置信息
     */
    private static synchronized void createTreeId(StringBuilder keySb, String pId, Record keyRecord) {
        //pId如果为空默认为0
        pId = CommonTools.isEmpty(pId)?"1":pId;
        String levelCol = keyRecord.getStr("levelCol");
        String pidCol = CommonTools.isEmpty(keyRecord.getStr("pidCol"))?"pId":keyRecord.getStr("pidCol");
        String idCol = keyRecord.getStr("keyCol");
        String doName = Db.queryStr("select tableName from t_sys_bo where boName = ?", keyRecord.getStr("boName"));
        //根据选中的ID, 查询选中的原始数据
        Record parent = findFirst("select "+levelCol+ " as level  from " + doName + " where " + idCol +"=?", pId);
        int parentLevel = 0;
        if(parent !=null){
            parentLevel=Integer.valueOf(parent.get("level"));
        }
        int nowLevel= parentLevel+1;
        // 根据现有级别找到最大的节点
        ISQLService sqlService = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
        StringBuilder sqlBuilder = new StringBuilder();
        sqlService.getMaxIdSql(sqlBuilder, doName, keyRecord.getStr("keyCol"), pidCol);
        String treeDataId = Db.queryStr(sqlBuilder.toString(), pId);

        if (CommonTools.isEmpty(treeDataId)) {

            //先将父id拼在前面
            keySb.append(pId);
            //如果为空证明不存在子结点
            //将结构转为字符数组233 = {2， 3， 3}
            char[] c = keyRecord.getStr("struct").toCharArray();
            //得到当前级的结构位数
            int keyLen = Integer.parseInt(c[nowLevel - 1] + "");
            for (int i = 0; i < keyLen - 1; i++) {
                keySb.append("0");
            }
            keySb.append("1");
        } else {
            //不为空直接在id上面加1
            keySb.append(new BigDecimal(treeDataId).add(BigDecimal.ONE).toString());
        }
    }

    /**
     * 生成流水号方法
     * 若服务器中途停机，此处要加个策略解决
     * 缓存中存放的流水信息{key: id ,{beginWith:"20161010", flowNum: "000001"}}{key: id ,{beginWith:"mmm", flowNum: "000001"}},
     * 此处需要测试并发主键生成是否重复
     *
     * @param keySb     存储生成的主键容器
     * @param id        主键
     * @param width     流水号长度
     * @param beginWith 开始流水号
     */
    public static synchronized void createFlowNum(StringBuilder keySb, String id, int width, String beginWith) {
        //存储生成的流水号
        StringBuilder flowNumSb = new StringBuilder();
        //从缓存中获得当前的流水
        Map<String, String> valMap = getFlowNumCache(id);
        if (valMap == null) {
            //若缓存中不存在流水，从1开始
            for (int i = 0; i < width - 1; i++) {
                keySb.append("0");
                flowNumSb.append("0");
            }
            keySb.append("1");
            flowNumSb.append("1");
        } else {
            //从缓存中拿到流水加1
            keySb.append(new BigDecimal(valMap.get("beginWith")).add(BigDecimal.ONE));
        }
        //将流水放到缓存中
        Map<String, String> newValMap = new HashMap<>();
        newValMap.put(beginWith, flowNumSb.toString());
        setFlowNumCache(id, newValMap);
    }

    /**
     * 将主键的值放在缓存中
     *
     * @param key
     * @param valMap
     */
    private static void setFlowNumCache(String key, Map<String, String> valMap) {
        Redis.use("dssm").set(key, valMap);
    }

    /**
     * 得到当前主键的值
     *
     * @param key
     * @return
     */
    private static Map<String, String> getFlowNumCache(String key) {
        return Redis.use("dssm").get(key);
    }
}
