package edu.dsideal.campus.framework.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import java.util.List;

/**
 * Created by dsideal-lee on 2016/10/25.
 */
public class OracleServiceImpl implements ISQLService {
    @Override
    public void getMaxIdSql(StringBuilder sqlBuilder, String tableName, String keyCol, String pidCol) {
        sqlBuilder.append("select ").append(keyCol)
                .append(" from ").append(tableName)
                .append(" where rownum < 2 and ").append(pidCol).append(" = ?")
                .append(" order by id desc");
    }

    @Override
    public void buildCrtRoleTableSql(StringBuilder crtSql, List<String> sqlList) {
        crtSql.append("id number(11,0)," +
                " roleId varchar(64) not null," +
                " dataId varchar2(64) not null," +
                " r_r varchar2(5)," +
                " r_w varchar2(5)," +
                " r_u varchar2(5)," +
                " r_d varchar2(5)," +
                " crt_time varchar2(32)," +
                " crt_user varchar2(64)," +
                " domain varchar2(64)" +
                " )");
        sqlList.add(crtSql.toString());
        sqlList.add(
                " create sequence RS_EMPLOYEE_SEQ " +
                        " minvalue 1 " +
                        " maxvalue 99999999999 " +
                        " start with 0 " +
                        " increment by 1 " +
                        " cache 20 ");
    }

    @Override
    public void buildColConfig(StringBuilder crtSql, Record r, String colType) {
        switch (colType) {
            case "varchar":
            case "varchar2":
                crtSql.append(" varchar2(").append(r.getStr("colLen")).append(")");
                break;
            case "int":
                crtSql.append(" number(11,0)");
                break;
            case "unsigned":
                crtSql.append(" number(64,0)");
                break;
            default:
                crtSql.append(colType);
                break;
        }
        //如果是主键就
        if ("true".equals(r.get("isKey"))) {
            crtSql.append(" PRIMARY KEY ");
        }
        crtSql.append(",");
    }

    @Override
    public void buildCrtSql(StringBuilder crtSql, List<Record> recordList) {
        boolean isExistDomainCol = false;
        //循环构造建表字段
        for (Record r : recordList) {
            if ("domain".equals(r.getStr("colName"))) {
                isExistDomainCol = true;
            }
            crtSql.append(r.getStr("colName"));
            //判断字段类型
            String colType = r.getStr("type");
            buildColConfig(crtSql, r, colType);
        }
        if (!isExistDomainCol) {
            crtSql.append("domain varchar(64) COMMENT '企业域',");
        }
        crtSql.deleteCharAt(crtSql.length() - 1);
        crtSql.append(")");
    }

    @Override
    public boolean checkTableIsExist(String tableName) {
        long count = Db.queryLong("SELECT COUNT(1) from user_tables where table_name = ?", tableName.toUpperCase());
        return 1 == count;
    }

    @Override
    public void buildFilterSql(StringBuilder sqlBuilder, int page, int rows) {
        sqlBuilder.append(" and rownum between ").append((page - 1) * rows).append(" and ").append(page * rows).append(" ");
    }

    @Override
    public String getRolesStr(String loginId) {
        return Db.queryStr("select wm_concat(roleId) from t_sys_user where loginId = ? ", loginId);
    }

    @Override
    public String getRoleCols(String boName, LoginUserObj loginUserObj) {
        if ("domain".equals(loginUserObj.getDomain())) {
            return Db.queryStr("select wm_concat(t.colName) from t_sys_boconfig t where t.boName = ?", boName);
        }
        StringBuilder sqlBuilder = new StringBuilder("select wm_concat(t.colName) from t_sys_boconfig t " +
                " inner join t_sys_boconfig_role t1 on t.id = t1.dataId" +
                " where t1.roleId in (").append(loginUserObj.getRoles());
        DBServiceFactory.addDomainFilter(sqlBuilder, loginUserObj.getDomain(), "t");
        sqlBuilder.append(" and boName = ?");
        return Db.queryStr(sqlBuilder.toString(), boName);
    }
}
