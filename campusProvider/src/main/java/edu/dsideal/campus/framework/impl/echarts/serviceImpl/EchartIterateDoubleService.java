package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EchartIterateDoubleService implements IEchartIterateService{
	/**
	 * 图表类型为多柱图/多折线图时, 对查询结果的遍历方式
	 * 功能: 	两次遍历, 生成多柱图/多折线图option属性中关键的
	 * 		legend中data属性, series总体属性和xAxis中data属性(横向柱图时对应yAxis中data属性)
	 * 		以map形式返回, 为最终的图表option属性拼接做准备
	 */
	@Override
	public Map<String, String> createMySettingData(List<Record> echartDataList, Record echartRecord) {
		// 第一维度取值字段
		String xAxisColumn = echartRecord.get("xAxisColumn");
		// 第二维度取值字段
		String yAxisColumn = echartRecord.get("yAxisColumn");
		// 数字数据取值字段
		String dataColumn = echartRecord.get("dataColumn");
		// 根据是否为柱状图以及是否总计风格, 确定系列(series)中type属性值
		String echartTypeOpt;
		if("4".equals(echartRecord.get("echartsType"))){
			echartTypeOpt = "type:'line',";
		}else{
			String barSumFlag = echartRecord.get("barSumFlag");
			if(CommonTools.isNotEmpty(barSumFlag) && "1".equals(barSumFlag)){
				echartTypeOpt = "type:'bar',stack: '总量',";
			}else{
				echartTypeOpt = "type:'bar',";
			}
		}
		
		// 折线图导航数据对应的字符串(legend中data属性)
		StringBuffer legendDataSb = new StringBuffer("");
		// 折线图x轴提示数据对应的字符串(xAxis中data属性)
		StringBuffer axisDataSb = new StringBuffer("");
		// 折线图中所有配置项及数据对应的字符串(series属性)
		StringBuffer seriesItemSb = new StringBuffer("");
		
		// 去重后, 所有的x轴配置项集合
		Set<String> xAxisDataSet = new LinkedHashSet<String>();
		// 去重后, 所有的y轴配置项集合
		Set<String> yAxisDataSet = new LinkedHashSet<String>();
		if(null!=echartDataList && echartDataList.size()>0){
			// 查询结果中x轴配置项对应的汉字信息
			Object xAxisStr;
			// 查询结果中y轴配置项对应的汉字信息
			Object yAxisStr;
			int echartDataSize = echartDataList.size();
			for(int i=0; i<echartDataSize ; i++){
				Record echartData = echartDataList.get(i);
				xAxisStr = echartData.get(xAxisColumn);
				yAxisStr = echartData.get(yAxisColumn);
				// 获取x轴所有出现的配置项集合
				if(xAxisStr!=null){
					xAxisDataSet.add(xAxisStr.toString());
				}
				// 获取y轴所有出现的配置项集合
				if(yAxisStr!=null){
					yAxisDataSet.add(yAxisStr.toString());
				}
			}
			
			// 遍历x轴配置项, 得到导航栏data属性
			Iterator<String> xDataIterator = xAxisDataSet.iterator();
			while (xDataIterator.hasNext()) {
				axisDataSb.append("'").append(xDataIterator.next()).append("',");
			}
			
			// 一个系列中(y轴配置项series)的data属性字符串
			StringBuffer seriesDataSb;
			Iterator<String> yIterator = yAxisDataSet.iterator();
			// 遍历y轴配置项, 拼接数据时得到y大项
			while (yIterator.hasNext()) {
				String yAxisName = yIterator.next();
				
				seriesDataSb = new StringBuffer("");
				// 是否在查询结果中找到了当前x配置和y配置对应数据标识
				boolean findDataFlag;
				Iterator<String> xIterator = xAxisDataSet.iterator();
				while (xIterator.hasNext()) {

					String xAxisName = xIterator.next();
					
					Object dataObj;
					findDataFlag = false;
					// 遍历图表数据结果, 一对一对一确认每个属性组对应的数字结果
					for(int j=0; j<echartDataSize ; j++){
						Record echartData = echartDataList.get(j);
						xAxisStr = echartData.get(xAxisColumn);
						yAxisStr = echartData.get(yAxisColumn);
						
						if(xAxisName.equals(xAxisStr) && yAxisName.equals(yAxisStr)){
							dataObj = echartData.get(dataColumn);
							if(dataObj!=null){
								seriesDataSb.append(dataObj).append(",");
							}else{
								seriesDataSb.append("0,");
							}
							findDataFlag = true;
							break;
						}
					}
					// 查询结果中不存在当前x配置和y配置对应数据, 则拼接0
					if(!findDataFlag){
						seriesDataSb.append("0,");
					}
				}
				// 导航栏提示使用y轴中对应配置项
				legendDataSb.append("'").append(yAxisName).append("',");
				// 整合每一个系列(series总体属性)
				String seriesData = seriesDataSb.length()==0? "": seriesDataSb.deleteCharAt(seriesDataSb.length()-1).toString();
				seriesItemSb.append("{").append(echartTypeOpt).append("name:'").append(yAxisName).append("',");
				seriesItemSb.append("data:[").append(seriesData).append("]},");
			}
		}
		// 去掉多余的逗号
		String legendData = legendDataSb.length()==0? "": legendDataSb.deleteCharAt(legendDataSb.length()-1).toString();
		String axisData = axisDataSb.length()==0? "": axisDataSb.deleteCharAt(axisDataSb.length()-1).toString();
		String seriesItem = seriesItemSb.length()==0? "": seriesItemSb.deleteCharAt(seriesItemSb.length()-1).toString();
		
		Map<String, String> mySettingMap = new HashMap<String, String>();
		mySettingMap.put("legendData", legendData);
		mySettingMap.put("axisData", axisData);
		mySettingMap.put("seriesItem", seriesItem);
		return mySettingMap;
	}
}
