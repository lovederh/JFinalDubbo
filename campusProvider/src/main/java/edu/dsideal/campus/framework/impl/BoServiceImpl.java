package edu.dsideal.campus.framework.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.interceptor.MetaObjectIntercept;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.common.Crud;
import edu.dsideal.campus.framework.common.JFinalHelper;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.framework.service.RoleTreeFactory;
import edu.dsideal.campus.framework.sql.BoSql;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by dsideal-lee on 2016/10/21.
 */
public class BoServiceImpl implements IBoService {
    private MetaObjectIntercept metaObjectIntercept;

    @Override
    @Before(Tx.class)
    public boolean delTreeNodeById(Map<String, String[]> paraMap) throws Exception{
        String domain = paraMap.get("domain")[0];
        String dtoName = paraMap.get("dtoName")[0];
        String ids = paraMap.get("ids")[0];
        Record boAndKeyRule = getBoAndKeyRule(dtoName, domain);
        String tableName = boAndKeyRule.getStr("tableName");
        metaObjectIntercept = Crud.getInterceptor(dtoName);
        Record record = Db.findFirst(new StringBuilder("select * from ")
                .append(tableName).append(" where codeId = ?").toString(), ids);
        if(metaObjectIntercept != null){
            record.set("tableName", tableName);
            metaObjectIntercept.delTreeNodeBefore(record);
        }
        StringBuilder sqlBuilder = new StringBuilder("delete from ")
                .append(tableName)
                .append(" where ").append(boAndKeyRule.getStr("keyCol")).append(" like '").append(ids)
                .append("%' and ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, null);
        //delete tree data
        int count = Db.update(sqlBuilder.toString());
        //删除权限中相关的数据
        RoleTreeFactory roleTreeFactory = new RoleTreeFactory(tableName, domain, ids);
        roleTreeFactory.registRoleService();
        roleTreeFactory.doDelRole();
        if(metaObjectIntercept != null){
            metaObjectIntercept.delTreeNodeAfter(record);
        }
        return count>=0;
    }

    @Override
    public Record saveTreeNode(BaseTable model) throws SQLException{
        Record boAndKeyRule = getBoAndKeyRule(model.getStr("dtoName"), model.getStr("domain"));
        if(CommonTools.isNotEmpty(boAndKeyRule.getStr("keyCol"))) {
            model.set(boAndKeyRule.getStr("keyCol"), model.getStr("nodeKey"));
        }else{
            model.set("id", model.getStr("nodeKey"));
        }
        model.set(boAndKeyRule.getStr("caption"), model.getStr("name"));
        boolean flag = Crud.add(model);
        return new Record().set("id", model.get("id")).set("flag", flag);
    }

    @Override
    public Map<String, String> getValidate(String dtoName) {
        Map<String, String> validMap = null;
        //查出当前模型需要校验的列及配置
        List<Record> validColConf = Db.find("select t.colName, t.editrule, t1.realValue from t_sys_boconfig t, t_sys_dict t1 " +
                " where t.editrule = t1.dictId " +
                " AND t.editrule > 'AAF001' and boName = ?", dtoName);
        if (validColConf != null && validColConf.size() > 0) {
            validMap = new HashMap<String, String>();
            for (Record r : validColConf) {
                validMap.put(r.getStr("colName"), r.getStr("realValue"));
            }
        }
        return validMap;
    }

    @Override
    public Map<String, Map<String, String>> getModelDict(String dtoName, String domain) {
        //查询当前模型关联的所有字典表
        List<Record> refTableCol = Db.find("select refTable,dictId, dictName,dictType, refTableFilter from t_sys_boconfig " +
                " where edittype in( 'select','checkbox') and boName = ?", dtoName);
        if (refTableCol != null && refTableCol.size() > 0) {

            //存放所有字典的Map
            Map<String, Map<String, String>> dictsMap = new HashMap<String, Map<String, String>>();
            //存放单个字典的Map
            Map<String, String> everyDictMap = null;
            for (Record r : refTableCol) {
                //查询字典数据sql
                String dictId = CommonTools.isEmpty(r.getStr("dictId")) ? "dictId" : r.getStr("dictId");
                String dictName = CommonTools.isEmpty(r.getStr("dictName")) ? "dictName" : r.getStr("dictName");
                StringBuilder selDictSql = new StringBuilder("select ").append(dictId).append(" as dictId,").append(dictName)
                        .append(" as dictName from ").append(r.getStr("refTable")).append(" where ");
                DBServiceFactory.addDomainFilter(selDictSql, domain, null);
                if(CommonTools.isNotEmpty(r.getStr("refTableFilter"))){
                    selDictSql.append(" and ").append(r.getStr("refTableFilter"));
                }
                everyDictMap = new LinkedHashMap<String, String>();
                everyDictMap.put("", "全部");
                //查找单个字典数据
                List<Record> dictRecord = Db.find(selDictSql.toString());
                for (Record dictRow : dictRecord) {
                    //字典数据放入map中
                    everyDictMap.put(dictRow.getStr("dictId"), dictRow.getStr("dictName"));
                }
                //单个字典放到字典集合中
                dictsMap.put(r.getStr("dictType"), everyDictMap);
            }
            return dictsMap;
        }
        return null;
    }

    @Override
    public Record getDoDataById(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String dtoName = paraMap.get("dtoName")[0];
        String domain = paraMap.get("domain")[0];
        String id = paraMap.get("id")[0];
        //获得bo信息
        Record bo = getBo(paraMap.get("dtoName")[0], domain);
        //查询出拥有权限列的数据
        if (ConstStatic.ADMIN_DOMAIN.equals(domain)) {
            return Db.findFirst("select * from " + bo.getStr("tableName") + " where id = ?", id);
        }
        List<Record> boconfig = getBoConfig(dtoName, loginUserObj);
        String roleCols = getRoleCols(boconfig, domain, "t");
        return Db.findFirst("select " + roleCols + " from " + bo.getStr("tableName") + " t where id = ?", id);
    }

    @Override
    public List<Record> getFormShowCol(String dtoName, LoginUserObj loginUserObj) {
        StringBuilder sqlBuilder = new StringBuilder();
        BoSql.formShowSql(sqlBuilder, loginUserObj);
        return Db.find(sqlBuilder.toString(), dtoName);
    }

    @Override
    public Record getBoAndKeyRule(String boName, String domain) {
        return BoSql.getBoAndKeyRule(boName, domain);
    }

    @Override
    public String getRoleCols(List<Record> boconfig, String domain, String alias) {
        return BoSql.getRoleCols(boconfig, domain, alias);
    }

    @Override
    public String getContactTableData(String boName, String where, String domain) {
        StringBuilder selValue = new StringBuilder();
        if ("t_sys_dict".equals(boName)) {
            StringBuilder sqlBuilder = new StringBuilder(" select t.dictId, t.dictName from t_sys_dict t where ");
            DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
            sqlBuilder.append(" and not exists (select 1 from t_sys_dict tsd where tsd.id = t.id and tsd.pdictId = 'root' ) and dictType = ?");
            List<Record> ctdList = Db.find(sqlBuilder.toString(), where);
            for (Record r : ctdList) {
                selValue.append(String.valueOf(r.getStr("dictId"))).append(":").append(r.getStr("dictName")).append(";");
            }
        } else {
            //查询配置中的关联表中的名称列
            Record bo = getBoAndKeyRule(boName, domain);
            //查询关联表的数据
            String caption = bo.getStr("caption");
            StringBuilder sqlBuilder = new StringBuilder("select id, ").append(caption).append(" from ")
                    .append(bo.getStr("tableName")).append(" where  ");
            DBServiceFactory.addDomainFilter(sqlBuilder, domain, null);
            List<Record> ctdList = Db.find(sqlBuilder.toString());
            //构造jqgrid的select value值
            for (Record r : ctdList) {
                if ("t_sys_tables".equals(boName)) {
                    //基础表的关联信息需要特殊处理
                    selValue.append(String.valueOf(r.getStr("tableName"))).append(":").append(r.getStr(caption)).append(";");
                } else {
                    selValue.append(String.valueOf(r.getInt("id"))).append(":").append(r.getStr(caption)).append(";");
                }
            }
        }
        return selValue.deleteCharAt(selValue.length() - 1).toString();
    }

    @Override
    public Map<String, Object> getDoByPage(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String dtoName = paraMap.get("dtoName")[0];
        String domain = paraMap.get("domain")[0];
        String limit = paraMap.get("limit")[0];
        String offset = paraMap.get("offset")[0];
        //得到sql构造工厂
        ISQLService sqlService = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
        //根据dtoName获得bo信息
        Record bo = getBoAndKeyRule(dtoName, domain);
        //查询权限列
        List<Record> boConfList = getBoConfig(dtoName, loginUserObj);
        //查询详细列表数据sql语句
        StringBuilder detailSqlBuilder = new StringBuilder("select ");
        //放入权限列
        detailSqlBuilder.append(getRoleCols(boConfList, domain, "t"));
        //查询总条数数据sql
        StringBuilder totalSqlBuilder = new StringBuilder("select count(1) ");
        //查询总条数
        appendFromSql(totalSqlBuilder, bo, boConfList, paraMap, loginUserObj);
        appendFromSql(detailSqlBuilder, bo, boConfList, paraMap, loginUserObj);
        //分页查询
        offset = CommonTools.isEmpty(offset) ? "1" : offset;
        limit = CommonTools.isEmpty(limit) ? "10" : limit;
        int rows = Integer.parseInt(limit);
        int page = Integer.parseInt(offset) == 0 ? 1 : (Integer.parseInt(offset) / rows + 1); //如果为第一页那么offset为0
        //构建查询语句
        sqlService.buildFilterSql(detailSqlBuilder, page, rows);
        //返回的处理完的列表数据
        List<Record> rowDatas = Db.find(detailSqlBuilder.toString());
        //查询总条数
        long total = Db.queryLong(totalSqlBuilder.toString());
        //构造bootStrapTable的分页数据
        Map<String, Object> reMap = new HashMap<String, Object>();
        reMap.put("total", total);
        reMap.put("rows", rowDatas);
        return reMap;
    }

    private void appendFromSql(StringBuilder sqlBuilder, Record boAndKeyRule,
                               List<Record> boConfList, Map<String, String[]> paraMap,
                               LoginUserObj loginUserObj) {
        sqlBuilder.append(" from ");
        sqlBuilder.append(boAndKeyRule.getStr("tableName"));

        if ("domain".equals(boAndKeyRule.getStr("domain"))) {
            sqlBuilder.append(" t where 1 = 1 ");
        }else{
            sqlBuilder.append(" t left join ").append(paraMap.get("dtoName")[0])
                    .append("_role t1 on t.id = t1.dataId where t1.roleId in (")
                    .append(loginUserObj.getRoles()).append(") ");
        }
        if(CommonTools.isNotEmpty(CommonTools.getValueFromParaMap(paraMap, "id"))){
            sqlBuilder.append(" and ").append(boAndKeyRule.getStr("keyCol"))
                    .append(" like '").append(paraMap.get("id")[0]).append("%' ");
        }
        String searchSql = buildSearchSql(boConfList, paraMap);
        if (CommonTools.isNotEmpty(searchSql)) {
            sqlBuilder.append(searchSql);
        }
    }

    private String buildSearchSql(List<Record> boConfList, Map<String, String[]> paraMap) {
        StringBuilder searchSql = new StringBuilder();
        for (Record r : boConfList) {
            if ("true".equals(r.getStr("search"))) {
                String colName = r.getStr("colName");
                String searchColValue = CommonTools.getValueFromParaMap(paraMap, colName);
                if (CommonTools.isNotEmpty(searchColValue)) {
                    String edittype = r.getStr("edittype");
                    searchSql.append(" and ");
                    //判断字段类型
                    switch (edittype) {
                        //日期型，下拉弄都是相等
                        case "date":
                        case "select":
                        case "radio":
                        case "checkbox":
                            searchSql.append(colName).append(" = '").append(searchColValue).append("'");
                            break;
                        default:
                            searchSql.append(colName).append(" like '%").append(searchColValue).append("%'");
                            break;
                    }
                }
            }
        }
        return searchSql.toString();
    }

    @Override
    public List<Record> getDoDataList(String dtoName, String domain) {
        Record record = BoSql.getDoNameAndBoType(dtoName, domain);
        StringBuilder sqlBuilder = new StringBuilder("select * from ").append(record.getStr("tableName"))
                .append(" where ");
        if("t_sys_role".equals(dtoName)){
            DBServiceFactory.addDomainFilterReverse(sqlBuilder, domain, null);
        }else {
            DBServiceFactory.addDomainFilter(sqlBuilder, domain, null);
        }
        if("tree".equals(record.getStr("boType"))){
            sqlBuilder.append(" order by codeId asc ");
        }else{
            sqlBuilder.append(" order by id asc ");
        }
        return Db.find(sqlBuilder.toString());
    }

    @Override
    public Record getBo(String boName, String domain) {
        return Db.findFirst("select * from t_sys_bo where boName = ? ", boName);
    }

    @Override
    public String getBoRoleCols(String dtoName, LoginUserObj loginUserObj) {
        final ISQLService sqlService = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
        return sqlService.getRoleCols(dtoName, loginUserObj);
    }

    @Override
    public List<Record> getBoConfig(String dtoName, LoginUserObj loginUserObj) {
        return DBServiceFactory.getBoConfigSql(dtoName, loginUserObj.getDomain(), loginUserObj.getRoles());
    }

    @Override
    @Before(Tx.class)
    public boolean save(final BaseTable model) throws Exception {
        //得到缓存中的全局数据库方言
        String dialet = Redis.use("dssm").get("dialet");
        //得到构建数据库语句的service
        final ISQLService sqlService = new DBServiceFactory(dialet).getSQLService();
        return Db.tx(new IAtom() {
            @Override
            public boolean run() throws SQLException {
                //保存业务配置表
                boolean flag = model.save();
                Integer id = model.get("id");
                String tableName = model.getStr("tableName");
                //自动将业务表中的字段数据插入到t_sys_boconfig表中
                int countIns = Db.update(new StringBuilder("insert into t_sys_boconfig(colName, tableName, type, colLen, isKey, showName, pId, boName)")
                        .append(" select colName, tableName, type, colLen, isKey, showName, ")
                        .append(id).append(" as pId,'").append(model.getStr("boName"))
                        .append("' as boName from t_sys_columns where tableName = ?").toString(), tableName);
                //创建表，首先判断表是否创建过
                boolean isExist = sqlService.checkTableIsExist(tableName);
                int countCrt = 0;
                if (!isExist) {
                    //如果表不存在则生成表
                    countCrt = Db.update(getCrtSql(sqlService, tableName));
                    //如果业务类型为tree生成权限表
                    if ("tree".equals(model.getStr("boType"))) {
                        List<String> sqlList = new ArrayList<String>();
                        getCrtRoleTableSql(sqlService, tableName, sqlList);
                        Db.batch(sqlList, sqlList.size());
                    }
                }
                //修改所有字段为已经生成
                int countBuild = Db.update("update t_sys_columns set isBuild = '1' where tableName = ?", tableName);
                return flag && countIns >= 0 && countCrt >= 0 && countBuild >= 0;
            }
        });
    }

    private String getCrtRoleTableSql(ISQLService sqlService, String tableName, List<String> sqlList) {
        StringBuilder crtSql = new StringBuilder("create table ").append(tableName)
                .append("_role (");
        sqlService.buildCrtRoleTableSql(crtSql, sqlList);
        return crtSql.toString();
    }

    /**
     * 创建表
     *
     * @param tableName 表名
     */
    private String getCrtSql(ISQLService sqlService, String tableName) throws SQLException {

        StringBuilder crtSql = new StringBuilder("create table ").append(tableName).append("(");
        //获得基础表结构
        List<Record> baseRecord = Db.find("select * from t_sys_columns where tableName = ?", tableName);
        //构造创建语句
        if (baseRecord == null || baseRecord.size() == 0) {
            throw new SQLException("没有建列字段！");
        }
        sqlService.buildCrtSql(crtSql, baseRecord);
        return crtSql.toString();
    }

    @Override
    public boolean update(BaseTable model) {
        return model.update();
    }

    @Override
    public boolean deleteById(BaseTable model, String[] ids) {
        String dtoName = model.get("dtoName");
        List<String> batchDelList = new ArrayList<String>();
        for (String id : ids) {
            batchDelList.add(new StringBuilder(" delete from ").append(dtoName).append(" where domain ='").append(model.getStr("domain")).append("' and id = ").append(id).toString());
        }
        if ("t_sys_bo".equals(dtoName)) {
            batchDelList.add(new StringBuilder("delete from t_sys_boconfig where pId = '").append(model.getStr("id")).append("'").toString());
        }
        Db.batch(batchDelList, batchDelList.size());
        return true;
    }

    @Override
    public Record getKeyRuleData(BaseTable model) {
        String dtoName = model.getStr("dtoName");
        String domain = model.getStr("domain");
        String boName = model.get("boName");
        StringBuilder sqlBuilder = new StringBuilder("select * from t_sys_keyrule where boName = ? and ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, null );
        Record keyRule = Db.findFirst(sqlBuilder.toString(), boName);
        List<Record> keyRuleConfig = JFinalHelper.getKeyRuleConfig(domain);
        if (keyRule == null) {
            keyRule = new Record();
            keyRule.set("dtoName", dtoName);
            keyRule.set("boName", boName);
            keyRule.set("keyCol", model.get("keyCol"));
            keyRule.set("domain", domain);
            keyRule.set("oper", "add");
        } else {
            keyRule.set("dtoName", dtoName);
            keyRule.set("oper", "edit");
        }
        keyRule.set("keyRuleConfig", keyRuleConfig);
        return keyRule;
    }

    @Override
    public Record getKeyRuleData(String boName, String domain) {
        StringBuilder sqlBuilder = new StringBuilder("select * from t_sys_keyrule where boName = ? and ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, null );
        return Db.findFirst(sqlBuilder.toString(), boName);
    }

    @Override
    public boolean saveKeyRuleConfig(BaseTable model) {
        if ("add".equals(model.getStr("oper"))) {
            //id为自增主键，所以将Id移除，新增时自动增加
            model.remove("id");
            return model.save();
        } else {
            return model.update();
        }
    }

    @Override
    public List<Record> getTreeGridData(String dtoName, String domain) {
        StringBuilder sqlBuilder = new StringBuilder();
        return null;
    }

}
