package edu.dsideal.campus.framework.sql;

/**
 * Created by dsideal-lee on 2017/2/14.
 */
public class RoleSql{
    public static String getInsertRoleSql(String tableName){
        StringBuilder sqlBuilder = new StringBuilder("insert into ");
        sqlBuilder.append(tableName).append("_role (").append("roleId, dataId, crt_time, crt_user) ");
        sqlBuilder.append(" values(?,?,?,?)");
        return sqlBuilder.toString();
    }
}
