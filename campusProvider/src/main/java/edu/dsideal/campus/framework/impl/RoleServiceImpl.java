package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.framework.service.IRoleService;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.framework.service.IRoleService;

/**
 * Created by dsideal-lee on 2016/11/1.
 */
public class RoleServiceImpl implements IRoleService {

    @Override
    public String getRolesByLoginId(String loginId, String domain) {
        ISQLService sqlService = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
        return sqlService.getRolesStr(loginId);
    }
}
