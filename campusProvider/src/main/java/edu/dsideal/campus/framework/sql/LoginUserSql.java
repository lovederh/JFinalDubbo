package edu.dsideal.campus.framework.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;

/**
 * 登陆对象统一处理sql
 * 2017/1/18
 */
public class LoginUserSql {

    /**
     * 通过登陆人账号, 后台查询重要信息
     */
    public static Record findUserRecordByLoginId(String loginId){
        return CommonTools.isEmpty(loginId)? null :
                Db.findFirst(
                    "select tsu.id," +
                    "       tsu.loginId," +
                    "       tsu.userName," +
                    "       tsu.passwd," +
                    "       tsu.orgId," +
                    "       o.orgName," +
                    "       tsu.domain," +
                    "       tsu.email," +
                    "       tsu.cardNo," +
                    "       tsu.qq," +
                    "       tsu.wexin," +
                    "       tsu.phone," +
                    "       tsu.gender," +
                    "       tsu.isUsed," +
                    "       tsf.path," +
                    "       tsf.thumbnailPath " +
                    " from  t_sys_user tsu left join t_sys_file tsf on tsu.headUrlId = tsf.id " +
                    "                      left join t_sys_org o on tsu.orgId = o.id " +//关联查询部门表
                    "where loginId=? ", loginId);
    }
}
