package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IUserService;
import edu.dsideal.campus.common.CommonTools;

import java.util.Map;

/**
 * 2016/11/3 0003
 */
public class UserServiceImpl implements IUserService {
    /**
     * 保存个人资料信息
     * @param paramMap 信息数据Map
     * @return
     */
    @Override
    public String saveMyInfo(Map<String, String> paramMap){
        boolean saveResult = false;//保存成功标记
        String saveMsg = null;//操作完成提示
        String id = paramMap.get("id");
        if(CommonTools.isEmpty(id)){
            saveMsg = "保存处理异常,数据错误!";
        }else{
            Record userInfo = Db.findById("t_sys_user","id",id);
            userInfo.set("userName",paramMap.get("userName"));
            userInfo.set("orgId", CommonTools.isEmpty(paramMap.get("orgId")) ? userInfo.get("orgId"): paramMap.get("orgId"));
            userInfo.set("domain",CommonTools.isEmpty(paramMap.get("domain")) ? userInfo.get("domain"): paramMap.get("domain"));
            userInfo.set("email",paramMap.get("email"));
            userInfo.set("cardNo",paramMap.get("cardNo"));
            userInfo.set("qq",paramMap.get("qq"));
            userInfo.set("wexin",paramMap.get("wexin"));
            userInfo.set("phone",paramMap.get("phone"));
            userInfo.set("gender",paramMap.get("gender"));
            Db.update("t_sys_user", "id", userInfo);
            saveResult = true;
            saveMsg = "恭喜, 个人信息资料已保存!";
        }
        return CommonTools.getMsgJson(saveResult, saveMsg);
    }

    /**
     * 通过loginId查到用户名userName
     * @param loginId
     * @return
     */
    @Override
    public Record findUserNameByLoginId(String loginId){
        return Db.findFirst(
                "select id, loginId, passwd, userName, orgId, domain, email, cardNo, qq, gender, wexin, phone " +
                "from t_sys_user " +
                "where isUsed='1' and loginId=?", loginId);
    }
}
