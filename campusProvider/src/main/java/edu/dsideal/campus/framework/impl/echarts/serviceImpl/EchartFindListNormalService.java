package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartFindListService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartFindListService;

import java.util.List;

public class EchartFindListNormalService implements IEchartFindListService {
	
	/**
	 * 对手工录入的统计sql执行查询操作, 返回Record集合
	 * 功能: 对于sql查询前未做任何处理, 查询后的结果同样未做任何处理
	 */
	@Override
	public List<Record> doEchartsSqlFindList(Record echartRecord){
		// 通过图表项信息拿到已配置的统计sql
		String echartsSql = echartRecord.get("echartsSql");
		// 执行查询后, 返回未做任何处理的Record集合
		return Db.find(echartsSql);
	}
}
