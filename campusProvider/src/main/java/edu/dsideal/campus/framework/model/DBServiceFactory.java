package edu.dsideal.campus.framework.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.common.ConstStatic;

import java.util.List;

/**
 * Created by dsideal-lee on 2016/10/25.
 */
public class DBServiceFactory {
    private static String dialet;
    static ISQLService sqlService;

    public DBServiceFactory(final String dialet) {
        this.dialet = dialet;
    }

    public ISQLService getSQLService() {
        switch (dialet) {
            case "mysql":
                sqlService = new MysqlServiceImpl();
                break;
        }
        return sqlService;
    }

    public static List<Record> getBoConfigSql(String dtoName, String domain, String roles) {
        if ("domain".equals(domain)) {
            return Db.find("select t.id,t.colName,t.showName," +
                            "t.type,t.colLen,t.isKey," +
                            "t.tableName,t.pId,t.hidden," +
                            "t.hidedlg,t.search,t.searchoption," +
                            "t.editable,t.sortable," +
                            "t.title,t.viewable," +
                            "t.editrule,t.edittype," +
                            "t.width,t.align,t.ord," +
                            "t.domain,t.refTable,t.refTableFilter," +
                            "t.dictId,t.dictName," +
                            "t.dictId,t.dictName,t.dictType " +
                            " from t_sys_boconfig t where boName = ?",
                    dtoName);
        }
        StringBuilder sqlBuilder = new StringBuilder("select * from t_sys_boconfig t inner join t_sys_boconfig_role t1 on t.id = t1.dataId " +
                " where t1.roleid in (").append((roles == null ? "''" : roles)).append(") and ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
        sqlBuilder.append(" and boName = ? ");
        return Db.find(sqlBuilder.toString(), dtoName);
    }

    public static void addDomainFilter(StringBuilder sqlBuilder, String domain, String alias) {
        sqlBuilder.append(CommonTools.isEmpty(alias) ? "" : alias + ".").append("domain ");
        if (ConstStatic.ADMIN_DOMAIN.equals(domain)) {
            sqlBuilder.append(" = '").append(domain).append("' ");
        } else {
            sqlBuilder.append(" in ('").append("domain','").append(domain).append("') ");
        }
    }

    public static void addDomainFilterReverse(StringBuilder sqlBuilder, String domain, String alias){

        if (ConstStatic.ADMIN_DOMAIN.equals(domain)) {
            sqlBuilder.append(" 1=1 ");
        } else {
            sqlBuilder.append(CommonTools.isEmpty(alias) ? "" : alias + ".").append("domain ");
            sqlBuilder.append(" = '").append(domain).append("' ");
        }
    }
    public static String getDoNameByBoName(String boName, String domain){
        String sql ="select tableName from t_sys_bo where domain = ? and boName = ?";
        String tableName = Db.queryStr(sql, domain, boName );
        if(CommonTools.isEmpty(tableName)){
            tableName = Db.queryStr(sql, "domain", boName);
        }
        return tableName;
    }
}
