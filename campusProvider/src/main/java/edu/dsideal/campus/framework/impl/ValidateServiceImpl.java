package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.service.IValidateService;

public class ValidateServiceImpl implements IValidateService {

    @Override
    public boolean checkUnique(BaseTable model) {
        //获得传输对象名
        String dtoName = model.get("dtoName");
        //构造查询语句
        StringBuilder sqlSb = new StringBuilder("select count(1) from ").append(dtoName);
        if("t_sys_bo".equals(dtoName)){
            sqlSb.append(" where boName = '").append(model.getStr("boName")).append("'");
        }else if("t_sys_tables".equals(dtoName)){
            sqlSb.append(" where tableName = '").append(model.getStr("tableName")).append("'");
        }else{
            sqlSb.append(" where 1 = 2");
        }
        long count = Db.queryLong(sqlSb.toString());
        if(count == 0){
            return true;
        }
        return false;
    }
}
