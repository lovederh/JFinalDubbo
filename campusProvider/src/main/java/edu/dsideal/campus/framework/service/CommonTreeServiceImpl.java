package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/12/20.
 */
public class CommonTreeServiceImpl implements IRoleTreeInitService{
    @Override
    public List<Record> getRoleTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        return RoleTreeService.getTreeData(paraMap, loginUserObj);
    }
    @Override
    public boolean delRoleData(String tableName, String domain, String roleId) throws SQLException{
        return RoleTreeService.delRoleData(tableName, domain, roleId);
    }
}
