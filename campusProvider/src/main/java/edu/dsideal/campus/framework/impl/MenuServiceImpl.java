package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IMenuService;
import edu.dsideal.campus.framework.common.JFinalHelper;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IMenuService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单管理处理service
 * 2016/10/31 0031
 */
public class MenuServiceImpl implements IMenuService {
    // 处理类使用表名
    private static final String TABLE_NAME = "t_sys_menu";
    // 表单中字段(可以不用包括id)
    private static final String TABLE_FORM_COLUMNS = "menuId,menuName,menuUrl,pId,businessType,isEnabled,relatedBo,orderNo,isLeaf,level";

    /**
     * 根据ID, 按表查询一条记录
     * @param id
     * @return
     */
    @Override
    public Record findMenuDefineById(String id){
        if(CommonTools.isEmpty(id)){
            return null;
        }
        return Db.findById(TABLE_NAME, id);
    }

    /**
     * 根据菜单编码, 删除当前菜单及其子菜单
     * @param delMenuId
     */
    @Override
    public void deleteMenuDefine(String delMenuId) {
        if(CommonTools.isEmpty(delMenuId)){
            return;
        }
        StringBuilder deleteSql = new StringBuilder();
        deleteSql.append("delete from t_sys_menu where menuId like '").append(delMenuId).append("%' ");
        Db.update(deleteSql.toString());
    }

    /**
     * 保存菜单定义方法
     * @return
     */
    @Override
    public String saveMenuDefine(Map<String, String[]> paraMap) {
        boolean operateResult = false;//操作成功标记
        String operateMsg = null;//操作完成提示
        //封装要保存的菜单数据Record
        Record menuRecord = JFinalHelper.searchRecordPutParams(TABLE_FORM_COLUMNS, paraMap, TABLE_NAME);
        if(menuRecord != null){
            Object id = menuRecord.get("id");
            boolean insertFlag = false;
            if(id==null || CommonTools.isEmpty(String.valueOf(id))){
                id = null;
                insertFlag = true;
            }
            // 先校验菜单编码是否重复
            boolean menuIdExisted = JFinalHelper.checkValueExisted(menuRecord.get("menuId"), id, TABLE_NAME, "menuId", null);
            if(menuIdExisted){
                operateMsg = "菜单编码冲突, 请修改!";
            }else{
                // 修改和保存分别调用不同的方法
                if (insertFlag) {
                    Db.save(TABLE_NAME, menuRecord);
                }else {
                    Db.update(TABLE_NAME, menuRecord);
                }
                operateResult = true;
                operateMsg = "恭喜, 菜单信息已保存!";
            }
        }else {
            operateMsg = "保存处理异常, 数据封装错误!";
        }
        return CommonTools.getMsgJson(operateResult, operateMsg);
    }

    /**
     * 查询所有菜单定义集合
     */
    @Override
    public List<Record> findAllMenuDefine(){
        return findMenuDefineByCondition(true);
    }

    /**
     * 查询指定子菜单集合
     * @param nowNodeId 当前节点菜单编码
     * @param nowLevel  当前节点级别
     * @return
     */
    @Override
    public List<Record> findMenuDefineChildren(String nowNodeId, String nowLevel){
        return findMenuDefineByCondition(false, nowNodeId, nowLevel);
    }

    /**
     * 拼接sql, 执行查询菜单定义集合操作
     * @param isFindAll     是否查询全部标识
     * @param nowNodeInfo   当前节点信息数组: 第一项为节点菜单编码; 第二项为节点当前级别
     */
    private List<Record> findMenuDefineByCondition(boolean isFindAll, String... nowNodeInfo){
        // 查询树节点使用的sql
        StringBuilder treeSql = new StringBuilder();
        treeSql.append(
            "select m.id as primaryId," +
            "	    m.menuId," +
            "	    m.menuName," +
            "	    m.pId," +
            "	    m.menuUrl," +
            "	    m.businessType," +
            "	    m.isEnabled," +
            "	    m.relatedBo," +
            "	    m.orderNo," +
            "	    m.level," +
            "	    case m.isLeaf when '1' then 'true' else 'false' end as isLeaf, " +
            "	    'false' as expanded " +
            "  from t_sys_menu m "
        );

        // 菜单树节点数据
        List<Record> treeData = new ArrayList<Record>();
        if(isFindAll){
            // 查询全部则加入虚拟根节点
            Record root = new Record();
            root.set("menuId", 0);
            root.set("menuName", "根节点");
            root.set("isEnabled", "0");
            root.set("level", 0);
            root.set("isLeaf", false);
            root.set("expanded", true);

            treeData.add(root);
        }else{
            // 向下级查询时则加入查询下级过滤条件
            String nowNodeId = nowNodeInfo[0];
            int nowLevel = Integer.parseInt(nowNodeInfo[1]);
            treeSql.append( " where m.level = ").append(nowLevel + 1).append(
                            " and m.menuId like '").append(nowNodeId).append("%' ");
        }
        treeSql.append(" order by m.menuId, m.orderNo ");

        // 查询并构造树列表数据集合
        List<Record> findData = Db.find(treeSql.toString());
        if(findData!=null && !findData.isEmpty()){
            treeData.addAll(findData);
        }
        return treeData;
    }

    /**
     * 根据登录用户查询角色对应的菜单
     * @param loginRoles
     * @return
     */
    @Override
    public List<Map<String, Object>> findRoleMenuList(String loginRoles){
        String commonMenuSql =
            "select t.id, " +
            "       t.menuId, " +
            "       t.menuName, " +
            "       t.menuUrl, " +
            "       t.pId, " +
            "       t.relatedBo " +
            "from   t_sys_menu t " +
            "where  t.isEnabled = '1' " +
            " and   t.level=?  " +
            " and   exists( select 1 " +
            "               from t_sys_menu_role r " +
            "               where t.id = r.dataid and r.roleId in(" + loginRoles + ") )" +
            "order by t.orderNo";
        // 先查询所有一级菜单(带有权限)
        List<Record> firstLevelMenus = Db.find(commonMenuSql, "1");

        //查询所有二级菜单(带有权限)
        List<Record> secondLevelMenus = Db.find(commonMenuSql, "2");

        // 返回结果
        List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();

        String id;
        String menuId;
        String menuName;
        String relatedBo;
        Map<String, Object> menuMap;
        List<Map<String, String>> subList;
        if(firstLevelMenus!=null && !firstLevelMenus.isEmpty()){
            for (Record firstLevelMenu : firstLevelMenus){
                    id = firstLevelMenu.get("id").toString();
                    menuId = firstLevelMenu.getStr("menuId");
                    menuName = firstLevelMenu.getStr("menuName");
                    relatedBo = firstLevelMenu.getStr("relatedBo");

                    menuMap = new HashMap<String, Object>();
                    menuMap.put("menuName", menuName);
                    menuMap.put("menuUrl", genMenuUrl(firstLevelMenu.getStr("menuUrl"), id,relatedBo));

                    if(secondLevelMenus!=null && !secondLevelMenus.isEmpty()) {
                        subList = new ArrayList<Map<String, String>>();

                        String subMenuId;
                        String subMenuName;
                        String subPmenuId;
                        String subRelatedBo;
                        Map<String, String> subMenuMap;
                        for (Record secondLevelMenu : secondLevelMenus) {
                            subPmenuId = secondLevelMenu.getStr("pId");
                            if (!subPmenuId.equals(menuId)) {
                                continue;
                            }
                            // 说明当前菜单节点为该一级菜单的子菜单
                            subMenuId = secondLevelMenu.get("menuId").toString();
                            subMenuName = secondLevelMenu.getStr("menuName");
                            subRelatedBo = secondLevelMenu.getStr("relatedBo");

                            subMenuMap = new HashMap<String, String>();
                            subMenuMap.put("menuName", subMenuName);
                            subMenuMap.put("menuUrl", genMenuUrl(secondLevelMenu.getStr("menuUrl"), subMenuId,subRelatedBo));

                            subList.add(subMenuMap);
                        }
                        // 放入子菜单对象
                        menuMap.put("subMenu",subList);
                    }
                // 把封装好的菜单对象放入结果集合中
                menuList.add(menuMap);
            }
        }
        return menuList;
    }

    /**
     * 关联业务模型
     * @return
     */
    @Override
    public List<Record> getRelatedBoData() {
        List<Record> relatedBoDataList = Db.find("select concat(showName,' (',boName,')') as showName, boName from t_sys_bo ");
        return relatedBoDataList;
    }

    /**
     * 拼接菜单menuUrl
     * @param menuUrl
     * @param id
     * @param relatedBo
     * @return
     */
    private String genMenuUrl(String menuUrl, String id, String relatedBo){
        if(CommonTools.isEmpty(menuUrl) || "#".equals(menuUrl)){
            return menuUrl;
        }
        StringBuilder menuUrlSb = new StringBuilder(menuUrl);
        if(menuUrl.indexOf("?") >= 0){
            menuUrlSb.append("&");
        }else{
            menuUrlSb.append("?");
        }
        menuUrlSb.append("menuId=").append(id).append("&dtoName=").append(relatedBo);
        return menuUrlSb.toString();
    }


}
