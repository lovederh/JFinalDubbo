package edu.dsideal.campus.framework.model;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
public interface ISQLService {
    /**
     * 查询表中id最大值
     * @param sqlBuilder 查询语句构造
     * @param tableName 表名
     * @param keyCol 主键列
     * @param pidCol 父id列
     */
    void getMaxIdSql(StringBuilder sqlBuilder,String tableName, String keyCol, String pidCol);
    /**
     * 创建权限表的sql语句，当一个表为树或者字典表时，需要构造权限表来控制权限
     * @param crtSql 创建语句
     * @param sqlList 语句list
     */
    void buildCrtRoleTableSql(StringBuilder crtSql, List<String> sqlList);

    /**
     * 构造列属性 colName varchar(20) comment '',
     * @param crtSql 创建语句
     * @param r      record
     * @param colType 列类型
     */
    void buildColConfig(StringBuilder crtSql,Record r, String colType);
    /**
     * 创建表的sql语句
     * @param crtSql 创建语句
     * @param recordList record列表
     */
    void buildCrtSql(StringBuilder crtSql, List<Record> recordList);

    /**
     * 检查表是否存在
     * @param tableName
     * @return
     */
    boolean checkTableIsExist(String tableName);

    /**
     * 创建过滤条件的sql
     * @param sqlBuilder
     * @param page
     * @param rows
     */
    void buildFilterSql(StringBuilder sqlBuilder, int page, int rows);

    /**
     * 获得role  例如 1 ，2， 3
     * @param loginId  登陆用户ID
     * @return
     */
    String getRolesStr(String loginId);

    /**
     * 获得登陆用户权限列
     * @param boName        业务模型名称
     * @param loginUserObj  当前登陆用户对象
     * @return
     */
    String getRoleCols(String boName, LoginUserObj loginUserObj);
}
