package edu.dsideal.campus.framework.cache;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;

public class FrameworkCacheManager {
    
    public static void loadCache(){
        setGlobalCache("11", "测试redis已经好用了！");
        setDBDialet();
    }
    public static void setGlobalCache(String key, Object value){
        Cache cache = Redis.use("dssm");
        cache.set(key, value);
    }

    public static void setDBDialet(){
        Prop p = PropKit.getProp("jdbc.properties");
        setGlobalCache("dialet", p.get("dialet"));
    }
}
