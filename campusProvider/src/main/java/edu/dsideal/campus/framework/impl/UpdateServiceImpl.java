package edu.dsideal.campus.framework.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.service.IUpdateService;
import edu.dsideal.campus.framework.update.UpdateUtil;
import edu.dsideal.campus.framework.service.IUpdateService;
import edu.dsideal.campus.framework.update.UpdateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 2016/11/12 0012
 */
public class UpdateServiceImpl implements IUpdateService {
    /**
     * 构造升级模块信息树
     */
    @Override
    public List<Record> initUpdateTree(){
        // 构造虚拟根节点
        Record root = new Record();
        root.set("id", "0");
        root.set("name", "升级模块信息");
        root.set("open", "true");

        StringBuilder updateTreeSql = new StringBuilder();
        updateTreeSql.append(
            "select u.id as id," +
            "       u.showName as name," +
            "       u.pId as parentId," +
            "       'false' as `open`, " +
            "       u.tableName as tableName " +
            "from   t_sys_update u"
        );
        // 查询并构造树列表数据集合
        List<Record> updateData = Db.find(updateTreeSql.toString());
        updateData = updateData==null? new ArrayList<Record>() : updateData;
        updateData.add(0, root);
        return updateData;
    }

    /**
     *  查找升级信息列表
     */
    @Override
    public List<Map<String, String>> findUpdate(String modelName){
        UpdateUtil updateUtil = new UpdateUtil(modelName);
        List<Map<String, String>> updateDataList = updateUtil.findUpdateDataList();
        return updateDataList;
    }

    /**
     *  系统升级按钮功能
     */
    @Override
    public String updateSelectedData(String rows){
        boolean saveResult = false;//保存成功标记
        String jsonMsg = "";
        //解析json数组
        JSONArray jsonArray = JSONArray.parseArray(rows);
        if(jsonArray != null){
            int size = jsonArray.size();
            JSONObject jsonObject = null;
            List<String> sqlList = new ArrayList<String>();
            //循环取出取出sql数据，放在List中，然后批量执行
            for(int i = 0; i< size; i++){
                jsonObject = jsonArray.getJSONObject(i);
                //version是否为最新的
                String isMax = jsonObject.getString("isMax");
                if("true".equals(isMax)){
                //更新数据库的version
                sqlList.add(new StringBuilder()
                        .append("update t_sys_update set version = ")
                        .append(jsonObject.getString("version"))
                        .append(" where tableName = '")
                        .append(jsonObject.getString("tableId")).append("'").toString());
                }
                //取出sql,可能有多条，用;号分隔
                String[] sqls = jsonObject.getString("sql").split(";");
                for(String sql : sqls){
                    if(CommonTools.isNotEmpty(sql)){
                        sqlList.add(sql);
                    }
                }
            }
            if(sqlList.size() > 0){
                // 批量执行sql
                Db.batch(sqlList, sqlList.size());
                saveResult = true;
                jsonMsg = CommonTools.getMsgJson(saveResult, "恭喜你,升级成功!");
            }
        }else{
            jsonMsg = CommonTools.getMsgJson(saveResult, "很抱歉,升级失败!");
        }
        return jsonMsg;
    }
}
