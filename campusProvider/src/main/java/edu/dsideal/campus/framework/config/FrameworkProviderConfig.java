package edu.dsideal.campus.framework.config;


import com.jfinal.config.Routes;
import edu.dsideal.campus.framework.controller.IndexController;
import edu.dsideal.campus.framework.controller.IndexController;

public class FrameworkProviderConfig extends Routes {
    @Override
    public void config() {
        add("/", IndexController.class, "/WEB-INF/view");
    }
}
