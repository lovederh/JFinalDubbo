package edu.dsideal.campus.framework.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * 用户自定义表单Dao
 * Created by Lu on 2017/3/3.
 */
public class FormSql {

    /**
     * 查询自定义表单管理的列表数据
     * @return 自定义表单列表数据
     */
    public static Page<Record> queryForms(int pageNumber, int pageSize, String title){
        // t1.formType = 'user' 表示表单类型为用户自定义的表单类型
        StringBuilder sqlSelect = new StringBuilder(" SELECT id, formName, formDisplay, version, formType, formUrl, dataTable, dataFields, domain, createTime, usedModule ");
        StringBuilder sqlFrom = new StringBuilder(" FROM `t_sys_userform` t1 WHERE t1.formType = 'user' ");

        if (title != null && !"".equals(title.trim())){
            sqlFrom.append(" AND t1.formDisplay LIKE '%").append(title).append("%' ");
        }

        // 返回查询结果
        Page<Record> results = Db.paginate(pageNumber, pageSize, sqlSelect.toString(), sqlFrom.toString());
        return results;
    }

    public static Page<Record> queryFormForEdit(int pageNumber, int pageSize){

        return null;
    }

}
