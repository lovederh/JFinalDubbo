package edu.dsideal.campus.framework.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.model.DBServiceFactory;

import java.util.List;

/**
 * Created by dsideal-lee on 2017/2/7.
 */
public class BoSql {
    /**
     * 返回权限列数据
     *
     * @param boconfig bo列数据
     * @param domain   企业域
     * @param alias    表别名
     * @return alias.col1, alias.col2, alias.col3
     */
    public static String getRoleCols(List<Record> boconfig, String domain, String alias) {
        StringBuilder colBuilder = new StringBuilder();
        if (boconfig != null && boconfig.size() > 0) {
            for (Record r : boconfig) {
                String refTable = r.getStr("refTable");
                String editType = r.getStr("edittype");
                String dictName = r.getStr("dictName");
                if ("select".equals(editType) || "checkbox".equals(editType)) { //如果字段为下拉菜单，查询出下拉菜单关联的表数据
                    String caption = getBoNameCol(refTable, domain);
                    colBuilder.append("(select trt.");
                    if (CommonTools.isEmpty(dictName)) {//如果没有配置显示名称信息
                        colBuilder.append(caption);
                    } else {
                        colBuilder.append(dictName);
                    }
                    colBuilder.append(" from ").append(refTable).append(" trt where ");
                    //关联的表为字典表
                    if ("t_sys_dict".equals(refTable)) {
                        colBuilder.append(" trt.dictId = ").append(CommonTools.isEmpty(alias) ? "" : alias + ".").append(r.getStr("colName"))
                                .append(" and not EXISTS ( select 1 from t_sys_dict tsd where trt.id = tsd.id and tsd.pdictId = 'root') and ");
                    } else {
                        colBuilder.append(" trt.").append(r.getStr("dictId")).append("=")
                                .append(CommonTools.isEmpty(alias) ? "" : alias + ".").append(r.getStr("colName"))
                                .append(" and ");
                    }
                    if(CommonTools.isNotEmpty(r.getStr("refTableFilter"))){
                        colBuilder.append(r.getStr("refTableFilter")).append(" and ");
                    }
                    DBServiceFactory.addDomainFilter(colBuilder, domain, null);
                    colBuilder.append(") as ").append(r.getStr("colName")).append(",");
                } else {
                    colBuilder.append(CommonTools.isEmpty(alias) ? "" : alias + ".").append(r.getStr("colName")).append(",");
                }
            }
            colBuilder.deleteCharAt(colBuilder.length() - 1);
        }
        return colBuilder.toString();
    }

    /**
     * bo表数据和主键规则配置数据
     *
     * @param boName bo名称
     * @param domain 企业域
     * @return Record
     */
    public static Record getBoAndKeyRule(String boName, String domain) {
        StringBuilder sqlBuilder = new StringBuilder("select t.id,t.tableName," +
                "t.boType,t.doAop,t.jsPath," +
                "t.boName,t.showName,t.domain," +
                "t.dataFilter ,t.defaultOrd," +
                " t1.treeNameCol caption," +
                " t1.struct, t1.keyRule," +
                " t1.keyCol  from t_sys_bo t " +
                " inner join t_sys_keyrule t1 on t.boName=t1.boName " +
                " where t.domain = ? and t.boName = ?");
        Record boAndKeyRule = Db.findFirst(sqlBuilder.toString(), domain, boName);
        if (boAndKeyRule == null || boAndKeyRule.getColumnValues() == null) {
            boAndKeyRule = Db.findFirst(sqlBuilder.toString(), ConstStatic.ADMIN_DOMAIN, boName);
        }
        return boAndKeyRule;
    }

    public static String getBoNameCol(String boName, String domain) {
        StringBuilder sqlBuilder = new StringBuilder("select t1.treeNameCol caption from t_sys_bo t " +
                " inner join t_sys_keyrule t1 on t.boName=t1.boName " +
                " where t.domain = ? and t.boName = ?");
        String treeNameCol = Db.queryStr(sqlBuilder.toString(), domain, boName);
        if (CommonTools.isEmpty(treeNameCol)) {
            treeNameCol = Db.queryStr(sqlBuilder.toString(), ConstStatic.ADMIN_DOMAIN, boName);
        }
        return treeNameCol;
    }

    /**
     * 显示表单列的sql语句构造方法
     *
     * @param sqlBuilder   欲构造的sql语句
     * @param loginUserObj 当前登陆用户
     */
    public static void formShowSql(StringBuilder sqlBuilder, LoginUserObj loginUserObj) {
        sqlBuilder.append("select t.id,t.colName,t.showName," +
                "t.type,t.colLen," +
                "t.isKey,t.tableName," +
                "t.pId,t.hidden,t.hidedlg," +
                "t.search,t.searchoption," +
                "t.editable,t.sortable," +
                "t.title,t.viewable," +
                "t.editrule,t.edittype," +
                "t.width,t.align,t.ord," +
                "t.domain,t.refTable," +
                "t.refTableFilter,t.dictId," +
                "t.dictName,t.dictId,t.dictName," +
                "t.dictType  " +
                "from t_sys_boconfig t ");
        //超级管理员
        if ("domain".equals(loginUserObj.getDomain())) {
            sqlBuilder.append(" where t.isUsed = 'true' and boName = ? " +
                    "order by hidedlg desc, ord asc, id asc");
        } else {
            sqlBuilder.append("inner join t_sys_boconfig_role t1 on t.id = t1.dataId " +
                    "where t.isUsed = 'true' ");
            if (loginUserObj.getRoles() != null) {
                sqlBuilder.append(" and t1.roleid in (").append(loginUserObj.getRoles()).append(")");
            }
            sqlBuilder.append(" and t.boName = ? order by t.hidedlg desc, ord asc, id asc");
        }
    }

    public static String getDoName(String boName, String domain) {
        String sql = "select tableName from t_sys_bo where boName = ? and domain = ?";
        String tableName = Db.queryStr(sql, boName, domain);
        if (CommonTools.isEmpty(tableName)) {
            tableName = Db.queryStr(sql, boName, "domain");
        }
        return tableName;
    }

    public static Record getDoNameAndBoType(String boName, String domain) {
        String sql = "select tableName, boType from t_sys_bo where boName = ? and domain = ?";
        Record r = Db.findFirst(sql, boName, domain);
        if (r == null || r.getColumnValues() == null) {
            r = Db.findFirst(sql, boName, "domain");
        }
        return r;
    }
}
