package edu.dsideal.campus.framework.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IFormManageService;
import edu.dsideal.campus.framework.sql.FormSql;

import java.util.List;

/**
 * 自定义表单Service层实现类
 * Created by Lu on 2017/3/3.
 */
public class FormManageServiceImpl implements IFormManageService {

    /**
     * 自定义表单管理列表数据查询
     * @return
     */
    @Override
    public Page<Record> queryForms(int pageNumber, int pageSize, String title) {
        // 表单管理Dao
        Page<Record> results = FormSql.queryForms(pageNumber, pageSize, title);
        return results;
    }

    /**
     * 查询自定义表单 - 编辑定制表单显示
     */
    @Override
    public void queryFormForEdit() {

    }

    /**
     * 保存自定义表单
     */
    @Override
    public void saveForm() {

    }

    /**
     * 查询自定义表单 - 用户录入显示表单
     */
    @Override
    public void queryFormForInput() {

    }

    /**
     * 查询自定义表单 - 用户录入后的表单页面及数据
     */
    @Override
    public void queryInputForm() {

    }
}
