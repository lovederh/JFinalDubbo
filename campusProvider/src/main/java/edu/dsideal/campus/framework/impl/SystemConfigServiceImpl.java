package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.MD5Util;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.service.ISystemConfigService;

import java.math.BigDecimal;
import java.util.List;

/**
 * 2016/11/17 0017
 */
public class SystemConfigServiceImpl implements ISystemConfigService {

    /**
     * 添加系统配置功能
     *
     * @param configId  配置信息ID
     * @param webTitle  网站标签标题
     * @param mainTitle 版权所有信息
     */
    @Override
    public String addConfig(String configId, String webTitle, String mainTitle) {
        boolean flag;
        String msg;
        Record addConfigInfo;
        if (CommonTools.isEmpty(configId)) {
            flag = false;
            msg = "对不起,添加失败,请重新添加!";
        } else {
            addConfigInfo = new Record();
            addConfigInfo.set("id", configId);
            addConfigInfo.set("webTitle", webTitle);
            addConfigInfo.set("mainTitle", mainTitle);
            Db.save("t_sys_systemconfig", addConfigInfo);
            flag = true;
            msg = "恭喜你,添加成功!";
        }
        return CommonTools.getMsgJson(flag, msg);
    }

    /**
     * 添加系统配置功能
     *
     * @param configId  配置信息ID
     * @param webTitle  网站标签标题
     * @param mainTitle 版权所有信息
     */
    @Override
    public String saveConfig(String configId, String webTitle, String mainTitle, String isPass) {
        boolean flag;
        String msg;
        if (CommonTools.isEmpty(configId)) {
            flag = false;
            msg = "对不起,修改失败,请重新修改!";
        } else {
            Record saveConfigInfo = Db.findById("t_sys_systemconfig", configId).set("webTitle", webTitle).set("mainTitle", mainTitle).set("isPass", isPass);
            Db.update("t_sys_systemconfig", saveConfigInfo);
            flag = true;
            msg = "恭喜你,修改成功!";
        }
        return CommonTools.getMsgJson(flag, msg);
    }


    /**
     * 查找系统配置信息
     *
     * @return
     */
    @Override
    public Record findSystemConfigInfo(String domain) {
        // 查询信息配置信息
        Record systemConfigInfo = Db.findFirst(
                "select s.id, " +
                        "		s.mainTitle, " +
                        "		s.webTitle, " +
                        "		s.isPass " +
                        "from   t_sys_systemconfig s LEFT JOIN t_sys_user u ON s.id = u.domain " +
                        "where  s.id=? ", domain);
        return systemConfigInfo;
    }

    /**
     *  项目初始化启动时, 加载系统定制项信息
     * 	先判断是否存在定制项信息, 存在则直接返回
     * 	若不存在, 则插入一条默认数据
     */
    //public static void loadSystemConfigInfo(){
    //    // 判断定制项表中是否存在数据
    //    if(Db.queryLong("select count(*) from t_sys_systemconfig ") > 0){
    //        // 存在则直接返回
    //        return;
    //    }
    //    // 默认的版权所有取学校名称, 用ConstStatic类中默认学校名称
    //    String defaultMainTitle = ConstStatic.DEFAULE_SCHOOL_NAME_CN;
    //
    //    Record systemConfigInfo = new Record();
    //    systemConfigInfo.set("id", 1);
    //    systemConfigInfo.set("mainTitle", defaultMainTitle);
    //    // 使用ConstStatic类中默认网页标题
    //    systemConfigInfo.set("webTitle", ConstStatic.DEFAULE_WEB_PAGE_TITLE);
    //    Db.save("t_sys_systemconfig", systemConfigInfo);
    //}

    /**
     * 查询学校配置信息
     */
    @Override
    public List<Record> queryMembersConfigInfo() {
        return Db.find(
                "select id,mainTitle," +
                        "webTitle,loginID," +
                        "companyName,companyAdd," +
                        "tel,corporation," +
                        "faxNum,emailAdd," +
                        "isPass ,applyTime, passTime " +
                        "from t_sys_systemconfig where delFlag =0 order by applyTime desc ");
    }

    /**
     * 删除配置信息按钮功能
     *
     * @param ids 删除选中行数组
     */
    public String delConfig(String[] ids) {
        String[] configIds = ids[0].split(",");
        for (String config : configIds) {
            Db.update("t_sys_systemconfig", Db.findById("t_sys_systemconfig", config).set("delFlag", "1"));
        }
        return CommonTools.getMsgJson(true, "恭喜你,删除成功!");
    }

    /**
     * 通过按钮功能
     *
     * @param ids 通过选中行数组
     */
    public String passConfig(String[] ids, BaseTable model) {
        String[] configIds = ids[0].split(",");
        int id = Integer.parseInt(model.getStr("id"));
        boolean hasIsPass = false;
        for (String config : configIds) {
            Record systemconfigRecord = Db.findFirst("select isPass from t_sys_systemconfig where id=?", config);
            if (systemconfigRecord != null && systemconfigRecord.getStr("isPass").equals("通过")) {
                hasIsPass = true;
            }
        }

        if (hasIsPass) {
            return CommonTools.getMsgJson(false, "审核失败，包含已通过用户!");
        } else {
            for (String config : configIds) {
                //修改t_sys_systemconfig中的isPass为通过
                Db.update("t_sys_systemconfig", Db.findById("t_sys_systemconfig", config).set("isPass", "通过"));
                //审核成功后向角色表t_sys_role插入一个角色，用单位的名称+角色，例如 长春工业学校角色
                Record companyNameRecord = Db.findFirst("select companyName,loginID,emailAdd,tel from t_sys_systemconfig where id=?", config);
                Record record = new Record();
                record.set("id", id);
                String companyNameNew = companyNameRecord.getStr("companyName") + "角色";
                record.set("roleName", companyNameNew);
                record.set("pId", model.getStr("pId"));
                record.set("level", model.getStr("level"));
                record.set("isLeaf", model.getStr("isLeaf"));
                record.set("ord", model.getStr("ord"));
                record.set("domain", config);
                record.set("expanded", model.getStr("expanded"));
                Db.save("t_sys_role", record);
                //审核成功后向t_sys_user表中插入一个用户（用户名是学校简拼，密码123456用md5加密，企业域等信息都加入）
                Record userRecord = new Record();
                userRecord.set("loginId", companyNameRecord.getStr("loginID"));
                userRecord.set("userName", companyNameRecord.getStr("companyName"));
                userRecord.set("passwd", MD5Util.MD5("123456"));
                userRecord.set("orgId", model.getStr("ord"));
                userRecord.set("domain", config);
                userRecord.set("email", companyNameRecord.getStr("emailAdd"));
                userRecord.set("phone", companyNameRecord.getStr("tel"));
                userRecord.set("isUsed", "1");
                Db.save("t_sys_user", userRecord);
                //通过审核后向t_sys_org表中插入一条数据，orgId找到第一级的最大的+1,名称就是企业和名称，其他信息按规则来
                Record orgRecord = new Record();
                orgRecord.set("orgName", companyNameRecord.getStr("companyName"));
                Record getOrgIdRecord = Db.findFirst("select orgId from t_sys_org where pId=? order by orgId desc limit 1", 0);
                StringBuilder keySb = new StringBuilder();
                if (getOrgIdRecord == null) {
                    keySb.append(model.getStr("pId"));
                    Record keyRecord = Db.findFirst("select struct from t_sys_keyrule where boName=?", "t_dept_org");
                    char[] c = keyRecord.getStr("struct").toCharArray();
                    int keyLen = Integer.parseInt(c[0] + "");
                    for (int i = 0; i < keyLen - 1; i++) {
                        keySb.append("0");
                    }
                    keySb.append("1");
                } else {
                    orgRecord.set("orgId", new BigDecimal(getOrgIdRecord.getStr("orgId")).add(BigDecimal.ONE).toString());
                }
                orgRecord.set("pId", "0");
                orgRecord.set("level", model.getStr("level"));
                orgRecord.set("isLeaf", model.getStr("isLeaf"));
                orgRecord.set("ord", model.getStr("ord"));
                orgRecord.set("ord", model.getStr("ord"));
                orgRecord.set("domain", config);
                orgRecord.set("expanded", model.getStr("expanded"));
                orgRecord.set("delFlag", "0");
                Db.save("t_sys_org", orgRecord);

                //通过成功后向t_sys_user_role插入数据 loginId来源于t_sys_user表的loginID，roleID来源于t_sys_role表的id
                Record user_roleRecord = new Record();
                user_roleRecord.set("roleid", id);
                user_roleRecord.set("loginId", companyNameRecord.getStr("loginID"));
                Db.save("t_sys_user_role", user_roleRecord);
                id++;//选择多行时ID+1
            }
            return CommonTools.getMsgJson(true, "审核通过!");
        }
    }

    /**
     * 通过按钮功能
     *
     * @param ids 通过选中行数组
     */
    public String notPassConfig(String[] ids) {
        String[] configIds = ids[0].split(",");
        for (String config : configIds) {
            Db.update("t_sys_systemconfig", Db.findById("t_sys_systemconfig", config).set("isPass", "不通过"));
        }
        return CommonTools.getMsgJson(true, "审核不通过!");
    }
}
