package edu.dsideal.campus.framework.model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;

/**
 * Created by dsideal-lee on 2016/10/25.
 */
public class MysqlServiceImpl implements ISQLService {
    @Override
    public void getMaxIdSql(StringBuilder sqlBuilder,String tableName, String keyCol, String pidCol) {
        sqlBuilder.append("select ").append(keyCol)
                .append(" from ").append(tableName)
                .append(" where ").append(pidCol).append(" = ?")
                .append(" order by id desc limit 0,1");
    }

    @Override
    public void buildCrtRoleTableSql(StringBuilder crtSql, List<String> sqlList) {
        crtSql.append("id int primary key auto_increment, roleId varchar(64) not null, dataId varchar(64) not null," +
                "r_r varchar(5), r_w varchar(5), r_u varchar(5), r_d varchar(5), crt_time varchar(32), crt_user varchar(64), domain varchar(64) " +
                " )ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ");
        sqlList.add(crtSql.toString());
    }
    @Override
    public void buildColConfig(StringBuilder crtSql,Record r, String colType){
        switch (colType){
            case "varchar":
            case "varchar2":
                crtSql.append(" varchar(").append(r.getStr("colLen")).append(")");
                break;
            case "clob":
                crtSql.append(" longtext ");
                break;
            default:
                //增加字段和类型之间的空格
                crtSql.append(" ").append(colType);
                break;
        }
        //如果是主键就
        if("true".equals(r.get("isKey"))){
            if("int".equals(colType)) {
                crtSql.append(" PRIMARY KEY AUTO_INCREMENT ");
            }else{
                crtSql.append(" PRIMARY KEY ");
            }
        }
        crtSql.append(" COMMENT '").append(r.getStr("showName")).append("'").append(",");
    }
    @Override
    public void buildCrtSql(StringBuilder crtSql, List<Record> recordList) {
        boolean isExistsDomainCol = false;
        //循环构造建表字段
        for(Record r : recordList){
            crtSql.append(r.getStr("colName"));
            //判断字段类型
            String colType = r.getStr("type");
            buildColConfig(crtSql, r, colType);
            if("domain".equals(r.getStr("colName"))){
                isExistsDomainCol = true;
            }
        }
        if(!isExistsDomainCol){
            crtSql.append("domain varchar(64) COMMENT '企业域',");
        }
        crtSql.deleteCharAt(crtSql.length()-1);
        crtSql.append(")ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8");
    }

    @Override
    public boolean checkTableIsExist(String tableName) {
        long count = Db.queryLong("select count(1) from information_schema.tables where table_name = ?", tableName);
        return count == 1;
    }

    @Override
    public void buildFilterSql(StringBuilder sqlBuilder, int page, int rows) {
        sqlBuilder.append(" limit ").append((page - 1) * rows).append(",").append(page * rows);
    }

    @Override
    public String getRolesStr(String loginId) {
        return Db.queryStr(" select group_concat(roleId) from t_sys_user_role where loginId = ?", loginId);
    }

    @Override
    public String getRoleCols(String boName, LoginUserObj loginUserObj) {
        if("domain".equals(loginUserObj.getDomain())){
            return Db.queryStr("select group_concat(t.colName) from t_sys_boconfig t where t.boName = ?", boName);
        }
        StringBuilder sqlBuilder = new StringBuilder("select group_concat(t.colName) from t_sys_boconfig t " +
                " inner join t_sys_boconfig_role t1 on t.id = t1.dataId" +
                " where t1.roleId in (").append(loginUserObj.getRoles());
        DBServiceFactory.addDomainFilter(sqlBuilder, loginUserObj.getDomain(), "t");
        sqlBuilder.append(" and boName = ?");
        return Db.queryStr(sqlBuilder.toString(), boName);
    }

}
