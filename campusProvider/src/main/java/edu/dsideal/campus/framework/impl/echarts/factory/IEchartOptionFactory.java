package edu.dsideal.campus.framework.impl.echarts.factory;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartBarOptionService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartLineOptionService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartPieOptionService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartBarOptionService;

public class IEchartOptionFactory {
	/**
	 * 根据图表类型返回对应类型的属性生成处理类
	 * 	其中:	饼图/南丁格尔图: 使用饼图属性生成器类处理
	 * 		单折线/多折线: 使用折线图属性生成器类处理
	 * 		柱状图(单柱/多柱/横向/纵向): 使用柱状图属性生成器类处理
	 * @param echartRecord	传入record, 便于以后个性化处理
	 * @return
	 */
	public static IEchartOptionService createOptionService(Record echartRecord){
		// 根据属性, 判断分别使用哪个工厂
		String echartsType = echartRecord.get("echartsType");
		IEchartOptionService iOptionService;
		switch (echartsType) {
			// 饼图
			case "1":
			// 南丁格尔图
			case "2":
				// 饼图属性生成器类处理
				iOptionService = new EchartPieOptionService();
				break;
			// 单折线
			case "3":
			// 多折线
			case "4":
				// 折线图属性生成器类处理
				iOptionService = new EchartLineOptionService();
				break;
			default:
				// 柱状图属性生成器类处理
				iOptionService = new EchartBarOptionService();
				break;
		}
		return iOptionService;
	}
	
}
