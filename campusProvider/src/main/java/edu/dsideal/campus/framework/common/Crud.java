package edu.dsideal.campus.framework.common;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.interceptor.MetaObjectIntercept;
import edu.dsideal.campus.framework.sql.BoSql;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.interceptor.MetaObjectIntercept;
import edu.dsideal.campus.framework.sql.BoSql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsideal-lee on 2016/12/7.
 */
public class Crud {

    private static MetaObjectIntercept interceptor;

    @Before(Tx.class)
    public static boolean add(BaseTable model) throws SQLException {
        boolean flag = false;
        //保存前干点啥
        interceptor = getInterceptor(model.getStr("dtoName"));
        if (interceptor != null) {
            interceptor.addBefore(model);
        }
        //保存方法
        flag = model.save();
        //保存后干点啥
        if (interceptor != null) {
            interceptor.addAfter(model);
        }
        return flag;
    }

    @Before(Tx.class)
    public static boolean update(BaseTable model) throws SQLException {
        boolean flag = false;
        //更新前干点啥方法
        interceptor = getInterceptor(model.getStr("dtoName"));
        if (interceptor != null) {
            interceptor.updateBefore(model);
        }
        //更新方法
        flag = model.update();
        //更新后干点啥
        if (interceptor != null) {
            interceptor.updateAfter(model);
        }
        return flag;
    }

    @Before(Tx.class)
    public static boolean deleteById(BaseTable model, String[] ids) throws SQLException {
        //删除前干点啥
        interceptor = getInterceptor(model.getStr("dtoName"));
        if (interceptor != null) {
            interceptor.deleteBefore(model);
        }
        String boName = model.getStr("dtoName");
        String domain = model.getStr("domain");
        List<String> batchDelList = new ArrayList<String>();
        if("t_sys_columns".equals(boName) || "t_sys_tables".equals(boName)){
            for (String id : ids) {
                batchDelList.add(new StringBuilder(" delete from ").append(boName)
                        .append(" where id = '").append(id).append("'").toString());
            }
        }else {
            // 获取基础本信息
            Record boAndRuleConf = BoSql.getBoAndKeyRule(boName, domain);

            //获得选中的列，用逗号分隔

            for (String id : ids) {
                batchDelList.add(new StringBuilder(" delete from ").append(boAndRuleConf.getStr("tableName"))
                        .append(" where id = '").append(id).append("'").toString());
            }
        }
        Db.batch(batchDelList, batchDelList.size());
        //删除后干点啥
        if (interceptor != null) {
            interceptor.deleteAfter(model);
        }
        return true;
    }
    public static MetaObjectIntercept getInterceptor(String dtoName) {
        return CommonTools.initIntercept(Db.queryStr("select doAop from t_sys_bo where boName = ?", dtoName));
    }
}
