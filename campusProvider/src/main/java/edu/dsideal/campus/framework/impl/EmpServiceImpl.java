package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IEmpService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jfinal.plugin.activerecord.Db.find;

/**
 * 2016/12/20 0020
 */
public class EmpServiceImpl implements IEmpService {

    /**
     * 构造组织机构树
     *
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    @Override
    public List<Record> initDeptTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        // 构造组织机构树的关系表--业务表t_dept_org
        String businessTable = CommonTools.getValueFromParaMap(paraMap, "businessTable");
        String domain = loginUserObj.getDomain();
        //获得树的配置信息
        Record boAndRuleConf = Db.findFirst(
                "select t.id, " +
                        "       t.domain, " +
                        "       t.showName, " +
                        "       t.boName, " +
                        "       t.tableName, " +
                        "       t1.treeNameCol caption, " +
                        "       t1.struct, " +
                        "       t1.keyRule," +
                        "       t1.keyCol  " +
                        "from   t_sys_bo t " +
                        "inner  join t_sys_keyrule t1 on t.boName=t1.boName " +
                        "where  t.domain = 'domain' and t.boName=?", businessTable);
        String name = "组织机构信息";
        // 构造虚拟根节点
//        Record root = new Record();
//        root.set("id", "1");

        if (!"domain".equals(domain)) {
            Record domainInfo = Db.findFirst(
                    "select s.mainTitle from t_sys_systemconfig s where s.id =( select u.domain from t_sys_user u where u.loginId=? )",
                    loginUserObj.getLoginId());
            if (domainInfo != null) {
                name = domainInfo.get("mainTitle");
            }
        }
//        root.set("name", name);
//        root.set("open", "true");
//        root.set("level", "0");
//        root.set("isLeaf", "0");
//        root.set("isParent", "true");
        // 获取上面机构业务表的基础表t_sys_org
        String tableName = boAndRuleConf.getStr("tableName");
        // 构造根节点下的叶子节点
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("select t.id as dataId, t.");
        sqlBuilder.append(boAndRuleConf.getStr("caption"));
        sqlBuilder.append(" as name,t.codeId as id,t.pId, t.level, t.isLeaf ");
        sqlBuilder.append(" from ").append(tableName);
        sqlBuilder.append(" t left join ").append(tableName).append("_role t1 on t.id = t1.dataId ");
        sqlBuilder.append(" where t.delFlag ='0' and t.domain = ? ");
        //判断是否有权限控制
        if (ConstStatic.KEY_RULE_CONF_DICT[4].equals(boAndRuleConf.getStr("keyRule"))) {
            sqlBuilder.append(" and t1.roleId in(" + loginUserObj.getRoles() + ")");
        }
        // 查询并构造树列表数据集合
        List<Record> deptData = find(sqlBuilder.toString(), domain);
        deptData = deptData == null ? new ArrayList<Record>() : deptData;
//        deptData.add(0, root);
        return deptData;
    }

    /**
     * 显示选择节点机构的人员信息
     * 查询机构树列表右侧显示相对应的的人员信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Map<String, Object> queryEmpPager(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String orgId = CommonTools.getValueFromParaMap(paraMap, "orgId");
        String domain = loginUserObj.getDomain();
        // 分页信息构造
        String limitStr = CommonTools.getValueFromParaMap(paraMap, "limit");//每页限制数
        String offsetStr = CommonTools.getValueFromParaMap(paraMap, "offset");//起始条数(从0开始计)
        int limit = CommonTools.isNotEmpty(limitStr) ? Integer.parseInt(limitStr) : 10;
        // 构造页数
        int pageNumber = CommonTools.isNotEmpty(offsetStr) ? Integer.parseInt(offsetStr) / limit + 1 : 1;

        String sqlSelect =
                "select e.id," +
                        "       e.empName," +
                        "       e.fileUrlId," +
                        "       (select b.maintitle from t_sys_systemconfig b where b.id = e.domain) domain," +
                        "       (select t.orgName from t_sys_org t where t.id = e.orgId ) orgId, " +
                        "       (select d.dictName from t_sys_dict d where d.dictType='dict_empStatus' and d.pdictId <> 'root' and d.realValue = e.empStatus ) empStatus, " +
                        "       e.delFlag ";

        StringBuilder sqlConditionSb = new StringBuilder();

        sqlConditionSb.append(
                "from   t_sys_emp e " +
                        "left join t_sys_org o on o.id=e.orgId " +
                        "where  e.empStatus ='1' and o.delFlag ='0' and e.delFlag ='0'and e.domain = '" + domain + "' ");
        Object[] sqlParams = new Object[]{};
        if (!"0".equals(orgId)) {
            sqlConditionSb.append(
                    " and e.orgId IN (" +
                            "     select o.id" +
                            "     from    t_sys_org o" +
                            "     left join t_sys_org_role r1 on o.id = r1.dataid" +
                            "     where   o.domain ='" + domain + "' and " +
                            "             r1.roleid in (" + loginUserObj.getRoles() + ") and " +
                            "             o.delflag ='0' and" +
                            "             (o.codeId = ? or o.pid like '" + orgId + "%' )" +
                            ")");
            sqlParams = new Object[]{orgId};
        }

        Page<Record> page = Db.paginate(pageNumber, limit, sqlSelect, sqlConditionSb.toString(), sqlParams);
        // 返回bootstrapTable所支持的分页结果
        Map<String, Object> pageMap = new HashMap<String, Object>();
        pageMap.put("rows", page.getList());
        pageMap.put("total", page.getTotalRow());
        return pageMap;
    }

    /**
     * 人员调动保存方法
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String empMoveSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        boolean flag = false;
        String msg;
        String domain = loginUserObj.getDomain();
        // moveToNewDataId是t_sys_org的ID
        String moveToNewDataId = CommonTools.getValueFromParaMap(paraMap, "moveToNewDataId");
        String empId = CommonTools.getValueFromParaMap(paraMap, "empId");
        // 判断所选择的部门是否是叶子节点---0表示根节点,1表示叶子节点
        String selectOrgIsLeaf = Db.queryStr("select isLeaf from t_sys_org where id=?", moveToNewDataId);
        if ("0".equals(selectOrgIsLeaf)) {
            msg = "该节点还有子节点,请选择最后的叶子节点!";
        } else {
            StringBuilder empMoveSql = new StringBuilder();
            empMoveSql.append(" update t_sys_emp e set e.pcodeId='").append(moveToNewDataId);
            empMoveSql.append("' where e.delFlag = '0' and e.domain='").append(domain);
            empMoveSql.append("' and e.id in (").append(empId).append(")");
            Db.update(empMoveSql.toString());
            flag = true;
            msg = "恭喜你,调动成功!";
        }
        return CommonTools.getMsgJson(flag, msg);
    }

    /**
     * 获取人员状态变更字典信息
     *
     * @return
     */
    @Override
    public List<Record> findEmpStatusDict() {
        List<Record> empStatusDict = Db.find("select dictId,dictName,realValue from t_sys_dict where dictType = 'dict_empStatus' and pdictId <> 'root' ");
        return empStatusDict;
    }

    /**
     * 获取选中人员信息
     *
     * @param loginUserObj
     * @param empIds
     * @return
     */
    @Override
    public List<Record> findEmpInfoList(LoginUserObj loginUserObj, String empIds) {
        List<Record> empInfoList = Db.find("select id,empName,domain,orgId from t_sys_emp where domain=? and id in(" + empIds + ")", loginUserObj.getDomain());
        return empInfoList;
    }

    /**
     * 人员状态变更保存
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String empStatusSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String empIds = CommonTools.getValueFromParaMap(paraMap, "empIds");
        String empStatus = CommonTools.getValueFromParaMap(paraMap, "empStatus");
        Db.update("update t_sys_emp set empStatus = ? where delFlag = '0' and domain = ? and id in (" + empIds + ")", empStatus, loginUserObj.getDomain());
        return CommonTools.getMsgJson(true, "恭喜你,变更成功!");
    }

    /**
     * 添加家庭成员保存方法
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String familyMemberSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String id = CommonTools.getValueFromParaMap(paraMap, "id");
        Record familyMemberInfo;
        if (CommonTools.isNotEmpty(id)) {
            familyMemberInfo = Db.findById("t_sys_familyinfo", id);
        } else {
            familyMemberInfo = new Record();
        }

        familyMemberInfo.set("empId", CommonTools.getValueFromParaMap(paraMap, "empId"));
        familyMemberInfo.set("memberName", CommonTools.getValueFromParaMap(paraMap, "memberName"));
        familyMemberInfo.set("relation", CommonTools.getValueFromParaMap(paraMap, "relation"));
        familyMemberInfo.set("work", CommonTools.getValueFromParaMap(paraMap, "work"));
        familyMemberInfo.set("workUnit", CommonTools.getValueFromParaMap(paraMap, "workUnit"));
        familyMemberInfo.set("phone", CommonTools.getValueFromParaMap(paraMap, "phone"));
        familyMemberInfo.set("domain", domain);

        if (CommonTools.isEmpty(String.valueOf(familyMemberInfo.get("id") + ""))) {
            Db.save("t_sys_familyinfo", familyMemberInfo);
        } else {
            Db.update("t_sys_familyinfo", "id", familyMemberInfo);
        }
        return CommonTools.getMsgJson(true, "恭喜你添加成功!");
    }

    /**
     * 家庭成员信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> familyMemberList(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String empId = CommonTools.getValueFromParaMap(paraMap, "empId");
        String domain = loginUserObj.getDomain();
        StringBuilder familyMemberList = new StringBuilder();
        familyMemberList.append("select id,memberName,relation,work,workUnit,phone from t_sys_familyinfo where empId =? and domain=?");
        List<Record> familyMemberListInfo = find(familyMemberList.toString(), empId, domain);
        return familyMemberListInfo;
    }

    /**
     * 加载家庭成员信息
     *
     * @param loginUserObj
     * @param id
     * @return
     */
    @Override
    public Record loadInfoFamilyMember(LoginUserObj loginUserObj, String id) {
        if (CommonTools.isEmpty(id)) {
            return null;
        }
        return Db.findFirst("select id,memberName,relation,work,workUnit,phone from t_sys_familyinfo where id =? ", id);
    }

    /**
     * 删除家庭成员按钮
     *
     * @param loginUserObj
     * @param ids
     * @return
     */
    @Override
    public String delFamilyMember(LoginUserObj loginUserObj, String ids) {
        String domain = loginUserObj.getDomain();
        StringBuilder delFamilyMember = new StringBuilder();
        delFamilyMember.append("delete from t_sys_familyinfo where id in (").append(ids).append(") and domain = '").append(domain).append("'");
        Db.update(delFamilyMember.toString());
        return CommonTools.getMsgJson(true, "删除成功!");
    }


    /**
     * 添加/编辑学历信息保持方法
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String educationSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String id = CommonTools.getValueFromParaMap(paraMap, "id");
        Record educationInfo;
        if (CommonTools.isNotEmpty(id)) {
            educationInfo = Db.findById("t_sys_educationinfo", id);
        } else {
            educationInfo = new Record();
        }

        educationInfo.set("empId", CommonTools.getValueFromParaMap(paraMap, "empId"));
        educationInfo.set("graduateTime", CommonTools.getValueFromParaMap(paraMap, "graduateTime"));
        educationInfo.set("graduateSchool", CommonTools.getValueFromParaMap(paraMap, "graduateSchool"));
        educationInfo.set("educationBackground", CommonTools.getValueFromParaMap(paraMap, "educationBackground"));
        educationInfo.set("major", CommonTools.getValueFromParaMap(paraMap, "major"));
        educationInfo.set("educationFile", CommonTools.getValueFromParaMap(paraMap, "educationFile"));
        educationInfo.set("domain", domain);

        if (CommonTools.isEmpty(String.valueOf(educationInfo.get("id") + ""))) {
            Db.save("t_sys_educationinfo", educationInfo);
        } else {
            Db.update("t_sys_educationinfo", "id", educationInfo);
        }
        return CommonTools.getMsgJson(true, "恭喜你添加成功!");
    }

    /**
     * 学历信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> educationInfoList(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String empId = CommonTools.getValueFromParaMap(paraMap, "empId");
        String domain = loginUserObj.getDomain();
        StringBuilder educationList = new StringBuilder();
        educationList.append("select id,graduateTime,graduateSchool,educationBackground,major,educationFile,domain from t_sys_educationinfo where empId =? and domain=?");
        List<Record> educationListInfo = find(educationList.toString(), empId, domain);
        return educationListInfo;
    }

    /**
     * 加载学历信息
     *
     * @param loginUserObj
     * @param id
     * @return
     */
    @Override
    public Record loadInfoEducation(LoginUserObj loginUserObj, String id) {
        if (CommonTools.isEmpty(id)) {
            return null;
        }
        return Db.findFirst("select id,graduateTime,graduateSchool,educationBackground,major,educationFile from t_sys_educationinfo where id =? ", id);
    }

    /**
     * 删除学历信息按钮
     *
     * @param loginUserObj
     * @param ids
     * @return
     */
    @Override
    public String delEducation(LoginUserObj loginUserObj, String ids) {
        String domain = loginUserObj.getDomain();
        StringBuilder delEducation = new StringBuilder();
        delEducation.append("delete from t_sys_educationinfo where id in (").append(ids).append(") and domain = '").append(domain).append("'");
        Db.update(delEducation.toString());
        return CommonTools.getMsgJson(true, "删除成功!");
    }


//------------------------------------------------------------------------------------------------------------------------

    /**
     * 添加/编辑学历信息保存方法
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String resumeSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String id = CommonTools.getValueFromParaMap(paraMap, "id");
        Record resumeInfo;
        if (CommonTools.isNotEmpty(id)) {
            resumeInfo = Db.findById("t_sys_resumeinfo", id);
        } else {
            resumeInfo = new Record();
        }

        resumeInfo.set("empId", CommonTools.getValueFromParaMap(paraMap, "empId"));
        resumeInfo.set("workTime", CommonTools.getValueFromParaMap(paraMap, "workTime"));
        resumeInfo.set("workUnit", CommonTools.getValueFromParaMap(paraMap, "workUnit"));
        resumeInfo.set("workDirection", CommonTools.getValueFromParaMap(paraMap, "workDirection"));
        resumeInfo.set("domain", domain);

        if (CommonTools.isEmpty(String.valueOf(resumeInfo.get("id") + ""))) {
            Db.save("t_sys_resumeinfo", resumeInfo);
        } else {
            Db.update("t_sys_resumeinfo", "id", resumeInfo);
        }
        return CommonTools.getMsgJson(true, "恭喜你添加成功!");
    }

    /**
     * 学历信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> resumeInfoList(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String empId = CommonTools.getValueFromParaMap(paraMap, "empId");
        String domain = loginUserObj.getDomain();
        StringBuilder resumList = new StringBuilder();
        resumList.append("select id,workTime,workUnit,workDirection,domain from t_sys_resumeinfo where empId =? and domain=?");
        List<Record> resumeListInfo = find(resumList.toString(), empId, domain);
        return resumeListInfo;
    }

    /**
     * 加载学历信息
     *
     * @param loginUserObj
     * @param id
     * @return
     */
    @Override
    public Record loadInfoResume(LoginUserObj loginUserObj, String id) {
        if (CommonTools.isEmpty(id)) {
            return null;
        }
        return Db.findFirst("select id,workTime,workUnit,workDirection,domain from t_sys_resumeinfo where id =? ", id);
    }

    /**
     * 删除学历信息按钮
     *
     * @param loginUserObj
     * @param ids
     * @return
     */
    @Override
    public String delResume(LoginUserObj loginUserObj, String ids) {
        String domain = loginUserObj.getDomain();
        StringBuilder delResume = new StringBuilder();
        delResume.append("delete from t_sys_resumeinfo where id in (").append(ids).append(") and domain = '").append(domain).append("'");
        Db.update(delResume.toString());
        return CommonTools.getMsgJson(true, "删除成功!");
    }


}
