package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.sql.BoSql;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/12/19.
 */
public class RoleTreeService {
    public static List<Record> getTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String boName = paraMap.get("dtoName")[0];
        String domain = paraMap.get("domain")[0];
        String roleId = CommonTools.getValueFromParaMap(paraMap, "roleId");
        //获得树的配置信息
        Record boAndRuleConf = BoSql.getBoAndKeyRule(boName, domain);
        if(boAndRuleConf == null){
            throw new RuntimeException("没有配置业务模型主键规则！！！");
        }
        String tableName = boAndRuleConf.getStr("tableName");
        //查询权限树节点
        if(ConstStatic.ADMIN_DOMAIN.equals(domain)){
            return Db.find(new StringBuilder("select t.id as `key`, t.").append(boAndRuleConf.getStr("keyCol"))
                    .append(" as id, t.").append(boAndRuleConf.getStr("caption")).append(" as name, t.pId, t.level, t.isLeaf,")
                    .append("case when t1.id is null then 'false' else 'true' end as checked ")
                    .append(" from ").append(tableName).append(" t left join ")
                    .append(tableName).append("_role t1 on t.id = t1.dataId and t1.roleId = '")
                    .append(roleId).append("' order by t.id asc ").toString());
        }
        StringBuilder sqlBuilder = new StringBuilder("select t.id as `key`,t.").append(boAndRuleConf.getStr("keyCol"))
                .append(" as id, t.").append(boAndRuleConf.getStr("caption")).append(" as name, t.pId, t.level, t.isLeaf ")
                .append(" from ").append(tableName)
                .append(" t left join ").append(tableName).append("_role t1 on t.id = t1.dataId ")
                .append(" where ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
        //判断是否有权限控制
        if(ConstStatic.KEY_RULE_CONF_DICT[4].equals(boAndRuleConf.getStr("keyRule"))) {
            sqlBuilder.append(" and t1.roleId in(" + loginUserObj.getRoles() + ")");
        }
        sqlBuilder.append(" order by t.id asc ");
        return Db.find(sqlBuilder.toString());
    }
    public static boolean delRoleData(String tableName, String domain, String roleId) throws SQLException{
        StringBuilder sqlBuilder = new StringBuilder("delete from ");
        sqlBuilder.append(tableName).append("_role where roleId like '").append(roleId).append("%' ");
        return Db.update(sqlBuilder.toString()) >= 0;
    }
}
