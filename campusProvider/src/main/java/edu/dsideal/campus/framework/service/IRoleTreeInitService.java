package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/12/19.
 */
public interface IRoleTreeInitService {
    List<Record> getRoleTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj);
    boolean delRoleData(String tableName, String domain, String roleId) throws SQLException;
}
