package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartOptionService;

import java.util.Map;

public class EchartLineOptionService implements IEchartOptionService {
	/**
	 * 图表类型为折线图时, 拼接option属性字符串的操作
	 * 功能: 	拼接mySettingMap中供option属性使用的关键性字符串
	 * 		最终返回的可供前端页面使用的option属性字符串
	 * 		以字符串形式返回出去
	 */
	@Override
	public String createOptionData(Record echartRecord, Map<String, String> mySettingMap) {
		// 图表标题
		String echartName = echartRecord.get("echartsName");
		
		// 需要的属性配置字符串
		StringBuffer optionSb = new StringBuffer();
		optionSb.append(
			"{" +
			"	title: {" +
			"		text: '").append(echartName).append("'" +
			"	}," +
			"	tooltip: {" +
			"		trigger: 'axis'" +
			"	}," +
			"	legend: {" +
			"		data:[").append(mySettingMap.get("legendData")).append("]" +
			"	}," +
			"	grid: {" +
			"		left: '3%'," +
			"		right: '4%'," +
			"		bottom: '3%'," +
			"		containLabel: true" +
			"	}," +
			"	xAxis: {" +
			"		type: 'category'," +
			"		boundaryGap: false," +
			"		data: [").append(mySettingMap.get("axisData")).append("]" +
			"	}," +
			"	yAxis: {" +
			"		type: 'value'" +
			"	}," +
			"	series: [").append(mySettingMap.get("seriesItem")).append("]" +
			"}"
		);
		return optionSb.toString();
	}
}
