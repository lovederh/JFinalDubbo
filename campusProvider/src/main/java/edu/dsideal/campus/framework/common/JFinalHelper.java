package edu.dsideal.campus.framework.common;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.model.DBServiceFactory;
import edu.dsideal.campus.framework.model.ISQLService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 对数据模型Record做处理的工具类
 * 在JFinal原生处理形式上做一些简单封装, 方便共通处理
 * zhaoyou 2016/11/2
 */
public class JFinalHelper {

    /**
     * 分页查询
     *
     * @param pageNumberStr
     * @param pageSizeStr
     * @param sqlSelect
     * @param sqlExceptSelect
     * @return
     */
    public static Page<Record> searchPaginate(String pageNumberStr, String pageSizeStr, String sqlSelect, String sqlExceptSelect) {
        if (CommonTools.isEmpty(sqlSelect) || CommonTools.isEmpty(sqlExceptSelect)) {
            throw new RuntimeException("分页查询处理异常, sql参数传递错误!");
        }
        int pageNumber = CommonTools.isEmpty(pageNumberStr) ? 1 : Integer.parseInt(pageNumberStr);//当前页码默认1
        int pageSize = CommonTools.isEmpty(pageSizeStr) ? 10 : Integer.parseInt(pageSizeStr);//当前页数据数, 默认10
        return Db.paginate(pageNumber, pageSize, sqlSelect, sqlExceptSelect);
    }

    /**
     * 校验指定表指定字段中是否存在重复值, 返回布尔型
     *
     * @param validateValue 校验的值
     * @param nowId         当前ID值(新增时, 该值为null或-1)
     * @param tableName     指定表
     * @param columnName    指定列
     * @param andCondition  条件过滤语句(以and开头的可用sql语句)
     * @return
     */
    public static boolean checkValueExisted(Object validateValue, Object nowId, String tableName, String columnName, String andCondition) {
        // 构造校验sql
        StringBuffer checkSql = new StringBuffer();
        Object[] paras;
        checkSql.append(" select count(*) from ").append(tableName);
        checkSql.append(" where ").append(columnName).append(" = ? ");
        if (CommonTools.isNotEmpty(andCondition)) {
            checkSql.append(" ").append(andCondition);
        }
        if (nowId != null) {
            checkSql.append(" and id <> ? ");
            paras = new Object[]{validateValue, nowId};
        } else {
            paras = new Object[]{validateValue};
        }
        // 返回校验结果为布尔型
        long count = Db.queryLong(checkSql.toString(), paras);
        return count > 0;
    }

    /**
     * 获取前端表单中指标参数值, 填充对应同字段有效的Record
     * @param formColumns 表单中存在的所有列
     * @param paraMap     前端表单参数
     * @param tableName   表名, 可以传入空值, 空值则跳过根据ID查找Record的操作
     * @return Record     对应同字段有效的Record
     */
    public static Record searchRecordPutParams(String formColumns, Map<String, String[]> paraMap, String... tableName) {
        // 填充的记录
        Record record = null;
        // 找到表单中的主键ID
        String id = CommonTools.getValueFromParaMap(paraMap, "id");
        if (!"".equals(id) && tableName!=null && tableName.length>0 && CommonTools.isNotEmpty(tableName[0])) {
            // 如果条件符合, 则在数据库中找到一条记录
            record = Db.findById(tableName[0], id);
        }
        return CommonTools.putRecordUseParams(formColumns, paraMap, record);
    }

    /**
     * 按表执行批量删除(物理删除)
     *
     * @param tableName
     * @param ids       要删除的ID数组, 可以传入带有逗号的字符串
     */
    public static void deleteRecordByIds(String tableName, String... ids) {
        if (CommonTools.isEmpty(tableName)) {
            throw new RuntimeException("批量删除处理异常, 表名参数传递错误!");
        }
        String idSqlUses = joinStrings2SqlUse(ids);
        if (CommonTools.isEmpty(idSqlUses)) {
            return;
        }
        // 拼接删除语句
        StringBuilder deleteSql = new StringBuilder();
        deleteSql.append(
                " delete from ").append(tableName).append(
                " where id in(").append(idSqlUses).append(")");
        Db.update(deleteSql.toString());
    }

    /**
     * 标记某张表记录中指定字段为指定值, 支持批量操作
     *
     * @param tableName  操作的表名
     * @param columnName 标记的对应列名
     * @param tagValue   本次标记的指定值是什么
     * @param ids        要标记的所有的数据ID的数组, 可以传入带有逗号的字符串
     * @return
     */
    public static void updateTableRows2TagValue(String tableName, String columnName, Object tagValue, String... ids) {
        if (ids == null || ids.length == 0) {
            return;
        }
        String idSqlUses = joinStrings2SqlUse(ids);
        if (CommonTools.isEmpty(idSqlUses)) {
            return;
        }
        // 标记语句拼接
        StringBuilder updateSql = new StringBuilder();
        updateSql.append(" update ").append(tableName).append(" set ").append(columnName).append("=? ");
        updateSql.append(" where id in (").append(idSqlUses).append(")");
        Db.update(updateSql.toString(), tagValue);
    }

    /**
     * 更新某张表记录中指定多个字段为对应的指定值, 支持批量操作, 传入值集合形式
     *
     * @param tableName   操作的表名
     * @param colValueMap 标记的对应列和指定值map集合
     * @param ids         要标记的所有的数据ID的数组, 可以传入带有逗号的字符串
     * @return
     */
    public static void updateTableRows2ColValueMap(String tableName, Map<String, Object> colValueMap, String... ids) {
        if (ids == null || ids.length == 0 || colValueMap.isEmpty()) {
            return;
        }
        String idSqlUses = joinStrings2SqlUse(ids);
        if (CommonTools.isEmpty(idSqlUses)) {
            return;
        }
        // 标记语句拼接
        StringBuilder updateSql = new StringBuilder();
        // 参数集合
        List<Object> valueParas = new ArrayList<Object>();
        updateSql.append("update ").append(tableName).append(" set ");
        for (Map.Entry<String, Object> colValue : colValueMap.entrySet()) {
            updateSql.append(colValue.getKey()).append("=?,");
            valueParas.add(colValue.getValue());
        }
        updateSql.deleteCharAt(updateSql.length() - 1);
        updateSql.append(" where id in (").append(idSqlUses).append(")");
        Db.update(updateSql.toString(), valueParas.toArray());
    }

    /**
     * 根据传入原生字符串, 构造供sql使用的字符串拼接形式(即逗号连接, 单引号分割形式)
     * 传入的可变字符串数组中, 内部字符串同样可以带有逗号, 甚至原本即为sql可用的字符串形式
     * 例: 传入{"m111", "1,2,3", "'xyz','456'"} -->  处理结果: 'm111','1','2','3','xyz','456'
     *
     * @param strNatives
     * @return
     */
    public static String joinStrings2SqlUse(String... strNatives) {
        if (strNatives == null || strNatives.length == 0 || CommonTools.isEmpty(strNatives[0])) {
            return null;
        }
        StringBuilder strSqlUses = new StringBuilder("");
        String strNative;
        for (int i = 0, len = strNatives.length; i < len; i++) {
            if (i > 0) {
                strSqlUses.append(",");
            }
            // 统一使用单引号拼接逗号形式
            strNative = strNatives[i];
            strSqlUses.append("'").append(strNative.replace(",", "','")).append("'");
        }
        return strSqlUses.toString().replace("''", "'");
    }

    /**
     * 得到查询语句的where条件  where 1 = 1 and limit 1, 10 order by id asc
     *
     * @param sqlBuilder
     * @param model
     */
    public static void getFilter(StringBuilder sqlBuilder, BaseTable model) {
        sqlBuilder.append(" ");
        //查询配置的业务模型过滤条件
        Record boRecord = Db.findFirst("select dataFilter, boType from t_sys_bo where domain = ? and boName = ?", model.getStr("domain"), model.getStr("dtoName"));
        String dataFilter = boRecord.getStr("dataFilter"); //过滤条件
        String boType = boRecord.getStr("boType");         //业务模型类型
        //判断是否有数据过滤条件，若有加在前面， 若没有加where 1 = 1
        if (CommonTools.isNotEmpty(boRecord.getStr("dataFilter"))) {
            sqlBuilder.append(dataFilter);
        } else {
            sqlBuilder.append(" where 1 = 1 ");
        }
        if ("true".equals(model.getStr("_search"))) {
            String searchField = model.get("searchField"); //查询字段
            String searchString = model.get("searchField"); //查询值
            String searchOper = model.get("searchField"); //查询操作
            sqlBuilder.append(" and ");
            //构造条件
            CommonTools.buildJqFilter(sqlBuilder, searchOper, searchField, searchString);

        }

        if (!"tree".equals(boType)) { //树表不分页
            int page = model.get("searchField"); //页数
            int rows = model.get("searchField"); //每页条数
            //构造分页
            ISQLService serviceFactory = new DBServiceFactory(Redis.use("dssm").get("dialet")).getSQLService();
            serviceFactory.buildFilterSql(sqlBuilder, page, rows);
        }
        //构造排序
        if (CommonTools.isNotEmpty(boRecord.getStr("defaultOrd"))) {
            sqlBuilder.append(boRecord.getStr("defaultOrd")).append(" ");
            if (CommonTools.isNotEmpty(model.getStr("sidx"))) {
                sqlBuilder.append(model.getStr("sidx")).append(" ");
            }
            sqlBuilder.append(model.getStr("sord"));
        }

    }
    /**
     * 查询主规则配置
     * @param domain
     * @return
     */
    public static List<Record> getKeyRuleConfig(String domain) {
        return Db.find("select id, ruleName from t_sys_ruleconfig where isUsed = 'true' and domain = ?", domain);
    }
}
