package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IBuildingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.String;

/**
 * 2017/2/15 0020
 */
public class BuildingServiceImpl implements IBuildingService {

    /**
     * 构造楼宇树结构
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    @Override
    public List<Record> initBuildingTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {

        // 构造树结构的关系表--t_sys_buildings
        String businessTable = CommonTools.getValueFromParaMap(paraMap, "businessTable");
        String domain = loginUserObj.getDomain();

        //获得树的配置信息
        Record boAndRuleConf = Db.findFirst(
                "select t.id, " +
                        "       t.domain, " +
                        "       t.showName, " +
                        "       t.boName, " +
                        "       t.tableName, " +
                        "       t1.treeNameCol caption, " +
                        "       t1.struct, " +
                        "       t1.keyRule," +
                        "       t1.keyCol  " +
                        "from   t_sys_bo t " +
                        "inner  join t_sys_keyrule t1 on t.boName=t1.boName " +
                        "where  t.domain = 'domain' and t.boName=?", businessTable);
        String name = "楼宇类型";

        // 构造虚拟根节点
        Record root = new Record();
        root.set("id", "0");
        root.set("name", name);
        root.set("open", "true");
        root.set("level", "0");
        root.set("isLeaf", "0");
        root.set("isParent", "true");

        // 获取上面构造的基础表t_sys_buildings
        String tableName = boAndRuleConf.getStr("tableName");

        // 构造根节点下的叶子节点
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("select t.id as dataId, t.");
        sqlBuilder.append(boAndRuleConf.getStr("caption"));
        sqlBuilder.append(" as name,t.codeId as id,t.pId, t.level, t.isLeaf ");
        sqlBuilder.append(" from ").append(tableName);
        sqlBuilder.append(" t left join ").append(tableName).append("_role t1 on t.id = t1.dataId ");
        sqlBuilder.append(" where t.delFlag ='0' and t.domain = ? ");

        //判断是否有权限控制
        if (ConstStatic.KEY_RULE_CONF_DICT[4].equals(boAndRuleConf.getStr("keyRule"))) {
            sqlBuilder.append(" and t1.roleId in(" + loginUserObj.getRoles() + ")");
        }

        // 查询并构造树列表数据集合
        List<Record> deptData = Db.find(sqlBuilder.toString(), domain);
        deptData = deptData == null ? new ArrayList<Record>() : deptData;
        deptData.add(0, root);
        return deptData;
    }

    /**
     * 显示树结构节点的房间信息表
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Map<String, Object> queryBuildingPager(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String buildId = CommonTools.getValueFromParaMap(paraMap, "buildId");
        String domain = loginUserObj.getDomain();

        // 分页信息构
        String limitStr = CommonTools.getValueFromParaMap(paraMap, "limit");//每页限制数
        String offsetStr = CommonTools.getValueFromParaMap(paraMap, "offset");//起始条数(从0开始计)
        int limit = CommonTools.isNotEmpty(limitStr) ? Integer.parseInt(limitStr) : 10;

        // 构造页数
        int pageNumber = CommonTools.isNotEmpty(offsetStr) ? Integer.parseInt(offsetStr) / limit + 1 : 1;
        String sqlSelect =
                "select e.id," +
                        "       e.roomName," +
                        "       e.seatings," +
                        "       e.head," +
                        "       e.note," +
                        "       (select b.maintitle from t_sys_systemconfig b where b.id = e.domain) domain," +
                        "       (select t.buildName from t_sys_buildings t where t.id = e.buildId ) buildId, " +
                        "       (select d.dictName from t_sys_dict d where d.dictType='dict_buildingType' and d.pdictId <> 'root' and d.realValue = e.buildingType ) buildingType, " +
                        "       e.delFlag ";
        StringBuilder sqlConditionSb = new StringBuilder();
        sqlConditionSb.append(
                "from   t_sys_rooms e " +
                        "left join t_sys_buildings o on o.id=e.buildId " +
                        "where  e.buildingType ='12' and  o.delFlag ='0' and e.delFlag ='0'and e.domain = '" + domain + "' ");
        Object[] sqlParams = new Object[]{};
        if (!"0".equals(buildId)) {
            sqlConditionSb.append(
                    " and e.buildId IN (" +
                            "     select o.id" +
                            "     from    t_sys_buildings o" +
                            "     left join t_sys_buildings_role r1 on o.id = r1.dataid" +
                            "     where   o.domain ='" + domain + "' and " +
                            "             r1.roleid in (" + loginUserObj.getRoles() + ") and " +
                            "             o.delflag ='0' and" +
                            "             (o.codeId = ? or o.pid like '" + buildId + "%' )" +
                            ")");
            sqlParams = new Object[]{buildId};
        }

        Page<Record> page = Db.paginate(pageNumber, limit, sqlSelect, sqlConditionSb.toString(), sqlParams);

        // 返回bootstrapTable所支持的分页结果
        Map<String, Object> pageMap = new HashMap<String, Object>();
        pageMap.put("rows", page.getList());
        pageMap.put("total", page.getTotalRow());
        return pageMap;
    }

    /**
     * 编辑/删除分组节点
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String buildingTreeNote(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String opt = CommonTools.getValueFromParaMap(paraMap, "opt");
        String codeId = CommonTools.getValueFromParaMap(paraMap, "codeId");
        String name = CommonTools.getValueFromParaMap(paraMap, "name");
        if ("edit".equals(opt)) {
            Db.update("update t_sys_buildings set buildName=? where codeId=?", name, codeId);
        } else {
           //Db.deleteById("t_sys_buildings",codeId);
            Db.update("delete from t_sys_buildings where codeId=?",codeId);
        }

//        else if("add".equals(opt)){
//            Db.update("insert into t_sys_buildings()");
//        }

        return CommonTools.getMsgJson(true, "操作成功!");
    }

}
