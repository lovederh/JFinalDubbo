package edu.dsideal.campus.framework.impl.echarts.serviceImpl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EchartIterateSinglePieService implements IEchartIterateService{
	/**
	 * 图表类型为饼图/南丁格尔图时, 对查询结果的遍历方式
	 * 功能: 单次遍历, 生成饼图/南丁格尔图option属性中关键的legend中data属性和series中data属性字符串
	 * 		以map形式返回, 为最终的图表option属性拼接做准备
	 */
	@Override
	public Map<String, String> createMySettingData(List<Record> echartDataList, Record echartRecord) {
		// 第一维度取值字段
		String xAxisColumn = echartRecord.get("xAxisColumn");
		// 数字数据取值字段
		String dataColumn = echartRecord.get("dataColumn");
		
		// 导航列表数据对应的字符串(legend中data属性)
		StringBuffer legendDataSb = new StringBuffer("");
		// 内部系列(series中data属性)数据对应字符串
		StringBuffer seriesDataSb = new StringBuffer("");
		
		if(null!=echartDataList && echartDataList.size()>0){
			// 查询结果中x轴配置项对应的汉字信息
			Object xAxisStr;
			// 对应数字数据
			Object dataObj;
			Record echartData;
			for(int i=0,size=echartDataList.size(); i<size ; i++){
				echartData = echartDataList.get(i);
 				xAxisStr = echartData.get(xAxisColumn);
				dataObj = echartData.get(dataColumn);

				if(i>0){
					legendDataSb.append(",");
					seriesDataSb.append(",");
				}
				// 拼接导航条数据格式
				legendDataSb.append("'").append(xAxisStr).append("'");
				// 拼接数据条数据格式
				seriesDataSb.append("{value:");
				if(dataObj!=null){
					seriesDataSb.append(dataObj).append(",");
				}else{
					seriesDataSb.append("0,");
				}
				seriesDataSb.append("name:'").append(xAxisStr).append("'}");
			}
		}
		Map<String, String> mySettingMap = new HashMap<String, String>();
		mySettingMap.put("legendData", legendDataSb.toString());
		mySettingMap.put("seriesData", seriesDataSb.toString());
		return mySettingMap;
	}
}
