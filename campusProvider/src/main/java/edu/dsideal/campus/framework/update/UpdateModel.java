package edu.dsideal.campus.framework.update;

public class UpdateModel {
    protected String modelId;
    protected String version;
    protected String sql;
    protected String loginId;
    protected String isMax;
    
    public UpdateModel(){}
    public UpdateModel(String modelId, String version,  String loginId, String sql){
        this.modelId = modelId;
        this.version = version;
        this.sql = sql;
        this.loginId = loginId;
    }
    public String getModelId() {
        return modelId;
    }
    public void setModelId(String tableId) {
        this.modelId = tableId;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    public String getSql() {
        return sql;
    }
    public void setSql(String sql) {
        this.sql = sql;
    }
    public String getLoginId() {
        return loginId;
    }
    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
    
}
