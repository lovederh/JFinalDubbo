package edu.dsideal.campus.framework.impl.echarts.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public interface IEchartFindListService {
	/**
	 * 通过图表项信息拿到已配置的统计sql, 执行查询操作
	 * 	注: 如需加入个性化处理查询结果, 则加入这个接口的个性化实现类即可
	 * @param echartRecord
	 * @return
	 */
	public abstract List<Record> doEchartsSqlFindList(Record echartRecord);
}
