package edu.dsideal.campus.framework.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.INoticeService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jfinal.plugin.activerecord.Db.find;

public class NoticeServiceImpl implements INoticeService {

    /**
     * 显示通知公告列表分页
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Page<Record> queryNoticePager(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String userName = loginUserObj.getUserName();
        String initiator = CommonTools.getValueFromParaMap(paraMap, "initiator");//发布人
        String publishTimeBefore = CommonTools.getValueFromParaMap(paraMap, "publishTimeBefore");//发布时间前范围
        String publishTimeAfter = CommonTools.getValueFromParaMap(paraMap, "publishTimeAfter");//发布时间后范围
        String rowsStr = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        String pageStr = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        int row = CommonTools.isEmpty(rowsStr) ? 10 : Integer.parseInt(rowsStr);
        int pageNumber = CommonTools.isEmpty(pageStr) ? 1 : Integer.parseInt(pageStr);

        String noticeInfoHead =
                "select t.id," +
                        "       t.titile," +
                        "       t.content," +
                        "       t.publishTime," +
                        "       (select u.userName from t_sys_user u where u.loginId=t.initiator) initiator," +
                        "       t.readNum," +
                        "       t.unReadNum," +
                        "       t.file," +
                        "       t.scope," +
                        "       t.domain, " +
                        "       t.delFlag ";

        String noticeCondition =
                "from   (select  n.id, " +
                        "                n.titile, " +
                        "                n.content, " +
                        "                n.publishTime, " +
                        "                n.initiator, " +
                        "                n.readNum," +
                        "                n.unReadNum, " +
                        "                n.file, " +
                        "                n.scope, " +
                        "                n.domain, " +
                        "                n.delFlag " +
                        "       from   t_sys_notice n " +
                        "       where  n.delFlag = '0' and " +
                        "              n.initiator = '" + loginUserObj.getLoginId() + "' or " +
                        "              n.id in (select nu.noticeid from t_sys_noticeuser nu where  nu.delFlag='0' and nu.loginid = '" + loginUserObj.getLoginId() + "') and " +
                        "              n.domain = '" + domain + "') t where 1=1 ";

        //模糊查询过滤条件
        if (CommonTools.isNotEmpty(initiator)) {
            noticeCondition += " and t.initiator in (select u.loginId from t_sys_user u where u.userName like '%" + initiator + "%')";
        }

        if (CommonTools.isNotEmpty(publishTimeBefore)) {
            noticeCondition += " and t.publishTime >= '" + publishTimeBefore + "' ";
        }

        if (CommonTools.isNotEmpty(publishTimeAfter)) {
            noticeCondition += " and t.publishTime <= '" + publishTimeAfter + "' ";
        }


        return Db.paginate(pageNumber, row, noticeInfoHead, noticeCondition);
    }

    /**
     * 删除通知公告按钮
     *
     * @param loginUserObj
     * @param ids
     * @return
     */
    @Override
    public String delNotice(LoginUserObj loginUserObj, String ids) {
        String domain = loginUserObj.getDomain();
        String loginId = loginUserObj.getLoginId();
        String initiator;
        Record noticeInfo;
        String[] noticeIdStrArray;
        noticeIdStrArray = convertStrToArray(ids);
        for (int i = 0; i < noticeIdStrArray.length; i++) {
            noticeInfo = Db.findFirst("select n.initiator from t_sys_notice n where n.id='" + noticeIdStrArray[i] + "'");
            initiator = noticeInfo.getStr("initiator");
            //判断要删除的通知公告的发布人是否是当前登录人;
            //1.如果是 -- 删除t_sys_notice中的通知公告,同时也要删除通知公告人员关系表t_sys_noticeuser中对应该删除通知公告的数据;
            //2.如果不是 -- 保留t_sys_notice中的通知公告,但是要删除掉通知公告人员关系表t_sys_noticeuser中对满足该登录用户的loginId和要删除的noticeId匹配的数据;
            if (initiator.equals(loginId)) {
                Db.update("update t_sys_notice set delFlag = '1' where id ='" + noticeIdStrArray[i] + "' and domain = '" + domain + "'");
                Db.update("update t_sys_noticeuser set delFlag = '1' where noticeId='" + noticeIdStrArray[i] + "' ");
            } else {
                Db.update("update t_sys_noticeuser set delFlag = '1' where noticeId='" + noticeIdStrArray[i] + "' and loginId='" + loginId + "'");
            }
        }
        return CommonTools.getMsgJson(true, "删除成功!");
    }

    //使用String的split 方法
    public static String[] convertStrToArray(String str) {
        String[] strArray = null;
        strArray = str.split(","); //拆分字符为"," ,然后把结果交给数组strArray
        return strArray;
    }

    /**
     * 保存通告
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String saveNotice(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String loginId = loginUserObj.getLoginId();
        String scopeFlag = CommonTools.getValueFromParaMap(paraMap, "scopeFlag");
        String pickUsersStr = CommonTools.getValueFromParaMap(paraMap, "pickUsersStr");
        String titile = CommonTools.getValueFromParaMap(paraMap, "titile");
        String content = CommonTools.getValueFromParaMap(paraMap, "content");
        String fileArrayIds = CommonTools.getValueFromParaMap(paraMap, "fileArrayIds");
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
        String publishTime = dateFormat.format(now);

        // 保存通知公告
        Record saveNotice = new Record().set("initiator", loginId).set("titile", titile).set("scope", scopeFlag).set("publishTime", publishTime).set("content", content).set("domain", domain);
        Db.save("t_sys_notice", saveNotice);
        int noticeId = Db.queryInt("select id from t_sys_notice where delFlag ='0' and initiator=? and titile=? and domain=?", loginId, titile, domain);

        // 添加已发送的通知公告和接收人员的对应表
        String noticeUserSql =
                "insert into t_sys_noticeuser(loginId,noticeId,domain)" +
                        "           (select u.loginId, " +
                        "                   '" + noticeId + "' as  noticeId," +
                        "                   u.domain" +
                        "            from t_sys_user u " +
                        "           where ";
        if (!"1".equals(scopeFlag)) {
            noticeUserSql += " u.id in(" + pickUsersStr + ") and ";
        }
        noticeUserSql += " u.domain ='" + domain + "' and u.isUsed = '1' and u.userType is not null and u.loginId <> '" + loginUserObj.getLoginId() + "')";
        Db.update(noticeUserSql);

        // 接受通知公告的人数
        String sendCountNum = "select count(*) from t_sys_noticeuser n where n.noticeid='" + noticeId + "'";

        long sendSelectedUserCount = Db.queryLong(sendCountNum);

        // 已读通知公告的人数
        long isReadUserCount = Db.queryLong(sendCountNum + " and n.isRead='1'");

        //未读通知公告的人数
        long unReadUserCount = sendSelectedUserCount - isReadUserCount;

        Db.update("update t_sys_notice set readNum='" + isReadUserCount + "',unReadNum='" + unReadUserCount + "' where id='" + noticeId + "'");


        // 添加通知公告和文件关系的处理
        String noticeFileSql;
        if (CommonTools.isNotEmpty(fileArrayIds)) {
            String[] fileIds = fileArrayIds.split(",");
            for (String fileId : fileIds) {
                noticeFileSql = "insert into t_sys_noticefile(noticeId,fileId,domain)" +
                        "                            (select n.id," +
                        "                                    '" + fileId + "' as  fileId," +
                        "                                    n.domain " +
                        "                             from t_sys_notice n " +

                        "                             where n.delFlag='0' and id='" + noticeId + "')";
                Db.update(noticeFileSql);
            }
        }

        return CommonTools.getMsgJson(true, "发布成功!");
    }

    /**
     * 显示选择节点机构的人员信息
     * 查询机构树列表右侧显示相对应的的人员信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> queryTeacherData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String orgId = CommonTools.getValueFromParaMap(paraMap, "id");
        String searchTeacherInput = CommonTools.getValueFromParaMap(paraMap, "searchInput");
        String domain = loginUserObj.getDomain();

        StringBuilder queryTeacherSql = new StringBuilder();
        queryTeacherSql.append(
                "select u.id id," +
                        "       u.loginId loginId, " +
                        "       u.userName userName, " +
                        "       u.phone phone, " +
                        "       (select t.orgName from t_sys_org t where t.codeId = u.orgId and t.delFlag ='0' and t.domain='" + domain + "') orgId, " +
                        "       u.userType userType ");

        queryTeacherSql.append(
                "from   t_sys_user u " +
                        "left join t_sys_org o on o.codeId = u.orgId " +
                        "where u.isUsed='1' and u.userType ='1' and u.domain = '" + domain + "' ");

        if (CommonTools.isNotEmpty(orgId)) {
            queryTeacherSql.append(" and (o.codeId='" + orgId + "'or o.pid like '" + orgId + "%')");
        }

        if (CommonTools.isNotEmpty(searchTeacherInput)) {
            queryTeacherSql.append(" and (u.loginId like '%" + searchTeacherInput + "%' or u.userName like '%" + searchTeacherInput + "%' or u.phone like '%" + searchTeacherInput + "%' ");
            String orgIdStr = Db.queryStr("select group_concat(o.codeId) as orgid from t_sys_org o where o.isleaf='1' and o.orgname like '%" + searchTeacherInput + "%'");
            queryTeacherSql.append(" or u.orgId in (" + orgIdStr + "))");
        }

        List<Record> queryTeacher = Db.find(queryTeacherSql.toString());

        return queryTeacher;
    }

    /**
     * 查询已选择的人员列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Page<Record> querySelectedStaff(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String pickUsersStr = CommonTools.getValueFromParaMap(paraMap, "pickUsersStr"); //已选中的人员ID
        if ("undefined".equals(pickUsersStr)) {
            pickUsersStr = "''";
        } else {
            pickUsersStr = CommonTools.isEmpty(pickUsersStr) == true ? "''" : pickUsersStr;
        }
        String rowsStr = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        String pageStr = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        int row = CommonTools.isEmpty(rowsStr) ? 10 : Integer.parseInt(rowsStr);
        int pageNumber = CommonTools.isEmpty(pageStr) ? 1 : Integer.parseInt(pageStr);

        String querySelectedHead = "select u.id,u.userName,u.phone,(select t.orgName from t_sys_org t where t.orgId = u.orgId and t.delFlag ='0' and t.domain='" + domain + "' ) orgId ";
        String querySelectedCondition = " from t_sys_user u where u.isUsed='1' and u.domain= '" + domain + "'  and u.id in (" + pickUsersStr + ") ";

        return Db.paginate(pageNumber, row, querySelectedHead, querySelectedCondition);
    }

    /**
     * 查询分组下的选择人员生成树结构
     *
     * @param tableName
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    @Override
    public List<Record> groupTree(String tableName, Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String domain = loginUserObj.getDomain();
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(
                "select g.id as id," +
                        "       g.groupName as name," +
                        "       g.pId," +
                        "       g.level," +
                        "       g.isLeaf " +
                        "from " + tableName + " g " +
                        "where g.domain='" + domain + "'"
        );
        List<Record> groupData = find(sqlBuilder.toString());
        groupData = groupData == null ? new ArrayList<Record>() : groupData;
        return groupData;
    }

    /**
     * 查询行政部门下的教师生成树结构
     *
     * @param tableName
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    @Override
    public List<Record> teacherTree(String tableName, Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String domain = loginUserObj.getDomain();
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("select t.id as dataId, t.orgName as name,t.codeId as id,t.pId, t.level, t.isLeaf ");
        sqlBuilder.append(" from ").append(tableName).append(" t ");
        //sqlBuilder.append(" left join ").append(tableName).append("_role t1 on t.id = t1.dataId ");
        sqlBuilder.append(" where t.delFlag ='0' and t.domain = ? ");
        List<Record> dataList = find(sqlBuilder.toString(), domain);
        dataList = dataList == null ? new ArrayList<Record>() : dataList;
        return dataList;
    }

    /**
     * 查询专业班级下的学生生成树结构
     *
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    @Override
    public List<Record> studentTree(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        return null;
    }

    /**
     * 添加分组
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Object addGroup(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String pId = CommonTools.getValueFromParaMap(paraMap, "pId");
        String name = CommonTools.getValueFromParaMap(paraMap, "name");
        String domain = loginUserObj.getDomain();
        Record addGroupRecord = new Record().set("pId", pId).set("groupName", name).set("domain", domain).set("level", "1").set("isLeaf", "1");
        Db.save("t_sys_group", addGroupRecord);
        return addGroupRecord.get("id");
    }

    /**
     * 编辑/删除分组节点
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String editGroupNote(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String opt = CommonTools.getValueFromParaMap(paraMap, "opt");
        String id = CommonTools.getValueFromParaMap(paraMap, "id");
        String name = CommonTools.getValueFromParaMap(paraMap, "name");
        if ("edit".equals(opt)) {
            Db.update("update t_sys_group set groupName=? where id=?", name, id);
        } else {
            Db.deleteById("t_sys_group", id);
        }
        return CommonTools.getMsgJson(true, "操作成功!");
    }

    /**
     * 添加分组人员
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String addGroupUser(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String groupUserSelected = CommonTools.getValueFromParaMap(paraMap, "groupUserSelected");
        String groupId = CommonTools.getValueFromParaMap(paraMap, "groupId");
        String domain = loginUserObj.getDomain();
        String groupUserSql =
                "insert into t_sys_groupuser " +
                        "(userId,loginId,groupId,domain)" +
                        "(select t.id as userId,t.loginId as loginId,'" + groupId + "' as groupId,'" + domain + "' as domain " +
                        " from t_sys_user t" +
                        " where id in (" + groupUserSelected + "))";
        Db.update(groupUserSql);
        return CommonTools.getMsgJson(true, "添加成功!");
    }


    /**
     * 查询分组人员信息
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> queryGroupData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String groupId = CommonTools.getValueFromParaMap(paraMap, "id"); //已选中的人员ID

        List<Record> queryGroupUser = Db.find(
                "select u.id," +
                        "       u.userName," +
                        "       u.phone," +
                        "       (select t.orgName from t_sys_org t where t.codeId = u.orgId and t.delFlag ='0' and t.domain='" + domain + "' ) orgId   " +
                        "from   t_sys_user u " +
                        "where  u.isUsed='1' and " +
                        "       u.domain= '" + domain + "' and " +
                        "       u.id in(select g.userId from t_sys_groupuser g where g.groupId ='" + groupId + "' )");

        return queryGroupUser;
    }

    /**
     * 查看通知公告信息
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public Map<String, Object> viewNotice(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String id = CommonTools.getValueFromParaMap(paraMap, "id");

        //通知公告数据
        Record noticeInfo = Db.findById("t_sys_notice", id);
        //附件列表数据
        List<Record> fileList = Db.find(
                "select f.id,f.realName,f.path,f.thumbnailPath " +
                        "from t_sys_file f " +
                        "where f.id in (select fileId from t_sys_noticeFile where noticeId=?)", id
        );

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("fileList", fileList);
        map.put("notice", noticeInfo);

        return map;
    }

    /**
     * 保存分组成员
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public List<Record> saveGroupMember(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String domain = loginUserObj.getDomain();
        String rightMemberId = CommonTools.getValueFromParaMap(paraMap, "rightMemberId");
        String selectedGroupId = CommonTools.getValueFromParaMap(paraMap, "selectedGroupId");

        long groupUserNum = Db.queryLong("select count(*) from t_sys_groupuser g where g.groupid='" + selectedGroupId + "'");
        String groupUserSql;
        if (groupUserNum > 0) {
            Db.update("delete from t_sys_groupuser where groupId='" + selectedGroupId + "'");
        }

        groupUserSql =
                "insert into t_sys_groupuser " +
                        "(userId,loginId,groupId,domain)" +
                        "(select t.id as userId,t.loginId as loginId,'" + selectedGroupId + "' as groupId,'" + domain + "' as domain " +
                        " from t_sys_user t" +
                        " where id in (" + rightMemberId + "))";
        Db.update(groupUserSql);

        List<Record> groupUser = Db.find(
                "select u.id," +
                        "       u.userName," +
                        "       u.phone," +
                        "       (select t.orgName from t_sys_org t where t.codeId = u.orgId and t.delFlag ='0' and t.domain='" + domain + "' ) orgId  " +
                        "from   t_sys_user u " +
                        "where  u.isUsed='1' and u.domain= '" + domain + "' and " +
                        "       u.id in(select g.userId from t_sys_groupuser g where g.groupId ='" + selectedGroupId + "' )");

        return groupUser;
    }

    /**
     * 接收通知公告人员是否人数处理
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    @Override
    public String receiveNoticeUserNum(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String noticeId = CommonTools.getValueFromParaMap(paraMap, "noticeId");
        String loginId = loginUserObj.getLoginId();
        Db.update("update t_sys_noticeuser set isRead ='1' where noticeId='" + noticeId + "' and loginId='" + loginId + "'");

        // 接受通知公告的人数
        String sendCountNum = "select count(*) from t_sys_noticeuser n where n.noticeid='" + noticeId + "'";

        long sendSelectedUserCount = Db.queryLong(sendCountNum);

        // 已读通知公告的人数
        long isReadUserCount = Db.queryLong(sendCountNum + " and n.isRead='1'");

        //未读通知公告的人数
        long unReadUserCount = sendSelectedUserCount - isReadUserCount;

        Db.update("update t_sys_notice set readNum='" + isReadUserCount + "',unReadNum='" + unReadUserCount + "' where id='" + noticeId + "'");

        return CommonTools.getMsgJson(true, "添加成功!");
    }

}
