package edu.dsideal.campus.framework.impl.echarts.factory;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateDoubleService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateSinglePieService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateSingleService;
import edu.dsideal.campus.framework.impl.echarts.service.IEchartIterateService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateDoubleService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateSinglePieService;
import edu.dsideal.campus.framework.impl.echarts.serviceImpl.EchartIterateSingleService;

public class IEchartIterateFactory {
	/**
	 * 根据图表类型返回对查询数据遍历方式的处理类工厂
	 * 	其中:	多折线/多柱柱图(涉及多维度), 需要双遍历处理
	 * 		饼图/南丁格尔图: 单遍历处理(饼状图处理方式)
	 * 		其它: 一次遍历即可处理完成(折线/柱图处理方式)
	 * @param echartRecord	传入record, 便于以后个性化处理
	 * @return
	 */
	public static IEchartIterateService createIterateService(Record echartRecord){
		// 根据属性, 判断分别使用哪个实现类实现接口
		String echartsType = echartRecord.get("echartsType");
		IEchartIterateService iIterateService;
		switch (echartsType) {
			// 饼图
			case "1":
			// 南丁格尔图
			case "2":
				// 单遍历处理查询数据(饼状图处理方式)
				iIterateService = new EchartIterateSinglePieService();
				break;
			// 多折线
			case "4":
			// 多柱柱图（竖向）
			case "6":
			// 多柱柱图（横向）
			case "8":
				// 双遍历处理查询数据
				iIterateService = new EchartIterateDoubleService();
				break;
			default:
				// 单遍历处理查询数据(单折线/单柱图处理方式)
				iIterateService = new EchartIterateSingleService();
				break;
		}
		return iIterateService;
	}
}
