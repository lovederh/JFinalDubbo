package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.model.DBServiceFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/12/22.
 */
public class MenuBtnTreeServiceImpl implements IRoleTreeInitService {
    private final static String TABLE_NAME = "t_sys_menubtn";
    @Override
    public List<Record> getRoleTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj) {
        String domain = paraMap.get("domain")[0];
        String roleId = paraMap.get("roleId")[0];
        StringBuilder sqlBuilder = new StringBuilder("SELECT t2.id as `key`,t2.menuId as id," +
                "t2.menuName AS name," +
                "t2.pId," +
                "t2.level," +
                "t2.isLeaf," +
                " 'true' as nocheck,"+
                "'false' as checked " +
                " from t_sys_menu t2 where  ");
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t2");
        sqlBuilder.append(" union all " +
                " SELECT t.id as `key`,t.id as id," +
                "t3.btnName AS name," +
                "t.menuId as pId," +
                "t.level, t.isLeaf," +
                " 'false' as nocheck,"+
                "CASE WHEN t1.id IS NULL THEN 'false' ELSE 'true' END AS checked " +
                " FROM t_sys_menubtn t " +
                " LEFT JOIN t_sys_menubtn_role t1 ON t.id = t1.dataId and t1.roleid = '").append(roleId)
                .append("' inner join t_sys_buttons t3 on t3.id = t.btnId ")
                .append(" inner join t_sys_menu t2 on t2.menuId = t.menuId where ");
        //查询权限树节点
        DBServiceFactory.addDomainFilter(sqlBuilder, domain, "t");
        sqlBuilder.append(" order by id asc ");
        return Db.find(sqlBuilder.toString());
    }
    @Override
    public boolean delRoleData(String tableName, String domain, String roleId) throws SQLException {
        return RoleTreeService.delRoleData(TABLE_NAME, domain, roleId);
    }
}
