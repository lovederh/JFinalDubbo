package edu.dsideal.campus.framework.impl.echarts.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.Map;

public interface IEchartOptionService {
	/**
	 * 依照图表类型, 拼接mySettingMap中供option属性使用的关键性字符串
	 * 最终返回最终的可供前端页面使用的option属性字符串
	 * 可以通过实现该接口完成查询结果处理方式的扩展
	 * @param echartRecord
	 * @param mySettingMap
	 * @return
	 */
	public abstract String createOptionData(Record echartRecord, Map<String, String> mySettingMap);
}
