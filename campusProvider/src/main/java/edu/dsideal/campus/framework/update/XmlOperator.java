package edu.dsideal.campus.framework.update;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class XmlOperator {
    /**
     * 读取xml文件
     * 
     * @return Document
     */
    public static Document readXml(String filePath) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document xmldoc = null;
        try {
            db = factory.newDocumentBuilder();
            xmldoc = db.parse(new File(filePath));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xmldoc;
    }

    /**
     * 保存xml
     * 
     * @param fileName 文件名称包含文件路径
     */
    public static void saveXml(String fileName, Document xmldoc) {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("indent", "yes");
            DOMSource source = new DOMSource();
            source.setNode(xmldoc);
            StreamResult result = new StreamResult();
            result.setOutputStream(new FileOutputStream(fileName));
            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建当前版本的xml节点
     * 
     * @param xmldoc xml document
     * @param updateModel 更新模型
     */
    protected static void createVerElement(Document xmldoc, UpdateModel updateModel) {
        Element root = null, table = null, tableId = null, version = null, loginId = null;
        Element sql = null;
        root = xmldoc.getDocumentElement();
        table = xmldoc.createElement("table");
        // 创建tableId节点
        tableId = xmldoc.createElement("tableId");
        tableId.setTextContent(updateModel.getModelId());
        table.appendChild(tableId);
        // 创建版本号节点
        version = xmldoc.createElement("version");
        version.setTextContent(updateModel.getVersion());
        table.appendChild(version);
        // 创建登陆用户节点
        loginId = xmldoc.createElement("loginId");
        loginId.setTextContent(updateModel.getLoginId());
        table.appendChild(loginId);
        // 创建语句节点
        CDATASection cdata = xmldoc.createCDATASection(updateModel.getSql());
        sql = xmldoc.createElement("sql");
        sql.appendChild(cdata);
        table.appendChild(sql);
        
        root.appendChild(table);
    }

    /**
     * 将xml转为固定的json数据
     * 
     * @param rootNode 查到的NodeList 集合
     * @return String [{},{}]
     */
    private static String xml2Json(NodeList rootNode, String dbver) {
        StringBuilder sb = new StringBuilder("[");
        // 拿到nodeList后，最多只有二级，所以进行二次循环
        for (int i = 0, count = rootNode.getLength(); i < count; i++) {
            NodeList jNodes = rootNode.item(i).getChildNodes();
            for (int j = 0; j < jNodes.getLength(); j++) {
                switch (jNodes.item(j).getNodeName()) {
                    case "tableId":
                        sb.append("{\"tableId\":\"").append(jNodes.item(j).getTextContent())
                                .append("\",");
                        break;
                    case "version":
                        sb.append("\"version\":\"").append(jNodes.item(j).getTextContent())
                                .append("\",");
                        break;
                    case "loginId":
                        sb.append("\"loginId\":\"").append(jNodes.item(j).getTextContent())
                                .append("\",");
                        break;
                    case "sql":
                        sb.append("\"sql\":\"").append(jNodes.item(j).getTextContent())
                                .append("\"},");
                        break;
                    default:
                        break;
                }
            }
        }
        // 删除最后一个逗号
        sb.delete(sb.length() - 1, sb.length()).append("]");
        System.out.println(sb.toString());
        return sb.toString();
    }

    protected static void output(Node node) {// 将node的XML字符串输出到控制台
        TransformerFactory transFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transFactory.newTransformer();
            transformer.setOutputProperty("encoding", "utf-8");
            transformer.setOutputProperty("indent", "yes");
            DOMSource source = new DOMSource();
            source.setNode(node);
            StreamResult result = new StreamResult();
            result.setOutputStream(System.out);

            transformer.transform(source, result);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查找单个节点
     * 
     * @param express
     * @param source
     * @return
     */
    public static Node selectSingleNode(String express, Object source) {// 查找节点，并返回第一个符合条件节点
        Node result = null;
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (Node) xpath.evaluate(express, source, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 找到要更新的节点
     * 
     * @param express dom4j xpath的过滤表达式
     * @param source xmldoc的root节点
     * @return NodeList
     */
    public static NodeList selectNodes(String express, Object source) {// 查找节点，返回符合条件的节点集。
        NodeList result = null;
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        try {
            result = (NodeList) xpath.evaluate(express, source, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return result;
    }
    public static NodeList getUpdateNode(String dbVersion, String filePath){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document xmldoc = null;
        NodeList someNodes = null;
        try {
            db = factory.newDocumentBuilder();
            xmldoc = db.parse(new File(filePath));
            Element root = xmldoc.getDocumentElement();
            // 查找/tables/table[version>数据库的version]
            someNodes = XmlOperator.selectNodes(new StringBuilder("/tables/table").append("[version>").append(dbVersion).append("]").toString(), root);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return someNodes;
    }
    /**
     * 查询版本号高于数据库的版本的更新语句
     * @return NodeList
     */
    public static String getUpdateJsonData(String dbVersion, String filePath) {
        return xml2Json(getUpdateNode(dbVersion, filePath), dbVersion);
    }
}
