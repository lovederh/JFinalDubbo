package edu.dsideal.campus.snaker.patterns.taskFormAttr;

import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;
import edu.dsideal.campus.snaker.sql.HistTaskInfoSql;

/**
 * 任务环节处理表单展示对象的包装类
 * 针对流程类型为审批流程时, 展示的明细页面
 * 各任务环节的展示页面统一: 上面展示第一环节填报的表单(最近一次的), 下面展示审批列表
 * 2017/1/11
 */
public class TaskFormAttrWrapperApprovalView extends ITaskFormAttrWrapper {

    public TaskFormAttrWrapperApprovalView() {
        super();
        this.defaultFormUrl = "approvalType/openApprovalView.html";
        //变更展示类型为view(之前为approvalTypeView是为了确认处理子类)
        this.showType = "view";
    }

    /**
     * 个性化装饰任务环节表单对象
     */
    @Override
    protected void initFormAttrWrapper() throws Exception{

        //由于审批界面展示第一环节的表单信息, 故以下把第一环节数据放置taskInfo/businessBean对象中
        taskInfo = HistTaskInfoSql.findFirstTaskInfoLast(orderId);
        if(taskInfo == null){
            throw new RuntimeException("对不起, 任务环节历史数据缺失!");
        }

        //根据节点业务数据表(即用户表单对应的数据存储表), 取出第一环节业务数据
        businessBean = HistTaskInfoSql.findBeanByTaskInfo(taskInfo);

        //明细时, 使用当时第一环节处理使用的对应表单配置
        String taskFormValue = taskInfo.get("formValue");
        //展示第一环节, 故需要扩展表单, 调用父类方法
        initExtFormUrl(taskFormValue);
    }

    /**
     * 是否需要获取紧邻当前环节的处理环节下, 处理者集合标示
     */
    @Override
    protected boolean checkGetNextActorsFlag() throws Exception{
        return false;
    }
}
