package edu.dsideal.campus.snaker.patterns.processAll;

import com.jfinal.plugin.activerecord.Db;
import edu.dsideal.campus.snaker.patterns.ProcessAllNodeSet;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;

/**
 * 用户定制流程的整体配置对象
 * 包含: 整体配置/处理环节配置/判断节点配置
 * 2017/1/20
 */
public class UserProcessAllNodeSet extends ProcessAllNodeSet {

    /********************************** 流程安装后保存各种配置 ***********************************/
    /**
     * 保存流程整体配置(wf_processSet)
     */
    @Override
    protected void saveFlowSetEndDeploy() throws Exception{
        //用户定制的流程, 直接执行历史配置查找
        processSet = Db.findFirst(
                "select s.id, s.processName, s.processType, s.isDeploy from wf_processSet s, wf_process p " +
                "where p.name=s.processName and s.isDeploy='N' and s.processType='user' " +
                "and s.domain=? and s.deployFile=? and p.id=? ",
            loginUserObj.getDomain(), fileName, processId);
        processSet.set("isDeploy", "Y");
        processSet.set(ATTR_PROCESS_ID, processId);
        processSet.set("domain", loginUserObj.getDomain());
        Db.update("wf_processSet", processSet);
    }
    /**
     * 保存可处理任务节点配置(wf_processTaskSet)
     */
    @Override
    protected void saveFlowTaskSetEndDeploy() throws Exception{

    }
    /**
     * 保存判断类型节点配置(wf_processDecisionNode)
     */
    @Override
    protected void saveFlowDecisionsEndDeploy() throws Exception{

    }
}
