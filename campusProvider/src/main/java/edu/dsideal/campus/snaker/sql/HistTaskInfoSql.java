package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;

/**
 * wf_hist_taskInfo表主要操作
 * 2017/1/21
 */
public class HistTaskInfoSql {

    /**
     * 根据工单ID和环节英文名称, 查询任务节点处理副表(wf_hist_taskInfo), 获取最近一条任务处理信息
     * 注: 为了更好的代码复用性, 这里的处理为如果taskName传入空值, 那么就查询第一环节的
     * 可以加入操作人过滤, 这样匹配指定人员填写的
     */
    public static Record findLastInfoByTaskName(String orderId, String taskName, String... dealUsers){
        if(CommonTools.isNotEmpty(orderId)){
            String findSql ="select * from wf_hist_taskInfo " +
                            "where infoType='1' and orderId =? ";
            if(CommonTools.isEmpty(taskName)){
                //如果传入的taskName为null, 则查询第一环节
                findSql += " and isStartTask='Y' ";
            }else{
                findSql += " and taskName='" + taskName + "' ";
            }
            //操作人过滤, 用于防止多人操作环节时, 会看到别人填写的历史数据
            if(dealUsers!=null && dealUsers.length>0 && CommonTools.isNotEmpty(dealUsers[0])){
                findSql += " and operator = '" + dealUsers[0] + "' ";
            }
            findSql += " order by operateTime desc ";
            return Db.findFirst(findSql, orderId);
        }
        return null;
    }

    /**
     * 根据工单ID, 查询任务节点处理副表(wf_hist_taskInfo), 获取最近一次第一环节的任务处理信息
     */
    public static Record findFirstTaskInfoLast(String orderId){
        return findLastInfoByTaskName(orderId, null);
    }

    /**
     * 根据工单ID和环节ID, 查询所在任务节点下处理副表信息(wf_hist_taskInfo)
     */
    public static Record findTaskInfoByTaskId(String orderId, String taskId){
        if(CommonTools.isNotEmpty(orderId) && CommonTools.isNotEmpty(taskId)){
            return Db.findFirst(
                    "select * from wf_hist_taskInfo " +
                    "where infoType='1' and orderId =? and taskId=? ", orderId, taskId);
        }
        return null;
    }

    /**
     * 根据节点业务数据表(即用户表单对应的数据存储表), 取出业务数据
     */
    public static Record findBeanByTaskInfo(Record taskInfo){
        if(null != taskInfo){
            String dataTable = taskInfo.get("dataTable");//任务节点使用数据表
            String dataIdValue = taskInfo.get("dataIdValue");//业务数据ID
            if(CommonTools.isNotEmpty(dataTable) && CommonTools.isNotEmpty(dataIdValue)){
                return Db.findById(dataTable, dataIdValue);
            }
        }
        return null;
    }
}
