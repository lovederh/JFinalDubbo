package edu.dsideal.campus.snaker.patterns.dealState;

import edu.dsideal.campus.snaker.patterns.IDealTaskState;

/**
 * 任务环节处理: 通用的环节节点处理
 * 注: 不涉及审批结果的判断, 直接流转至下一环节
 * 2017/1/12
 */
public class DealTaskNormalState extends IDealTaskState {

    /**
     * 是否需要保存业务数据标识
     */
    @Override
    protected boolean checkSaveFormDataFlag() {
        //当前为启动环节/或配置多任务多表单为真说明存在表单, 需要保存业务数据
        if("Y".equals(processTaskSet.get("isStartTask"))){
            return true;
        }
        if("Y".equals(processSet.get("isMultiForm"))){
            return true;
        }
        return false;
    }

    /**
     * 提交流程引擎处理, 实现工单流转
     */
    @Override
    protected void engineExecuteTask() throws Exception{
        engine.executeTask(taskId, loginUserObj.getLoginId(), dealArgs);
    }
}
