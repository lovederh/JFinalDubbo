package edu.dsideal.campus.snaker.handler.decision;

import com.greenpineyu.fel.FelEngine;
import com.greenpineyu.fel.FelEngineImpl;
import com.greenpineyu.fel.context.FelContext;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.sql.FlowApiDataSql;
import edu.dsideal.campus.snaker.sql.HistTaskInfoSql;
import org.snaker.engine.DecisionHandler;
import org.snaker.engine.core.Execution;

import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_LOGIN_USER_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_RECORD_ID_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_TABLE_NAME_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.MAX_DECISION_FIELD_NUM;

/**
 * 统一的流程判断环节处理类
 * 配置判断环节handleClass属性: CommonDecisionHandler
 * 依据当前的处理环节节点名, 查找wf_processDecisionFrom表, 判断其邻近的判断节点
 * 取出判断条件, 遍历判断, 最终返回最符合要求的流程走向线, 供流程引擎进行下一步处理
 * 注意: 目前判断节点处, 作为判断条件的字段数目为20, 且这些字段存于wf_processDecisions
 */
public class CommonDecisionHandler implements DecisionHandler {

    @Override
    public String decide(Execution execution) {
        //先取出当前工单中, 最近一次的第一环节所填报数据
        Record businessBean = findOrderFirstTaskBean(execution);
        if(null == businessBean){
            throw new RuntimeException("处理异常, 未找到第一环节填报数据!");
        }

        //处理环节其后面跟随的判断节点下, 所有的决策条件列表
        List<Record> decisionsList = findProcessDecisionsList(execution);
        if(decisionsList==null || decisionsList.isEmpty()){
            throw new RuntimeException("处理异常, 未找到正确的提交环节!");
        }

        //确认当前登录人所属角色/部门
        Map<String, Object> args = execution.getArgs();//执行参数
        Object loginId = args.get(ARGS_LOGIN_USER_KEY);
        //当前用户拥有的全部角色
        List<String> userRoles = FlowApiDataSql.findUserRoles(loginId);
        //当前用户拥有的全部部门(org)
        List<String> userOrgs = FlowApiDataSql.findUserOrg(loginId);

        //引入fel表达式进行接下来处理条件的判断
        FelEngine felEngine = new FelEngineImpl();
        FelContext felContext = felEngine.getContext();
        //遍历确定本次使用的decision表达式
        String toTransition = null;//最终前往的连线(返回结果)
        String default2Transition = null;//缺省的前往连线(对应空条件那个配置值)
        String decisions;//判断表达式
        boolean checkOutFlag;
        for(Record decisionsObj : decisionsList){
            //角色限制判断
            checkOutFlag = checkDecisionBaseLimit(userRoles, decisionsObj.get("rolesLimit"));
            if(! checkOutFlag){
                continue;
            }
            //部门限制判断
            checkOutFlag = checkDecisionBaseLimit(userOrgs, decisionsObj.get("orgsLimit"));
            if(! checkOutFlag){
                continue;
            }

            //确定当前表达式是否有效
            decisions = decisionsObj.get("decisions");
            toTransition = decisionsObj.get("toTransition");
            if(CommonTools.isEmpty(decisions)){
                //将满足角色与部门限制, 同时未指定判断条件的数据作为缺省值
                if(CommonTools.isNotEmpty(toTransition)) {
                    default2Transition = toTransition;
                }
                continue;
            }
            //角色与部门校验都通过了, 那么进行表单内数据的条件校验
            decisions = replaceFelFieldValue(decisions, decisionsObj, businessBean, felContext);

            //判断表达式运行结果是否为真
            Object felResult = felEngine.eval(decisions);
            System.out.println("判断表达式: " + decisions + "; 判断结果: " + felResult);
            if("true".equals(felResult.toString())){
                break;
            }
        }

        //如果没有符合要求的判断条件, 则使用缺省前往连线
        toTransition = CommonTools.isEmpty(toTransition)? default2Transition: toTransition;
        //未找到可以进入的判断环节, 给出用户直观提示
        if(CommonTools.isEmpty(toTransition)){
            throw new RuntimeException("处理异常, 未找到正确的提交环节!");
        }
        System.out.println("finally------流程走向线------" + toTransition);
        return toTransition;
    }

    /**
     * 找到当前工单第一环节对应的表单中, 填报的业务数据(最近一次)
     */
    private Record findOrderFirstTaskBean(Execution execution){
        //当前工单ID
        String orderId = execution.getOrder().getId();

        //取出最近一次的第一环节填报的表单数据
        Record taskInfo = HistTaskInfoSql.findFirstTaskInfoLast(orderId);
        //根据节点业务数据表(即用户表单对应的数据存储表), 取出第一环节业务数据
        Record businessBean = HistTaskInfoSql.findBeanByTaskInfo(taskInfo);
        if(null == businessBean){
            //可能是新派单情况, 则根据传递数据集合确认表单
            Object businessTable = execution.getArgs().get(ARGS_TABLE_NAME_KEY);
            Object businessBeanId = execution.getArgs().get(ARGS_RECORD_ID_KEY);
            if(businessTable!=null && businessBeanId!=null){
                businessBean = Db.findById(String.valueOf(businessTable), businessBeanId);
            }
        }
        return businessBean;
    }

    /**
     * 根据当前所在处理环节, 查询配置
     * 先获取其后面跟随的判断节点, 并取出节点下的所有的决策条件列表
     */
    private List<Record> findProcessDecisionsList(Execution execution){
        //流程ID
        String processId = execution.getProcess().getId();
        //获取决策节点来源环节(即前置的处理环节)
        String taskName = execution.getTask().getTaskName();

        //查询wf_processDecisionFrom, 获取满足条件的决策节点总表ID
        Object decisionNodeId = Db.queryFirst(
                "select f.decisionNodeId from wf_processDecisionFrom f " +
                "where f.processId=? and f.fromTaskName=? ", processId, taskName
        );
        if(CommonTools.isEmpty(String.valueOf(decisionNodeId))){
            return null;
        }

        //关联wf_processDecisionNode主表, 去wf_processDecisions获取所有的决策条件
        StringBuilder decisionsSqlSb = new StringBuilder(
                "select s.decisions, s.toTransition, s.rolesLimit, s.orgsLimit ");
        for(int i=1; i<=MAX_DECISION_FIELD_NUM ; i++){
            decisionsSqlSb.append(", s.field").append(i);
        }
        decisionsSqlSb.append(
                " from wf_processDecisions s " +
                " where s.processId=? and s.decisionNodeId=? " +
                " order by s.priorityLevel asc, s.rolesLimit, s.orgsLimit "
        );
        //找到所有的决策条件
        return Db.find(decisionsSqlSb.toString(), processId, decisionNodeId);
    }

    /**
     * 整合替换fel表达式中, 使用各个字段(替换表达式内变量对应的字段)
     */
    private String replaceFelFieldValue(String decisions,
                Record decisionsObj, Record businessBean, FelContext felContext){
        String fieldValue;//field1-20字段内具体字段
        String beanFieldValue;
        Object felContextValue;
        for(int i=1; i<=MAX_DECISION_FIELD_NUM ; i++){
            fieldValue = decisionsObj.get("field" + i);
            //先替换表达式内变量对应的字段
            if(CommonTools.isNotEmpty(fieldValue)){
                if(felContext.get(fieldValue) == null){
                    //选取字段对应值, 尝试进行类型转换处理
                    beanFieldValue = businessBean.get(fieldValue);
                    try {
                        felContextValue = Double.valueOf(beanFieldValue);
                    }catch (Exception e){
                        felContextValue = beanFieldValue;
                    }
                    felContext.set(fieldValue, felContextValue);
                }
                //注意预留空格, 防止出现field12被field1内对应字段名替换的情况
                decisions = decisions.replace("field" + i + " ", fieldValue);
            }
        }
        return decisions;
    }


    /**
     * 决策节点, 判断决策条件时, 基础权限数据的判断方法
     * 满足一个规则: 如果需要的权限值为空, 则说明无需限制, 即返回真
     * 用于: 用户所属角色判断/用户所属部门判断
     */
    private boolean checkDecisionBaseLimit(List<String> userPrivs, String privsLimit){
        if(CommonTools.isEmpty(privsLimit)){
            return true;
        }
        if(userPrivs==null || userPrivs.isEmpty()){
            return false;
        }
        for(String limit : privsLimit.split(",")){
            if(userPrivs.contains(limit)){
                return true;
            }
        }
        return false;
    }
}
