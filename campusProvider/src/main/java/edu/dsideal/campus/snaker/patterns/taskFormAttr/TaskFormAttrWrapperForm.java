package edu.dsideal.campus.snaker.patterns.taskFormAttr;

import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;
import edu.dsideal.campus.snaker.sql.HistTaskInfoSql;

import static edu.dsideal.campus.snaker.FlowConstDefine.APPROVAL_NOT_PASS_VALUE;

/**
 * 任务环节处理表单展示对象的包装类
 * 针对各个环节页面, 展示要填报的的表单
 * 如果为重复之前环节, 则加载最近一次填报的数据供用户修改后提交
 * 2017/1/11
 */
public class TaskFormAttrWrapperForm extends ITaskFormAttrWrapper {

    public TaskFormAttrWrapperForm() {
        super();
        this.defaultFormUrl = "deal/openTaskForm.html";
        this.showType = "form";
    }

    /**
     * 个性化装饰任务环节表单对象
     */
    @Override
    protected void initFormAttrWrapper() throws Exception{
        //根据环节名称, 去查最近一条任务处理信息(自己填写的)
        if(CommonTools.isNotEmpty(taskName)) {
            taskInfo = HistTaskInfoSql.findLastInfoByTaskName(orderId, taskName, loginUserObj.getLoginId());
        }

        //根据节点业务数据表(即用户表单对应的数据存储表), 取出业务数据
        businessBean = HistTaskInfoSql.findBeanByTaskInfo(taskInfo);

        //打开当前处理表单时, 使用的表单配置都基于配置表
        String isMultiForm = processSet.get("isMultiForm");//是否多任务多表单
        String taskFormValue = processTaskSet.get("formValue");//环节表单值
        String dataTable = processTaskSet.get("dataTable");//环节表单值
        String isStartTask = processTaskSet.get("isStartTask");//是否为第一环节

        //taskInfo不为空情况下, 说明存在同名环节历史信息, 那么需要比对任务数据使用表
        if(taskInfo != null){
            //环节数据表与上次存储数据的表不一致, 则呈现历史数据会出问题, 故移除
            if(CommonTools.isEmpty(taskInfo.get("dataTable")) || !taskInfo.get("dataTable").equals(dataTable)){
                businessBean = null;
            }
            //本次审批结果和上一次不一致, 则处理备注没实际意义, 移除
            if(CommonTools.isNotEmpty(approvalResult) && !approvalResult.equals(taskInfo.get("approvalResult"))){
                taskInfo.set("approvalRemark", "");
            }
        }else{
            businessBean = null;
        }

        //两种情况下(1)第一环节(2)多任务多表单开启, 并且审批结果不为不通过. 则需要扩展表单
        if("Y".equals(isStartTask) || ("Y".equals(isMultiForm) && !APPROVAL_NOT_PASS_VALUE.equals(approvalResult))){
            initExtFormUrl(taskFormValue);
        }
    }

    /**
     * 是否需要获取紧邻当前环节的处理环节下, 处理者集合标示
     */
    @Override
    protected boolean checkGetNextActorsFlag() throws Exception{
        //审批不通过则工单直接返回上一环节, 不用获取处理者
        return ! APPROVAL_NOT_PASS_VALUE.equals(approvalResult);
    }
}
