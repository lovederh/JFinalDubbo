package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 表单数据转化为业务数据对象的处理类
 * 2017/1/12
 */
public class Form2BusinessBeanObj {
    //登录人对象
    private LoginUserObj loginUserObj;
    //表单数据集合
    private Map<String, String[]> paraMap;

    //表单对象ID
    private String formId;
    //表单对象英文名(编码)
    private String formName;
    //表单类型(sys: 系统表单; user: 用户定制表单)
    private String formType = "";
    //使用表单的定义对象
    private Record formObj;

    //存储表名
    private String dataTable;
    //存储业务数据后, 最终对应的业务数据ID
    private Object dataIdValue;
    //是否已执行业务数据存储标识
    private boolean savedDataDone = false;

    public Form2BusinessBeanObj(LoginUserObj loginUserObj, Map<String, String[]> paraMap, String formId) {
        this.loginUserObj = loginUserObj;
        this.paraMap = paraMap;
        this.formId = formId;
    }

    /**
     * 执行任务节点处理信息表(任务节点处理副表)保存之前, 将业务数据处理信息放入taskInfo中
     * 如果包括: 当前环节使用的表单类型; 使用的表单值; 业务存储表表名; 对应的数据ID
     */
    public void putBusiness2TaskInfo(Record taskInfo){
        if(taskInfo==null || ! savedDataDone){
            return;
        }
        taskInfo.set("formValue", formId);
        taskInfo.set("formName", formName);
        taskInfo.set("formType", formType);
        taskInfo.set("dataTable", dataTable);
        taskInfo.set("dataIdValue", dataIdValue);
    }

    /**
     * 根据使用的表单定义ID, 获取存储信息(表名/表内字段)
     * 再现有传递过来的表单数据, 构造Record对象, 并执行保存
     */
    public void saveForm2BusinessBean() throws Exception{
        if(CommonTools.isEmpty(formId)){
            return;
        }
        formObj = Db.findById("t_sys_userForm", formId);
        if(null == formObj){
            return;
        }
        dataTable = formObj.get("dataTable");//业务表名赋值
        formName = formObj.get("formName");//表单编码(英文名)
        formType = formObj.get("formType");//表单类型
        if(CommonTools.isEmpty(dataTable) || CommonTools.isEmpty(formType)){
            return;
        }
        //根据不同的表单类型, 执行不同的存储方法
        Record businessMainBean = null;//业务数据存储主表对象
        switch(formType){
            case "sys":
                businessMainBean = saveSysTypeForm2Bean();
                break;
            case "user":
                businessMainBean = saveUserTypeForm2Bean();
                break;
        }
        if(null != businessMainBean){
            //赋主表记录的业务数据ID
            dataIdValue = businessMainBean.get("id");
            savedDataDone = true;
        }
    }

    /**
     * 表单类型为系统表单时, 存储方法
     */
    private Record saveSysTypeForm2Bean(){
        //系统表单, 存储表内字段都存放在dataFields字段内
        String dataFields = formObj.getStr("dataFields");
        //构造最终的存储对象
        Record businessBean = CommonTools.putRecordUseParams(dataFields, paraMap);
        //执行表单内业务数据保存
        Db.save(dataTable, businessBean);
        return businessBean;
    }

    /**
     * 表单类型为用户定制表单时, 存储方法
     * 注: 用户定制保存可能还涉及主表副表
     * 故处理方式与系统表单拆分开, 便于以后的扩展
     */
    private Record saveUserTypeForm2Bean() {
        List<String> dataFieldList = new ArrayList<String>();
        //自定制表单, 存储表内字段需要从配置表中查询

        //TODO
        dataFieldList.add("helloMsg");
        dataFieldList.add("helloCode");

        //构造最终的存储对象
        Record businessMainBean = new Record();
        //遍历流程业务表内所有字段, 根据表单值对进行各字段值的填充
        if(!dataFieldList.isEmpty()){
            for(String field : dataFieldList){
                businessMainBean.set(field, CommonTools.getValueFromParaMap(paraMap, field));
            }
        }
        //执行表单内业务数据保存
        Db.save(dataTable, businessMainBean);
        return businessMainBean;
    }

    public LoginUserObj getLoginUserObj() {
        return loginUserObj;
    }
    public void setLoginUserObj(LoginUserObj loginUserObj) {
        this.loginUserObj = loginUserObj;
    }

    public Map<String, String[]> getParaMap() {
        return paraMap;
    }
    public void setParaMap(Map<String, String[]> paraMap) {
        this.paraMap = paraMap;
    }

    public String getFormId() {
        return formId;
    }
    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormType() {
        return formType;
    }
    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormName() {
        return formName;
    }
    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Record getFormObj() {
        return formObj;
    }
    public void setFormObj(Record formObj) {
        this.formObj = formObj;
    }

    public String getDataTable() {
        return dataTable;
    }
    public void setDataTable(String dataTable) {
        this.dataTable = dataTable;
    }

    public Object getDataIdValue() {
        return dataIdValue;
    }
    public void setDataIdValue(Object dataIdValue) {
        this.dataIdValue = dataIdValue;
    }

    public boolean isSavedDataDone() {
        return savedDataDone;
    }
    public void setSavedDataDone(boolean savedDataDone) {
        this.savedDataDone = savedDataDone;
    }
}
