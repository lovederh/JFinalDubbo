package edu.dsideal.campus.snaker.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;
import edu.dsideal.campus.snaker.bean.ProcessTaskSetObj;
import edu.dsideal.campus.snaker.cache.SnakerCacheManager;
import edu.dsideal.campus.snaker.patterns.ProcessAllNodeSet;
import edu.dsideal.campus.snaker.patterns.processAll.SysProcessAllNodeSet;
import edu.dsideal.campus.snaker.service.IFlowSetService;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import edu.dsideal.campus.snaker.sql.FlowUserForm;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskActorSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskSetSql;
import org.snaker.engine.SnakerEngine;
import org.snaker.jfinal.plugin.SnakerPlugin;
import edu.dsideal.campus.snaker.patterns.processAll.UserProcessAllNodeSet;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_ROLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_TEACHER;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;

/**
 * 流程配置信息服务类(已安装流程)
 */
public class FlowSetServiceImpl implements IFlowSetService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /********************************** 卸载与流程文件删除 ***********************************/
    /**
     * 流程文件删除, 防止该文件出现在用户的待安装列表中
     * 注: (1)物理删除, 不可恢复; (2)文件名中不能包含空格
     */
    @Override
    public String deleteFlowFile(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        //流程文件路径
        String fileResource = CommonTools.getValueFromParaMap(paraMap, "fileResource");
        //流程配置信息表ID
        String processSetId = CommonTools.getValueFromParaMap(paraMap, "processSetId");
        //找到路径, 执行文件删除
        if(CommonTools.isNotEmpty(fileResource)){
            File file = new File(FlowSetServiceImpl.class.getClassLoader().getResource(fileResource).getPath());
            if(file.exists() && file.isFile()){
                file.delete();
            }
        }
        //同时删除配置信息表数据
        FlowSqlUtil.deleteAllProcessSet(processSetId);
        return CommonTools.getMsgJson(true, "流程文件已删除!");
    }

    /**
     * 控制已安装过的系统流程不再可用
     * 操作: 对于所有安装该流程的企业域, 均执行该流程的卸载操作
     */
    @Override
    @Before(Tx.class)
    public void batchUndeployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        String fileResource = CommonTools.getValueFromParaMap(paraMap, "fileResource");//流程文件路径
        String domains = CommonTools.getValueFromParaMap(paraMap, "domains");//禁用企业域
        if(CommonTools.isNotEmpty(fileResource)){
            String fileName = fileResource.substring(fileResource.lastIndexOf("/") + 1);//流程文件名
            //根据文件全路径去wf_processSet表, 找到所有依照该文件安装的流程
            List<Record> deployedSysList = ProcessSetSql.findDeployedSysByDeployFileAndDomains(fileName, domains);
            if(deployedSysList!=null && !deployedSysList.isEmpty()){
                deployedSysList.forEach(this::undeployFlowByProcessSet);
            }
        }
    }
    /**
     * 根据指定的流程ID, 执行流程卸载
     */
    @Override
    @Before(Tx.class)
    public void undeployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        //流程配置信息
        String processId = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_ID);
        Record processSet = ProcessSetSql.findSetByProcessId(processId);
        //执行流程卸载
        undeployFlowByProcessSet(processSet);
    }

    /**
     * 卸载方法, 流程卸载后, 设置processSet状态为不可用
     */
    private void undeployFlowByProcessSet(Record processSet){
        if(processSet!=null && CommonTools.isNotEmpty(processSet.get(ATTR_PROCESS_ID))){
            engine.process().undeploy(processSet.get(ATTR_PROCESS_ID));
            //更新流程配置表内是否已安装字段
            processSet.set("isDeploy", "N");
            Db.update("wf_processSet", processSet);
            //删除流程配置对应的缓存
            SnakerCacheManager.delCache("processSet_" + processSet.get(ATTR_PROCESS_ID));
        }
    }

    /********************************** 流程安装处理 ***********************************/
    /**
     * 根据指定的流程文件资源全路径, 执行流程安装方法
     */
    @Override
    @Before(Tx.class)
    public void deployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        String processType = CommonTools.getValueFromParaMap(paraMap, "processType");//获取流程类型
        String fileResource = CommonTools.getValueFromParaMap(paraMap, "fileResource");//流程文件路径
        if(CommonTools.isEmpty(fileResource)){
            throw new Exception("流程文件不存在, 无法安装!");
        }
        deployFlowByDomainAndType(loginUserObj, processType, fileResource);
    }
    /**
     * 系统流程的批量安装
     */
    @Override
    @Before(Tx.class)
    public void batchDeploySysFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception {
        String fileResource = CommonTools.getValueFromParaMap(paraMap, "fileResource");//流程文件路径
        String domains = CommonTools.getValueFromParaMap(paraMap, "domains");//企业域
        String flowInterceptor = CommonTools.getValueFromParaMap(paraMap, "flowInterceptor");//流程使用拦截器
        String[] domainArray;
        if(CommonTools.isEmpty(fileResource)){
            throw new Exception("流程文件不存在, 无法安装!");
        }
        if(CommonTools.isEmpty(domains)){
            //如果没有选择要安装流程的企业域, 则安装全部可用的企业域
            String fileName = fileResource.substring(fileResource.lastIndexOf("/") + 1);//流程文件名
            List<Record> noInstallDomains = ProcessSetSql.findFlowDomainsByDeployFileAndType(fileName, "noInstall");
            int domainSize = noInstallDomains.size();
            domainArray = new String[noInstallDomains.size()];
            for(int i=0; i<domainSize ; i++){
                domainArray[i] = noInstallDomains.get(i).getStr("id");
            }
        }else{
            domainArray = domains.split(",");
        }
        //封装系统流程安装时可能有一些额外的配置参数
        Map<String, String> sysFlowConfig = new HashMap<String, String>();
        sysFlowConfig.put("interceptor", flowInterceptor);//map中key值要与wf_processSet表内字段一致
        //遍历执行安装方法
        LoginUserObj obj = new LoginUserObj();
        obj.setLoginId(loginUserObj.getLoginId());
        for(String domain : domainArray){
            obj.setDomain(domain);
            deployFlowByDomainAndType(obj, "sys", fileResource, sysFlowConfig);
        }
    }

    /**
     * 流程安装方法, 为不同的企业域安装, 企业域信息通过loginUserObj传递
     * 可变参数: sysFlowConfig, 表示流程安装时可能有一些额外的配置参数(系统流程才会用到)
     */
    private void deployFlowByDomainAndType(LoginUserObj loginUserObj, String processType, String fileResource, Map<String, String>... sysFlowConfigs) throws Exception{
        InputStream input= null;//流程文件流
        try {
            ClassLoader loader = FlowSetServiceImpl.class.getClassLoader();
            input = loader.getResourceAsStream(fileResource);

            ProcessAllNodeSet allNodeSet;
            if("sys".equals(processType)){
                allNodeSet = new SysProcessAllNodeSet();
            }else{
                allNodeSet = new UserProcessAllNodeSet();
            }
            allNodeSet.engine = engine;
            allNodeSet.loginUserObj = loginUserObj;
            allNodeSet.fileResource = fileResource;
            if(null!=sysFlowConfigs && sysFlowConfigs.length>0){
                allNodeSet.sysFlowConfig = sysFlowConfigs[0];
            }
            allNodeSet.deployProcessSaveAllSet(input);
        }finally {
            if(null != input){
                input.close();
            }
        }
    }
    /*********************************** 已安装流程修改配置 ***********************************/
    /**
     * 查找流程环节配置基础信息, 只包含配置ID和环节名称, 详细信息需要单独再依据ID查询
     */
    @Override
    public List<ProcessTaskSetObj> findBaseFlowTaskSets(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        String processSetId = CommonTools.getValueFromParaMap(paraMap, "processSetId");
        if(CommonTools.isEmpty(processSetId)){
            return null;
        }
        //查询配置表中任务节点配置列表
        List<Record> baseTaskSets = ProcessTaskSetSql.findSetListByProcessSetId(processSetId);
        // 封装之后的节点配置对象列表
        List<ProcessTaskSetObj> taskSetObjs = new ArrayList<ProcessTaskSetObj>();
        //通过遍历, 设置任务节点的其它配置信息, 返回整体对象列表
        if(null!=baseTaskSets && !baseTaskSets.isEmpty()){
            for(Record baseTaskSet : baseTaskSets){
                taskSetObjs.add(new ProcessTaskSetObj(baseTaskSet));
            }
        }
        return taskSetObjs;
    }
    /**
     * 查找流程环节配置详细信息, 返回ProcessTaskSetObj对象
     * 覆盖wf_processTaskSet(人员需要单独查询)
     */
    @Override
    public ProcessTaskSetObj findOneFlowTaskSetObj(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String taskSetId = CommonTools.getValueFromParaMap(paraMap, "taskSetId");
        if(CommonTools.isEmpty(taskSetId)){
            return null;
        }
        Record taskSet = Db.findById("wf_processTaskSet", taskSetId);
        return new ProcessTaskSetObj(taskSet);
    }
    /**
     * 查找流程环节配置的处理者信息
     */
    @Override
    public ProcessTaskAllActorsObj findProcessTaskActors(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String taskSetId = CommonTools.getValueFromParaMap(paraMap, "taskSetId");
        if(CommonTools.isEmpty(taskSetId)){
            return null;
        }
        //先构造一个环节配置对象
        Record taskSet = new Record();
        taskSet.set("taskSetId", taskSetId);
        //调用环节处理对象的构造方法
        return ProcessTaskActorSql.buildTaskActorsObjBySetId(taskSet, loginUserObj.getDomain());
    }

    /**
     * 已经安装过的流程, 整体配置修改
     * 针对已经安装过的流程, 本次保存没有使流程图发生改变的配置项
     */
    @Override
    @Before(Tx.class)
    public void updateFlowSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        //前端传过来的参数
        String id = CommonTools.getValueFromParaMap(paraMap, "id");//主键ID
        String processDisplay = CommonTools.getValueFromParaMap(paraMap, "processDisplay");//流程名称
        String isMultiForm = CommonTools.getValueFromParaMap(paraMap, "isMultiForm");//是否每个任务启用单独表单
        String formValue = CommonTools.getValueFromParaMap(paraMap, "formValue");//流程启动表单值
        formValue = CommonTools.isEmpty(formValue)? "": formValue;
        if(CommonTools.isEmpty(id)){
            throw new Exception("对不起, 保存失败, 未找到配置信息。");
        }
        //根据主键, 获取流程配置记录
        Record processSet = Db.findById("wf_processSet", id);
        processSet.set("processDisplay", processDisplay);
        processSet.set("isMultiForm", isMultiForm);
        //判断流程启动表单值是否改变, 改变则修改关联的配置属性
        if(! formValue.equals(processSet.get("formValue"))){
            FlowUserForm.putFormInfo2ProcessSet(processSet, formValue, true, "wf_processSet");
        }
        Db.update("wf_processSet", processSet);
        //更新缓存
        SnakerCacheManager.setCache("processSet_" + processSet.get(ATTR_PROCESS_ID), processSet);
        //更改流程配置本表
        if(CommonTools.isNotEmpty(processSet.get(ATTR_PROCESS_ID))){
            Db.update("update wf_process set display_Name=? where id=? ", processDisplay, processSet.getStr(ATTR_PROCESS_ID));
        }
    }

    /**
     * 流程环节配置的更新方法, 主要包含环节表单配置和审批人配置
     */
    @Override
    @Before(Tx.class)
    public void updateFlowTaskSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception {
        String taskSetId = CommonTools.getValueFromParaMap(paraMap, "taskSetId");//任务环节配置ID
        String isStartTask = CommonTools.getValueFromParaMap(paraMap, "isStartTask");//是否第一环节标识
        String taskDisplay = CommonTools.getValueFromParaMap(paraMap, "taskDisplay");//任务环节显示名
        String hasCcOperator = CommonTools.getValueFromParaMap(paraMap, "hasCcOperator");//是否开启抄送
        String formValue = CommonTools.getValueFromParaMap(paraMap, "formValue");//任务环节使用表单值
        formValue = CommonTools.isEmpty(formValue)? "": formValue;
        //根据主键ID, 获取现有的配置信息
        Record taskSet = Db.findById("wf_processTaskSet", taskSetId);
        if(taskSet == null){
            throw new RuntimeException("未找到当前环节配置信息, 无法保存!");
        }
        //判断环节表单值是否改变, 改变则修改关联的配置属性
        if(! formValue.equals(taskSet.get("formValue"))){
            //若改变的为第一环节表单, 那么需要同步修改wf_processSet表
            FlowUserForm.putFormInfo2ProcessSet(taskSet, formValue, "Y".equals(isStartTask), "wf_processTaskSet");
        }
        taskSet.set("taskDisplay", taskDisplay);
        taskSet.set("hasCcOperator", hasCcOperator);
        Db.update("wf_processTaskSet", taskSet);

        //最终需要新增加的任务处理人记录
        List<Record> addActorSetList = new ArrayList<Record>();
        String roleDataStr = CommonTools.getValueFromParaMap(paraMap, "roleDataStr");
        String teacherDataStr = CommonTools.getValueFromParaMap(paraMap, "teacherDataStr");

        operateSetByActorType(addActorSetList, roleDataStr, ACTOR_TYPE_ROLE, taskSetId, taskSet, loginUserObj.getDomain());
        operateSetByActorType(addActorSetList, teacherDataStr, ACTOR_TYPE_TEACHER, taskSetId, taskSet, loginUserObj.getDomain());
        //统一保存操作者设置
        if(! addActorSetList.isEmpty()){
            Db.batchSave("wf_processTaskActorSet", addActorSetList, addActorSetList.size());
        }
    }

    /**
     * 处理操作者集合
     * 步骤: 先删除本次操作移除的操作者
     *      再获取最终要新加入的处理者缓存到集合中
     */
    private void operateSetByActorType(List<Record> addActorSetList, String actorDataStr, String actorType, String taskSetId, Record taskSet, String domain){
        //保存任务环节处理人, 删除本次操作移除的操作者操作
        String delOldActorSql =
                "delete from wf_processTaskActorSet " +
                "where actorType=? and processTaskSetId=? ";
        if("".equals(actorDataStr)){
            //本次没有选择处理者, 那么之前处理者需全部删除, 并直接返回
            Db.update(delOldActorSql, actorType, taskSetId);
            return;
        }
        //删除本次操作移除的操作者
        delOldActorSql += " and actorValue not in('" + actorDataStr.replace(",", "','") + "') ";
        Db.update(delOldActorSql, actorType, taskSetId);
        //遍历获取此时还有的处理者, 获取最终要新加入的处理者
        List<String> existsActors = Db.query(
                "select actorValue from wf_processTaskActorSet " +
                "where actorType=? and processTaskSetId=?", actorType, taskSetId);
        if(existsActors!=null && !existsActors.isEmpty()){
            actorDataStr = "," + actorDataStr + ",";
            for(String exists : existsActors){
                //移除当前选择处理者中已经存在的数据
                actorDataStr = actorDataStr.replace("," + exists + ",", ",");
            }
        }
        //构造最终要插入的新处理者对象
        Record actorSet;
        for(String actorStr : actorDataStr.split(",")){
            if(CommonTools.isEmpty(actorStr)){
                continue;
            }
            actorSet = new Record();
            actorSet.set("actorType", actorType);
            actorSet.set("actorValue", actorStr);
            actorSet.set("processTaskSetId", taskSetId);
            actorSet.set("processSetId", taskSet.get("processSetId"));
            actorSet.set("domain", domain);
            actorSet.set(ATTR_PROCESS_ID, taskSet.get(ATTR_PROCESS_ID));
            actorSet.set(ATTR_PROCESS_NAME, taskSet.get(ATTR_PROCESS_NAME));
            actorSet.set(ATTR_TASK_NAME, taskSet.get(ATTR_TASK_NAME));
            addActorSetList.add(actorSet);
        }
    }
}
