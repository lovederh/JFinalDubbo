package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.IFlowQueryService;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import edu.dsideal.campus.snaker.util.SnakerHelper;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.engine.entity.Process;
import org.snaker.engine.entity.Task;
import org.snaker.engine.helper.AssertHelper;
import org.snaker.engine.model.ProcessModel;
import org.snaker.jfinal.plugin.SnakerPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程查询服务类
 */
public class FlowQueryServiceImpl implements IFlowQueryService{
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /**
     * 依据类型, 查询某指定流程对应的企业域安装情况
     */
    @Override
    public List<Record> sysFlowDomainsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String type = CommonTools.getValueFromParaMap(paraMap, "type");//查询类型
        String fileResource = CommonTools.getValueFromParaMap(paraMap, "fileResource");//流程文件全路径
        if(CommonTools.isNotEmpty(fileResource)){
            String fileName = fileResource.substring(fileResource.lastIndexOf("/") + 1);//流程文件名
            return ProcessSetSql.findFlowDomainsByDeployFileAndType(fileName, type);
        }
        return null;
    }

    /**
     * 查询已安装的流程列表
     * 查询条件: 用户所在企业域/流程类型/模糊查询流程名称
     * 注: 同名流程全部取出, 依照版本排序
     */
    public Page<Record> installedFlowsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        // 查询条件获取
        String searchProcessDisplay = CommonTools.getValueFromParaMap(paraMap, "searchProcessDisplay");//流程名称
        // 分页信息构造
        String page = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        String rows = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        int pageNumber = CommonTools.isEmpty(page)? 1: Integer.parseInt(page);
        int pageSize = CommonTools.isEmpty(rows)? 10: Integer.parseInt(rows);

        //拼接查询使用sql语句
        String sqlSelect =
                "select p.id as processId," +
                "       p.name as processName, " +
                "       p.display_Name as processDisplay, " +
                "       p.version, " +
                "       p.create_Time as createTime, " +
                "       u.userName as creatorName, " +
                "       s.id as processSetId, " +
                "       s.customType ";
        StringBuilder sqlConditionSb = new StringBuilder(
                "from wf_process p left join wf_processSet s on s.processId=p.id " +
                "                  left join t_sys_user u on u.loginId=p.creator " +
                "where p.state = 1 and s.isDeploy = 'Y' and s.domain = ? "
        );
        if(CommonTools.isNotEmpty(searchProcessDisplay)){
            sqlConditionSb.append(" and p.display_Name like '%").append(searchProcessDisplay).append("%' ");
        }
        sqlConditionSb.append(" order by p.display_Name, p.version ");
        return Db.paginate(pageNumber, pageSize, sqlSelect, sqlConditionSb.toString(), loginUserObj.getDomain());
    }

    /**
     * 已安装流程前提下, 查询我的可申请流程集合
     * 注意: 存在同名流程情况下, 则只取最高版本
     */
    @Override
    public List<Record> iCanStartFlowsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        //是否加载流程图片标识
        String loadImgFlag = CommonTools.getValueFromParaMap(paraMap, "loadImgFlag");
        List<Record> iCanStartFlows = ProcessSetSql.findMaxVersionFlowsByDomain(loginUserObj.getDomain());
        if("true".equals(loadImgFlag) && null!=iCanStartFlows && !iCanStartFlows.isEmpty()){
            //如需要加载流程图片, 则遍历设置图片缩略图路径
            String processImg;
            for(Record flow : iCanStartFlows){
                processImg = flow.get("processImg");
                //流程对应图片不存在, 则设置默认值
                processImg = CommonTools.isNotEmpty(processImg)? processImg: "/resource/third/snaker/images/48/flow_sequence.png";
                flow.set("processImgSrc", processImg);
            }
        }
        return iCanStartFlows;
    }

    /**
     * 所有流程数据的分页查询, 可以传入是否只查询自己的标识控制查询
     * @return 自己构造的分页对象, 转为json形式返回
     */
    @Override
    public Page<Record> allOrderGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        // 分页信息构造
        String page = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        String rows = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        int pageNumber = CommonTools.isEmpty(page)? 1: Integer.parseInt(page);
        int pageSize = CommonTools.isEmpty(rows)? 10: Integer.parseInt(rows);

        //拼接查询使用sql语句
        String sqlSelect =
                "select t.processId, t.orderId, " +
                "       t.processDisplay, t.processName, " +
                "       t.orderTitle, t.orderState, " +
                "       t.creatorDisplay, t.createTime ";
        StringBuilder sqlConditionSb = new StringBuilder(
                "from ( select o.process_Id as processId," +
                "           o.id as orderId, " +
                "           p.name as processName," +
                "           p.display_Name as processDisplay, " +
                "           o.orderTitle," +
                "           o.order_State as orderState," +
                "           o.creator, " +
                "           u.userName as creatorDisplay," +
                "           o.create_Time as createTime, " +
                "           o.domain " +
                "      from wf_hist_order o left join wf_process p on p.id = o.process_Id " +
                "                           left join t_sys_user u on u.loginId = o.creator " +
                "       where o.delFlag='0' " +
                "   ) t where t.domain=? "
        );
        List<Object> sqlParamList = new ArrayList<Object>();
        sqlParamList.add(loginUserObj.getDomain());
        //拼接查询条件
        buildSearchFilterSql(loginUserObj, paraMap, sqlConditionSb, sqlParamList);
        //排序
        sqlConditionSb.append(" order by t.processDisplay, t.createTime desc ");
        //构造分页查询
        Object[] sqlParams = new Object[sqlParamList.size()];
        sqlParamList.toArray(sqlParams);
        return Db.paginate(pageNumber, pageSize, sqlSelect, sqlConditionSb.toString(), sqlParams);
    }

    /**
     * 由我发起的工单列表数据, 分页查询结果
     */
    @Override
    public Page<Record> myStartOrdersData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        // 分页信息构造
        String page = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        String rows = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        int pageNumber = CommonTools.isEmpty(page)? 1: Integer.parseInt(page);
        int pageSize = CommonTools.isEmpty(rows)? 10: Integer.parseInt(rows);

        //拼接查询使用sql语句
        String sqlSelect =
                "select t.processId, t.orderId, t.taskInfoId, " +
                "       t.processDisplay, t.processName, " +
                "       t.orderTitle, t.orderState, " +
                "       t.creatorDisplay, t.createTime ";
        StringBuilder sqlConditionSb = new StringBuilder(
                "from ( select o.process_Id as processId," +
                "           o.id as orderId, " +
                "           '' as taskInfoId, " +
                "           p.name as processName," +
                "           p.display_Name as processDisplay, " +
                "           o.orderTitle," +
                "           o.order_State as orderState," +
                "           o.creator, " +
                "           u.userName as creatorDisplay," +
                "           o.create_Time as createTime, " +
                "           o.domain " +
                "      from wf_hist_order o left join wf_process p on p.id = o.process_Id " +
                "                           left join t_sys_user u on u.loginId = o.creator " +
                "       where o.delFlag='0' " +
                "   union all " +
                "       select i.processId, " +
                "           '' as orderId, " +
                "           i.id as taskInfoId, " +
                "           p.name as processName," +
                "           p.display_Name as processDisplay, " +
                "           i.orderTitle," +
                "           -1 as orderState," +
                "           i.operator as creator," +
                "           u.userName as creatorDisplay," +
                "           i.operateTime as createTime, " +
                "           i.domain " +
                "       from wf_hist_taskInfo i left join wf_process p on p.id = i.processId " +
                "                               left join t_sys_user u on u.loginId =i.operator " +
                "       where i.infoDraftUsed = 'N' and i.infoType='-1' " +
                ") t  " +
                "where t.domain=? and t.creator=? "
        );
        List<Object> sqlParamList = new ArrayList<Object>();
        sqlParamList.add(loginUserObj.getDomain());
        sqlParamList.add(loginUserObj.getLoginId());
        //拼接查询条件
        buildSearchFilterSql(loginUserObj, paraMap, sqlConditionSb, sqlParamList);
        //排序
        sqlConditionSb.append(" order by t.createTime desc ");
        //构造分页查询
        Object[] sqlParams = new Object[sqlParamList.size()];
        sqlParamList.toArray(sqlParams);
        return Db.paginate(pageNumber, pageSize, sqlSelect, sqlConditionSb.toString(), sqlParams);
    }

    /**
     * 我的待办列表, 包括待处理和抄送给我的工单, 不分页
     */
    @Override
    public Page<Record> myDealGridData(LoginUserObj loginUserObj, String[] userPriv, Map<String, String[]> paraMap){
        // 分页信息构造
        String page = CommonTools.getValueFromParaMap(paraMap, "page");//取得当前页数
        String rows = CommonTools.getValueFromParaMap(paraMap, "rows");//取得每页显示行数
        int pageNumber = CommonTools.isEmpty(page)? 1: Integer.parseInt(page);
        int pageSize = CommonTools.isEmpty(rows)? 10: Integer.parseInt(rows);
        //权限组集合查询处理
        if(userPriv==null || userPriv.length<=0){
            return null;
        }
        StringBuilder actorsSb = new StringBuilder();
        for(int i=0, len=userPriv.length; i<len ; i++){
            if(i != 0){
                actorsSb.append(", ");
            }
            actorsSb.append(" '").append(userPriv[i]).append("' ");
        }
        String actorsStr = actorsSb.toString();

        //拼接查询使用sql语句
        String sqlSelect =
                "select t.processId, t.orderId, " +
                "       t.taskId, t.taskName, t.taskDisplay," +
                "       t.processDisplay, t.processName, " +
                "       t.orderTitle, t.orderState, " +
                "       t.creatorDisplay, t.createTime, " +
                "       t.isCcOrderFlag ";
        StringBuilder sqlConditionSb = new StringBuilder(
                "from ( select p.id as processId, " +//我的待处理
                "	        t.order_Id as orderId, " +
                "	        t.id as taskId, " +
                "           t.task_Name as taskName, " +
                "           t.display_Name as taskDisplay, " +
                "	        p.name as processName, " +
                "	        p.display_Name as processDisplay, " +
                "	        o.orderTitle, " +
                "	        o.order_State as orderState, " +
                "	        o.creator, " +
                "	        u.userName as creatorDisplay, " +
                "	        o.create_Time as createTime,  " +
                "	        'false' as isCcOrderFlag,  " +
                "	        o.domain " +
                "        from wf_task t left join wf_hist_order o ON t.order_id = o.id " +
                "                       left join t_sys_user u on u.loginId = o.creator  " +
                "                       left join wf_process p ON p.id = o.process_id " +
                "       where t.task_Type = 0 " +
                "       and (exists(select 1 from  wf_task_actor ta where ta.task_id = t.id " +
                "                   and t.operator is null and ta.actor_Id IN (" + actorsStr + ") " +
                "               ) or t.operator IN (" + actorsStr + ")) " +
                "   union all " +
                "       select p.id as processId, " +//联合查询抄送给我的工单
                "	        cc.order_id as orderId, " +
                "	        '' as taskId, " +
                "           '' as taskName, " +
                "           '抄送' as taskDisplay, " +
                "	        p.name as processName, " +
                "	        p.display_Name as processDisplay, " +
                "	        o.orderTitle, " +
                "	        o.order_State as orderState, " +
                "	        o.creator, " +
                "	        u.userName as creatorDisplay, " +
                "	        o.create_Time as createTime,  " +
                "	        'true' as isCcOrderFlag,  " +
                "	        o.domain " +
                "       from wf_cc_order cc left join wf_hist_order o ON cc.order_id = o.id " +
                "                           left join t_sys_user u on u.loginId = o.creator  " +
                "                           left join wf_process p ON p.id = o.process_id " +
                "       where cc.status = 1 and cc.actor_Id IN (" + actorsStr + ")" +
                ") t where t.domain=? "
        );
        List<Object> sqlParamList = new ArrayList<Object>();
        sqlParamList.add(loginUserObj.getDomain());
        //拼接查询条件
        buildSearchFilterSql(loginUserObj, paraMap, sqlConditionSb, sqlParamList);
        //排序
        sqlConditionSb.append(" order by t.createTime desc ");
        //构造分页查询
        Object[] sqlParams = new Object[sqlParamList.size()];
        sqlParamList.toArray(sqlParams);
        return Db.paginate(pageNumber, pageSize, sqlSelect, sqlConditionSb.toString(), sqlParams);
    }

    /**
     * 执行工单数据查询时, 根据前端的过滤条件, 执行查询sql拼接的处理器
     * @param sqlSb         查询sql, 传入StringBuilder
     * @param sqlParamList  sql查询时涉及的注入参数集合
     */
    private void buildSearchFilterSql(LoginUserObj loginUserObj, Map<String, String[]> paraMap, StringBuilder sqlSb, List<Object> sqlParamList){
        // 查询条件获取
        String searchProcessName = CommonTools.getValueFromParaMap(paraMap, "searchProcessName");//流程英文名称
        String searchCreateTimeStart = CommonTools.getValueFromParaMap(paraMap, "searchCreateTimeStart");//发起时间(开始)
        String searchCreateTimeEnd = CommonTools.getValueFromParaMap(paraMap, "searchCreateTimeEnd");//发起时间(截至)
        String searchCreatorDisplay = CommonTools.getValueFromParaMap(paraMap, "searchCreatorDisplay");//工单发起人
        String searchOrderState = CommonTools.getValueFromParaMap(paraMap, "searchOrderState");//工单状态
        if(CommonTools.isNotEmpty(searchProcessName)){
            sqlSb.append(" and t.processName = ? ");
            sqlParamList.add(searchProcessName);
        }
        if(CommonTools.isNotEmpty(searchCreateTimeStart)){
            sqlSb.append(" and t.createTime >= ? ");
            sqlParamList.add(searchCreateTimeStart + " 00:00:00");
        }
        if(CommonTools.isNotEmpty(searchCreateTimeEnd)){
            sqlSb.append(" and t.createTime <= ? ");
            sqlParamList.add(searchCreateTimeEnd + " 23:59:59");
        }
        if(CommonTools.isNotEmpty(searchOrderState)){
            sqlSb.append(" and t.orderState = ? ");
            sqlParamList.add(searchOrderState);
        }
        if(CommonTools.isNotEmpty(searchCreatorDisplay)){
            sqlSb.append(" and t.creatorDisplay like ? ");
            sqlParamList.add("%" + searchCreatorDisplay + "%");
        }
    }

    /**
     * 按照工单ID, 先查询该工单下历史任务节点列表, 即查询wf_hist_task
     * 根据传入的是否包含活跃任务标识, 决定是否查询wf_task表
     * 注: 自己重写的方法, 使用原生sql查询
     */
    @Override
    public List<Record> historyGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap){
        String orderId = CommonTools.getValueFromParaMap(paraMap, "orderId");//工单ID
        String includeActive = CommonTools.getValueFromParaMap(paraMap, "includeActive");//是否包含活跃任务
        if(CommonTools.isEmpty(orderId)){
            return null;
        }
        //构造查询使用的sql
        String findSql =
                "select t.id, t.task_Name, t.display_Name, t.task_State, t.create_Time, t.finish_Time, t.operatorName " +
                "from (select ht.id, ht.task_Name, ht.display_Name, ht.task_State, ht.create_Time, ht.finish_Time," +
                "           u.userName as operatorName " +
                "       from wf_hist_task ht left join t_sys_user u on ht.operator = u.loginId " +
                "       where ht.order_Id=? " +
                "       order by ht.create_Time asc, ht.finish_Time asc " +
                ") t ";
        if("true".equals(includeActive)){
            //需要包含活跃任务, 则联合查询wf_task表
            findSql +=
                " union all " +
                "select k.id, k.task_Name, k.display_Name, '1' as task_State, k.create_Time, k.finish_Time," +
                "       u.userName as operatorName " +
                " from wf_task k left join t_sys_user u on k.operator = u.loginId " +
                "where k.order_Id= '" + orderId + "' ";
        }
        return Db.find(findSql, orderId);
    }

    /**
     * 请求流程图数据
     */
    @Override
    public Map<String, String> flowPictureData(String processId, String orderId) {
        Process process = engine.process().getProcessById(processId);
        AssertHelper.notNull(process);
        ProcessModel model = process.getModel();

        Map<String, String> pictureData = new HashMap<String, String>();
        if(model != null) {
            pictureData.put("process", SnakerHelper.getModelJson(model));
        }
        if(CommonTools.isNotEmpty(orderId)) {
            List<Task> tasks = engine.query().getActiveTasks(new QueryFilter().setOrderId(orderId));
            List<HistoryTask> historyTasks = engine.query().getHistoryTasks(new QueryFilter().setOrderId(orderId));
            pictureData.put("state", SnakerHelper.getStateJson(model, tasks, historyTasks));
        }
        return pictureData;
    }
}
