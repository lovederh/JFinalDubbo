package edu.dsideal.campus.snaker.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.FlowSource;
import edu.dsideal.campus.snaker.service.IFlowCustomService;
import edu.dsideal.campus.snaker.sql.FlowUserForm;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import org.snaker.engine.SnakerEngine;
import org.snaker.jfinal.plugin.SnakerPlugin;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.SYS_FLOWS_FILES_PATH;
import static edu.dsideal.campus.snaker.FlowConstDefine.USER_FLOWS_FILES_PATH;

/**
 * 流程用户定制服务类
 */
public class FlowCustomServiceImpl implements IFlowCustomService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /**
     * 所有的系统流程列表数据
     * 系统流程: /flows/sys目录下所有文件遍历
     */
    @Override
    public List<FlowSource> allSysFlowGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        List<FlowSource> developFlows = new ArrayList<FlowSource>();// 待安装的流程列表

        File[] flowFiles = findListFiles(SYS_FLOWS_FILES_PATH);
        // 遍历所有流程文件, 填充待安装流程列表
        if(flowFiles!=null && flowFiles.length>0){
            String fileName;//流程文件名
            FlowSource flowSource;//待安装流程对象
            for(File flowFile : flowFiles){
                fileName = flowFile.getName();
                //不为有效文件/文件名称不对, 那么会跳出本次循环
                if(flowFile.isDirectory() || !fileName.endsWith(".snaker")){
                    continue;
                }
                flowSource = new FlowSource();
                flowSource.setProcessType("sys");//流程类型
                flowSource.setProcessDisplay(fileName.substring(0, fileName.lastIndexOf(".")));//流程名
                flowSource.setFileName(fileName);//流程文件名称
                flowSource.setFileResource(SYS_FLOWS_FILES_PATH + "/" + fileName);//流程文件资源名
                developFlows.add(flowSource);
            }
        }
        return developFlows;
    }
    /**
     * 查询待安装的, 用户定制流程列表
     * 读取流程配置表wf_processSet中, 所有未安装列表数据
     */
    @Override
    public List<FlowSource> userNoInstallData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        List<FlowSource> developFlows = new ArrayList<FlowSource>();// 待安装的流程列表
        //获取该企业域下所有未安装的流程集合
        List<Record> undeployFlows = ProcessSetSql.findNoInstallUserFlowsByDomain(loginUserObj.getDomain());
        //遍历确认未安装列表中这些文件都是否有效
        if(undeployFlows!=null && !undeployFlows.isEmpty()){
            File flowFile;
            String fileName;
            FlowSource flowSource;//待安装流程对象
            for(Record record : undeployFlows){
                //确认流程文件是存在的, 不存在则跳出本次循环
                fileName = record.get("deployFile");
                flowFile = new File(FlowCustomServiceImpl.class.getClassLoader().getResource(USER_FLOWS_FILES_PATH).getPath() + "/" + fileName);
                if(!flowFile.exists() || flowFile.isDirectory()){
                    continue;
                }
                //当前流程文件存在, 则加入待安装列表
                flowSource = new FlowSource();
                flowSource.setProcessType("user");//流程类型
                flowSource.setProcessSetId("" + record.get("id"));
                flowSource.setProcessDisplay(record.get("processDisplay"));
                flowSource.setCustomType(record.get("customType"));
                flowSource.setCustomVersion("" + record.get("customVersion"));
                flowSource.setFormType(record.get("formType"));
                flowSource.setFormDisplay(record.get("formDisplay"));
                flowSource.setFileName(fileName);
                flowSource.setFileResource(USER_FLOWS_FILES_PATH + "/" + fileName);
                developFlows.add(flowSource);
            }
        }
        return developFlows;
    }


    /**
     * 遍历获取指定文件夹下的所有文件, 返回文件对象数组
     */
    private File[] findListFiles(String rootPath){
        URL url = FlowCustomServiceImpl.class.getClassLoader().getResource(rootPath);
        if(url == null){
            return null;
        }
        File root = new File(url.getPath());
        if(!root.exists() || root.isFile()) {
            return null;
        }
        return root.listFiles();
    }

    /**
     * 定制流程保存
     */
    @Before(Tx.class)
    public void saveCustomFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        //前端传过来的参数
        String id = CommonTools.getValueFromParaMap(paraMap, "id");//主键ID
        String processDisplay = CommonTools.getValueFromParaMap(paraMap, "processDisplay");//流程名称
        String customType = CommonTools.getValueFromParaMap(paraMap, "customType");//流程定制类型
        String isMultiForm = CommonTools.getValueFromParaMap(paraMap, "isMultiForm");//是否每个任务启用单独表单
        String expireDay = CommonTools.getValueFromParaMap(paraMap, "expireDay");//工单时限
        String formValue = CommonTools.getValueFromParaMap(paraMap, "formValue");//流程启动表单值

        String processName;//流程编码(英文名)
        int customVersion = 0;//定制版本号

        //根据主键, 获取流程配置记录
        Record processSet = null;
        if(CommonTools.isNotEmpty(id)){
            processSet = Db.findById("wf_processSet", id);
        }

        //是否需要新建流程文件标识
        boolean createFlowXmlFlag = false;

        if(null == processSet){
            // 新增时肯定要建一条数据
            createFlowXmlFlag = true;

            //流程名称使用企业域+时间戳方式
            processName = loginUserObj.getDomain() + "_" + CommonTools.getCurrentDateYmd() + "_" + System.currentTimeMillis();
            customVersion = 0;

        }else{
            processName = processSet.get("processName");
            customVersion = processSet.getInt("customVersion");
            //判断本次是否发生流程节点改动, 若发生改动, 则需要重新生成一个新流程
            if(!customType.equals(processSet.get("customType"))){
                createFlowXmlFlag = true;
            }


            if(createFlowXmlFlag){
                //取出当前流程下已定制的最高版本+1, 作为新定制版本号
                customVersion = Db.queryInt(
                    "select customVersion from wf_processSet " +
                    "where processName=? and domain=? " +
                    "order by customVersion desc limit 0, 1 ", processName, loginUserObj.getDomain()
                ) + 1;
            }

            //工单时限发生变更, 需要新建流程版本
        }


        if(createFlowXmlFlag){
            //生成流程图
            //流程启动url参数需要拼接流程名称

            String flowFileName = processName + customVersion  + ".snaker";

            processSet = new Record();
            processSet.set("processName", processName);
            processSet.set("customVersion", customVersion);
            processSet.set("isDeploy", "N");
            processSet.set("deployFile", flowFileName);
            processSet.set("deployFilePath", "flows/user/" + flowFileName);
            processSet.set("formType", "user");//表单类型均为自定制表单
        }

        processSet.set("processType", "user");//用户定制类型
        processSet.set("processDisplay", processDisplay);
        processSet.set("customType", customType);
        processSet.set("isMultiForm", isMultiForm);
        processSet.set("formValue", formValue);

        FlowUserForm.putFormInfo2ProcessSet(processSet, formValue, false);
        if(CommonTools.isNotEmpty(expireDay)){
            processSet.set("expireDay", expireDay);
        }
        processSet.set("domain", loginUserObj.getDomain());//企业域一定要保存


        //TODO 流程环节配置表wf_processTaskSet需要修改
        //同步保存processSetId

        //TODO 流程与任务操作人配置表wf_processTaskActorSet需要修改
        //同步保存processSetId

        //TODO 流程内决策节点配置wf_processDecisionNode需要修改
        //同步保存processSetId


        if(createFlowXmlFlag){
            Db.save("wf_processSet", processSet);
        }else{
            Db.update("wf_processSet", processSet);
        }


        //更改流程配置本表
        if(CommonTools.isNotEmpty(processSet.get(ATTR_PROCESS_ID))){
            Db.update("update wf_process set display_Name=? where id=? ", processDisplay, processSet.getStr(ATTR_PROCESS_ID));
        }
    }
}
