package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.cache.SnakerCacheManager;

import java.util.List;

/**
 * wf_processSet表主要操作
 * 2017/1/19
 */
public class ProcessSetSql {

    /**************************************单表查询*************************************/
    /**
     * 根据流程配置表ID, 取出一条流程配置信息
     */
    public static Record findSetById(String processSetId){
        if(CommonTools.isEmpty(processSetId)){
            return null;
        }
        return Db.findById("wf_processSet", processSetId);
    }

    /**
     * 通过已经安装过的流程ID, 获取流程配置信息(wf_processSet表)
     */
    public static Record findSetByProcessId(String processId){
        if(CommonTools.isEmpty(processId)){
            return null;
        }
        //先启用缓存查找
        Record set = (Record)(SnakerCacheManager.getCache("processSet_" + processId));
        if(set == null){
            set = Db.findFirst("select * from wf_processSet where processId=? ", processId);
        }
        return set;
    }

    /**
     * 根据流程定义名和企业域, 查找最新版本的该流程配置信息
     */
    public static Record findSetByDomainAndProcessName(String domain, String processName){
        if(CommonTools.isEmpty(processName)){
            return null;
        }
        return Db.findFirst(
                "select s.* " +
                "from wf_processSet s, wf_process p " +
                "where s.processId=p.id and s.isDeploy='Y' and p.state=1 and s.domain=? and p.name=? " +
                "order by p.version desc ", domain, processName);
    }


    /*************************************关联wf_process查询*************************************/
    /**
     * 已安装流程中, 所有企业域下, 可用的流程配置(企业域分组时最高版本流程)
     * 条件: 满足企业域分组条件下, 同名流程取出最高版本的那个
     */
    public static List<Record> findAllDomainUsingFlows(){
        return Db.find(
                "select s.* " +
                "from wf_process p left join wf_processSet s on s.processId=p.id " +
                "where exists(select 1 from(select max(p.version) as maxVersion, p.name, s.domain " +//匹配同名流程下最高版本
                "                           from wf_process p left join wf_processSet s on s.processId=p.id" +
                "                           where p.state = 1 and s.isDeploy = 'Y' " +
                "                           group by p.name, s.domain " +
                "                   )t where t.maxVersion=p.version and t.name=p.name and s.domain=t.domain) "
        );
    }


    /**
     * 找到指定企业域中, 最高版本流程的列表(即该企业域内所有可申请流程)
     */
    public static List<Record> findMaxVersionFlowsByDomain(String domain){
        String findSql =
                "select p.id as processId," +
                "       p.name as processName, " +
                "       p.display_Name as processDisplay," +
                "       s.processImg " +
                " from wf_process p left join wf_processSet s on s.processId=p.id " +
                "where exists(select 1 from(select max(p.version) as maxVersion, p.name " +//匹配同名流程下最高版本
                "                           from wf_process p left join wf_processSet s on s.processId=p.id" +
                "                           where p.state = 1 and s.isDeploy = 'Y' and s.domain = ? " +
                "                           group by p.name" +
                "                   )t where t.maxVersion=p.version and t.name=p.name) ";
        return Db.find(findSql, domain);
    }

    /**
     * 查询未安装的, 用户定制流程列表
     * 读取流程配置表wf_processSet中, 所有未安装列表数据
     */
    public static List<Record> findNoInstallUserFlowsByDomain(String domain){
        return Db.find(
                "select s.id, s.processDisplay, " +
                "       s.customType, s.customVersion, " +
                "       s.formType, f.formName as formDisplay, " +
                "       s.deployFile " +
                "from wf_processSet s left join t_sys_userForm f on f.id = s.formValue " +
                "where s.isDeploy='N' and s.processType='user' and s.domain=? ", domain);
    }

    /*************************************通过流程文件名查询*************************************/
    /**
     * 依据类型, 查询某指定流程对应的企业域安装情况
     */
    public static List<Record> findFlowDomainsByDeployFileAndType(String deployFile, String type){
        String sql ="select d.id, d.companyName, d.loginID, d.companyAdd, d.tel, d.corporation " +
                    "from t_sys_systemconfig d " +
                    "where isPass = '通过' " +
                    "and id ";
        //查询类型: install已经装过的/noInstall没有安装过的
        if("noInstall".equals(type)){
            sql += "not";
        }
        sql +=      " in ( select s.domain from wf_processSet s, wf_process p" +
                    "      where p.state=1 and s.isDeploy='Y' and s.processType='sys' and s.deployFile=? ) ";
        return Db.find(sql, deployFile);
    }

    /**
     * 通过流程安装时文件名, 找到所有的已安装系统流程列表
     * 可以使用企业域过滤
     */
    public static List<Record> findDeployedSysByDeployFileAndDomains(String deployFile, String domains){
        String sql ="select s.id, s.processId " +
                    "from wf_processSet s, wf_process p " +
                    "where p.state=1 and s.isDeploy='Y' and s.processType='sys' and s.deployFile=? ";
        if(CommonTools.isNotEmpty(domains)){
            domains = domains.replace(",", "','");
            sql += " and domain in('" + domains + "')";
        }
        return Db.find(sql, deployFile);
    }
}
