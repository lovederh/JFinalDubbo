package edu.dsideal.campus.snaker.patterns;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;
import edu.dsideal.campus.snaker.bean.TaskFormAttr;
import edu.dsideal.campus.snaker.interceptor.FlowInterceptorTemp;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import edu.dsideal.campus.snaker.sql.FlowUserForm;
import edu.dsideal.campus.snaker.sql.ProcessTaskActorSql;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 任务环节处理表单展示对象的包装类
 * 设计模式: 模板方法模式
 */
public abstract class ITaskFormAttrWrapper {

    /**
     * 任务环节表单对象, 包装后返回的最终对象
     * 供执行任务环节的表单展示使用
     */
    protected TaskFormAttr formAttr = new TaskFormAttr();

    //流程定义ID
    public String processId;
    //工单ID
    public String orderId;
    //任务ID
    public String taskId;
    //任务英文名称
    public String taskName;
    //展示类型:view展示明细; form:展示表单(无值时默认)
    protected String showType;

    //登录人对象
    public LoginUserObj loginUserObj;
    //当前流程信息配置
    public Record processSet;
    //当前所处任务节点配置
    public Record processTaskSet;
    //流程明细查看时使用的拦截器
    protected FlowInterceptorTemp interceptor = null;

    //打开当前处理表单时选择的审批结果
    public String approvalResult = "";
    //任务节点基本信息
    protected Record taskInfo;
    //节点业务数据(用户表单数据)
    protected Record businessBean;
    //其它数据, 可以任意构造(符合json格式即可, 便于前端使用)
    private Object otherData = null;

    //用户自定义表单时有效, 为前端使用的表单html字符串
    protected String formHtmlObj = "";
    //最终url处理方式(forward请求转发/render打开页面-默认)
    protected String formUrlType = "render";
    //展示环节表单的url地址
    protected String formUrl;
    //各类型的默认表单url, 子类必须赋值(处理过程中若表单url为空, 则使用此值)
    protected String defaultFormUrl;


    //紧邻当前环节的处理环节下, 处理者集合
    List<ProcessTaskAllActorsObj> nextTaskActors = new ArrayList<ProcessTaskAllActorsObj>();

    /**
     * 通过处理传入的基础信息, 构造并返回任务环节表单展示对象
     * 实现步骤:
     *  (1)如果为新派工单情况, 需要确认工单主题
     *  (2)确认及填充任务节点信息和业务数据
     *  (3)确认本次打开表单使用的最终url
     *  (4)TaskFormAttr对象, 各个属性赋值并返回
     */
    public TaskFormAttr buildTaskFormAttr() throws Exception{
        //构造工单查看对象之前的前置拦截器
        beforeViewInterceptor();

        //表单及业务数据填充, 且各子类会整合最终的表单url
        initFormAttrWrapper();

        //获取紧邻当前环节的处理环节下, 处理者集合
        boolean getNextActors = checkGetNextActorsFlag();
        if(getNextActors){
            initNextTaskActors();
        }

        //处理最终的url, 如果表单url为空, 则使用默认值
        if(CommonTools.isEmpty(formUrl)){
            if(CommonTools.isEmpty(defaultFormUrl)){
                throw new RuntimeException("展示表单的默认url未绑定, 无法跳转!");
            }
            formUrl = defaultFormUrl;
            formUrlType = "render";
        }

        //构造工单查看对象之后的后置拦截器
        afterViewInterceptor();

        //通用的数据填充
        formAttr.setApprovalResult(approvalResult);
        formAttr.setBusinessBean(businessBean);
        formAttr.setFormHtmlObj(formHtmlObj);
        formAttr.setFormUrl(formUrl);
        formAttr.setFormUrlType(formUrlType);
        formAttr.setProcessSet(processSet);
        formAttr.setProcessTaskSet(processTaskSet);
        formAttr.setTaskInfo(taskInfo);
        formAttr.setNextTaskActors(nextTaskActors);
        formAttr.setOtherData(otherData);
        return formAttr;
    }

    /**
     * 个性化装饰任务环节表单对象
     * 交给各子类处理的
     */
    protected abstract void initFormAttrWrapper() throws Exception;

    /**
     * 获取紧邻当前环节的处理环节下, 处理者集合, 只在工单处理时需要获取
     * 如果处理时, 有额外的操作, 子类可以覆盖该方法
     */
    protected void initNextTaskActors() throws Exception{
        //获取当前环节下, 紧邻的处理环节名称(使用紧邻的环节英文名toTaskName关联配置表)
        List<Record> nextTasks = Db.find(
                "select t.toTaskName as taskName, s.taskDisplay, s.id as taskSetId " +
                "from wf_processTaskTo t left join wf_processTaskSet s on s.taskName=t.toTaskName and t.processId=s.processId " +
                "where t.taskName=? and t.processId=? ", taskName, processId);
        if(nextTasks!=null && !nextTasks.isEmpty()){
            for(Record nextTask : nextTasks){
                nextTaskActors.add(ProcessTaskActorSql.buildTaskActorsObjBySetId(nextTask, loginUserObj.getDomain()));;
            }
        }
    }

    /**
     * 整合最终扩展的表单url, 供子类调用
     * 子类自己判断是否需要扩展表单, 如果无需扩展, 那么也无需调用
     * 如果当前提供的方法不满足子类, 则子类可以覆盖这个方法
     * 本质上两种情况下(1)第一环节(2)多任务多表单开启, 并且审批结果不为不通过
     */
    protected void initExtFormUrl(String taskFormValue) throws Exception{
        //当前环节表单对象
        Record taskForm = FlowUserForm.findUserFormById(taskFormValue);
        if(taskForm!=null && "sys".equals(taskForm.get("formType"))){
            //展示默认表单, 是一个url形式, 请求方式为跳转(forward)
            formUrl = taskForm.get("formUrl");
            formUrlType = "forward";
        } else {
            //赋最终的前端html字符串对象
            formHtmlObj = FlowUserForm.findFormHtmlObj(taskForm, showType, businessBean);
        }
    }

    /**
     * 构造工单查看对象之前的前置拦截器, 默认触发配置的前置拦截
     */
    protected void beforeViewInterceptor() throws Exception{
        //明细时候需要初始化拦截器(拦截器包含工单查看的拦截方法)
        if("view".equals(showType)){
            initFlowInterceptor();
            if(interceptor != null) {
                //拦截器处理, 环节处理之前
                interceptor.doBeforeView();
            }
        }
    }

    /**
     * 构造工单查看对象之后的后置拦截器, 默认触发配置的后置拦截
     */
    protected void afterViewInterceptor() throws Exception{
        if(interceptor != null) {
            //拦截器处理, 环节处理后置拦截
            interceptor.doAfterView();
        }
    }

    protected void initFlowInterceptor() throws Exception{
        //初始化拦截器
        interceptor = FlowSqlUtil.initFlowInterceptor(processSet);
        if(interceptor != null){
            //反射调用set方法赋值
            Method setFunc = interceptor.getClass().getMethod("setFormAttrWrapper", ITaskFormAttrWrapper.class);
            setFunc.invoke(interceptor, this);
        }
    }

    /**
     * 是否需要获取紧邻当前环节的处理环节下, 处理者集合标示
     * 由各个子类确定该标识的值
     */
    protected abstract boolean checkGetNextActorsFlag() throws Exception;

    public TaskFormAttr getFormAttr() {
        return formAttr;
    }
    public void setFormAttr(TaskFormAttr formAttr) {
        this.formAttr = formAttr;
    }
    public Record getTaskInfo() {
        return taskInfo;
    }
    public void setTaskInfo(Record taskInfo) {
        this.taskInfo = taskInfo;
    }
    public Record getBusinessBean() {
        return businessBean;
    }
    public void setBusinessBean(Record businessBean) {
        this.businessBean = businessBean;
    }
    public String getFormHtmlObj() {
        return formHtmlObj;
    }
    public void setFormHtmlObj(String formHtmlObj) {
        this.formHtmlObj = formHtmlObj;
    }
    public String getFormUrlType() {
        return formUrlType;
    }
    public void setFormUrlType(String formUrlType) {
        this.formUrlType = formUrlType;
    }
    public String getFormUrl() {
        return formUrl;
    }
    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }
    public String getDefaultFormUrl() {
        return defaultFormUrl;
    }
    public void setDefaultFormUrl(String defaultFormUrl) {
        this.defaultFormUrl = defaultFormUrl;
    }
    public FlowInterceptorTemp getInterceptor() {
        return interceptor;
    }
    public void setInterceptor(FlowInterceptorTemp interceptor) {
        this.interceptor = interceptor;
    }
    public String getShowType() {
        return showType;
    }
    public void setShowType(String showType) {
        this.showType = showType;
    }

    public Object getOtherData() {
        return otherData;
    }
    public void setOtherData(Object otherData) {
        this.otherData = otherData;
    }
    public List<ProcessTaskAllActorsObj> getNextTaskActors() {
        return nextTaskActors;
    }
    public void setNextTaskActors(List<ProcessTaskAllActorsObj> nextTaskActors) {
        this.nextTaskActors = nextTaskActors;
    }
}
