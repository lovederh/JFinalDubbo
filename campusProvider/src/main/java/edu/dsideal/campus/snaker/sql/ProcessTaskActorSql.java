package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.bean.ProcessTaskActors;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;

import java.util.ArrayList;
import java.util.List;

import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_ROLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_SELF_PICK;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_TEACHER;

/**
 * wf_processTaskActorSet表主要操作
 * 2017/1/23
 */
public class ProcessTaskActorSql {

    //构造人员(教师)选择双选器时使用的sql语句头
    private static final String PICK_TEACHER_SQL_HEAD =
            "select concat('teacher_', u.id) as id," +
            "       u.loginId as actorValue, " +
            "       u.userName as actorDisplay, " +
            "       '" + ACTOR_TYPE_TEACHER + "' as actorType, " +
            "       '教师' as actorTypeDisplay, " +
            "       u.phone as phone, " +
            "       u.cardNo as cardNo, " +
            "       o.orgName " +
            "  from t_sys_user u left join t_sys_org o on o.codeId = u.orgId ";

    /**
     * 根据当前选中的树节点(部门ID), 查询获取可供用户选择的人员(教师)列表
     * 处理者配置时使用的方法, 构造左侧双选器
     */
    public static List<Record> findAllTeacherByOrgId(String orgId, String searchKey, String domain) {
        StringBuilder sqlSb = new StringBuilder(PICK_TEACHER_SQL_HEAD).append(
            " where u.isUsed='1' " +
            //"and u.userType ='1' " +
            "and u.domain =? "
        );
        List<Object> sqlParamList = new ArrayList<Object>();
        sqlParamList.add(domain);
        if (CommonTools.isNotEmpty(orgId)) {
            sqlSb.append(" and o.codeId like ? ");
            sqlParamList.add(orgId + "%");
        }
        if (CommonTools.isNotEmpty(searchKey)) {
            //姓名/登录名/身份证号模糊查询
            sqlSb.append(" and (u.userName like ? or u.loginId like ? or u.cardNo like ?) ");
            sqlParamList.add("%" + searchKey + "%");
            sqlParamList.add("%" + searchKey + "%");
            sqlParamList.add("%" + searchKey + "%");
        }
        sqlSb.append(" order by u.userName ");
        Object[] sqlParams = new Object[sqlParamList.size()];
        sqlParamList.toArray(sqlParams);
        return Db.find(sqlSb.toString(), sqlParams);
    }


    /**
     * 通过流程任务配置ID, 关联查询用户表, 返回环节处理者中的所有人员(教师)
     * 基于配置查找处理者使用, 构造处理者双选器右侧
     */
    private static List<Record> findTeacherActorsByTaskSetId(Object taskSetId, String domain){
        return Db.find(
                PICK_TEACHER_SQL_HEAD +
                " where u.isUsed='1' and u.domain=? " +
                "   and exists (select 1 from wf_processTaskActorSet a " +
                "               where a.actorValue=u.loginId " +
                "               and a.actorType=? and a.processTaskSetId=?) " +
                " order by u.userName ",
            domain, ACTOR_TYPE_TEACHER, taskSetId
        );
    }

    //构造角色选择双选器时使用的sql语句头
    private static final String PICK_ROLE_SQL_HEAD =
            "select concat('role_', r.id) as id," +
            "       r.id as actorValue, " +
            "       r.roleName as actorDisplay, " +
            "       '" + ACTOR_TYPE_ROLE + "' as actorType, " +
            "       '角色' as actorTypeDisplay, " +
            "       '-' as phone, " +
            "       '-' as cardNo, " +
            "       '-' as orgName " +
            "  from t_sys_role r ";

    /**
     * 通过流程任务配置ID, 关联查询角色表, 返回环节处理者中的所有角色
     * 基于配置查找处理者使用, 构造处理者双选器右侧
     */
    private static List<Record> findRoleActorsByTaskSetId(Object taskSetId, String domain){
        return Db.find(
                PICK_ROLE_SQL_HEAD +
                " where r.level<>'0' and r.level<>'1' and r.domain=? " +
                "   and exists (select 1 from wf_processTaskActorSet a " +
                "               where a.actorValue=r.id " +
                "               and a.actorType=? and a.processTaskSetId=?) " +
                " order by r.pId, r.ord, r.roleName ",
            domain, ACTOR_TYPE_ROLE, taskSetId
        );
    }

    /**
     * 根据当前选中的树节点(总角色ID), 查询获取可供流程选择的角色列表(即查出企业域下所有角色)
     * 处理者配置时使用的方法, 构造左侧双选器
     */
    public static List<Record> findAllDomainRole(String roleId, String searchKey, String domain){
        StringBuilder sqlSb = new StringBuilder(PICK_ROLE_SQL_HEAD).append(
                " where r.level<>'0' and r.level<>'1' and r.domain=? "
        );
        List<Object> sqlParamList = new ArrayList<Object>();
        sqlParamList.add(domain);
        if (CommonTools.isNotEmpty(roleId)) {
            sqlSb.append(" and r.codeId like ? ");
            sqlParamList.add(roleId + "%");
        }
        if (CommonTools.isNotEmpty(searchKey)) {
            sqlSb.append(" and r.roleName like ? ");
            sqlParamList.add("%" + searchKey + "%");
        }
        sqlSb.append(" order by r.pId, r.ord, r.roleName ");
        Object[] sqlParams = new Object[sqlParamList.size()];
        sqlParamList.toArray(sqlParams);
        return Db.find(sqlSb.toString(), sqlParams);
    }

    /**
     * 通过流程ID和任务环节英文名称以及处理者类型, 获取环节处理人列表
     * 排除处理这类型为[手动选择]的值
     */
    public static List<Record> findActorsByTaskNameAndActorType(String processId, String taskName, String actorType) {
        return Db.find(
                "select s.actorType, s.actorValue " +
                "from wf_processTaskActorSet s " +
                "where s.actorType<>? and actorType=? and s.taskName=? and s.processId=? ",
            ACTOR_TYPE_SELF_PICK, actorType, taskName, processId
        );
    }


    /**
     * 通过流程ID和任务环节英文名称, 获取环节处理对象ProcessTaskActors
     */
    public static ProcessTaskAllActorsObj buildActorsObjByTaskName(String processId, String taskName){
        //先获取配置的角色(actorType=role)
        List<Record> roleList = findActorsByTaskNameAndActorType(processId, taskName, ACTOR_TYPE_ROLE);
        ProcessTaskActors roleActors = new ProcessTaskActors(roleList, ACTOR_TYPE_ROLE);
        //再获取配置的教师人员(actorType=1)
        List<Record> teacherList = findActorsByTaskNameAndActorType(processId, taskName, ACTOR_TYPE_TEACHER);
        ProcessTaskActors teacherActors = new ProcessTaskActors(teacherList, ACTOR_TYPE_TEACHER);
        return new ProcessTaskAllActorsObj(roleActors, teacherActors);
    }

    /**
     * 通过环节配置(主要包含taskSetId/环节英文名/环节中文名)对象和企业域
     * 获取满足当前环节配置的环节处理对象ProcessTaskActors
     */
    public static ProcessTaskAllActorsObj buildTaskActorsObjBySetId(Record taskSet, String domain){
        Object taskSetId = taskSet.get("taskSetId");
        //先获取配置的角色(actorType=role)
        List<Record> roleList = findRoleActorsByTaskSetId(taskSetId, domain);
        ProcessTaskActors roleActors = new ProcessTaskActors(roleList, ACTOR_TYPE_ROLE);
        //再获取配置的教师人员(actorType=1)
        List<Record> teacherList = findTeacherActorsByTaskSetId(taskSetId, domain);
        ProcessTaskActors teacherActors = new ProcessTaskActors(teacherList, ACTOR_TYPE_TEACHER);
        return new ProcessTaskAllActorsObj(taskSet, roleActors, teacherActors);
    }
}
