package edu.dsideal.campus.snaker.patterns;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.ProcessDecisionNodeWrapper;
import edu.dsideal.campus.snaker.cache.SnakerCacheManager;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.entity.Process;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;

/**
 * 流程整体配置对象
 * 包含: 整体配置/处理环节配置/判断节点配置
 * 2017/1/20
 */
public abstract class ProcessAllNodeSet {

    //企业域登录人对象
    public LoginUserObj loginUserObj;
    //流程引擎处理对象
    public SnakerEngine engine;
    //流程文件资源(路径+名称)
    public String fileResource;
    //安装流程使用的文件名
    protected String fileName;
    //流程使用的拦截器
    public Map<String, String> sysFlowConfig;

    //流程定义对象(snaker提供的)
    protected Process process;
    //流程定义ID
    protected String processId;
    //流程定义英文名
    protected String processName;

    //流程主配置信息
    protected Record processSet;
    //流程信息配置ID
    protected Object processSetId;

    //流程内决策节点总集(与流程图内决策节点数目相同)
    protected List<ProcessDecisionNodeWrapper> processDecisionNode = new ArrayList<ProcessDecisionNodeWrapper>();;

    /**
     * 安装流程, 并保存相关的所有流程配置
     * 注: 系统流程与用户自定制流程分别处理
     */
    public void deployProcessSaveAllSet(InputStream input) throws Exception{
        //执行流程安装
        processId = engine.process().deploy(input, loginUserObj.getLoginId());

        process = engine.process().getProcessById(processId);//流程定义对象(snaker提供的)
        processName = process.getName();//流程定义英文名
        fileName = fileResource.substring(fileResource.lastIndexOf("/") + 1);//安装流程使用的文件名

        //保存流程整体配置(wf_processSet)
        saveFlowSetEndDeploy();
        //更新缓存
        SnakerCacheManager.setCache("processSet_" + processId, processSet);

        //保存所有的可处理任务节点配置(wf_processTaskSet)
        saveFlowTaskSetEndDeploy();

        //保存所有的决策类型节点配置(wf_processDecisionNode)
        saveFlowDecisionsEndDeploy();
    }


    /**
     * 保存流程整体配置(wf_processSet)
     */
    protected abstract void saveFlowSetEndDeploy() throws Exception;

    /**
     * 保存所有的可处理任务节点配置(wf_processTaskSet)
     */
    protected abstract void saveFlowTaskSetEndDeploy() throws Exception;

    /**
     * 保存所有的判断类型节点配置(wf_processDecisionNode)
     */
    protected abstract void saveFlowDecisionsEndDeploy() throws Exception;

    /**
     * 基于processBaseSet对象, 为后续保存的配置对象注入流程配置的基础信息
     */
    public static void iocProcessBaseSet(Record set, Object processId, Object processName, Object processSetId, Object domain){
        set.set(ATTR_PROCESS_ID, processId);
        set.set(ATTR_PROCESS_NAME, processName);
        set.set("processSetId", processSetId);
        set.set("domain", domain);
    }


    public Process getProcess() {
        return process;
    }
    public void setProcess(Process process) {
        this.process = process;
    }
    public String getProcessId() {
        return processId;
    }
    public void setProcessId(String processId) {
        this.processId = processId;
    }
    public String getProcessName() {
        return processName;
    }
    public void setProcessName(String processName) {
        this.processName = processName;
    }
    public Record getProcessSet() {
        return processSet;
    }
    public void setProcessSet(Record processSet) {
        this.processSet = processSet;
    }
    public Object getProcessSetId() {
        return processSetId;
    }
    public void setProcessSetId(Object processSetId) {
        this.processSetId = processSetId;
    }
    public List<ProcessDecisionNodeWrapper> getProcessDecisionNode() {
        return processDecisionNode;
    }
    public void setProcessDecisionNode(List<ProcessDecisionNodeWrapper> processDecisionNode) {
        this.processDecisionNode = processDecisionNode;
    }
}
