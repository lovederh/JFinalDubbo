package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;

import java.util.List;

/**
 * wf_processTaskSet表主要操作
 * 2017/1/21
 */
public class ProcessTaskSetSql {
    /**
     * 通过流程信息配置ID(processSetId), 查询取出流程与任务节点配置列表
     */
    public static List<Record> findSetListByProcessSetId(Object processSetId){
        String sql ="select s.taskDisplay, s.isStartTask, s.id " +
                    "from wf_processTaskSet s where processSetId=? ";
        return Db.find(sql, processSetId);
    }

    /**
     * 通过流程ID和环节信息集合, 获取流程与任务节点配置信息(wf_processTaskSet表)
     * 注: 环节信息中存放流程名称或ID, 存放其一即可
     */
    public static Record findSetByTaskNameOrId(String processId, String taskName, String... taskIds){
        if(CommonTools.isNotEmpty(processId)){
            String sql ="select * from wf_processTaskSet " +
                    "where processId=? and taskName";
            Object[] paras;
            if(CommonTools.isNotEmpty(taskName)){
                sql += " =? ";
                paras = new Object[]{processId, taskName};
            }else if(taskIds!=null && taskIds.length>0 && CommonTools.isNotEmpty(taskIds[0])){
                //任务ID可能存在于wf_hist_task/wf_task这两张表内
                sql +=  " in(" +
                        "   (select task_Name from wf_hist_task where id=?), " +
                        "   (select task_Name from wf_task where id=?)" +
                        ") ";
                paras = new Object[]{processId, taskIds[0], taskIds[0]};
            }else{
                return null;
            }
            return Db.findFirst(sql, paras);
        }
        return null;
    }
}
