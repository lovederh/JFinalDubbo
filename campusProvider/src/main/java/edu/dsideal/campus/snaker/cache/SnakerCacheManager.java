package edu.dsideal.campus.snaker.cache;

import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.redis.Cache;
import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;

import java.util.List;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;

/**
 * 已安装的流程配置数据使用的缓存处理类
 * 2017/1/19
 */
public class SnakerCacheManager {
    /**
     * 缓存对象使用名称
     */
    private static final String SNAKER_CACHE_NAME = "snaker";


    public static void loadCache(){
        //遍历所有已安装流程, 把这些流程的基本配置processSet缓存起来
        cacheProcessSetList();
    }

    public static Cache initCache(){
        return Redis.use(SNAKER_CACHE_NAME);
    }

    public static void setCache(String key, Object value){
        Cache cache = initCache();
        cache.set(key, value);
    }

    public static Object getCache(String key){
        Cache cache = initCache();
        return cache.get(key);
    }

    public static void delCache(String key){
        Cache cache = initCache();
        cache.del(key);
    }

    /**
     * 已安装流程中, 所有企业域下, 可用的流程配置processSet缓存
     */
    private static void cacheProcessSetList(){
        List<Record> setList = ProcessSetSql.findAllDomainUsingFlows();
        if(setList!=null && !setList.isEmpty()){
            for(Record set : setList){
                setCache("processSet_" + set.get(ATTR_PROCESS_ID), set);
            }
        }
    }
}
