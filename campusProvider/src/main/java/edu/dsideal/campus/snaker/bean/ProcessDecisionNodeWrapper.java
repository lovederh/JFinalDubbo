package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.snaker.patterns.ProcessAllNodeSet;
import org.snaker.engine.model.DecisionModel;
import org.snaker.engine.model.EndModel;
import org.snaker.engine.model.NodeModel;
import org.snaker.engine.model.StartModel;
import org.snaker.engine.model.TransitionModel;
import org.snaker.engine.model.WorkModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;

/**
 * 流程图内决策类型节点对象
 * 2017/1/14
 */
public class ProcessDecisionNodeWrapper {
    //流程配置基础对象, 包含processSetId/processId/processName/domain
    protected Record processBaseSet;

    //流程内决策节点整体对象
    protected Record decisionNode;
    //流程内决策节点ID
    protected Object decisionNodeId;

    //决策节点来源的环节表(递归查询到可处理的task)
    protected List<Record> decisionFromList = new ArrayList<Record>();
    //决策条件配置列表
    protected List<Record> decisionsList = new ArrayList<Record>();


    public ProcessDecisionNodeWrapper(Record processBaseSet) {
        this.processBaseSet = processBaseSet;
    }

    /**
     * 每个决策类型节点的配置保存方法
     * 包括: 决策节点整体对象/决策节点来源的环节/决策条件的配置
     */
    public void saveSysFlowDecisionNode(DecisionModel decisionModel) throws Exception{
        decisionNode = new Record();
        iocProcessBaseSet(decisionNode);
        decisionNode.set("decisionNodeName", decisionModel.getName());
        decisionNode.set("decisionNodeDisplay", decisionModel.getDisplayName());
        Db.save("wf_processDecisionNode", decisionNode);
        decisionNodeId = decisionNode.get("id");

        Record temp;
        //插入流程内决策条件
        List<TransitionModel> outTransitions = decisionModel.getOutputs();
        for(TransitionModel transition : outTransitions){
            temp = new Record();
            iocProcessBaseSet(temp);
            temp.set("decisionNodeId", decisionNodeId);//总决策节点ID
            temp.set("toTransition", transition.getName());
            temp.set("toTransitionDisplay", transition.getDisplayName());
            decisionsList.add(temp);
        }
        Db.batchSave("wf_processDecisions", decisionsList, decisionsList.size());

        //插入决策节点来源的环节信息
        List<TransitionModel> inTransitions = decisionModel.getInputs();
        List<Map<String, String>> frontTasks = findDecisionFrontTasks(inTransitions);
        for(Map<String, String> frontTask : frontTasks){
            temp = new Record();
            iocProcessBaseSet(temp);
            temp.set("decisionNodeId", decisionNodeId);//总决策节点ID
            temp.set("fromTaskName", frontTask.get("taskName"));
            temp.set("fromTaskDisplay", frontTask.get("taskDisplay"));
            decisionFromList.add(temp);
        }
        Db.batchSave("wf_processDecisionFrom", decisionFromList, decisionFromList.size());
    }

    /**
     * 基于processBaseSet对象, 为后续保存的配置对象注入流程配置的基础信息
     */
    private void iocProcessBaseSet(Record set){
        ProcessAllNodeSet.iocProcessBaseSet(set,
                processBaseSet.get(ATTR_PROCESS_ID), processBaseSet.get(ATTR_PROCESS_NAME),
                processBaseSet.get("processSetId"), processBaseSet.get("domain")
        );
    }
    /**
     * 找到前置的任务环节对象
     */
    private List<Map<String, String>> findDecisionFrontTasks(List<TransitionModel> inTransitions, List<Map<String, String>>... hasFrontTasks) throws Exception{
        List<Map<String, String>> frontTasks;
        if(hasFrontTasks==null || hasFrontTasks.length<=0 || null==hasFrontTasks[0]){
            frontTasks = new ArrayList<Map<String, String>>();
        }else{
            frontTasks = hasFrontTasks[0];
        }
        if(inTransitions==null || inTransitions.isEmpty()){
            return frontTasks;
        }
        //根据流程指向, 确认当前决策节点的前置任务环节
        NodeModel model;
        Map<String, String> taskMap;
        for(TransitionModel transition : inTransitions){
            model = transition.getSource();
            //确认当前节点的前置是否为任务环节
            if(model instanceof WorkModel){
                taskMap = new HashMap<String, String>();
                taskMap.put("taskName", model.getName());
                taskMap.put("taskDisplay", model.getDisplayName());
                frontTasks.add(taskMap);
            } else if(model instanceof StartModel){
                //一直查到了开始/结束节点, 说明流程图配置有问题
                throw new Exception("配置异常: 决策环节不能立即出现在起始环节之后!");
            } else if(model instanceof EndModel){
                throw new Exception("配置异常: 决策环节不能出现在结束环节之后!");
            }else{
                //前置不为任务环节, 则递归向前找
                frontTasks = findDecisionFrontTasks(model.getInputs(), frontTasks);
            }
        }
        return frontTasks;
    }
}
