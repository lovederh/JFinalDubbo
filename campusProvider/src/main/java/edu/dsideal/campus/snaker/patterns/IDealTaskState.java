package edu.dsideal.campus.snaker.patterns;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.Form2BusinessBeanObj;
import edu.dsideal.campus.snaker.interceptor.FlowInterceptorTemp;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import org.snaker.engine.SnakerEngine;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_ROLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_TEACHER;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_FORM_TYPE_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_FORM_VALUE_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_LOGIN_DOMAIN_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_LOGIN_USER_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_RECORD_ID_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_TABLE_NAME_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.ROLE_CC_OPERATORS;
import static edu.dsideal.campus.snaker.FlowConstDefine.ROLE_MIX_SUFFIX;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELECT_DEAL_ROLES;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELECT_DEAL_TEACHERS;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELF_PICK_DEAL_ACTORS;
import static edu.dsideal.campus.snaker.FlowConstDefine.TASK_APPROVAL_REMARK;
import static edu.dsideal.campus.snaker.FlowConstDefine.TEACHER_CC_OPERATORS;

/**
 * 任务环节处理类, 控制流程走向, 同时保存用户填报的业务数据
 * 设计模式: 建造者模式, 当前类兼顾指挥者功能
 * 2017/01/12
 */
public abstract class IDealTaskState {
    //登录人对象
    public LoginUserObj loginUserObj;
    //表单数据集合
    public Map<String, String[]> paraMap;
    //流程引擎处理对象
    public SnakerEngine engine;

    //流程定义ID
    public String processId;
    //工单ID
    public String orderId;
    //任务ID
    public String taskId;
    //任务英文名称
    public String taskName;

    //当前流程信息配置
    public Record processSet;
    //当前所处任务节点配置
    public Record processTaskSet;
    //流程处理使用的拦截器
    protected FlowInterceptorTemp interceptor;

    //流转时传递的数据集合, 供后续配置的拦截器/决策器/处理器等使用
    protected Map<String, Object> dealArgs = new HashMap<String, Object>();

    //表单与业务数据的处理对象
    protected Form2BusinessBeanObj businessBean;
    //任务节点基本信息(任务节点处理副表)
    protected Record taskInfo = new Record();
    //任务节点处理者(抄送对象)集合
    protected List<Record> taskInfoActors = new ArrayList<Record>();

    /**
     * 执行任务节点处理, 同时保存业务数据
     * 整体处理的指挥者方法, 规划整体执行顺序, 同时包含拦截器机制
     */
    public void dealTaskSaveBusiness() throws Exception{
        //初始化流程处理使用的拦截器
        interceptor = FlowSqlUtil.initFlowInterceptor(processSet);
        if(interceptor != null){
            Method setFunc = interceptor.getClass().getMethod("setDealTaskState", IDealTaskState.class);
            setFunc.invoke(interceptor, this);
        }

        dealArgs.put(ARGS_LOGIN_USER_KEY, loginUserObj.getLoginId());
        dealArgs.put(ARGS_LOGIN_DOMAIN_KEY, loginUserObj.getDomain());
        //放入在表单中, 用户完全手动选择的审批人(包含角色和人员)
        dealArgs.put(SELF_PICK_DEAL_ACTORS, CommonTools.getValueFromParaMap(paraMap, SELF_PICK_DEAL_ACTORS));
        //放入用户从配置好的处理者中选取的值
        paraMap.keySet().stream().filter(paraKey ->
                paraKey.startsWith(SELECT_DEAL_ROLES + "_") || paraKey.startsWith(SELECT_DEAL_TEACHERS + "_")
            ).forEach(paraKey -> {
                dealArgs.put(paraKey, CommonTools.getValueFromParaMap(paraMap, paraKey));
        });
        /*
        for(String paraKey : paraMap.keySet()){
            //放入用户从配置好的处理者中选取的值
            if(paraKey.startsWith(SELECT_DEAL_ROLES + "_") || paraKey.startsWith(SELECT_DEAL_TEACHERS + "_")){
                dealArgs.put(paraKey, CommonTools.getValueFromParaMap(paraMap, paraKey));
            }
        }
        */


        //保存业务数据节点数据之前的拦截器
        beforeDealInterceptor();

        //判断是否需要保存业务数据标识
        boolean saveFormDataFlag = checkSaveFormDataFlag();
        if(saveFormDataFlag){
            //保存业务数据
            saveForm2BusinessBean();
        }

        //提交流程引擎处理, 实现工单流转
        engineExecuteTask();

        //处理抄送
        if("Y".equals(processTaskSet.get("hasCcOperator"))){
            engineCreateCcOrder();
        }

        //任务节点处理信息表(任务节点处理副表)保存
        saveTaskInfo();
        //保存任务节点抄送对象集合
        saveTaskInfoActors();

        //任务处理完成后的后置拦截器
        afterDealInterceptor();
    }

    /**
     * 是否需要保存业务数据标识, 由各个子类决定
     */
    protected abstract boolean checkSaveFormDataFlag();

    /**
     * 提交流程引擎处理, 实现工单流转
     */
    protected abstract void engineExecuteTask() throws Exception;

    /**
     * 整体任务处理的前置拦截器, 默认触发配置的前置拦截
     * 在提交流程引擎保存业务数据之前, 各个子类覆盖该方法进行一些操作
     */
    protected void beforeDealInterceptor() throws Exception{
        if(interceptor != null) {
            //拦截器处理, 环节处理之前
            interceptor.doBeforeDeal();
        }
    }

    /**
     * 整体任务处理完成后的后置拦截器, 默认触发配置的后置拦截
     * 各个子类处理完后, 可以后续进行一些个性化操作
     * 场景: (1)新申请环节, 如果为草稿派发, 则需要更新是否派发过标识
     *      (2)审批型环节, 需要把审批结果放入任务节点处理对象taskInfo中
     */
    protected void afterDealInterceptor() throws Exception{
        if(interceptor != null) {
            //拦截器处理, 环节处理后置拦截
            interceptor.doAfterDeal();

            //判断当前工单是否已经结束(正常归档), 相应的需要触发工单结束拦截器
            int orderState = FlowSqlUtil.checkOrderEndState(orderId);
            if(0 == orderState){
                //工单正常归档, 则触发工单结束拦截器
                interceptor.doAfterOrderEnd();
            }
        }
    }

    /**
     * 处理抄送
     */
    private void engineCreateCcOrder() throws Exception{
        String teacherCcOperators = CommonTools.getValueFromParaMap(paraMap, TEACHER_CC_OPERATORS);//教师抄送列表
        String roleCcOperators = CommonTools.getValueFromParaMap(paraMap, ROLE_CC_OPERATORS);//角色抄送列表
        //最终整合完毕的抄送集合
        List<String> ccOperatorList = new ArrayList<String>();
        Record infoActorTemp;
        if(CommonTools.isNotEmpty(teacherCcOperators)) {
            for(String ccOperator : teacherCcOperators.split(",")){
                ccOperatorList.add(ccOperator);
                //设置任务节点抄送对象-教师
                infoActorTemp = new Record();
                buildTaskInfoActor(infoActorTemp);
                infoActorTemp.set("actorType", ACTOR_TYPE_TEACHER);
                infoActorTemp.set("actorValue", ccOperator);
                taskInfoActors.add(infoActorTemp);
            }
        }
        if(CommonTools.isNotEmpty(roleCcOperators)) {
            for(String ccOperator : roleCcOperators.split(",")){
                ccOperatorList.add(ccOperator + ROLE_MIX_SUFFIX);
                //设置任务节点抄送对象-角色
                infoActorTemp = new Record();
                buildTaskInfoActor(infoActorTemp);
                infoActorTemp.set("actorType", ACTOR_TYPE_ROLE);
                infoActorTemp.set("actorValue", ccOperator);
                taskInfoActors.add(infoActorTemp);
            }
        }
        //集合转换为数组
        String[] ccOperatorArr = ccOperatorList.toArray(new String[ccOperatorList.size()]);
        engine.order().createCCOrder(orderId, loginUserObj.getLoginId(), ccOperatorArr);
    }

    /**
     * 构造任务节点处理者(抄送对象)
     * 设置通用的字段(包括流程ID/环节名称/企业域等)
     */
    protected void buildTaskInfoActor(Record infoActorTemp){
        infoActorTemp.set("infoType", "1");//内容类型: 1工单数据
        infoActorTemp.set(ATTR_PROCESS_ID, processId);
        infoActorTemp.set(ATTR_ORDER_ID, orderId);
        infoActorTemp.set(ATTR_TASK_NAME, taskName);
        infoActorTemp.set("isCcActors", "Y");//是否为抄送的处理者
        infoActorTemp.set("domain", loginUserObj.getDomain());
    }

    /**
     * 保存环节业务数据的方法, 由子类返回的保存业务数据标识决定是否调用该方法
     * 执行完成后, 把重要的业务数据放入dealArgs集合中
     * 包括: 当前环节使用的表单类型; 使用的表单值; 业务存储表表名; 对应的数据ID
     */
    protected void saveForm2BusinessBean() throws Exception{
        //根据配置, 确认环节使用的表单对应值
        String formValue = processTaskSet.get("formValue");
        //表单与业务数据的处理对象
        businessBean = new Form2BusinessBeanObj(loginUserObj, paraMap, formValue);
        //执行保存
        businessBean.saveForm2BusinessBean();

        //流转时传递的数据集合中, 放入重要的业务处理数据
        if(businessBean.isSavedDataDone()){
            //表单对象
            dealArgs.put(ARGS_FORM_VALUE_KEY, businessBean.getFormId());
            dealArgs.put(ARGS_FORM_TYPE_KEY, businessBean.getFormType());
            //业务数据对象
            dealArgs.put(ARGS_TABLE_NAME_KEY, businessBean.getDataTable());
            dealArgs.put(ARGS_RECORD_ID_KEY, businessBean.getDataIdValue());
        }
    }

    /**
     * 任务节点处理信息表(任务节点处理副表)保存
     */
    protected void saveTaskInfo() throws Exception{
        //构造节点业务数据(wf_hist_taskInfo), 存放处理结果和本次各项配置
        taskInfo.set(ATTR_PROCESS_ID, processId);
        taskInfo.set(ATTR_ORDER_ID, orderId);
        taskInfo.set(ATTR_TASK_ID, taskId);
        taskInfo.set(ATTR_TASK_NAME, taskName);
        //环节处理备注
        taskInfo.set(TASK_APPROVAL_REMARK, CommonTools.getValueFromParaMap(paraMap, TASK_APPROVAL_REMARK));
        taskInfo.set("infoType", "1");//内容类型: 1工单数据

        taskInfo.set("isMultiForm", processSet.get("isMultiForm"));//是否多任务多表单
        taskInfo.set("taskType", processTaskSet.get("taskType"));//任务节点类型
        taskInfo.set("isStartTask", processTaskSet.get("isStartTask"));//存入是否第一环节标识
        taskInfo.set("hasCcOperator", processTaskSet.get("hasCcOperator"));

        taskInfo.set("operator", loginUserObj.getLoginId());//操作人
        taskInfo.set("operateTime", CommonTools.getCurrentTimeYmdhms());//操作时间
        taskInfo.set("domain", loginUserObj.getDomain());

        //将业务数据处理信息放入taskInfo中
        if(null != businessBean){
            businessBean.putBusiness2TaskInfo(taskInfo);
        }
        //执行保存
        Db.save("wf_hist_taskInfo", taskInfo);
    }

    /**
     * 保存任务节点抄送对象集合(wf_hist_taskInfo_actors表)
     */
    protected void saveTaskInfoActors() {
        if(! taskInfoActors.isEmpty()){
            for (Record taskInfoActor : taskInfoActors) {
                taskInfoActor.set("taskInfoId", taskInfo.get("id"));
            }
            Db.batchSave("wf_hist_taskInfo_actors", taskInfoActors, taskInfoActors.size());
        }
    }


    public Map<String, Object> getDealArgs() {
        return dealArgs;
    }
    public void setDealArgs(Map<String, Object> dealArgs) {
        this.dealArgs = dealArgs;
    }
    public Form2BusinessBeanObj getBusinessBean() {
        return businessBean;
    }
    public void setBusinessBean(Form2BusinessBeanObj businessBean) {
        this.businessBean = businessBean;
    }
    public Record getTaskInfo() {
        return taskInfo;
    }
    public void setTaskInfo(Record taskInfo) {
        this.taskInfo = taskInfo;
    }
}
