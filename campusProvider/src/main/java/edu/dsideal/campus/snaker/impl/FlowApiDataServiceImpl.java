package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.IFlowApiDataService;
import edu.dsideal.campus.snaker.sql.FlowApiDataSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskActorSql;

import java.util.List;
import java.util.Map;

/**
 * 流程数据中, 后期需要从外部获取的请求处理
 * 如: 跨库的人员/角色/分组信息
 * 2017/2/27
 */
public class FlowApiDataServiceImpl implements IFlowApiDataService {
    /**
     * 根据当前登录人所在企业域, 初始化获取角色根节点
     */
    @Override
    public List<Record> initDomainRoleTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        return FlowApiDataSql.initDomainRoleTree(loginUserObj.getDomain());
    }

    /**
     * 根据当前选中的树节点, 查询获取可供用户选择的角色列表
     */
    @Override
    public List<Record> findAllDomainRole(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String treeId = CommonTools.getValueFromParaMap(paraMap, "treeId");//选中的树ID即为部门ID
        String searchInput = CommonTools.getValueFromParaMap(paraMap, "searchInput");//填写的输入框
        return ProcessTaskActorSql.findAllDomainRole(treeId, searchInput, loginUserObj.getDomain());
    }

    /**
     * 根据当前选中的树节点(部门ID), 查询获取可供用户选择的人员(教师)列表
     */
    @Override
    public List<Record> findAllTeacherByOrgId(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        //选中的树ID即为部门ID
        String treeId = CommonTools.getValueFromParaMap(paraMap, "treeId");
        //填写的输入框
        String searchInput = CommonTools.getValueFromParaMap(paraMap, "searchInput");
        //执行查询
        return ProcessTaskActorSql.findAllTeacherByOrgId(treeId, searchInput, loginUserObj.getDomain());
    }
}
