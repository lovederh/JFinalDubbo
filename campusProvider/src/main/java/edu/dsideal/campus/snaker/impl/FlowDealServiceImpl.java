package edu.dsideal.campus.snaker.impl;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.Form2BusinessBeanObj;
import edu.dsideal.campus.snaker.patterns.IDealTaskState;
import edu.dsideal.campus.snaker.patterns.dealState.DealTaskApprovalState;
import edu.dsideal.campus.snaker.patterns.dealState.DealTaskNormalState;
import edu.dsideal.campus.snaker.patterns.dealState.DealTaskStartState;
import edu.dsideal.campus.snaker.service.IFlowDealService;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskSetSql;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.entity.Task;
import org.snaker.engine.model.TaskModel;
import org.snaker.jfinal.plugin.SnakerPlugin;

import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_TITLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.TASK_APPROVAL_RESULT;

/**
 * 流程处理服务类
 */
public class FlowDealServiceImpl implements IFlowDealService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /************************************工单通用处理*************************************/
    /**
     * 标记抄送工单为已读, 支持批量操作
     */
    @Override
    public String ccOrdersRead(LoginUserObj loginUserObj, String[] userPriv, Map<String, String[]> paraMap){
        // 需要标记的工单ID(批量)
        String orderIds = CommonTools.getValueFromParaMap(paraMap, "orderIds");
        //处理结果
        String operateMsg;
        boolean operateResult;
        if(CommonTools.isEmpty(orderIds)){
            operateMsg = "请选择要标记为已读的工单!";
            operateResult = false;
        }else{
            for(String orderId : orderIds.split(",")){
                engine.order().updateCCStatus(orderId, userPriv);
            }
            operateMsg = "标记已阅成功!";
            operateResult = true;
        }
        return CommonTools.getMsgJson(operateResult, operateMsg);
    }

    /**
     * 工单强制归档
     */
    @Override
    public String terminateOrder(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        //强制归档的工单ID
        String orderId = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_ID);
        //处理结果
        String operateMsg;
        boolean operateResult;
        if(CommonTools.isEmpty(orderId)){
            operateMsg = "请选择要强制归档的工单!";
            operateResult = false;
        }else{
            engine.order().terminate(orderId, loginUserObj.getLoginId());
            operateMsg = "工单已强制归档!";
            operateResult = true;
        }
        return CommonTools.getMsgJson(operateResult, operateMsg);
    }

    /**
     * 物理删除指定工单, 根据工单ID完成删除
     * 先执行强制归档, 再进行工单删除(只删除wf_hist_order表数据)
     */
    @Override
    public void physicalDeleteOrder(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception {
        //要彻底删除的工单ID
        String orderId = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_ID);
        //处理结果
        if(CommonTools.isEmpty(orderId)){
            throw new RuntimeException("请选择要彻底删除的工单!");
        }else{
            //先执行强制归档
            engine.order().terminate(orderId, loginUserObj.getLoginId());
            //删除对应工单数据
            FlowSqlUtil.physicalDeleteOrderById(orderId);
        }
    }

    /**
     * 当前登录人接单(确认受理)
     */
    @Override
    public void acceptOrderTask(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        //任务ID
        String taskId = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_ID);
        if(CommonTools.isEmpty(taskId)){
            return;
        }
        engine.task().take(taskId, loginUserObj.getLoginId());
    }

    /************************************环节表单展示处理*************************************/
    /**
     * 通过传入的流程名称, 获取对应最新版本的流程配置信息
     */
    @Override
    public Record findProcessSetByPara(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String processId = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_ID);//流程ID
        String processName = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_NAME);//流程名称
        Record processSet = ProcessSetSql.findSetByProcessId(processId);
        if(processSet == null){
            // 根据流程定义名和企业域, 查找最新版本的该流程配置信息
            processSet = ProcessSetSql.findSetByDomainAndProcessName(loginUserObj.getDomain(), processName);
        }
        return processSet;
    }

    /**
     * 查询并构造当前活跃环节使用的表单url, 同时拼接当前环节名称taskName
     * 步骤: 通过环节ID是否有值, 判断当前工单是否已经存在, 存在则取出该环节的配置url
     *      若不存在, 则基于api去查找流程中第一个环节, 取出第一环节对应的url
     * 实际: 非特殊流程情况下, 均会查询并返回/flowUserForm/initTaskForm?taskName=...
     */
    @Override
    public String findTaskFormUrlByTaskId(LoginUserObj loginUserObj, String processId, String taskId) {
        String taskName = "";//当前环节英文名称
        String taskFormUrl = null;//环节表单使用的url
        if(CommonTools.isNotEmpty(taskId)){
            //当前工单为已流转状态, 则查询当前工单的任务节点信息
            Task activeTask = engine.query().getTask(taskId);
            taskName = activeTask.getTaskName();
            taskFormUrl = activeTask.getActionUrl();
        }else{
            //取出流程定义中所有模型对象, 然后拿到第一个
            List<TaskModel> taskModels = FlowSqlUtil.findTaskModelsByProcessId(engine, processId);
            if(taskModels!=null && !taskModels.isEmpty()) {
                TaskModel firstModel = taskModels.get(0);
                taskFormUrl = firstModel.getForm();
                taskName = firstModel.getName();
            }
        }
        //若节点表单url未配置, 则使用/flowUserForm/initTaskForm
        if(CommonTools.isEmpty(taskFormUrl)){
            taskFormUrl = "/flowUserForm/initTaskForm";
        }
        //拼接当前环节名称taskName
        taskFormUrl = CommonTools.appendUrlOptions(taskFormUrl, "showType=form", "taskName=" + taskName);
        return taskFormUrl;
    }

    /************************************草稿与模板*************************************/
    /**
     * 删除草稿
     */
    @Override
    public void deleteDraft(String taskInfoId) {
        if(CommonTools.isNotEmpty(taskInfoId)){
            Db.deleteById("wf_hist_taskInfo", taskInfoId);
        }
    }
    /**
     * 保存草稿数据, 不启动工单
     */
    @Override
    public void saveDraftData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception {
        //草稿信息使用的ID
        String draftTaskInfoId = CommonTools.getValueFromParaMap(paraMap, "draftTaskInfoId");

        String processId = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_ID);//流程定义ID
        String orderTitle = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_TITLE);//工单主题
        String taskName = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_NAME);//任务名称

        //1.取出流程与任务节点配置
        Record processTaskSet = ProcessTaskSetSql.findSetByTaskNameOrId(processId, taskName);//环节配置
        if (null == processTaskSet) {
            throw new RuntimeException("对不起, 流程配置信息异常, 无法进行处理!");
        }

        //2.根据配置, 执行草稿业务数据保存
        Form2BusinessBeanObj businessBean = new Form2BusinessBeanObj(loginUserObj, paraMap, processTaskSet.get("formValue"));
        businessBean.saveForm2BusinessBean();

        //3.构建草稿整体处理对象
        Record taskInfo;//草稿数据存放对象
        boolean updateDraftFlag;//草稿更新标识
        if(CommonTools.isEmpty(draftTaskInfoId)){
            updateDraftFlag = false;
            //草稿数据设值
            taskInfo = new Record();
            taskInfo.set(ATTR_PROCESS_ID, processId);
            taskInfo.set("infoType", "-1");//内容类型: -1草稿
            taskInfo.set("infoDraftUsed", "N");
            taskInfo.set("operator", loginUserObj.getLoginId());//操作人
            taskInfo.set("operateTime", CommonTools.getCurrentTimeYmdhms());//操作时间
            taskInfo.set("domain", loginUserObj.getDomain());
        }else{
            updateDraftFlag = true;
            taskInfo = Db.findById("wf_hist_taskInfo", draftTaskInfoId);
        }
        taskInfo.set(ATTR_TASK_NAME, taskName);
        taskInfo.set(ATTR_ORDER_TITLE, orderTitle);//草稿时存放工单主题
        taskInfo.set("taskType", processTaskSet.get("taskType"));//任务节点类型
        taskInfo.set("isStartTask", processTaskSet.get("isStartTask"));//存入是否第一环节标识
        taskInfo.set("hasCcOperator", processTaskSet.get("hasCcOperator"));

        //4.将业务数据处理信息放入taskInfo中
        businessBean.putBusiness2TaskInfo(taskInfo);

        //5.保存/修改草稿整体处理信息, 存入wf_hist_taskInfo表
        if(updateDraftFlag){
            Db.update("wf_hist_taskInfo", taskInfo);
        }else{
            Db.save("wf_hist_taskInfo", taskInfo);
        }
    }

    /************************************环节处理保存*************************************/
    /**
     * 执行任务节点处理, 同时保存业务数据
     * 步骤:
     *  1.保存业务数据
     *  2.提交流程引擎事项任务环节跳转
     *  3.保存节点业务数据(taskInfo), 存放当前环节各项配置, 便于以后展示明细
     */
    @Override
    @Before(Tx.class)
    public void dealTaskSaveBusiness(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        String processId = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_ID);//流程ID
        String orderId = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_ID);//工单ID
        String taskId = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_ID);//任务ID
        String taskName = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_NAME);//任务英文名称
        //通过是否存在审批结果, 确定当前是否为审批类型节点
        String approvalResult = CommonTools.getValueFromParaMap(paraMap, TASK_APPROVAL_RESULT);//处理结果

        // 取出流程信息配置和流程与任务节点配置
        Record processSet = ProcessSetSql.findSetByProcessId(processId);//流程配置
        Record processTaskSet = ProcessTaskSetSql.findSetByTaskNameOrId(processId, taskName, taskId);//环节配置
        if (null == processSet || null == processTaskSet) {
            throw new RuntimeException("对不起, 流程配置信息异常, 无法进行处理!");
        }
        //注: 在这里取出配置并传递, 而不在状态处理类中取, 这样做的目的是: 为以后针对配置做逻辑判断预留

        //确认任务环节的状态处理类
        IDealTaskState dealTaskState;
        if(CommonTools.isEmpty(orderId)){
            //新建工单情况
            String draftTaskInfoId = CommonTools.getValueFromParaMap(paraMap, "draftTaskInfoId");//草稿数据使用的ID
            dealTaskState = new DealTaskStartState(draftTaskInfoId);
        }else if(CommonTools.isNotEmpty(approvalResult)){
            //流程类型为审批流程情况下
            dealTaskState = new DealTaskApprovalState(approvalResult);
        }else{
            dealTaskState = new DealTaskNormalState();
        }

        //状态处理类, 各属性赋值
        dealTaskState.loginUserObj = loginUserObj;
        dealTaskState.paraMap = paraMap;
        dealTaskState.engine = engine;
        dealTaskState.processId = processId;
        dealTaskState.orderId = orderId;
        dealTaskState.taskId = taskId;
        dealTaskState.taskName = taskName;
        dealTaskState.processSet = processSet;
        dealTaskState.processTaskSet = processTaskSet;

        //执行任务节点处理, 同时保存业务数据
        dealTaskState.dealTaskSaveBusiness();
    }
}
