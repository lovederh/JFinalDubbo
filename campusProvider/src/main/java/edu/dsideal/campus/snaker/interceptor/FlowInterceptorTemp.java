package edu.dsideal.campus.snaker.interceptor;

import edu.dsideal.campus.snaker.patterns.IDealTaskState;
import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;

/**
 * 流程运行处理时使用的拦截器
 * (1)该类为模板类, 声明拦截器内包含的所有方法
 * (2)若某流程需要引入拦截器机制, 那么先添加一个当前类的子类再使用
 * (3)内部方法处理时, 通常需要依照环节名称进行判断
 * (4)配置方式: wf_processSet表中interceptor字段值
 * 2017/1/21
 */
public class FlowInterceptorTemp {

    //任务环节处理表单展示对象, 明细查看时使用
    protected ITaskFormAttrWrapper formAttrWrapper;

    //任务环节处理整体对象, 环节处理时使用
    protected IDealTaskState dealTaskState;

    /**
     * 执行环节处理之前的操作
     */
    public void doBeforeDeal(){

    }
    /**
     * 执行环节处理之后的操作
     * 常用: 流程与其它业务耦合时, 向其它业务表写入/删除/修改一些数据
     */
    public void doAfterDeal(){

    }
    /**
     * 工单启动时候, 之前的操作
     */
    public void doBeforeOrderStart(){

    }
    /**
     * 工单启动时候, 之后的操作
     */
    public void doAfterOrderStart(){

    }
    /**
     * 工单结束(正常归档)之后, 进行的操作
     */
    public void doAfterOrderEnd(){

    }

    /**
     * 执行构造工单查看对象之前的操作
     * 常用: 更改浏览次数等(当然这样的话也可以在后置拦截时处理)
     */
    public void doBeforeView(){

    }
    /**
     * 执行构造工单查看对象之后的操作
     * 常用: 处理更改业务数据的展示值
     */
    public void doAfterView(){

    }


    public ITaskFormAttrWrapper getFormAttrWrapper() {
        return formAttrWrapper;
    }
    public void setFormAttrWrapper(ITaskFormAttrWrapper formAttrWrapper) {
        this.formAttrWrapper = formAttrWrapper;
    }

    public IDealTaskState getDealTaskState() {
        return dealTaskState;
    }
    public void setDealTaskState(IDealTaskState dealTaskState) {
        this.dealTaskState = dealTaskState;
    }
}
