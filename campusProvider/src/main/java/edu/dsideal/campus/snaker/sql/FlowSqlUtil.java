package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.interceptor.FlowInterceptorTemp;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.entity.Process;
import org.snaker.engine.model.TaskModel;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程配置域处理共通类
 * 包括主要的可复用的静态查询方法
 */
public class FlowSqlUtil {

    /*************************************配置查询*************************************/
    /**
     * 根据字典类型, 查询字典返回真实值与显示名, 统一返回别名value/display
     */
    public static List<Record> findDictListByType(String domain, String dictType) {
        if(CommonTools.isEmpty(dictType)){
            return new ArrayList<Record>();
        }
        String sql ="select dictId as value, dictName as display from t_sys_dict " +
                    "where pdictId<>'root' and dictType=? " +
                    "and domain in('domain', '" + domain + "') " +
                    "order by orderNo ";
        return Db.find(sql, dictType);
    }

    /**
     * 根据流程ID, 找到该流程下所有任务节点对象, 返回任务模型集合
     */
    public static List<TaskModel> findTaskModelsByProcessId (SnakerEngine engine, String processId){
        if(CommonTools.isNotEmpty(processId)){
            Process process = engine.process().getProcessById(processId);
            if(null != process){
                // 取出流程定义中所有模型对象
                return process.getModel().getModels(TaskModel.class);
            }
        }
        throw new RuntimeException("流程配置异常, 请联系系统管理员!");
    }

    /**
     * 根据流程信息配置ID(processSetId), 删除所有相关配置
     */
    public static void deleteAllProcessSet(String processSetId) {
        if(CommonTools.isNotEmpty(processSetId)){
            //删除任务环节配置数据
            Db.update("delete from wf_processTaskActorSet where processSetId=?", processSetId);
            Db.update("delete from wf_processTaskSet where processSetId=?", processSetId);
            //删除流程内决策节点配置
            Db.update("delete from wf_processDecisionFrom where processSetId=?", processSetId);
            Db.update("delete from wf_processDecisionNode where processSetId=?", processSetId);
            Db.update("delete from wf_processDecisions where processSetId=?", processSetId);
            //删除总流程配置数据
            Db.deleteById("wf_processSet", processSetId);
        }
    }

    /**
     * 工单流转处理时, 往往在处理开始前/结束后的时候, 我们可能需要执行一些插入结算
     * /工单信息查看时, 也同理, 在执行取出查看结果之前/取出查看结果之后
     * 因此, 引入了拦截器机制: 通用的拦截器父类为snaker.interceptor.FlowInterceptorTemp
     */
    public static FlowInterceptorTemp initFlowInterceptor(Record processSet){
        //根据流程信息配置取出拦截器对应存放的值
        String interceptorStr = processSet.getStr("interceptor");
        if(CommonTools.isNotEmpty(interceptorStr)){
            try {
                // 实例化自定义拦截器
                Object interceptor = Class.forName(interceptorStr).newInstance();
                if(interceptor instanceof FlowInterceptorTemp){
                    return (FlowInterceptorTemp)interceptor;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**************************************流程数据处理***************************************/
    /**
     * 通过工单ID, 执行工单所有相关数据的删除-物理删除, 无法恢复
     */
    public static void physicalDeleteOrderById(String orderId) throws Exception{
        if(CommonTools.isEmpty(orderId)){
            return;
        }
        List<String> batchSqls = new ArrayList<String>();
        batchSqls.add("delete from wf_cc_order where order_Id = '" + orderId + "' ");//删除抄送表
        batchSqls.add("delete from wf_hist_task_actor where task_Id in(select id from wf_hist_task where order_Id = '" + orderId + "' )");//删除环节操作者历史
        batchSqls.add("delete from wf_task_actor where task_Id in(select id from wf_task where order_Id = '" + orderId + "' )");//删除环节操作者
        batchSqls.add("delete from wf_hist_task where order_Id = '" + orderId + "' ");//删除环节表历史
        batchSqls.add("delete from wf_task where order_Id = '" + orderId + "' ");//删除环节表
        batchSqls.add("delete from wf_hist_taskInfo where orderId = '" + orderId + "' ");//删除环节处理副表
        batchSqls.add("delete from wf_hist_order where id = '" + orderId + "' ");//删除工单表历史
        batchSqls.add("delete from wf_order where id = '" + orderId + "' ");//删除工单表
        Db.batch(batchSqls, batchSqls.size());
    }

    /**
     * 根据工单ID, 查询一条当前工单信息(查询wf_hist_order表)
     */
    public static Record findHistoryOrderById(String orderId){
        return CommonTools.isEmpty(orderId)? null : Db.findFirst(
                "select o.orderTitle, o.order_No, o.create_Time, o.end_Time, o.creator, u.userName as creatorName " +
                "from wf_hist_order o left join t_sys_user u on u.loginId=o.creator " +
                "where o.id=? ", orderId);
    }

    /**
     * 确认工单归档状态, 根据当前所处状态, 返回不同的值:
     *  正常归档, 返回0;
     *  强制归档, 返回2;
     *  流转中/其他情况, 返回1.
     */
    public static int checkOrderEndState(String orderId){
        Record orderState = Db.findFirst("select order_State as orderState from wf_hist_order where id=?", orderId);
        if(orderState == null){
            return 1;
        }
        return orderState.getInt("orderState");
    }
}
