package edu.dsideal.campus.snaker.patterns.dealState;

import com.jfinal.plugin.activerecord.Db;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.patterns.IDealTaskState;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import org.snaker.engine.access.QueryFilter;
import org.snaker.engine.entity.Order;
import org.snaker.engine.entity.Task;

import java.util.List;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_TITLE;

/**
 * 任务环节处理: 工单启动
 * 2017/1/12
 */
public class DealTaskStartState extends IDealTaskState {

    public DealTaskStartState(String draftTaskInfoId) {
        super();
        this.draftTaskInfoId = draftTaskInfoId;
    }

    //草稿数据使用的ID, 有值则说明为草稿派发
    private String draftTaskInfoId;

    /**
     * 是否需要保存业务数据标识, 派发环节必然有表单
     */
    @Override
    protected boolean checkSaveFormDataFlag() {
        return true;
    }

    /**
     * 提交流程引擎处理, 实现工单流转
     */
    @Override
    protected void engineExecuteTask() throws Exception {
        String orderTitle = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_TITLE);//工单主题
        try {
            //启动流程, 完成新建工单操作
            Order order = engine.startInstanceById(processId, loginUserObj.getLoginId(), dealArgs);
            orderId = order.getId();
            //设置工单主题和企业域
            Db.update("update wf_hist_order set orderTitle=?, domain=?, delFlag='0' where id=? ",
                    orderTitle, loginUserObj.getDomain(), orderId);
            // 任务跳转至紧邻的审批环节
            List<Task> tasks = engine.query().getActiveTasks(new QueryFilter().setOrderId(order.getId()));
            //接下来处理使用新的任务ID
            taskId = tasks.get(0).getId();


            //TODO 启动环节, 需要遍历确认接单人(完成跳步操作)


            //还需要提交流程引擎一次, 使工单流转至紧邻的审批节点
            engine.executeTask(taskId, loginUserObj.getLoginId(), dealArgs);
        } catch (Exception e) {
            //新派工单情况下出现异常, 则手动删除工单完成回滚
            FlowSqlUtil.physicalDeleteOrderById(orderId);
            throw e;
        }
    }

    /**
     * 工单启动时, 会先触发工单启动的前置拦截方法
     */
    protected void beforeDealInterceptor() throws Exception {
        if (interceptor != null) {
            interceptor.doBeforeOrderStart();
        }
        super.beforeDealInterceptor();
    }

    /**
     * (1)如果本次派发为草稿派发, 则更新是否派发过标识为true
     * (2)工单启动完成后, 触发工单启动的后置拦截方法
     * (3)最后触发配置的通用处理后置拦截器
     */
    @Override
    protected void afterDealInterceptor() throws Exception {
        //判断是否为草稿派发
        if (CommonTools.isNotEmpty(draftTaskInfoId)) {
            Db.update("update wf_hist_taskInfo set infoDraftUsed='Y' where id=? ", draftTaskInfoId);
        }
        // 触发工单启动时后置的拦截器
        if (interceptor != null) {
            interceptor.doAfterOrderStart();
        }
        super.afterDealInterceptor();
    }
}

