package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static edu.dsideal.campus.snaker.FlowConstDefine.ORG_MIX_SUFFIX;
import static edu.dsideal.campus.snaker.FlowConstDefine.ROLE_MIX_SUFFIX;

/**
 * 流程数据中, 后期需要从外部获取的请求处理
 * 如: 跨库的人员/角色/分组信息
 * 2017/3/3
 */
public class FlowApiDataSql {

    /**
     * 根据当前登录人所在企业域, 初始化获取角色根节点
     */
    public static List<Record> initDomainRoleTree(String domain){
        return Db.find(
                "select r.codeId as id, " +
                "       r.roleName as name, " +
                "       r.pId," +
                "       r.level," +
                "       '1' as isLeaf " +
                "  from t_sys_role r " +
                " where r.level='1' and r.domain=? ", domain);
    }


    /************************************权限信息*************************************/
    /**
     * 根据登陆人ID, 拿到其所在权限组集合(主要包括部门、角色、权限组等)
     * 	用处:(1)CustomAccessStrategy类, 访问策略中的接单权限判断
     * 		(2)人员登录, 点击待办列表时, 作为可见工单的权限判断
     */
    public static List<String> initUserPrivs(String loginId){
        // 构造权限列表: 人员登录帐号, 角色
        List<String> privList = new ArrayList<String>();
        privList.add(loginId);//人员登录帐号
        //人员拥有的角色
        List<String> roleList = findUserRoles(loginId);
        if(roleList!=null && !roleList.isEmpty()){
            privList.addAll(roleList.stream().map(role -> role + ROLE_MIX_SUFFIX).collect(Collectors.toList()));
            /*
            for (String role : roleList){
                privList.add(role + ROLE_MIX_SUFFIX);//角色
            }
            */
        }
        //获取人员所属部门, 加入权限列表
        List<String> orgList = findUserOrg(loginId);
        if(orgList!=null && !orgList.isEmpty()){
            privList.addAll(orgList.stream().map(role -> role + ORG_MIX_SUFFIX).collect(Collectors.toList()));
        }
        return privList;
    }

    /**
     * 根据当前登录人, 查找人员拥有的所有角色列表
     */
    public static List<String> findUserRoles(Object loginId){
        return Db.query("select roleId from t_sys_user_role where loginId=? ", loginId);
    }

    /**
     * 根据当前登录人, 查找人员拥有的所有部门(orgId)
     */
    public static List<String> findUserOrg(Object loginId){
        return Db.query("select orgId from t_sys_user where loginId=? ", loginId);
    }
}
