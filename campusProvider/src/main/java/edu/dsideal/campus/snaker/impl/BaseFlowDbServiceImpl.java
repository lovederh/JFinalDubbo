package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.sql.FlowApiDataSql;
import edu.dsideal.campus.snaker.sql.FlowSqlUtil;
import edu.dsideal.campus.snaker.sql.FlowUserForm;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import org.snaker.engine.SnakerEngine;
import org.snaker.engine.entity.HistoryTask;
import org.snaker.jfinal.plugin.SnakerPlugin;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用的流程数据服务
 * 2016/12/16
 */
public class BaseFlowDbServiceImpl implements IBaseFlowDbService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /**********************************字典/表单数据************************************/
    /**
     * 用户定制的表单列表
     */
    @Override
    public List<Record> findUserFormList(LoginUserObj loginUserObj) {
        return FlowUserForm.findUserFormList(loginUserObj.getDomain());
    }

    /**
     * 根据字典类型, 查询字典返回真实值与显示名, 统一返回别名value/display
     */
    @Override
    public List<Record> findDictListByType(LoginUserObj loginUserObj, String dictType) {
        if(CommonTools.isEmpty(dictType)){
            return new ArrayList<Record>();
        }
        return FlowSqlUtil.findDictListByType(loginUserObj.getDomain(), dictType);
    }

    /************************************权限信息*************************************/
    /**
     * 根据登陆人ID, 拿到其所在权限组集合(主要包括部门、角色、权限组等)
     * @param loginUserObj 登陆人对象
     * @return List<String>
     */
    @Override
    public String[] initUserPrivs(LoginUserObj loginUserObj){
        String loginId = loginUserObj.getLoginId();
        List<String> privList = FlowApiDataSql.initUserPrivs(loginId);
        String[] privArray = new String[privList.size()];
        privList.toArray(privArray);
        return privArray;
    }

    /************************************流程数据查询*************************************/
    /**
     * 根据流程配置表ID, 取出一条流程配置信息
     */
    @Override
    public Record findProcessSetById(String processSetId) {
        return ProcessSetSql.findSetById(processSetId);
    }
    /**
     * 通过已经安装过的流程ID, 获取流程配置信息(wf_processSet表)
     */
    @Override
    public Record findProcessSetByProcessId(String processId) {
        return ProcessSetSql.findSetByProcessId(processId);
    }

    /**
     * 根据工单ID, 查询一条当前工单信息(查询wf_hist_order表)
     */
    @Override
    public Record findHistoryOrderById(String orderId){
        return FlowSqlUtil.findHistoryOrderById(orderId);
    }

    /**
     * 根据历史节点信息, 查询一条历史任务, 即查询wf_hist_task表
     */
    @Override
    public HistoryTask findHistoryTaskById(String taskId) {
        return CommonTools.isEmpty(taskId)? null : engine.query().getHistTask(taskId);
    }
}
