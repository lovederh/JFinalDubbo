package edu.dsideal.campus.snaker.patterns.taskFormAttr;

import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;
import edu.dsideal.campus.snaker.sql.HistTaskInfoSql;

import static edu.dsideal.campus.snaker.FlowConstDefine.APPROVAL_NOT_PASS_VALUE;

/**
 * 任务环节处理表单展示对象的包装类
 * 针对明细页面, 只展示指定环节下填入的表单内数据
 * 注意: 展示时, 基于填报时的配置, 在wf_hist_taskInfo中留存的
 * 2017/1/10
 */
public class TaskFormAttrWrapperView extends ITaskFormAttrWrapper {

    public TaskFormAttrWrapperView() {
        super();
        this.defaultFormUrl = "deal/openTaskView.html";
        this.showType = "view";
    }

    /**
     * 个性化装饰任务环节表单对象
     */
    @Override
    protected void initFormAttrWrapper() throws Exception{
        //填入的节点业务数据副表
        taskInfo = HistTaskInfoSql.findTaskInfoByTaskId(orderId, taskId);
        if(taskInfo == null){
            throw new RuntimeException("对不起, 任务环节历史数据缺失!");
        }

        //根据节点业务数据表(即用户表单对应的数据存储表), 取出业务数据
        businessBean = HistTaskInfoSql.findBeanByTaskInfo(taskInfo);

        //明细时, 使用当时工单处理使用的对应表单配置
        String taskFormValue = taskInfo.get("formValue");
        String isMultiForm = taskInfo.get("isMultiForm");
        String isStartTask = taskInfo.get("isStartTask");
        //明细情况下, 审批结果手动赋值, 用于前端展示审批结果(通过/不通过)
        approvalResult = taskInfo.get("approvalResult");

        //两种情况下(1)第一环节(2)多任务多表单开启, 并且审批结果不为不通过. 则需要扩展表单
        if("Y".equals(isStartTask) || ("Y".equals(isMultiForm) && !APPROVAL_NOT_PASS_VALUE.equals(approvalResult))){
            initExtFormUrl(taskFormValue);
        }
    }

    /**
     * 是否需要获取紧邻当前环节的处理环节下, 处理者集合标示
     */
    @Override
    protected boolean checkGetNextActorsFlag() throws Exception{
        return false;
    }
}
