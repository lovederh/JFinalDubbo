package edu.dsideal.campus.snaker.sql;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.cache.SnakerCacheManager;

import java.util.List;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;

/**
 * 流程使用的相关用户表单查询与处理工具类
 * 2016/12/28
 */
public class FlowUserForm {
    /*********************************** 表单对象查询 ***********************************/
    /**
     * 根据用户表单ID获取表单信息
     */
    public static Record findUserFormById(String formId){
        return CommonTools.isEmpty(formId)? null: Db.findById("t_sys_userForm", formId);
    }
    /**
     * 用户定制的表单列表
     * 注: 需要根据企业域和表单英文名过滤, 同名表单则取最高版本出来
     */
    public static List<Record> findUserFormList(String domain) {
        String sql ="select f.id, " +
                    "       f.formDisplay " +
                    "from t_sys_userForm f " +
                    "where exists(select 1 from(select max(f.version) as maxVersion, f.formName " +
                    "							from t_sys_userForm f " +
                    "                           where f.domain in('domain', '" + domain + "') " +
                    "                           group by f.formName " +
                    "           )ff where ff.maxVersion = f.version and ff.formName = f.formName)";
        return Db.find(sql);
    }

    /**
     * 通过表单配置信息, 构造返回供前端使用的数据字符串
     */
    public static String findFormHtmlObj(Record taskForm, String showType, Record businessBean){
        if(taskForm == null){
            return "";
        }
        //TODO 获取并设置表单html字符串
        String formHtmlObj;
        if("view".equals(showType)){
            formHtmlObj = "文字: 你好世界<br/>数字: 88";
        }else{
            formHtmlObj = "文字:<input name='helloMsg'/><br/>数字:<input name='helloCode'/>";
        }
        return formHtmlObj;
    }


    /*********************************** 表单配置保存 ***********************************/
    /**
     * 设置表单相关信息至processSet(processTaskSet)对象
     * 根据传入的是否更新关联的配置表标识, 决定相应操作(processSet与processTaskSet相互关联)
     * 如果是否更新标识为false, 那么当前配置表表名无需传入
     * 主要设置: 表单类型/表单值/表单英文名/表单保存使用的存储表
     */
    public static void putFormInfo2ProcessSet(Record setRecord, String formValue, boolean updateJoinSetFlag, String... setTableName){
        String formType = "";
        String formName = "";
        String dataTable = "";
        Record formRecord = FlowUserForm.findUserFormById(formValue);
        if(formRecord != null){
            formType = formRecord.get("formType");
            formName = formRecord.get("formName");
            dataTable = formRecord.get("dataTable");
        }else{
            formValue = "";
        }
        setRecord.set("formValue", formValue);
        setRecord.set("formName", formName);
        setRecord.set("formType", formType);
        setRecord.set("dataTable", dataTable);
        //更新关联的配置表操作
        if(updateJoinSetFlag && setTableName!=null && setTableName.length>0 && CommonTools.isNotEmpty(setTableName[0])){
            if("wf_processSet".equals(setTableName[0])){
                //执行sql语句, 修改流程与任务节点配置表
                Db.update(  "update wf_processTaskSet set formValue=?, formName=?, formType=?, dataTable=? " +
                            "where isStartTask='Y' and processSetId=? ", formValue, formName, formType, dataTable, setRecord.get("id"));
            }else if("wf_processTaskSet".equals(setTableName[0])){
                //取出流程整体配置对象, 更新表单参数并缓存
                Record processSet = Db.findById("wf_processSet", setRecord.get("processSetId"));
                processSet.set("formValue", formValue).set("formName", formName);
                processSet.set("formType", formType).set("dataTable", dataTable);
                Db.update("wf_processSet", processSet);
                //更新缓存
                SnakerCacheManager.setCache("processSet_" + processSet.get(ATTR_PROCESS_ID), processSet);
            }
        }
    }
}
