package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import org.snaker.engine.SnakerEngine;
import org.snaker.jfinal.plugin.SnakerPlugin;
import edu.dsideal.campus.snaker.service.IFlowApprovalTypeService;

import java.util.List;
import java.util.Map;

/**
 * 审批类型工单处理特殊服务类
 * 2016/12/29
 */
public class FlowApprovalTypeServiceImpl implements IFlowApprovalTypeService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /**
     * 只查询和展示涉及审批环节的处理信息列表
     * 主要获取环节处理时间/环节名称/环节处理结果/环节备注
     */
    @Override
    public List<Record> approvalResultGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String orderId = CommonTools.getValueFromParaMap(paraMap, "orderId");//工单ID
        if(CommonTools.isEmpty(orderId)){
            return null;
        }
        return Db.find(
                "select t.id, t.task_Name, t.display_Name as taskDisplay, t.create_Time, t.finish_Time," +
                "       i.approvalResult, i.approvalRemark," +
                "       u.userName as operatorName " +
                "from wf_hist_taskInfo i, wf_hist_task t " +
                "   left join t_sys_user u on t.operator = u.loginId " +
                "where i.taskId = t.id and i.infoType='1' and i.isStartTask='N' and t.order_Id=? " +
                "order by t.create_Time asc, t.finish_Time asc ",
            orderId);
    }
}
