package edu.dsideal.campus.snaker.handler.operator;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.bean.ProcessTaskActors;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;
import edu.dsideal.campus.snaker.sql.ProcessTaskActorSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskSetSql;
import org.snaker.engine.Assignment;
import org.snaker.engine.core.Execution;
import org.snaker.engine.entity.Process;
import org.snaker.engine.model.TaskModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_ROLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_TEACHER;
import static edu.dsideal.campus.snaker.FlowConstDefine.ARGS_LOGIN_USER_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.ROLE_MIX_SUFFIX;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELECT_DEAL_ROLES;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELECT_DEAL_TEACHERS;
import static edu.dsideal.campus.snaker.FlowConstDefine.SELF_PICK_DEAL_ACTORS;

/**
 * 统一的流程参与者查找类
 * 配置流程图各环节assignmentHandler属性: CommonAssignment
 * 依据各流程, 各环节节点, 查找wf_processTaskActorSet表配置
 * 返回流程参与者集合
 */
public class CommonAssignment extends Assignment {

	/**
	 * 根据配置表, 取出下一环节的处理人
     * 注:  特殊配置可以在执行通用方法前执行判断并返回结果, 不过会破环程序的整体性,
     *      更好的处理方式应为建立新的参与者实现类, 供特殊流程配置使用
	 * @param model		即将执行的任务节点(即下一环节节点对象)
     * @param execution 当前执行对象, 注: 该对象内部也有Task对象, 但只是当前节点, 不是下一步节点
     * @return Object	参与者对象, 查看源码可知此处支持逗号分割的字符串/数组/List集合
     * 					故为了处理简单, 此处处理方式为直接把查询结果的字符串集合作为返回结果
	 */
	@Override
	public Object assign(TaskModel model, Execution execution) {
        Map<String, Object> args = execution.getArgs();//执行参数
		Process process = execution.getProcess();//当前流程对象
		if(null == process){
			throw new RuntimeException("当前流程配置错误, 请联系系统管理员!");
		}
		String processId = process.getId();//流程ID
        String nextTaskName = model.getName();//下一个任务节点
		//String nowTaskName = execution.getTask().getTaskName();// 当前任务节点
        //String domain = String.valueOf(args.get(ARGS_LOGIN_DOMAIN_KEY));//企业域

		// 基于配置表, 判断下一节点是否为第一个环节(申请环节)
        Record processTaskSet = ProcessTaskSetSql.findSetByTaskNameOrId(processId, nextTaskName);
		if(null!=processTaskSet && "Y".equals(processTaskSet.get("isStartTask"))){
            return findStartTaskOperator(model, execution, args, nextTaskName);
		}

        //不是第一环节, 则先判断手选的处理者
        List<String> operators = checkSelectDealActors(model, execution, args, nextTaskName);
        //存在已选的处理者, 则直接将工单流转至所选处理者
        if(! operators.isEmpty()){
            return operators;
        }

        //用户没进行手动选择处理者的操作, 那么查询默认配置
        ProcessTaskAllActorsObj defaultTaskActors = ProcessTaskActorSql.buildActorsObjByTaskName(processId, nextTaskName);
        Map<String, ProcessTaskActors> actorMap = defaultTaskActors.getActorMap();
        ProcessTaskActors roleActors = actorMap.get(ACTOR_TYPE_ROLE);
        ProcessTaskActors teacherActors = actorMap.get(ACTOR_TYPE_TEACHER);
        if(roleActors != null){
            operators.addAll(roleActors.getActorList().stream().map(
                    role -> role.get("actorValue") + ROLE_MIX_SUFFIX).collect(Collectors.toList()));
            /*
            for(Record role : roleActors.getActorList()){
                operators.add(role.get("actorValue") + ROLE_MIX_SUFFIX);
            }
             */
        }
        if(teacherActors != null){
            operators.addAll(teacherActors.getActorList().stream().map(
                    teacher -> teacher.getStr("actorValue")).collect(Collectors.toList()));
        }

        if(operators.isEmpty()){
            throw new RuntimeException("流程配置异常, 未找到下一环节接单人!");
        }
        return operators;
	}

    /**
     * 当前环节名称为第一环节时, 确认本次处理者
     * 如果为新派工单情况, 那么派单人取当前登陆人;
     * 否则取第一次派单人作为本次操作人(流程图中画了重新派发这一环节情况下)
     */
    private String findStartTaskOperator(TaskModel model, Execution execution, Map<String, Object> args, String nextTaskName) {
        // 第一次进入时取当前登陆人, 否则
        String lastOperator = Db.queryFirst(
                "select operator from wf_hist_task " +
                "where finish_Time is not null and order_Id=? and task_Name=? " +
                "order by finish_Time ", execution.getOrder().getId(), nextTaskName);
        if(CommonTools.isNotEmpty(lastOperator)){
            return lastOperator;
        }else{
            return String.valueOf(args.get(ARGS_LOGIN_USER_KEY));
        }
    }


    /**
     * 工单流转时, 先判断是否存在已选的处理者, 若存在则直接将工单流转至所选处理者
     */
    private List<String> checkSelectDealActors(TaskModel model, Execution execution, Map<String, Object> args, String nextTaskName) {
        List<String> operators = new ArrayList<String>();

        //TODO
        //手动选择的审批人, 包括角色和人员
        String selfPickDealActors = String.valueOf(args.get(SELF_PICK_DEAL_ACTORS));


        //从配置好的处理角色中选取的值
        String selectDealRoles = String.valueOf(args.get(SELECT_DEAL_ROLES + "_" + nextTaskName));
        //从配置好的处理人员(教师)中选取的值
        String selectDealTeachers = String.valueOf(args.get(SELECT_DEAL_TEACHERS + "_" + nextTaskName));
        //存在已选的处理者, 则直接将工单流转至所选处理者
        if(CommonTools.isNotEmpty(selectDealRoles)){
            for(String dealRole : selectDealRoles.split(",")){
                operators.add(dealRole + ROLE_MIX_SUFFIX);
            }
        }
        if(CommonTools.isNotEmpty(selectDealTeachers)){
            Collections.addAll(operators, selectDealTeachers.split(","));
        }
        return operators;
    }
}
