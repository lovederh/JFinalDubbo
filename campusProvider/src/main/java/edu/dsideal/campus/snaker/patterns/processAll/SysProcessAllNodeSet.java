package edu.dsideal.campus.snaker.patterns.processAll;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.snaker.bean.ProcessDecisionNodeWrapper;
import edu.dsideal.campus.snaker.patterns.ProcessAllNodeSet;
import org.snaker.engine.model.DecisionModel;
import org.snaker.engine.model.TaskModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;

/**
 * 系统流程的整体配置对象
 * 包含: 整体配置/处理环节配置/判断节点配置
 * 2017/1/20
 */
public class SysProcessAllNodeSet extends ProcessAllNodeSet {

    /********************************** 流程安装后新增各种配置 ***********************************/
    /**
     * 插入流程整体配置(wf_processSet)
     */
    @Override
    protected void saveFlowSetEndDeploy() throws Exception{
        processSet = new Record();
        processSet.set("customVersion", process.getVersion());//定制版本号使用安装的版本号
        processSet.set("processName", processName);
        processSet.set("processDisplay", process.getDisplayName());
        processSet.set("processType", "sys");
        processSet.set("isDeploy", "Y");
        processSet.set(ATTR_PROCESS_ID, processId);
        processSet.set("deployFilePath", fileResource);
        processSet.set("deployFile", fileName);
        processSet.set("isMultiForm", "N");//是否多任务多表单默认设置为否
        processSet.set("domain", loginUserObj.getDomain());
        //遍历设置系统流程安装时需要的额外配置
        if(sysFlowConfig != null){
            for(Map.Entry<String, String> entry : sysFlowConfig.entrySet()){
                processSet.set(entry.getKey(), entry.getValue());//目前: 只包括设置拦截器
            }
        }
        Db.save("wf_processSet", processSet);
        processSetId = processSet.get("id");
    }
    /**
     * 插入可处理任务节点配置(wf_processTaskSet)
     */
    @Override
    protected void saveFlowTaskSetEndDeploy() throws Exception{
        //向任务节点配置表wf_processTaskSet插入环节记录
        List<Record> taskSetList = new ArrayList<Record>();//缓存要保存的环节配置列表
        //当前环节前往的任务环节wf_processTaskTo
        List<Record> taskToList = new ArrayList<Record>();
        Record taskSet;
        Record taskTo;
        List<TaskModel> taskModels = process.getModel().getModels(TaskModel.class);
        if(null!=taskModels && !taskModels.isEmpty()){
            for(int i=0, size=taskModels.size(); i<size; i++){
                TaskModel taskModel = taskModels.get(i);
                taskSet = new Record();
                iocProcessBaseSet(taskSet, processId, processName, processSetId, loginUserObj.getDomain());
                taskSet.set(ATTR_TASK_NAME, taskModel.getName());//任务节点英文名
                taskSet.set("taskDisplay", taskModel.getDisplayName());//任务节点显示名
                taskSet.set("isStartTask", (i==0? "Y": "N"));//是否为启动环节
                taskSet.set("hasCcOperator", "Y");//默认每个环节都可以抄送
                taskSetList.add(taskSet);

                List<TaskModel> toModels = taskModel.getNextModels(TaskModel.class);
                if(null!=toModels && !toModels.isEmpty()) {
                    for (TaskModel toModel : toModels) {
                        taskTo = new Record();
                        iocProcessBaseSet(taskTo, processId, processName, processSetId, loginUserObj.getDomain());
                        taskTo.set(ATTR_TASK_NAME, taskModel.getName());//当前任务节点英文名
                        taskTo.set("toTaskName", toModel.getName());
                        taskToList.add(taskTo);
                    }
                }
            }
        }
        Db.batchSave("wf_processTaskSet", taskSetList, taskSetList.size());
        Db.batchSave("wf_processTaskTo", taskToList, taskToList.size());
    }

    /**
     * 插入判断类型节点配置(wf_processDecisionNode)
     */
    @Override
    protected void saveFlowDecisionsEndDeploy() throws Exception{
        //构造流程配置基础对象
        Record processBaseSet = new Record();
        processBaseSet.set(ATTR_PROCESS_ID, processId);
        processBaseSet.set(ATTR_PROCESS_NAME, processName);
        processBaseSet.set("processSetId", processSetId);
        processBaseSet.set("domain", loginUserObj.getDomain());

        List<DecisionModel> decisionModels  = process.getModel().getModels(DecisionModel.class);
        for(DecisionModel decisionModel : decisionModels){
            ProcessDecisionNodeWrapper decisionNode = new ProcessDecisionNodeWrapper(processBaseSet);
            decisionNode.saveSysFlowDecisionNode(decisionModel);
            processDecisionNode.add(decisionNode);
        }
    }
}
