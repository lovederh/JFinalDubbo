package edu.dsideal.campus.snaker.patterns.dealState;

import edu.dsideal.campus.snaker.patterns.IDealTaskState;

import static edu.dsideal.campus.snaker.FlowConstDefine.APPROVAL_NOT_PASS_VALUE;
import static edu.dsideal.campus.snaker.FlowConstDefine.TASK_APPROVAL_RESULT;

/**
 * 任务环节处理: 审批环节
 * 2017/1/12
 */
public class DealTaskApprovalState extends IDealTaskState {

    public DealTaskApprovalState(String approvalResult) {
        this.approvalResult = approvalResult;
    }

    //处理结果(agree:通过; disagree:不通过)
    private String approvalResult;

    /**
     * 是否需要保存业务数据标识
     */
    @Override
    protected boolean checkSaveFormDataFlag() {
        //多任务多表单为真并且审批结果为通过则需要存放业务数据
        if("N".equals(processSet.get("isMultiForm"))){
            return false;
        }
        if(APPROVAL_NOT_PASS_VALUE.equals(approvalResult)){
            return false;
        }
        return true;
    }

    /**
     * 提交流程引擎处理, 实现工单流转
     */
    @Override
    protected void engineExecuteTask() throws Exception{
        dealArgs.put(TASK_APPROVAL_RESULT, approvalResult);

        // 根据审批结果, 决定流程流转方向
        if(APPROVAL_NOT_PASS_VALUE.equals(approvalResult)){
            // 处理结果为不通过, 进行流程驳回跳转
            engine.executeAndJumpTask(taskId, loginUserObj.getLoginId(), dealArgs, null);
        }else{
            // 处理结果为通过, 处理任务执行
            engine.executeTask(taskId, loginUserObj.getLoginId(), dealArgs);
        }
    }

    /**
     * 重写父类任务节点处理信息保存方法
     * 把审批结果(通过/不通过)存放入任务节点处理对象taskInfo中
     */
    @Override
    protected void saveTaskInfo() throws Exception{
        //审批结果存入taskInfo对象中
        taskInfo.set(TASK_APPROVAL_RESULT, approvalResult);

        super.saveTaskInfo();
    }
}
