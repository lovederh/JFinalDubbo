package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.ISysFormsService;

import java.util.Map;

/**
 * 2017/1/18
 */
public class SysFormsServiceImpl implements ISysFormsService {
    /**
     * 请假申请-明细时处理
     */
    @Override
    public Record viewAsk4LeaveBean(Record bean) {
        if(bean==null || null==bean.get("id")){
            return bean;
        }
        //执行查询, 取出一些字段真实值
        Record view = Db.findFirst(
                "select d.dictName as leaveType, " +
                "       u.userName as leaveUser, " +
                "       o.orgName as leaveDept " +
                "from t_flow_ask4Leave t left join t_sys_dict d on d.dictId=t.leaveType and dictType='dict_leaveType' " +
                "                        left join t_sys_user u on u.loginId=t.leaveUser " +
                "                        left join t_sys_org o on o.id=t.leaveDept " +
                "where t.id=? ", bean.getInt("id")
        );
        if(view != null){
            bean.set("leaveType", view.get("leaveType"));//请假类型
            bean.set("leaveUser", view.get("leaveUser"));//请假人
            bean.set("leaveDept", view.get("leaveDept"));//申请部门
        }
        return bean;
    }

    /**
     * 根据文件类型, 判断文件是否需要直观呈现, 返回iframe的Url, 供前端请求展示
     */
    @Override
    public String initIframeFileUrl(LoginUserObj loginUserObj, Map<String, String[]> paraMap, Record fileInfo) {
        if(fileInfo==null || CommonTools.isEmpty(fileInfo.get("fileType"))){
            return null;
        }
        String fileType = fileInfo.get("fileType");
        //如果为word文档类型, 则找到同名html, 返回虚拟路径, 供前端重新定向
        if("doc".equals(fileType) || "docx".equals(fileType)){
            return "/" + fileInfo.get("path") + "/" + fileInfo.get("fileName") + ".html";
        }else if("bmp,jpg,jpeg,gif,png".contains(fileType)){
            //如果为图片类型的文件, 则直接提供浏览
            return "/" + fileInfo.get("path") + "/" + fileInfo.get("fileName");
        }
        return null;
    }
}
