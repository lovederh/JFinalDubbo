package edu.dsideal.campus.snaker.interceptor.doFlows;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.snaker.impl.SysFormsServiceImpl;
import edu.dsideal.campus.snaker.interceptor.FlowInterceptorTemp;

/**
 * 请假申请流程
 * edu.dsideal.campus.snaker.interceptor.doFlows.Ask4LeaveInterceptor
 * 2017/1/23
 */
public class Ask4LeaveInterceptor extends FlowInterceptorTemp {

    /**
     * 执行环节处理之前的操作
     */
    public void doBeforeDeal(){
        System.out.println("(测试) 环节处理之前...");
    }
    /**
     * 执行环节处理之后的操作
     * 常用: 流程与其它业务耦合时, 向其它业务表写入/删除/修改一些数据
     */
    public void doAfterDeal(){
        System.out.println("(测试) 环节处理之后...");
    }
    /**
     * 工单启动时候, 之前的操作
     */
    public void doBeforeOrderStart(){
        System.out.println("(测试) 工单启动之前...");
    }
    /**
     * 工单启动时候, 之后的操作
     */
    public void doAfterOrderStart(){
        System.out.println("(测试) 工单启动之后...");
    }
    /**
     * 工单结束(正常归档)之后, 进行的操作
     */
    public void doAfterOrderEnd(){
        System.out.println("(测试) 工单归档之后...");
    }

    /**
     * 执行工单查看之前的操作
     * 常用: 更改浏览次数等(当然这样的话也可以在后置拦截时处理)
     */
    public void doBeforeView(){
        System.out.println("(测试) 构造工单查看对象之前...");
    }
    /**
     * 执行工单查看之后的操作
     * 常用: 处理更改业务数据的展示值
     */
    public void doAfterView(){
        System.out.println("(测试) 构造工单查看对象之后...");
        //明细时, 把需要额外处理的字段做一下处理
        Record bean = formAttrWrapper.getBusinessBean();
        bean = new SysFormsServiceImpl().viewAsk4LeaveBean(bean);
        formAttrWrapper.setBusinessBean(bean);
    }
}
