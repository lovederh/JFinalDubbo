package edu.dsideal.campus.snaker.impl.aszjc;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.aszjc.IFlowAszjcService;
import edu.dsideal.campus.snaker.sql.FlowUserForm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;

/**
 * 2017/2/21 0021
 */
public class FlowAszjcServiceImpl implements IFlowAszjcService{

    @Override
    public List<Record> findAllLoginUsers(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        return Db.find(
            "select t.id, t.loginId, t.userName, t.cardNo " +
            "from t_sys_user t " +
            "where t.isUsed = '1' and t.domain = 'aszjc' "
        );
    }

    @Override
    public List<Record> findExistsUsers(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String taskSetId = CommonTools.getValueFromParaMap(paraMap, "taskSetId");
        if(CommonTools.isEmpty(taskSetId)){
            return null;
        }
        //取出节点操作人列表
        return Db.find(
                "select t.id, t.loginId, t.userName, t.cardNo " +
                "from t_sys_user t " +
                "where t.isUsed = '1' and t.domain = 'aszjc' " +
                "and exists(select 1 from wf_processTaskActorSet a " +
                "           where a.actorValue=t.loginId and a.actorType=1 and a.processTaskSetId=?)",
            taskSetId
        );
    }

    @Override
    public void updateFlowTaskSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        String taskSetId = CommonTools.getValueFromParaMap(paraMap, "taskSetId");//任务环节配置ID
        String isStartTask = CommonTools.getValueFromParaMap(paraMap, "isStartTask");//是否第一环节标识
        String taskDisplay = CommonTools.getValueFromParaMap(paraMap, "taskDisplay");//任务环节显示名
        String hasCcOperator = CommonTools.getValueFromParaMap(paraMap, "hasCcOperator");//是否开启抄送
        String formValue = CommonTools.getValueFromParaMap(paraMap, "formValue");//任务环节使用表单值
        formValue = CommonTools.isEmpty(formValue)? "": formValue;
        //根据主键ID, 获取现有的配置信息
        Record taskSet = Db.findById("wf_processTaskSet", taskSetId);
        if(taskSet == null){
            throw new RuntimeException("未找到当前环节配置信息, 无法保存!");
        }

        //判断环节表单值是否改变, 改变则修改关联的配置属性
        if(! formValue.equals(taskSet.get("formValue"))){
            //若改变的为第一环节表单, 那么需要同步修改wf_processSet表
            FlowUserForm.putFormInfo2ProcessSet(taskSet, formValue, "Y".equals(isStartTask), "wf_processTaskSet");
        }
        taskSet.set("taskDisplay", taskDisplay);
        taskSet.set("hasCcOperator", hasCcOperator);
        Db.update("wf_processTaskSet", taskSet);

        //遍历保存任务环节处理人
        String pickUsersStr = CommonTools.getValueFromParaMap(paraMap, "pickUsersStr");
        if(! "".equals(pickUsersStr)){
            Db.update("delete from wf_processTaskActorSet where processTaskSetId=? ", taskSetId);

            //新增加的任务处理人记录
            List<Record> addActorSetList = new ArrayList<Record>();
            Record actorSet;
            for(String pickUser : pickUsersStr.split(",")){
                actorSet = new Record();
                actorSet.set("actorType", "1");
                actorSet.set("actorValue", pickUser);
                actorSet.set("processTaskSetId", taskSetId);
                actorSet.set("processSetId", taskSet.get("processSetId"));
                actorSet.set("domain", loginUserObj.getDomain());
                actorSet.set(ATTR_PROCESS_ID, taskSet.get(ATTR_PROCESS_ID));
                actorSet.set(ATTR_PROCESS_NAME, taskSet.get(ATTR_PROCESS_NAME));
                actorSet.set(ATTR_TASK_NAME, taskSet.get(ATTR_TASK_NAME));
                addActorSetList.add(actorSet);
            }

            if(! addActorSetList.isEmpty()){
                Db.batchSave("wf_processTaskActorSet", addActorSetList, addActorSetList.size());
            }
        }
    }

    @Override
    public Map<String, String> findActorsInfo(LoginUserObj loginUserObj, Map<String, String[]> paraMap) {
        Map<String, String> actorsInfo = new HashMap<String, String>();
        String existsUserLoginIds = "";
        String existsUserNames = "";

        List<Record> taskActorSets = findExistsUsers(loginUserObj, paraMap);
        if(taskActorSets!=null && !taskActorSets.isEmpty()){
            for (int i=0, size=taskActorSets.size(); i<size ; i++){
                if(i != 0){
                    existsUserLoginIds += ",";
                    existsUserNames += ",";
                }
                existsUserLoginIds += taskActorSets.get(i).get("loginId");
                existsUserNames += taskActorSets.get(i).get("userName");
            }
        }
        actorsInfo.put("existsUserLoginIds", existsUserLoginIds);
        actorsInfo.put("existsUserNames", existsUserNames);
        return actorsInfo;
    }
}
