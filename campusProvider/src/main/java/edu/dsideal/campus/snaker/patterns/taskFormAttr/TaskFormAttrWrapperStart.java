package edu.dsideal.campus.snaker.patterns.taskFormAttr;

import com.jfinal.plugin.activerecord.Db;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;
import edu.dsideal.campus.snaker.sql.HistTaskInfoSql;

/**
 * 任务环节处理表单展示对象的包装类
 * 流程启动时表单展示, 包括新建派发/草稿派发/引用模板
 * 2017/1/10
 */
public class TaskFormAttrWrapperStart extends ITaskFormAttrWrapper {


    public TaskFormAttrWrapperStart(String taskInfoId) {
        super();
        this.defaultFormUrl = "deal/openTaskForm.html";
        this.showType = "form";
        //设置引用的草稿/模板信息ID
        this.taskInfoId = taskInfoId;
    }

    //任务节点副表信息ID, 有值则说明为草稿派发/引用模板
    private String taskInfoId;

    /**
     * 个性化装饰任务环节表单对象
     */
    @Override
    protected void initFormAttrWrapper() throws Exception{
        //取出草稿/模板信息放入taskInfo对象
        if(CommonTools.isNotEmpty(taskInfoId)){
            taskInfo = Db.findById("wf_hist_taskInfo", taskInfoId);
        }

        //新派工单时需要填入工单主题
        String orderTitle;
        if(taskInfo==null || CommonTools.isEmpty(taskInfo.get("orderTitle"))){
            orderTitle = loginUserObj.getUserName() + "发起的" +
                    processSet.get("processDisplay") + "_" + CommonTools.getCurrentDateYmd();
        }else{
            orderTitle = taskInfo.get("orderTitle");
        }
        formAttr.setOrderTitle(orderTitle);

        //根据节点业务数据表, 取出草稿(模板)对应的业务数据
        businessBean = HistTaskInfoSql.findBeanByTaskInfo(taskInfo);
        //判断草稿数据是否有效, 即是否使用同一张存储表
        if(taskInfo==null || businessBean==null || !taskInfo.get("dataTable").equals(processTaskSet.get("dataTable"))){
            //环节数据表与草稿(模板)存储数据的表不一致, 则草稿无效, 故移除
            businessBean = null;
        }else{
            //设置草稿派发标识
            formAttr.setIsDraftStart("Y");
        }

        //当前环节(即启动环节)表单值
        String taskFormValue = processTaskSet.get("formValue");
        //需要扩展表单, 则调用父类方法
        initExtFormUrl(taskFormValue);
    }

    /**
     * 是否需要获取紧邻当前环节的处理环节下, 处理者集合标示
     */
    @Override
    protected boolean checkGetNextActorsFlag() throws Exception{
        return true;
    }
}
