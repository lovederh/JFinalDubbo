package edu.dsideal.campus.snaker.impl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.TaskFormAttr;
import edu.dsideal.campus.snaker.patterns.ITaskFormAttrWrapper;
import edu.dsideal.campus.snaker.patterns.taskFormAttr.TaskFormAttrWrapperApprovalView;
import edu.dsideal.campus.snaker.patterns.taskFormAttr.TaskFormAttrWrapperForm;
import edu.dsideal.campus.snaker.patterns.taskFormAttr.TaskFormAttrWrapperStart;
import edu.dsideal.campus.snaker.patterns.taskFormAttr.TaskFormAttrWrapperView;
import edu.dsideal.campus.snaker.service.IFlowUserFormService;
import edu.dsideal.campus.snaker.sql.ProcessSetSql;
import edu.dsideal.campus.snaker.sql.ProcessTaskSetSql;
import org.snaker.engine.SnakerEngine;
import org.snaker.jfinal.plugin.SnakerPlugin;

import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;
import static edu.dsideal.campus.snaker.FlowConstDefine.TASK_APPROVAL_RESULT;

/**
 * 流程使用的用户表单(即业务表单)服务类
 * 2016/12/22
 */
public class FlowUserFormServiceImpl implements IFlowUserFormService {
    //通过插件获取流程引擎入口
    protected SnakerEngine engine = SnakerPlugin.getEngine();

    /**
     * 基于配置信息, 构造并返回任务环节表单展示对象
     * 注: 通用的处理方法, 内部依照各类型进行处理拆分
     */
    @Override
    public TaskFormAttr initTaskFormAttr(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception{
        String processId = CommonTools.getValueFromParaMap(paraMap, ATTR_PROCESS_ID);//流程ID
        String orderId = CommonTools.getValueFromParaMap(paraMap, ATTR_ORDER_ID);//工单ID
        String taskId = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_ID);//任务ID
        String taskName = CommonTools.getValueFromParaMap(paraMap, ATTR_TASK_NAME);//任务英文名称
        // 展示类型:view展示明细; form:展示表单(无值时默认)
        String showType = CommonTools.getValueFromParaMap(paraMap, "showType");
        //审批结果
        String approvalResult = CommonTools.getValueFromParaMap(paraMap, TASK_APPROVAL_RESULT);

        // 取出流程信息配置和流程与任务节点配置
        Record processSet = ProcessSetSql.findSetByProcessId(processId);//流程配置
        Record processTaskSet = ProcessTaskSetSql.findSetByTaskNameOrId(processId, taskName, taskId);//环节配置
        //注: 在这里取出配置并传递, 而不在包装类中取, 这样做的目的是: 为以后针对配置做逻辑判断预留

        //确认任务环节表单对象的包装类
        ITaskFormAttrWrapper wrapper;
        if(CommonTools.isEmpty(orderId)){
            //新建工单情况
            String taskInfoId = CommonTools.getValueFromParaMap(paraMap, "taskInfoId");//任务节点副表信息ID有值, 则说明为草稿派发
            wrapper = new TaskFormAttrWrapperStart(taskInfoId);
        }else if("approvalTypeView".equals(showType)){
            //流程类型为审批流程情况下, 工单详情展示
            wrapper = new TaskFormAttrWrapperApprovalView();
        }else if("view".equals(showType)){
            //指定环节下明细页面
            wrapper = new TaskFormAttrWrapperView();
        }else{
            wrapper = new TaskFormAttrWrapperForm();
        }

        //包装类, 各属性赋值
        wrapper.loginUserObj = loginUserObj;
        wrapper.processId = processId;
        wrapper.orderId = orderId;
        wrapper.taskId = taskId;
        wrapper.taskName = taskName;
        wrapper.approvalResult = approvalResult;
        wrapper.processSet = processSet;
        wrapper.processTaskSet = processTaskSet;

        //构造并返回任务环节表单展示对象
        TaskFormAttr formAttr = wrapper.buildTaskFormAttr();
        return formAttr;
    }
}
