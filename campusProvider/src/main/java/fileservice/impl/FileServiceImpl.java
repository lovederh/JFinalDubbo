package fileservice.impl;

import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import fileservice.service.IFileService;

import java.io.File;
import java.util.Date;

/**
 * 文件上传操作
 *
 * @author zhaoyou
 */
public class FileServiceImpl implements IFileService {

    // 初始化项目路径
    private static String webContextPath = JFinal.me().getServletContext().getRealPath("/");

    /**
     * 保存上传文件的方法
     *
     * @param fileInfo
     * @param fileId
     * @param loginId
     * @param fileType
     * @param saveSeverFile
     * @param file_name
     * @param real_name
     * @param selfRelativePath
     * @return
     */
    @Override
    public Record saveUploadFile(Record fileInfo, String fileId, String loginId, String fileType, File saveSeverFile, String file_name, String real_name, String selfRelativePath) {
        if (CommonTools.isNotEmpty(real_name)) {
            fileInfo.set("fileId", fileId);
            fileInfo.set("uploadUser", loginId);
            fileInfo.set("uploadDate", new Date());// 上传时间
            fileInfo.set("fileType", fileType);// 文件类型
            fileInfo.set("fileSize", saveSeverFile.length());// 文件大小
            fileInfo.set("fileName", file_name);// 保存在服务器中的文件名
            // 去除文件名中所有空格, 防止出现下载时文件名转码问题
            real_name = real_name.replace(" ", "");
            fileInfo.set("realName", real_name);// 文件本名
            fileInfo.set("path", selfRelativePath);
            Db.save("t_sys_file", "id", fileInfo);
            return fileInfo;
        }
        return null;
    }

    /**
     * 保存上传文件的方法, 同时在对应路径生成缩略图的方法
     *
     * @param fileInfo
     * @param fileId
     * @param loginId          当前登陆人
     * @param fileType
     * @param saveSeverFile
     * @param file_name
     * @param real_name
     * @param selfRelativePath 自定义的文件上传路径
     * @param thumbnailPath    缩略图存放路径
     * @param thumbnailWidth   生成缩略图宽度
     * @param thumbnailHeight  生成缩略图高度
     * @return
     */
    @Override
    public Record saveUploadFileAndCreateThumbnail(Record fileInfo, String fileId, String loginId, String fileType, File saveSeverFile, String file_name, String real_name, String selfRelativePath, String thumbnailPath, int thumbnailWidth, int thumbnailHeight) {
        if (CommonTools.isNotEmpty(real_name)) {
            fileInfo.set("fileId", fileId);
            fileInfo.set("uploadUser", loginId);
            fileInfo.set("uploadDate", new Date());
            fileInfo.set("fileType", fileType);
            fileInfo.set("fileSize", saveSeverFile.length());// 文件大小
            fileInfo.set("fileName", file_name);
            // 去除文件名中所有空格, 防止出现下载时文件名转码问题
            real_name = real_name.replace(" ", "");
            fileInfo.set("realName", real_name);
            fileInfo.set("path", selfRelativePath);
            fileInfo.set("thumbnailPath", thumbnailPath);
            fileInfo.set("delFlag", "0");
            Db.save("t_sys_file", "id", fileInfo);
            return fileInfo;
        }
        return null;
    }


    /**
     * 删除已上传文件
     *
     * @param id
     * @return
     */
    @Override
    public String delFile(String id) {
        //删除t_sys_file表中的文件
        StringBuilder delFile = new StringBuilder();
        delFile.append("update t_sys_file set delFlag = '1' where id = '").append(id).append("'");
        Db.update(delFile.toString());
        return CommonTools.getMsgJson(true, "删除成功!");
    }

    /**
     * 上传文件后下载请求
     *
     * @param id
     * @return
     */
    @Override
    public Record findDownloadFile(String id) {
        if (CommonTools.isEmpty(id)) {
            return null;
        }
        return Db.findById("t_sys_file", "id", id);
    }
}
