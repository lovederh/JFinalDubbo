package edu.dsideal.campus.administrative.config;

import edu.dsideal.campus.administrative.controller.NoticeController;
import com.jfinal.config.Routes;

/**
 * 2017/1/5 0005
 */
public class NoticeConfig extends Routes {
    @Override
    public void config() {
        add("/notice", NoticeController.class, "/WEB-INF/view");
    }

}
