package edu.dsideal.campus.administrative.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.INoticeService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2017/1/4 0004
 */
@Before(IocInterceptor.class)
public class NoticeController extends Controller {
    @Inject.BY_NAME
    INoticeService noticeService;

    @Before(InitInterceptor.class)
    public void index() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        render("bizroot/administrative/notice.html");
    }

    /**
     * 显示通知公告列表分页
     */
    public void queryNoticePager() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.queryNoticePager(loginUserObj, getParaMap()));
    }

    /**
     * 新增通知通告弹窗
     */
    public void addNotice() {
        setAttr("dtoName", getPara("dtoName"));
        setAttr("domain", getPara("domain"));
        render("bizroot/administrative/addNotice.html");
    }

    /**
     * 删除通知公告按钮
     */
    public void delNotice() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.delNotice(loginUserObj, getPara("ids")));
    }

    /**
     * 维护人员界面
     */
    public void selectMaintainStaff() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        setAttr("publisherId", loginUserObj.getLoginId());
        setAttr("dtoName", getPara("dtoName"));
        setAttr("domain", getPara("domain"));
        // 选择人员
        setAttr("pickUsersStr", getPara("pickUsersStr"));
        //render("bizroot/administrative/addStaffWayTab.html");
        render("bizroot/administrative/staffWayTab.html");
    }

    /**
     * 保存通告
     */
    public void saveNotice() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.saveNotice(loginUserObj, getParaMap()));
    }


    /**
     * 初始化树
     */
    public void initTree() {
        renderJson(getTreeId(getPara("paraTreeId")) == null ? "{}" : getTreeId(getPara("paraTreeId")));
    }

    /**
     * 不同页签显示不同的树
     */
    private List<Record> getTreeId(String treeId) {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        List<Record> selectedTyteTree = null;
        switch (treeId) {
            // 选择分组
            case "Group":
                selectedTyteTree = noticeService.groupTree("t_sys_group", getParaMap(), loginUserObj);
                break;
            // 选择学生
            case "Student":
                selectedTyteTree = noticeService.studentTree(getParaMap(), loginUserObj);
                break;
            // 选择教师
            case "Teacher":
                selectedTyteTree = noticeService.teacherTree("t_sys_org", getParaMap(), loginUserObj);
                break;
            // 选择家长
            case "Parents":
                selectedTyteTree = null;
                break;
            default:
                break;
        }
        return selectedTyteTree;
    }

    /**
     * 显示选择节点机构的人员信息
     * 查询机构树列表右侧显示相对应的的人员信息列表
     */
    public void queryTeacherData() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.queryTeacherData(loginUserObj, getParaMap()));
    }

    /**
     * 查询已选择的人员列表
     */
    public void querySelectedStaff() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.querySelectedStaff(loginUserObj, getParaMap()));

    }

    /**
     * 添加分组
     */
    public void addGroup() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        Map<String, Object> msgMap = new HashMap<String, Object>();
        msgMap.put("groupId", noticeService.addGroup(loginUserObj, getParaMap()));
        msgMap.put("status", ConstStatic.JSON_STATUS_SUCCESS);
        msgMap.put("msg", "新增节点成功!");
        renderJson(JsonKit.toJson(msgMap));
    }

    /**
     * 编辑/删除分组节点
     */
    public void editGroupNote() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.editGroupNote(loginUserObj, getParaMap()));
    }

    /**
     * 组员维护界面
     */
    public void editGroupMember() {
        setAttr("dtoName", getPara("dtoName"));
        setAttr("domain", getPara("domain"));
        // 选择人员
        setAttr("groupId", getPara("groupId"));
        //render("bizroot/administrative/editGroupUser.html");
        render("bizroot/administrative/editGroupMember.html");
    }

    /**
     * 添加分组人员
     */
    public void addGroupUser() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.addGroupUser(loginUserObj, getParaMap()));
    }

    /**
     * 查询分组人员信息
     */
    public void queryGroupData() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.queryGroupData(loginUserObj, getParaMap()));
    }

    /**
     * 查看通知公告信息
     */
    public void viewNotice() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        Map<String, Object> map = noticeService.viewNotice(loginUserObj, getParaMap());
        for (Map.Entry<String, Object> entry : map.entrySet()){
            setAttr(entry.getKey(), entry.getValue());
        }
        render("bizroot/administrative/viewNotice.html");
    }

    /**
     * 保存分组成员
     */
    public void saveGroupMember() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.saveGroupMember(loginUserObj, getParaMap()));
    }

    /**
     * 接收通知公告人员是否人数处理
     */
    public void receiveNoticeUserNum() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(noticeService.receiveNoticeUserNum(loginUserObj, getParaMap()));
    }

}
