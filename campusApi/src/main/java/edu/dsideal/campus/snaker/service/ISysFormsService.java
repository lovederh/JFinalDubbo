package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.Map;

/**
 * 系统表单请求处理类
 * 2017/1/18
 */
public interface ISysFormsService {
    /**
     * 请假申请-明细时处理
     */
    Record viewAsk4LeaveBean(Record bean);

    /**
     * 根据文件类型, 判断文件是否需要直观呈现, 返回iframe的Url, 供前端请求展示
     */
    String initIframeFileUrl(LoginUserObj loginUserObj, Map<String, String[]> paraMap, Record fileInfo);
}
