package edu.dsideal.campus.snaker;

/**
 * 流程处理中常量
 * 2016/11/21
 */
public class FlowConstDefine {
    /**
     * 工单流转中重要的交互字段(流程默认)
     */
    //流程定义ID
    public static final String ATTR_PROCESS_ID = "processId";
    //流程英文名称
    public static final String ATTR_PROCESS_NAME = "processName";
    //工单ID
    public static final String ATTR_ORDER_ID = "orderId";
    //任务ID
    public static final String ATTR_TASK_ID = "taskId";
    //任务英文名称
    public static final String ATTR_TASK_NAME = "taskName";


    /**
     * 工单流转中重要的交互字段(自定义)
     */
    //主题字段
    public static final String ATTR_ORDER_TITLE = "orderTitle";
    //任务节点中备注说明字段名(第一环节不需要填写)
    public static final String TASK_APPROVAL_REMARK = "approvalRemark";
    //任务节点中审批结果字段名
    public static final String TASK_APPROVAL_RESULT = "approvalResult";
    //任务节点中抄送角色的表单字段
    public static final String ROLE_CC_OPERATORS = "roleCcOperators";
    //任务节点中抄送人员(教师)的表单字段
    public static final String TEACHER_CC_OPERATORS = "teacherCcOperators";

    // 表单中手动选择的审批人字段
    public static final String SELF_PICK_DEAL_ACTORS = "selfPickActors";
    //处理环节, 处理角色选取的表单字段前缀(view/layout/snaker/formLayout.html)
    public static final String SELECT_DEAL_ROLES = "selectRoles";
    //处理环节, 处理教师选取的表单字段前缀(view/layout/snaker/formLayout.html)
    public static final String SELECT_DEAL_TEACHERS = "selectTeachers";

    //审批通过保存值
    public static final String APPROVAL_PASS_VALUE = "agree";
    //审批不通过保存值
    public static final String APPROVAL_NOT_PASS_VALUE = "disagree";


    /**
     * 任务环节处理, 流转时传递数据集合中对象key值
     * 供后续配置的拦截器/决策器/处理器等使用
     */
    //登陆人账号
    public static final String ARGS_LOGIN_USER_KEY = "userLoginId";
    //企业域
    public static final String ARGS_LOGIN_DOMAIN_KEY = "userDomain";
    //环节使用表单对象的ID
    public static final String ARGS_FORM_VALUE_KEY = "form.value";
    //环节使用表单对象的类型
    public static final String ARGS_FORM_TYPE_KEY = "form.type";
    //业务数据主表表名
    public static final String ARGS_TABLE_NAME_KEY = "data.table";
    //存放业务数据的ID
    public static final String ARGS_RECORD_ID_KEY = "data.recordId";


    /**
     * 任务节点信息表数据, 返回给前端时, 使用的key
     */
    //任务节点处理副表对象(wf_hist_taskInfo)
    public static final String BUSINESS_TASK_INFO_KEY = "taskInfo";
    //业务数据存储对象
    public static final String BUSINESS_BEAN_KEY = "bean";
    //使用表单对应的html字符串对象
    public static final String FORM_HTML_OBJ_KEY = "formHtmlObj";


    /**
     * 其它常量设置
     */
    //系统流程存放路径
    public static final String SYS_FLOWS_FILES_PATH = "flows/sys";
    //用户流程存放路径
    public static final String USER_FLOWS_FILES_PATH = "flows/user";
    //设置判断节点处, 最多的可选择作为条件的字段数目
    public static final int MAX_DECISION_FIELD_NUM = 20;
    //处理者为角色时, 前缀的特殊标识(防止与登陆账号重复)
    public static final String ROLE_MIX_SUFFIX = "_*role#";
    //处理者为部门(org)时, 前缀的特殊标识(防止与登陆账号重复)
    public static final String ORG_MIX_SUFFIX = "_*org#";


    /**
     * 环节处理人类型常量
     * 0手动选择; 1人员(教师); role角色; dept部门; 5分组
     */
    public static final String ACTOR_TYPE_SELF_PICK = "0";
    public static final String ACTOR_TYPE_TEACHER = "1";//此处为了之前版本兼容性, 依旧使用1作为人员类型
    public static final String ACTOR_TYPE_ROLE = "role";
    public static final String ACTOR_TYPE_ORG = "org";
    public static final String ACTOR_TYPE_USER_GROUP = "5";
}
