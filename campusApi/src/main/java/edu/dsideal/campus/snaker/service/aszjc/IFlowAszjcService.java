package edu.dsideal.campus.snaker.service.aszjc;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 2017/2/21 0021
 */
public interface IFlowAszjcService {

    List<Record> findAllLoginUsers(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    List<Record> findExistsUsers(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    void updateFlowTaskSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    Map<String, String> findActorsInfo(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
}
