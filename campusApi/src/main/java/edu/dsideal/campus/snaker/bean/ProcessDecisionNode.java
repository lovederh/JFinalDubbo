package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程图内决策类型节点对象
 * 2017/1/20
 */
public class ProcessDecisionNode {
    //流程定义ID
    private String processId;
    //流程定义英文名
    private String processName;
    //流程信息配置ID
    private Object processSetId;

    //流程内决策节点整体对象
    private Record decisionNode;
    //流程内决策节点ID
    private Object decisionNodeId;

    //决策节点来源的环节表(递归查询到可处理的task)
    private List<Record> decisionFromList = new ArrayList<Record>();

    //决策条件配置列表
    private List<Record> decisionsList = new ArrayList<Record>();


    public String getProcessId() {
        return processId;
    }
    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }
    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Object getProcessSetId() {
        return processSetId;
    }
    public void setProcessSetId(Object processSetId) {
        this.processSetId = processSetId;
    }

    public Record getDecisionNode() {
        return decisionNode;
    }
    public void setDecisionNode(Record decisionNode) {
        this.decisionNode = decisionNode;
    }

    public Object getDecisionNodeId() {
        return decisionNodeId;
    }
    public void setDecisionNodeId(Object decisionNodeId) {
        this.decisionNodeId = decisionNodeId;
    }

    public List<Record> getDecisionFromList() {
        return decisionFromList;
    }
    public void setDecisionFromList(List<Record> decisionFromList) {
        this.decisionFromList = decisionFromList;
    }

    public List<Record> getDecisionsList() {
        return decisionsList;
    }
    public void setDecisionsList(List<Record> decisionsList) {
        this.decisionsList = decisionsList;
    }
}
