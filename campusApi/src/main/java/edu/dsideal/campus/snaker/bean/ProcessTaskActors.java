package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Record;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 按照类型划分后, 单独查询出来的处理者对象
 * 包含信息: 处理者集合, 当前处理者类型, 构造的字符串
 * 注意: (1)此对象内所有的处理者必然为同一种处理者类型的
 *      (2)处理者的总集保存在ProcessTaskAllActorsObj
 * 2017/3/2
 */
public class ProcessTaskActors implements Serializable {

    //环节处理者集合, 内部key: actorDisplay/actorValue/actorType/actorTypeDisplay
    private List<Record> actorList = new ArrayList<Record>();

    //处理者类型(见FlowConstDefine.java)
    private String actorType;

    //已选处理者的字符串(中文信息)
    private String actorDisplayStr = "";

    //已选处理者的真实值存储字符串
    private String actorValueStr = "";


    /**
     * 构造方法, 传入环节处理者集合和处理者类型即可
     * 内部遍历处理构造actorDisplayStr/actorValueStr
     */
    public ProcessTaskActors(List<Record> tempList, String tempType) {
        actorType = tempType;
        actorList = tempList==null? new ArrayList<Record>(): tempList;
        Record temp;
        for (int i=0, size=actorList.size(); i<size ; i++){
            temp = actorList.get(i);
            if(i != 0){
                actorDisplayStr += ", ";
                actorValueStr += ", ";
            }
            actorDisplayStr += temp.get("actorDisplay");
            actorValueStr += temp.get("actorValue");
        }
    }


    public List<Record> getActorList() {
        return actorList;
    }
    public void setActorList(List<Record> actorList) {
        this.actorList = actorList;
    }

    public String getActorType() {
        return actorType;
    }
    public void setActorType(String actorType) {
        this.actorType = actorType;
    }

    public String getActorDisplayStr() {
        return actorDisplayStr;
    }

    public void setActorDisplayStr(String actorDisplayStr) {
        this.actorDisplayStr = actorDisplayStr;
    }

    public String getActorValueStr() {
        return actorValueStr;
    }

    public void setActorValueStr(String actorValueStr) {
        this.actorValueStr = actorValueStr;
    }
}
