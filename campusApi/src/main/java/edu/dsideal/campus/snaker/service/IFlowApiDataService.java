package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 流程数据中, 后期需要从外部获取的请求处理
 * 如: 跨库的人员/角色/分组信息
 * 2017/2/27
 */
public interface IFlowApiDataService {
    /**
     * 根据当前登录人所在企业域, 初始化获取角色根节点
     */
    List<Record> initDomainRoleTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 根据当前选中的树节点, 查询获取可供用户选择的角色列表
     */
    List<Record> findAllDomainRole(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 根据当前选中的树节点(部门ID), 查询获取可供用户选择的人员(教师)列表
     */
    List<Record> findAllTeacherByOrgId(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
}
