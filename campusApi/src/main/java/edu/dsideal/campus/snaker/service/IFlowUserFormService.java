package edu.dsideal.campus.snaker.service;

import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.TaskFormAttr;

import java.util.Map;

/**
 * 流程使用的用户表单(即业务表单)服务类
 * 2016/12/22
 */
public interface IFlowUserFormService {

    /**
     * 基于配置信息, 构造并返回任务环节表单展示对象
     * 注: 通用的处理方法, 内部依照各类型进行处理拆分
     */
    TaskFormAttr initTaskFormAttr(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
}
