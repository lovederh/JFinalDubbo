package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_ROLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.ACTOR_TYPE_TEACHER;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;

/**
 * 环节处理对象
 * 2017/2/24
 */
public class ProcessTaskAllActorsObj implements Serializable {
    //处理环节配置ID
    private Object taskSetId;
    //环节英文名称
    private String taskName;
    //环节展示名称
    private String taskDisplay;

    //按照各类型(即为key值, 参考FlowConstDefine.java)区分的处理者对象集合
    private Map<String, ProcessTaskActors> actorMap = new HashMap<String, ProcessTaskActors>();

    //环节处理者全集, 为taskActorMap取全集
    private List<Record> actorsAllData = new ArrayList<Record>();

    public ProcessTaskAllActorsObj(){

    }

    public ProcessTaskAllActorsObj(ProcessTaskActors... taskActorsArray){
        if(taskActorsArray!=null && taskActorsArray.length>0){
            for (ProcessTaskActors taskActors : taskActorsArray){
                actorMap.put(taskActors.getActorType(), taskActors);
            }
        }
    }

    public ProcessTaskAllActorsObj(Record taskSet, ProcessTaskActors... taskActorsArray){
        taskSetId = taskSet.get("taskSetId");
        taskName = taskSet.get(ATTR_TASK_NAME);
        taskDisplay = taskSet.get("taskDisplay");
        if(taskActorsArray!=null && taskActorsArray.length>0){
            for (ProcessTaskActors taskActors : taskActorsArray){
                actorMap.put(taskActors.getActorType(), taskActors);
                actorsAllData.addAll(taskActors.getActorList());
            }
        }
    }

    public Object getTaskSetId() {
        return taskSetId;
    }

    public void setTaskSetId(Object taskSetId) {
        this.taskSetId = taskSetId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDisplay() {
        return taskDisplay;
    }

    public void setTaskDisplay(String taskDisplay) {
        this.taskDisplay = taskDisplay;
    }

    public Map<String, ProcessTaskActors> getActorMap() {
        return actorMap;
    }

    public void setActorMap(Map<String, ProcessTaskActors> actorMap) {
        this.actorMap = actorMap;
    }

    public List<Record> getActorsAllData() {
        return actorsAllData;
    }

    public void setActorsAllData(List<Record> actorsAllData) {
        this.actorsAllData = actorsAllData;
    }
}
