package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Record;

import java.io.Serializable;
import java.util.List;

/**
 * 任务环节处理表单展示对象, 初始化完成后, 前端最终确认后执行跳转
 */
public class TaskFormAttr implements Serializable {

    //工单主题(新建工单有效)
    private String orderTitle;
    //草稿派发标识(新建工单有效)
    private String isDraftStart = "N";

    //流程信息配置对象(wf_processSet)
    private Record processSet;
    //当前任务节点配置对象(wf_processTaskSet)
    private Record processTaskSet;

    //审批结果
    private String approvalResult;
    //任务节点基本信息
    private Record taskInfo;
    //节点业务数据(用户表单数据)
    private Record businessBean;
    //其它数据, 可以任意构造(符合json格式即可, 便于前端使用)
    private Object otherData = null;

    //用户自定义表单时有效, 为前端使用的表单html字符串
    private String formHtmlObj = "";
    //最终url处理方式(forward请求转发/render打开页面-默认)
    private String formUrlType;
    //展示环节表单的url地址
    private String formUrl;

    //紧邻当前环节的处理环节下, 处理者集合
    private List<ProcessTaskAllActorsObj> nextTaskActors;

    public String getOrderTitle() {
        return orderTitle;
    }
    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getIsDraftStart() {
        return isDraftStart;
    }
    public void setIsDraftStart(String isDraftStart) {
        this.isDraftStart = isDraftStart;
    }

    public Record getProcessSet() {
        return processSet;
    }
    public void setProcessSet(Record processSet) {
        this.processSet = processSet;
    }

    public Record getProcessTaskSet() {
        return processTaskSet;
    }
    public void setProcessTaskSet(Record processTaskSet) {
        this.processTaskSet = processTaskSet;
    }

    public String getApprovalResult() {
        return approvalResult;
    }
    public void setApprovalResult(String approvalResult) {
        this.approvalResult = approvalResult;
    }

    public Record getTaskInfo() {
        return taskInfo;
    }
    public void setTaskInfo(Record taskInfo) {
        this.taskInfo = taskInfo;
    }

    public Record getBusinessBean() {
        return businessBean;
    }
    public void setBusinessBean(Record businessBean) {
        this.businessBean = businessBean;
    }

    public String getFormHtmlObj() {
        return formHtmlObj;
    }
    public void setFormHtmlObj(String formHtmlObj) {
        this.formHtmlObj = formHtmlObj;
    }

    public String getFormUrlType() {
        return formUrlType;
    }
    public void setFormUrlType(String formUrlType) {
        this.formUrlType = formUrlType;
    }

    public String getFormUrl() {
        return formUrl;
    }
    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }

    public Object getOtherData() {
        return otherData;
    }
    public void setOtherData(Object otherData) {
        this.otherData = otherData;
    }

    public List<ProcessTaskAllActorsObj> getNextTaskActors() {
        return nextTaskActors;
    }
    public void setNextTaskActors(List<ProcessTaskAllActorsObj> nextTaskActors) {
        this.nextTaskActors = nextTaskActors;
    }
}
