package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import org.snaker.engine.entity.HistoryTask;

import java.util.List;

/**
 * 通用的数据请求, 减少其余处理类的工作职责
 * 2016/12/16
 */
public interface IBaseFlowDbService {
    /**
     * 用户定制的表单列表(供下拉框用)
     */
    List<Record> findUserFormList(LoginUserObj loginUserObj);
    /**
     * 根据字典类型, 查询字典返回真实值与显示名(供下拉框用)
     */
    List<Record> findDictListByType(LoginUserObj loginUserObj, String dictType);

    /**
     * 根据登陆人ID, 拿到其所在权限组集合
     */
    String[] initUserPrivs(LoginUserObj loginUserObj);

    /**
     * 根据流程配置表ID, 取出一条流程配置信息
     */
    Record findProcessSetById(String processSetId);
    /**
     * 通过已经安装过的流程ID, 获取流程配置信息(wf_processSet表)
     */
    Record findProcessSetByProcessId(String processId);

    /**
     * 根据工单ID, 查询一条当前工单信息(查询wf_hist_order表)
     */
    Record findHistoryOrderById(String orderId);

    /**
     * 根据历史节点信息, 查询一条历史任务, 即查询wf_hist_task表
     */
    HistoryTask findHistoryTaskById(String taskId);
}
