package edu.dsideal.campus.snaker.bean;

import java.io.Serializable;

/**
 * 待安装流程
 */
public class FlowSource implements Serializable {
    // 流程配置表ID
    private String processSetId;

    // 流程类型(sys:系统流程; user:用户定制流程)
    private String processType;
	// 流程中文名称
	private String processDisplay;

    //流程定制类型
    private String customType;
    //流程定制版本号
    private String customVersion;

    //流程启动使用表单类型
    private String formType;
    //流程启动使用表单名
    private String formDisplay;

	// 流程文件名称
	private String fileName;
    // 文件资源名称(路径+名称)
    private String fileResource;


    public String getProcessSetId() {
        return processSetId;
    }
    public void setProcessSetId(String processSetId) {
        this.processSetId = processSetId;
    }

    public String getProcessDisplay() {
        return processDisplay;
    }
    public void setProcessDisplay(String processDisplay) {
        this.processDisplay = processDisplay;
    }

    public String getProcessType() {
        return processType;
    }
    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getCustomType() {
        return customType;
    }
    public void setCustomType(String customType) {
        this.customType = customType;
    }

    public String getCustomVersion() {
        return customVersion;
    }
    public void setCustomVersion(String customVersion) {
        this.customVersion = customVersion;
    }

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileResource() {
        return fileResource;
    }
    public void setFileResource(String fileResource) {
        this.fileResource = fileResource;
    }

    public String getFormType() {
        return formType;
    }
    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getFormDisplay() {
        return formDisplay;
    }
    public void setFormDisplay(String formDisplay) {
        this.formDisplay = formDisplay;
    }
}
