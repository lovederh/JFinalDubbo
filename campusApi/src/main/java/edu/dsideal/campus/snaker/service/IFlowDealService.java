package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.Map;

/**
 * 流程处理服务类
 */
public interface IFlowDealService {
    /**
     * 标记抄送给我的工单为已读
     */
    String ccOrdersRead(LoginUserObj loginUserObj, String[] userPriv, Map<String, String[]> paraMap) throws Exception;
    /**
     * 工单强制归档
     */
    String terminateOrder(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
    /**
     * 物理删除指定工单
     */
    void physicalDeleteOrder(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
    /**
     * 接单(确认受理)
     */
    void acceptOrderTask(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;


    /**
     * 通过传入的流程名称/流程ID, 获取对应最新版本的流程配置信息
     */
    Record findProcessSetByPara(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    /**
     * 查询并构造当前活跃环节使用的表单url, 同时拼接当前环节名称taskName
     * 实际: 非特殊流程情况下, 均会查询并返回/flowUserForm/initTaskForm?taskName=...
     */
    String findTaskFormUrlByTaskId(LoginUserObj loginUserObj, String processId, String taskId);


    /**
     * 删除草稿
     */
    void deleteDraft(String taskInfoId);
    /**
     * 保存草稿数据, 不启动工单
     */
    void saveDraftData(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;

    /**
     * 执行任务节点处理, 同时保存业务数据
     */
    void dealTaskSaveBusiness(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
}
