package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 流程查询服务类
 */
public interface IFlowQueryService {
    /**
     * 依据类型, 查询某指定流程对应的企业域安装情况
     */
    List<Record> sysFlowDomainsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 查询已安装的流程列表
     */
    Page<Record> installedFlowsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    /**
     * 已安装流程前提下, 查询我的可申请流程集合
     */
    List<Record> iCanStartFlowsData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 所有流程数据的分页查询
     */
    Page<Record> allOrderGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 由我发起的工单列表数据, 分页查询结果
     */
    Page<Record> myStartOrdersData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 我的待办列表, 包括待处理和抄送给我的工单
     */
    Page<Record> myDealGridData(LoginUserObj loginUserObj, String[] userPriv, Map<String, String[]> paraMap);

    /**
     * 工单流转的历史环节
     */
    List<Record> historyGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 流程图数据
     */
    Map<String,String> flowPictureData(String processId, String orderId);
}
