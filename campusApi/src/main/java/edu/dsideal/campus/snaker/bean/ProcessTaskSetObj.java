package edu.dsideal.campus.snaker.bean;

import com.jfinal.plugin.activerecord.Record;

import java.io.Serializable;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_NAME;

/**
 * 流程下所有环节配置信息使用的对象
 */
public class ProcessTaskSetObj implements Serializable {
    // 任务环节配置
    private Record taskSet;

    // 任务环节配置ID
    private Object taskSetId;
    //任务节点英文名
    private String taskName;
    // 任务环节中文名称
    private String taskDisplay;
    // 是否第一环节(Y: 是; N: 否)
    private String isStartTask;


    public ProcessTaskSetObj(){
        super();
    }

    public ProcessTaskSetObj(Record taskSet) {
        this.taskSet = taskSet;
        this.taskSetId = taskSet.get("id");
        this.taskName = taskSet.get(ATTR_TASK_NAME);
        this.taskDisplay = taskSet.get("taskDisplay");
        this.isStartTask = taskSet.get("isStartTask");
    }

    public Record getTaskSet() {
        return taskSet;
    }
    public void setTaskSet(Record taskSet) {
        this.taskSet = taskSet;
    }

    public Object getTaskSetId() {
        return taskSetId;
    }
    public void setTaskSetId(Object taskSetId) {
        this.taskSetId = taskSetId;
    }

    public String getTaskDisplay() {
        return taskDisplay;
    }
    public void setTaskDisplay(String taskDisplay) {
        this.taskDisplay = taskDisplay;
    }

    public String getIsStartTask() {
        return isStartTask;
    }

    public void setIsStartTask(String isStartTask) {
        this.isStartTask = isStartTask;
    }

    public String getTaskName() {
        return taskName;
    }
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }
}
