package edu.dsideal.campus.snaker.service;

import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;
import edu.dsideal.campus.snaker.bean.ProcessTaskSetObj;

import java.util.List;
import java.util.Map;

/**
 * 流程配置信息服务类(已安装流程)
 */
public interface IFlowSetService {
    /**
     * 流程文件删除
     */
    String deleteFlowFile(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 控制已安装过的系统流程不再可用
     * 操作: 对于所有安装该流程的企业域, 均执行该流程的卸载操作
     */
    void batchUndeployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
    /**
     * 根据指定的流程ID, 执行流程卸载
     */
    void undeployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;

    /**
     * 根据指定的流程文件资源全路径, 执行流程安装方法
     */
    void deployFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;

    /**
     * 系统流程的批量安装
     */
    void batchDeploySysFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;

    /**
     * 查找流程环节配置基础信息, 只包含配置ID和环节名称, 详细信息需要单独再依据ID查询
     */
    List<ProcessTaskSetObj> findBaseFlowTaskSets(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    /**
     * 查找流程环节配置详细信息, 返回ProcessTaskSetObj对象
     * 覆盖wf_processTaskSet(人员需要单独查询)
     */
    ProcessTaskSetObj findOneFlowTaskSetObj(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    /**
     * 查找流程环节配置的处理者信息
     */
    ProcessTaskAllActorsObj findProcessTaskActors(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 已经安装过的流程, 整体配置修改
     */
    void updateFlowSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;

    /**
     * 流程环节配置的更新方法, 主要包含环节表单配置和审批人配置
     */
    void updateFlowTaskSet(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;
}
