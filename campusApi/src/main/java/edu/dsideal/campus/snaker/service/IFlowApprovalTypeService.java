package edu.dsideal.campus.snaker.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 审批类型工单处理特殊服务类
 * 2016/12/29
 */
public interface IFlowApprovalTypeService {
    /**
     * 只查询和展示涉及审批环节的处理信息列表
     */
    List<Record> approvalResultGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
}
