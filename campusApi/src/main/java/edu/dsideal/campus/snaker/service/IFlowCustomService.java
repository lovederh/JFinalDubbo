package edu.dsideal.campus.snaker.service;

import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.bean.FlowSource;

import java.util.List;
import java.util.Map;

/**
 * 流程用户定制服务类
 */
public interface IFlowCustomService {
    /**
     * 所有的系统流程列表数据
     */
    List<FlowSource> allSysFlowGridData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    /**
     * 查询待安装的, 用户定制流程列表
     */
    List<FlowSource> userNoInstallData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 保存用户定制的流程
     */
    void saveCustomFlow(LoginUserObj loginUserObj, Map<String, String[]> paraMap) throws Exception;


}
