package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IBoService {

    /**
     * 根据树结点ID删除树数据
     */
    boolean delTreeNodeById(Map<String, String[]> paraMap) throws Exception;
    /**
     * 保存节点
     * @param model
     * @return
     */
    Record saveTreeNode(BaseTable model) throws SQLException;

    /**
     * 查询字段设置的校验信息
     * @return {colName: "require=true"}
     * @param dtoName
     * @return
     */
    Map<String, String> getValidate(String dtoName);

    /**
     * 返回字典数据的map
     * "dict_boolean":{"AAB001":"是","AAB002":"否"},
     * "dict_gender":{"AAC001":"男","AAC002":"女"}
     * @param dtoName
     * @return
     */
    Map<String, Map<String, String>> getModelDict(String dtoName, String domain);

    /**
     * 根据主键查询基础表数据
     *
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    Record getDoDataById(Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 查询出表单显示的列信息
     * @param dtoName
     * @param loginUserObj
     * @return
     */
    List<Record> getFormShowCol(String dtoName, LoginUserObj loginUserObj);

    /**
     * 查询业务模型和键配置
     *
     * @param boName
     * @param domain
     * @return
     */
    Record getBoAndKeyRule(String boName, String domain);

    /**
     * 查询权限列
     *
     * @param boconfig 列配置record
     * @param alias    别名
     * @return 返回权限列col1, col2, col3
     */
    String getRoleCols(List<Record> boconfig, String domain, String alias);

    /**
     * 查询关联字段的数据
     *
     * @param boName 传递业务模型的名
     * @param domain
     * @return
     */
    String getContactTableData(String boName, String where, String domain);

    /**
     * 获得分页数据
     * @param paraMap
     * @param loginUserObj
     * @return
     */

    Map<String, Object> getDoByPage(Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 查询bo关联do表中的数据
     *
     * @param dtoName
     * @param domain
     * @return
     */
    List<Record> getDoDataList(String dtoName, String domain);

    /**
     * 查询bo对象
     *
     * @param boName 业务模型名称
     * @param domain 企业域
     * @return
     */
    Record getBo(String boName, String domain);

    /**
     * 查询权限列
     * @param dtoName
     * @param loginUserObj
     * @return
     */
    String getBoRoleCols(String dtoName, LoginUserObj loginUserObj);

    /**
     * 列表显示方法
     * @param dtoName
     * @param loginUserObj
     * @return
     */

    List<Record> getBoConfig(String dtoName, LoginUserObj loginUserObj);

    /**
     * 业务模型保存方法
     *
     * @param model
     * @return
     * @throws Exception
     */
    boolean save(BaseTable model) throws Exception;

    /**
     * 业务模型更新方法
     *
     * @param model
     * @return
     */
    boolean update(BaseTable model);

    /**
     * 业务模型删除方法
     *
     * @param model
     * @param id
     * @return
     */
    boolean deleteById(BaseTable model, String[] id);

    /**
     * 业务模型主键规则配置
     *
     * @param model
     * @return
     */
    Record getKeyRuleData(BaseTable model);

    Record getKeyRuleData(String boName, String domain);
    /**
     * 业务模型主键规则保存
     *
     * @param model
     * @return
     */
    boolean saveKeyRuleConfig(BaseTable model);

    List<Record> getTreeGridData(String dtoName, String domain);
}
