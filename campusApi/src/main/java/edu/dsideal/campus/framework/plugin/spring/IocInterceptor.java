/**
 * Copyright (c) 2011-2015, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.dsideal.campus.framework.plugin.spring;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Field;

/**
 * IocInterceptor.
 */
public class IocInterceptor implements Interceptor {
	
	static ApplicationContext ctx;
    // ActionInvocation 改为2.0的Invocation
    public void intercept(Invocation ai) {
        // 先对父类的属性进行一次注入
        Field[] superFields = ai.getMethod().getDeclaringClass().getSuperclass().getDeclaredFields();
        initIocServices(ai, superFields);

        // 修改了这行 获取fields
              //重点也在这 追随2.0的AOP 不限于Controller 也可以在Service层注入
        Field[] fields = ai.getMethod().getDeclaringClass().getDeclaredFields();
        initIocServices(ai, fields);//子类的属性进行注入
        ai.invoke();
    }

    private void initIocServices (Invocation ai, Field[] fields){
        for (Field field : fields) {
            Object bean = null;
            if (field.isAnnotationPresent(Inject.BY_NAME.class))
                bean = ctx.getBean(field.getName());
            else if (field.isAnnotationPresent(Inject.BY_TYPE.class))
                bean = ctx.getBeansOfType(field.getType());
            else
                continue;
            try {
                if (bean != null) {
                    field.setAccessible(true);
                    field.set(ai.getTarget(), bean);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
