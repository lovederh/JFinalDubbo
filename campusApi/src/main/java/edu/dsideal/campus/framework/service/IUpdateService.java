package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IUpdateService {
    //Page<?> show(BaseTable model, int pageNumber, int pageSize);
    //boolean save(BaseTable model);
    //boolean update(BaseTable model);
    //boolean deleteById(BaseTable model, String[] id);

    /**
     *  构造升级模块信息树
     */
    List<Record> initUpdateTree();

    /**
     *  查找升级信息列表
     */
    List<Map<String, String>> findUpdate(String modelName);

    /**
     *  系统升级按钮
     */
    String updateSelectedData(String rows);

}
