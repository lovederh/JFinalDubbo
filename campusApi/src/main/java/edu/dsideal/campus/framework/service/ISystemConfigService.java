package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.base.BaseTable;

import java.util.List;

/**
 * 2016/11/17 0017
 */
public interface ISystemConfigService {

    /**
     *  添加系统配置功能
     * @param configId 配置信息ID
     * @param webTitle 网站标签标题
     * @param mainTitle 版权所有信息
     */
     String addConfig(String configId,String webTitle , String mainTitle);

    /**
     *  保存统配置功能
     * @param configId 配置信息ID
     * @param webTitle 网站标签标题
     * @param mainTitle 版权所有信息
     */
     String saveConfig(String configId,String webTitle , String mainTitle,String isPass);

    /**
     *  查找系统配置信息
     * @return
     */
     Record findSystemConfigInfo(String domain);

    /**
     * 查询学校配置信息
     */
     List<Record> queryMembersConfigInfo();

    /**
     * 删除配置信息按钮功能
     * @param ids 删除选中行数组
     */
    String delConfig(String[] ids);

    /**
     * 通过按钮功能
     * @param ids 通过选中行数组
     */
    String passConfig(String[] ids, BaseTable model);

    /**
     * 不通过按钮功能
     * @param ids 不通过选中行数组
     */
    String notPassConfig(String[] ids);
}
