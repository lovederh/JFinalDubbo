package edu.dsideal.campus.framework.plugin;

import javax.sql.DataSource;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

public class MyMetaBuilder extends MetaBuilder {
    public String tablePrefix;
    
    public MyMetaBuilder(DataSource dataSource) {
      super(dataSource);
    }
   
    public void setTablePrefix(String tablePrefix){
      this.tablePrefix = tablePrefix;
    }
   
    protected boolean isSkipTable(String tableName) {
      return !tableName.contains(tablePrefix);
    }

}
