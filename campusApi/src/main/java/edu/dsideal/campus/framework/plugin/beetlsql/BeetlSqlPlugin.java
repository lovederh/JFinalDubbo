package edu.dsideal.campus.framework.plugin.beetlsql;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.IPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.NameConversion;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.ext.jfinal.JFinalConnectonSource;

import javax.sql.DataSource;

/**
 * Created by dsideal-lee on 2016/10/18.
 */
public class BeetlSqlPlugin implements IPlugin{
    static SQLManager sqlManager = null;
    static String nc = null;
    static String sqlRoot = null;
    static String dbStyle = null;
    static String[] ins = null;
    static JFinalConnectonSource ds = null;
    @Override
    public boolean start() {
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }

    public static void init() {
        Prop prop = PropKit.use("jdbc.properties");
        DruidPlugin dp = new DruidPlugin(prop.get("jdbcUrl"),
                prop.get("user"), prop.get("password").trim());
        dp.start();
        ds = new JFinalConnectonSource(dp.getDataSource(), (DataSource[])null);
        initProp();
        initSQLMananger();
    }

    public static void init(DataSource master, DataSource[] slaves) {
        ds = new JFinalConnectonSource(master, slaves);
        initProp();
        initSQLMananger();
    }

    private static void initProp() {
        nc = PropKit.get("sql.nc", "org.beetl.sql.core.HumpNameConversion");
        sqlRoot = PropKit.get("sql.root", "/sql");
        String interceptors = PropKit.get("sql.interceptor");
        ins = null;
        if(interceptors != null) {
            ins = interceptors.split(",");
        }

        dbStyle = PropKit.get("sql.dbStyle", "org.beetl.sql.core.db.MySqlStyle");
    }

    private static void initSQLMananger() {
        DBStyle dbStyleIns = (DBStyle)instance(dbStyle);
        ClasspathLoader sqlLoader = new ClasspathLoader(sqlRoot);
        NameConversion ncIns = (NameConversion)instance(nc);
        Interceptor[] inters = null;
        if(ins != null) {
            inters = new Interceptor[ins.length];

            for(int i = 0; i < inters.length; ++i) {
                inters[i] = (Interceptor)instance(ins[i]);
            }
        } else {
            inters = new Interceptor[0];
        }

        sqlManager = new SQLManager(dbStyleIns, sqlLoader, ds, ncIns, inters);
    }

    private static Object instance(String clsName) {
        try {
            Object c = Class.forName(clsName).newInstance();
            return c;
        } catch (Exception var3) {
            throw new RuntimeException("初始化类错误" + clsName, var3);
        }
    }

    public static SQLManager dao() {
        if(sqlManager != null) {
            return sqlManager;
        } else {
            throw new RuntimeException("未初始化，需要调用init方法");
        }
    }
}
