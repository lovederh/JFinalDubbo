package edu.dsideal.campus.framework.base;

/**
 * Created by dsideal-lee on 2016/10/28.
 */
public class JsonObj {
    String model;
    String pId;
    Boolean bool;
    boolean bbb;

    public boolean isBbb() {
        return bbb;
    }

    public void setBbb(boolean bbb) {
        this.bbb = bbb;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }

    public String getModel() {

        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
