package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface ILoginService {

    /**
     * 通过登陆人账号, 后台查询重要信息
     */
    Record findUserRecordByLoginId(String loginId);
    /**
     * 登录时的后台校验方法, 校验项如下:
     * 1.用户账号loginId是否存在;
     * 2.用户账号是否可用(isUsed='1'可用);
     * 3.密码是否正确;
     */
    LoginUserObj checkLoginUser(Map<String, String[]> paraMap);

    /**
     * 检查注册用户账号是否存在
     * @param loginId
     * @return
     */
    String checkRegisterLoginID(String loginId);

    /**
     * 检查注册email地址是否存在
     * @param EmailAdd
     * @return
     */
    String checkRegisterEmailAdd(String EmailAdd);
    /**
     * 将注册信息保存到t_sys_systemconfig表中
     */
    String saveRegisterValue(ArrayList<String> registerValue);
}
