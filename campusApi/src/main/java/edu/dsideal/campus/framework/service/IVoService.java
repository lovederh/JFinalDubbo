package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Page;
import edu.dsideal.campus.framework.base.BaseTable;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IVoService {
    Page<?> show(BaseTable model, int pageNumber, int pageSize);
    boolean save(BaseTable model);
    boolean update(BaseTable model);
    boolean deleteById(BaseTable model, String[] id);
}
