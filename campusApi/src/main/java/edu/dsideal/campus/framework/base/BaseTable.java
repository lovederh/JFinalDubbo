package edu.dsideal.campus.framework.base;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.redis.Redis;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/15.
 */
public class BaseTable extends Model<BaseTable> implements IBean{
    private String model; //模型
    private String search; //查询否
    private String nd;
    private int rows;  //条数
    private int page;  //页数
    private String sidx;
    private String sord;  //排序
    private String searchField; //查询字段
    private String searchString; //查询数据
    private String searchOper;    //查询比较类型 等于， 开始于 大于 小于。。。
    private String filters;     //过滤
    private LoginUserObj loginUserObj;

    /**
     * 数据库切换Dao, 只有类第一次加载的时候会调用一次，以后不会再调用
     * @param model 要查询的模型
     * @param packageName 模型所在的包名 例如：com.dsideal.framework.framework.model.User 其中framework为packageName
     * @return Class 反回符合数据库类型的类
     */
    @SuppressWarnings("unchecked")
    public static <T> T duang(String model, String packageName) {
        Redis cache = new Redis();
        String dialet = cache.use("dssm").get("dialet");
        // 拼接包名和类名 例如：edu.dsideal.campus.framework.model.User 其中framework为packageName
        StringBuilder className =
                new StringBuilder().append(packageName).append(".model.")
                        .append(model);
        Class<?> clazz = null;
        try {
            switch (dialet) {
                case "mssql": //Sqlserver
                    className.append("Ms");
                    break;
                case "oracle": //oracle
                    className.append("Ora");
                    break;
                default:
                    break;
            }
            clazz = Class.forName(className.toString());
            return (T) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            //默认如果没有配置相关的类，采用默认父类
            try {
                clazz = Class.forName(new StringBuilder(packageName)
                        .append(".model.").append(model).toString());
                return (T) clazz.newInstance();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }
        }   catch (InstantiationException e1) {
            e1.printStackTrace();
        }   catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }
        return null;
    }
    public static BaseTable createInstance(Map<String, String[]> paraMap, String modelPackage){
        String dtoName = paraMap.get("dtoName")[0];
        //得到modelName并将首字母转为大写t_sys_tables 得到Tables
        String modelName = CommonTools.toUpperCaseFirstChar(dtoName.split("_")[2]);
        //反射生成model对象
        BaseTable model = duang(modelName, modelPackage);
        //循环将参数赋给model
        for(Map.Entry<String, String[]> para : paraMap.entrySet()){
            //如果有pId,并且为空，则设置默认值 为0
            if("pId".equals(para.getKey()) && CommonTools.isEmpty(para.getValue()[0])){
                model.set("pId", "1");
                continue;
            }
            model.set(para.getKey(), para.getValue()[0]);
        }
        return model;
    }
    public static BaseTable createInstance(Controller ctrl, String modelPackage){
        String dtoName = ctrl.getPara("dtoName");
        if(CommonTools.isEmpty(dtoName)){
            dtoName = ctrl.getAttr("dtoName");
        }
        //得到modelName并将首字母转为大写t_sys_tables 得到Tables
        String modelName = CommonTools.toUpperCaseFirstChar(dtoName.split("_")[2]);
        //反射生成model对象
        BaseTable model = duang(modelName, modelPackage);
        //设置model的实体表名
        model.set("dtoName", dtoName);
        model.set("domain", ctrl.getSessionAttr("domain"));
        //循环将参数赋给model
        Map<String, String[]> paraMap = ctrl.getParaMap();
        for(Map.Entry<String, String[]> para : paraMap.entrySet()){
            //如果有pId,并且为空，则设置默认值 为0
            if("pId".equals(para.getKey()) && CommonTools.isEmpty(para.getValue()[0])){
                model.set("pId", "1");
                continue;
            }
            model.set(para.getKey(), para.getValue()[0]);
        }
        //将权限信息加入到model中
        return model;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getNd() {
        return nd;
    }

    public void setNd(String nd) {
        this.nd = nd;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public LoginUserObj getLoginUserObj() {
        return loginUserObj;
    }

    public void setLoginUserObj(LoginUserObj loginUserObj) {
        this.loginUserObj = loginUserObj;
    }
}
