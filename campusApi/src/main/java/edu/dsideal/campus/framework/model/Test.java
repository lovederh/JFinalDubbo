package edu.dsideal.campus.framework.model;

import edu.dsideal.campus.framework.base.BaseTable;
import java.io.Serializable;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public class Test extends BaseTable implements Serializable {

	public static final String MODEL_NAME = "t_sys_test";

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setTestName(java.lang.String testName) {
		set("testName", testName);
	}

	public java.lang.String getTestName() {
		return get("testName");
	}

	public void setTestAge(java.lang.String testAge) {
		set("testAge", testAge);
	}

	public java.lang.String getTestAge() {
		return get("testAge");
	}

	public void setTestOrg(java.lang.String testOrg) {
		set("testOrg", testOrg);
	}

	public java.lang.String getTestOrg() {
		return get("testOrg");
	}

	public void setDomain(java.lang.String domain) {
		set("domain", domain);
	}

	public java.lang.String getDomain() {
		return get("domain");
	}

	public void setLongtext(java.lang.String longtext) {
		set("longtext", longtext);
	}

	public java.lang.String getLongtext() {
		return get("longtext");
	}

}
