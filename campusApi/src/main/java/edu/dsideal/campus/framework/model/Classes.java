package edu.dsideal.campus.framework.model;

import edu.dsideal.campus.framework.base.BaseTable;
import java.io.Serializable;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public class Classes extends BaseTable implements Serializable {

	public static final String MODEL_NAME = "t_sys_classes";

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setClassesName(java.lang.String classesName) {
		set("classesName", classesName);
	}

	public java.lang.String getClassesName() {
		return get("classesName");
	}

	public void setCodeId(java.lang.String codeId) {
		set("codeId", codeId);
	}

	public java.lang.String getCodeId() {
		return get("codeId");
	}

	public void setLevel(java.lang.String level) {
		set("level", level);
	}

	public java.lang.String getLevel() {
		return get("level");
	}

	public void setIsLeaf(java.lang.String isLeaf) {
		set("isLeaf", isLeaf);
	}

	public java.lang.String getIsLeaf() {
		return get("isLeaf");
	}

	public void setPId(java.lang.String pId) {
		set("pId", pId);
	}

	public java.lang.String getPId() {
		return get("pId");
	}

	public void setExpanded(java.lang.String expanded) {
		set("expanded", expanded);
	}

	public java.lang.String getExpanded() {
		return get("expanded");
	}

	public void setDelFlag(java.lang.String delFlag) {
		set("delFlag", delFlag);
	}

	public java.lang.String getDelFlag() {
		return get("delFlag");
	}

	public void setDomain(java.lang.String domain) {
		set("domain", domain);
	}

	public java.lang.String getDomain() {
		return get("domain");
	}

	public void setPcodeId(java.lang.String pcodeId) {
		set("pcodeId", pcodeId);
	}

	public java.lang.String getPcodeId() {
		return get("pcodeId");
	}

}
