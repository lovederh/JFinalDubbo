package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IUserService {
    /**
     * 个人资料保存
     * @return
     */
    String saveMyInfo(Map<String, String> paramMap);

    Record findUserNameByLoginId(String loginId);

}
