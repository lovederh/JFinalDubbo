package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.base.BaseTable;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IDoService {

    /**
     * 基础数据模型分页查询
     * @param model
     * @param pageNumber
     * @param pageSize
     * @param where
     * @param order
     * @return
     */
    Page<Record> show(BaseTable model, int pageNumber, int pageSize, String where, String order);

    /**
     * 基础数据模型保存方法
     * @param model
     * @return
     * @throws Exception
     */
    boolean save(BaseTable model) throws Exception;

    /**
     * 基础数据模型更新方法
     * @param model
     * @return
     * @throws Exception
     */
    boolean update(BaseTable model) throws Exception;

    /**
     * 基础数据模型删除方法
     * @param model
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteById(BaseTable model, String[] id) throws Exception;

    /**
     * 重构模型
     * @param model
     * @return
     * @throws Exception
     */
    boolean rebuild(BaseTable model) throws Exception;
}
