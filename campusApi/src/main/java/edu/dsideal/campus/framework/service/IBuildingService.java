package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 2017/2/14 0020
 */
public interface IBuildingService {

    /**
     *  构造楼宇树结构
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> initBuildingTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  显示树结构节点的房间信息
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Map<String, Object> queryBuildingPager(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 编辑/删除分组节点
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String buildingTreeNote(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

}
