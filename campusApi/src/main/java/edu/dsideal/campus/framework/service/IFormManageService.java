package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * Created by Lu on 2017/3/2.
 */
public interface IFormManageService {
    /**
     * 自定义表单管理列表数据查询
     *
     * @return
     */
    public Page<Record> queryForms(int pageNumber, int pageSize, String title);

    /**
     * 查询自定义表单 - 编辑定制表单显示
     */
    public void queryFormForEdit();

    /**
     * 保存自定义表单
     */
    public void saveForm();

    /**
     * 查询自定义表单 - 用户录入显示表单
     */
    public void queryFormForInput();

    /**
     * 查询自定义表单 - 用户录入后的表单页面及数据
     */
    public void queryInputForm();
}
