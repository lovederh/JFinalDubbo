package edu.dsideal.campus.framework.dao.impl;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * Created by dsideal-lee on 2016/11/9.
 */
public class BaseGrid {
    public void buildColNames(StringBuilder strBuilder, List<Record> recordList) {
        strBuilder.append("\"colNames\":[");
        for(Record r : recordList){
            strBuilder.append("\"").append(r.getStr("showName")).append("\",");
        }
        strBuilder.deleteCharAt(strBuilder.length()-1);
        strBuilder.append("]");
    }

    /**
     * 列模型
     * @param strBuilder
     * @param recordList [{id:1,showName:人员信息，colName: personInfo, width: 100}]
     */
    public void buildColModel(StringBuilder strBuilder, List<Record> recordList, String keyCol) {
        strBuilder.append("\"colModel\":[");
        for(Record r : recordList){
            strBuilder.append("{\"name\":\"").append(r.getStr("colName")).append("\",");
            strBuilder.append("\"index\":\"").append(r.getStr("colName")).append("\",");
            strBuilder.append("\"width\":\"").append(r.getStr("width")).append("\",");
            strBuilder.append("\"align\":\"").append(r.getStr("align")).append("\",");
            strBuilder.append("\"hidden\":").append("true".equals(r.getStr("hidden"))?"false":"true");
            if(r.getStr("colName").equals(keyCol)){
                strBuilder.append(",\"key\":").append("true");
            }
            strBuilder.append("},");
        }
        strBuilder.deleteCharAt(strBuilder.length()-1);
        strBuilder.append("]");
    }
}
