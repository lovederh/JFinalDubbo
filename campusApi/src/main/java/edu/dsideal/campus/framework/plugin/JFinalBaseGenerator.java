package edu.dsideal.campus.framework.plugin;

import javax.sql.DataSource;

import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import edu.dsideal.campus.framework.plugin.generator.Generator;

/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class JFinalBaseGenerator {

	public static DataSource getDataSource() {
		PropKit.use("jdbc.properties");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"),
                PropKit.get("driver"), PropKit.getInt("maxPoolSize"),
                PropKit.getInt("minPoolSize"), PropKit.getInt("initialPoolSize"),
                PropKit.getInt("maxIdleTime"), PropKit.getInt("acquireIncrement"));
		c3p0Plugin.start();
		return c3p0Plugin.getDataSource();
	}

	public static void generate(String packageName, String tablePrefix) {
		// framework.base framework.model 所使用的包名
		String baseModelPackageName = packageName+".model";
		// framework.base framework.model 文件保存路径
//		String baseModelOutputDir = PathKit.getWebRootPath() + "/../"+packageName+"/com/dsideal/"+packageName+"/framework.model/framework.base";
		String baseModelOutputDir = PathKit.getWebRootPath() + "/../campusApi/src/main/java/"+packageName.replace("." , "/")+ "/model";
		
		// framework.model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = packageName+".model";
		// framework.model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir;
		// 创建生成器
		Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir, "mysql");
		// 添加不需要生成的表名
		gernerator.addExcludedTable("blog");
		//添加需要生成的表前缀
		gernerator.setTablePrefix(tablePrefix);
		//添加包名
		gernerator.setPackageName(packageName);
		// 设置是否在 Model 中生成 framework.dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		gernerator.setRemovedTableNamePrefixes(tablePrefix);
		// 生成
		gernerator.generate();
	}
}




