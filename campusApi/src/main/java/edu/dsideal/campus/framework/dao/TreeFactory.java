package edu.dsideal.campus.framework.dao;

import edu.dsideal.campus.framework.dao.impl.JqGridGrid;
import edu.dsideal.campus.framework.dao.impl.JqGridGrid;

/**
 * Created by dsideal-lee on 2016/11/8.
 */
public class TreeFactory {

    public static ITreeUtil getTreeService(String condition) {
        ITreeUtil treeService;
        switch (condition) {
            case "jqgrid":
                treeService =  new JqGridGrid();
            break;
            case "ztree":
                treeService = new ZTreeUtil();
                break;
            default:
                treeService = new JqGridGrid();
                break;
        }
        return treeService;
    }
}
