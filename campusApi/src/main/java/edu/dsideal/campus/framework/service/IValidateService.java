package edu.dsideal.campus.framework.service;

import edu.dsideal.campus.framework.base.BaseTable;

/**
 * Created by dsideal-lee on 2016/10/24.
 */
public interface IValidateService {
    boolean checkUnique(BaseTable model);
}
