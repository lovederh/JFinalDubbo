package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 2016/12/6 0006
 */
public interface IDeptService {
    /**
     *  构造组织机构树
     */
//    List<Record> initDeptTree(BaseTable model);

    /**
     * 显示选择节点机构的信息
     * @param loginUserObj
     * @param orgId
     * @return
     */
    List<Record> queryDeptInfo(LoginUserObj loginUserObj, String orgId);

    /**
     * 增加选择节点子节点机构的信息
     * @param keyId
     * @return
     */
    String addDept(LoginUserObj loginUserObj,String keyId, Map<String, String[]> paraMap);

    /**
     * 删除所选择的节点
     * @param id
     * @param loginUserObj
     * @return
     */
    String delDept(LoginUserObj loginUserObj, String id);

    /**
     * 部门调整方法
     * @param loginUserObj
     * @param paraMap
     */
    String deptAdjustSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);


    /**
     * 查询所有组织机构集合
     */
    List<Record> findAllDeptDefine(LoginUserObj loginUserObj,Map<String, String[]> paraMap);

    /**
     * 按表插入保存(支持批量)
     */
    List<Record> findDeptChildren(LoginUserObj loginUserObj, Map<String, String[]> paraMap,String nowNodeId, String nowLevel);

}
