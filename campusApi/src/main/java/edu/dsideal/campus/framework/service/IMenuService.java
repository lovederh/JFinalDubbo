package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;
import java.util.Map;

/**
 * 基础共通处理类(基于JFinal框架的封装)
 * zhaoyou 使用
 * 2016/10/31 0031
 */
public interface IMenuService {
    /**
     * 根据主键ID, 按表查询一条记录
     */
    Record findMenuDefineById(String id);

    /**
     * 根据菜单编码, 删除当前菜单及其子菜单
     */
    void deleteMenuDefine(String delMenuId);

    /**
     * 保存菜单定义方法
     */
    String saveMenuDefine(Map<String, String[]> paraMap);

    /**
     * 查询所有菜单定义集合
     */
    List<Record> findAllMenuDefine();

    /**
     * 按表插入保存(支持批量)
     */
    List<Record> findMenuDefineChildren(String nowNodeId, String nowLevel);

    /**
     * 根据用户角色查询菜单
     */
    List<Map<String, Object>> findRoleMenuList(String loginRoles);

    /**
     *  关联业务模型
     * @return
     */
    List<Record> getRelatedBoData();
}
