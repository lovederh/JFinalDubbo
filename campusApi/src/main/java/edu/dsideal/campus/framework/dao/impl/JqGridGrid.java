package edu.dsideal.campus.framework.dao.impl;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.ITreeUtil;

/**
 * Created by dsideal-lee on 2016/11/8.
 */
public class JqGridGrid extends BaseGrid implements ITreeUtil {
    private String key;
    private String codeId;
    private String treeName;
    private String pId;
    private String level;
    private String isLeaf;
    private String expanded;

    @Override
    public void buildTreeReader(StringBuilder strBuilder, Record record) {
        strBuilder.append("\"treeReader\":").append("{\"level_field\":\"").append(record.getStr("levelCol"))
                .append("\",\"parent_id_field\":\"").append(record.getStr("pidCol"))
                .append("\",\"leaf_field\":\"").append(record.getStr("leafCol"))
                .append("\",\"expanded_field\":\"expanded\"}");
    }

    @Override
    public void buildTreeName(StringBuilder strBuilder, String treeName) {
        //放入哪个是树的名称列
        strBuilder.append("\"ExpandColumn\":\"").append(treeName).append("\"");
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTreeName() {
        return treeName;
    }

    public void setTreeName(String treeName) {
        this.treeName = treeName;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(String isLeaf) {
        this.isLeaf = isLeaf;
    }

    public String getExpanded() {
        return expanded;
    }

    public void setExpanded(String expanded) {
        this.expanded = expanded;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }
}
