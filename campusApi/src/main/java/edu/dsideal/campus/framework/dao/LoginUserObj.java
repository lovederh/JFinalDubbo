package edu.dsideal.campus.framework.dao;

import com.jfinal.plugin.activerecord.Record;

import java.io.Serializable;

/**
 * session域中的登陆用户对象
 * 以loginUserObj作为session中的key值
 */
public class LoginUserObj implements Serializable{
	// 登陆人账号
	private String loginId;
	// 登陆人用户名
	private String userName;
    //当前人员所处企业域
    private String domain;
    // 人员对应集团机构ID
    private String deptId;
    // 人员对应集团机构名称
    private String deptName;

    //用户状态标识(true: 可登录使用; false: 不可以登陆使用)
    private boolean userStatus = true;
    //用户状态备注(userStatus为false时, 该值会存在, 用于不能登陆提示)
    private String userStatusMsg;

    //登陆人头像对应存放路径
    private String thumbnailPath;

    // 动态角色ID字符串(sql使用)
	private String roles = null;

	public LoginUserObj() {
		super();
	}
	
	public LoginUserObj(Record sysUser) {
		this.loginId = sysUser.get("loginId");
		this.userName = sysUser.get("userName");
		this.domain = sysUser.get("domain");
		this.deptId = sysUser.get("orgId");
		this.deptName = sysUser.get("orgName");
		this.thumbnailPath = sysUser.get("thumbnailPath");
	}

	public String getLoginId() {
		return loginId;
	}
	
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}

    public String getDomain() {
        return domain;
    }

	public void setDomain(String domain) {
        this.domain = domain;
    }


    public String getDeptName() {
        return deptName;
    }
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public boolean isUserStatus() {
        return userStatus;
    }
    public void setUserStatus(boolean userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserStatusMsg() {
        return userStatusMsg;
    }
    public void setUserStatusMsg(String userStatusMsg) {
        this.userStatusMsg = userStatusMsg;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }
    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }
}
