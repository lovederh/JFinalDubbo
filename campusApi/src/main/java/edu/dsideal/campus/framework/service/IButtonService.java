package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/21.
 */
public interface IButtonService {
    List<Record> getBtns(Map<String, String[]> paraMap, LoginUserObj loginUserObj);
}
