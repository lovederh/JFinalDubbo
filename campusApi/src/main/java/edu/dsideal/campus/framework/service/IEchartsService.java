package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;
import java.util.Map;

/**
 * 2016/11/23 0023
 */
public interface IEchartsService {

    /**
     *  构造图表信息树
     */
    List<Record> initEchartsTree();

    /**
     * 查询选中一条的图表信息
     * @param echartId
     * @return
     */
    Record findOne(String echartId);


    /**
     *  图表类型列表
     * @return
     */
    List<Record> findEchartsTypeList();

    /**
     * 图表项维护信息的保存方法
     */
    Record saveEchartInfo(Map<String, String[]> echartMap);

    /**
     * 该类中对外提供的一个方法
     * 功能: 返回最终的可供前端页面使用的option属性字符串
     * 外部类直接调用即可(先利用图表项配置Record实例化出来)完成该功能, 降低耦合性
     * @return
     */
    String createOptionData(Record echartRecord);

    /**
     *  删除选中图表
     */
    void deleteEcharts(String echartId);
}
