package edu.dsideal.campus.framework.model;


import edu.dsideal.campus.framework.plugin.JFinalBaseGenerator;
import edu.dsideal.campus.framework.plugin.JFinalBaseGenerator;

/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _JFinalCoreGenerator extends JFinalBaseGenerator {
	
	public static void main(String[] args) {
	    JFinalBaseGenerator.generate("edu.dsideal.campus.framework", "t_sys_");
		JFinalBaseGenerator.generate("edu.dsideal.campus.framework", "t_dept_");
	}
}




