package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 2016/12/20 0020
 */
public interface IEmpService {

    /**
     *  构造组织机构树
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> initDeptTree(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  显示选择节点机构的人员信息
     *  查询机构树列表右侧显示相对应的的人员信息列表
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Map<String, Object> queryEmpPager(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  人员调动保存方法
     * @param loginUserObj
     * @param paraMap
     */
    String empMoveSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  获取人员状态变更字典信息
     */
    List<Record> findEmpStatusDict();

    /**
     *  获取选中人员信息
     * @param loginUserObj
     * @param empIds
     * @return
     */
    List<Record> findEmpInfoList(LoginUserObj loginUserObj,String empIds);

    /**
     *  人员状态变更保存
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String empStatusSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  添加家庭成员保存方法
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String familyMemberSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);


    /**
     *  家庭成员信息列表
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> familyMemberList(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  加载家庭成员信息
     * @param loginUserObj
     * @param id
     * @return
     */
    Record loadInfoFamilyMember(LoginUserObj loginUserObj, String id);

    /**
     *  删除家庭成员信息
     * @param loginUserObj
     * @param ids
     * @return
     */
    String delFamilyMember(LoginUserObj loginUserObj, String ids);

    /**
     *  添加/编辑学历信息保持方法
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String educationSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);



    /**
     *  学历信息列表
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> educationInfoList(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  加载学历信息
     * @param loginUserObj
     * @param id
     * @return
     */
    Record loadInfoEducation(LoginUserObj loginUserObj, String id);

    /**
     *  删除学历信息
     * @param loginUserObj
     * @param ids
     * @return
     */
    String delEducation(LoginUserObj loginUserObj, String ids);

    /**
     *  添加/编辑工作简历保存方法
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String resumeSave(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  工作简历信息列表
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> resumeInfoList(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     *  加载工作简历信息
     * @param loginUserObj
     * @param id
     * @return
     */
    Record loadInfoResume(LoginUserObj loginUserObj, String id);

    /**
     *  删除工作简历信息按钮
     * @param loginUserObj
     * @param ids
     * @return
     */
    String delResume(LoginUserObj loginUserObj, String ids);

}
