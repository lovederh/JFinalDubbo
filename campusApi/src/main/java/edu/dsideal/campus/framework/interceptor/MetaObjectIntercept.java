/**
 * Copyright (c) 2013-2015, Jieven. All rights reserved.
 * <p>
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package edu.dsideal.campus.framework.interceptor;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.base.BaseTable;

import java.sql.SQLException;
import java.util.List;

/**
 * 元对象业务拦截器<br>
 * 前置任务和后置任务(事务未提交)<br>
 * 成功之后(事务已提交)<br>
 */
public class MetaObjectIntercept {

    /**
     * 查询后置任务
     *
     * @param page 查询的分页数据集
     */
    public void queryAfter(Page<BaseTable> page) {
    }

    /**
     * 新增前置任务(事务内)
     *
     * @param record 当前操作数据
     * @throws SQLException
     */
    public void addBefore(BaseTable record) throws SQLException {
    }

    /**
     * 新增后置任务(事务内)
     * @param record 当前操作数据
     * @throws SQLException
     */
    public void addAfter(BaseTable record) throws SQLException {
    }

    /**
     * 新增成功之后(事务外)
     *
     * @param records 当前操作数据集
     * @throws SQLException
     */
    public void addSucceed(List<BaseTable> records) throws SQLException {
    }

    /**
     * 删除前置任务(事务内)
     *
     * @param record 当前操作数据(Form模式只有主键值)
     * @throws SQLException
     */
    public void deleteBefore(BaseTable record) throws SQLException {
    }

    /**
     * 删除后置任务(事务内)
     *
     * @param record 当前操作数据(Form模式只有主键值)
     * @throws SQLException
     */
    public void deleteAfter(BaseTable record) throws SQLException {
    }

    /**
     * 删除树节点前
     * @param record
     * @throws SQLException
     */
    public void delTreeNodeBefore(Record record) throws SQLException{
    }

    /**
     * 删除树结点后
     * @param record
     * @throws SQLException
     */
    public void delTreeNodeAfter(Record record) throws SQLException{
    }
    /**
     * 删除成功之后(事务外)
     *
     * @param records 当前操作数据集
     * @throws SQLException
     */
    public void deleteSucceed(List<BaseTable> records) throws SQLException {
    }

    /**
     * 更新前置任务(事务内)
     *
     * @param record 当前操作数据
     * @throws SQLException
     */
    public void updateBefore(BaseTable record) throws SQLException {
    }

    /**
     * 更新后置任务(事务内)
     *
     * @param record 当前操作数据
     * @throws SQLException
     */
    public void updateAfter(BaseTable record) throws SQLException {
    }

    /**
     * 更新成功之后(事务外)
     *
     * @param records 当前操作数据集
     * @throws SQLException
     */
    public void updateSucceed(List<BaseTable> records) throws SQLException {
    }

}