package edu.dsideal.campus.framework.service;

import edu.dsideal.campus.framework.base.BaseTable;

import java.util.Map;

/**
 * Created by dsideal-lee on 2016/10/20.
 */
public interface IKeyService {
    /**
     * 生成主键列或编码列的编码
     * @param paraMap
     * @return [主键列名，主键值]
     */
    String[] getKey(Map<String, String[]> paraMap);
}
