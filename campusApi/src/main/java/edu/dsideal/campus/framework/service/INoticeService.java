package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

public interface INoticeService {

    /**
     * 显示通知公告列表分页
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Page<Record> queryNoticePager(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 删除通知公告按钮
     *
     * @param loginUserObj
     * @param ids
     * @return
     */
    String delNotice(LoginUserObj loginUserObj, String ids);

    /**
     * 保存通告
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String saveNotice(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 回显已选择人员的名称
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    //String querySelectedPerson(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 查询分组下的选择人员生成树结构
     *
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> groupTree(String tableName, Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 查询行政部门下的教师生成树结构
     *
     * @param tableName
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> teacherTree(String tableName, Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 显示选择节点机构的人员信息
     * 查询机构树列表右侧显示相对应的的人员信息列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> queryTeacherData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 查询已选择的人员列表
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Page<Record> querySelectedStaff(LoginUserObj loginUserObj, Map<String, String[]> paraMap);


    /**
     * 查询专业班级下的学生生成树结构
     *
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> studentTree(Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 添加分组
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Object addGroup(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 编辑/删除分组节点
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String editGroupNote(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 添加分组人员
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String addGroupUser(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 查询分组人员信息
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> queryGroupData(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 查看通知公告信息
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    Map<String, Object> viewNotice(LoginUserObj loginUserObj, Map<String, String[]> paraMap);


    /**
     * 保存分组成员
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    List<Record> saveGroupMember(LoginUserObj loginUserObj, Map<String, String[]> paraMap);


    /**
     * 接收通知公告人员是否人数处理
     *
     * @param loginUserObj
     * @param paraMap
     * @return
     */
    String receiveNoticeUserNum(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    //*************************************************************************************************************************************


    ///**
    // * 显示所要选择人员列表
    // *
    // * @param loginUserObj
    // * @param paraMap
    // * @return
    // */
    //Page<Record> queryStaffList(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    //
    //
    //
    //
    //
    //
    //
    //
    ///**
    // * 机构人员树模糊查询
    // *
    // * @param loginUserObj
    // * @param paraMap
    // * @return
    // */
    //String searchDeptToTeacher(LoginUserObj loginUserObj, Map<String, String[]> paraMap);
    //
    //


}
