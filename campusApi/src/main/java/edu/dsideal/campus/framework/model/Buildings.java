package edu.dsideal.campus.framework.model;

import edu.dsideal.campus.framework.base.BaseTable;
import java.io.Serializable;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public class Buildings extends BaseTable implements Serializable {

	public static final String MODEL_NAME = "t_sys_buildings";

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setBuildName(java.lang.String buildName) {
		set("buildName", buildName);
	}

	public java.lang.String getBuildName() {
		return get("buildName");
	}

	public void setCodeId(java.lang.String codeId) {
		set("codeId", codeId);
	}

	public java.lang.String getCodeId() {
		return get("codeId");
	}

	public void setPId(java.lang.String pId) {
		set("pId", pId);
	}

	public java.lang.String getPId() {
		return get("pId");
	}

	public void setLevel(java.lang.String level) {
		set("level", level);
	}

	public java.lang.String getLevel() {
		return get("level");
	}

	public void setIsLeaf(java.lang.String isLeaf) {
		set("isLeaf", isLeaf);
	}

	public java.lang.String getIsLeaf() {
		return get("isLeaf");
	}

	public void setOrd(java.lang.String ord) {
		set("ord", ord);
	}

	public java.lang.String getOrd() {
		return get("ord");
	}

	public void setDomain(java.lang.String domain) {
		set("domain", domain);
	}

	public java.lang.String getDomain() {
		return get("domain");
	}

	public void setBuildType(java.lang.String buildType) {
		set("buildType", buildType);
	}

	public java.lang.String getBuildType() {
		return get("buildType");
	}

	public void setCampus(java.lang.String campus) {
		set("campus", campus);
	}

	public java.lang.String getCampus() {
		return get("campus");
	}

	public void setFloor(java.lang.String floor) {
		set("floor", floor);
	}

	public java.lang.String getFloor() {
		return get("floor");
	}

	public void setRoomNumber(java.lang.String roomNumber) {
		set("roomNumber", roomNumber);
	}

	public java.lang.String getRoomNumber() {
		return get("roomNumber");
	}

	public void setDelFlag(java.lang.String delFlag) {
		set("delFlag", delFlag);
	}

	public java.lang.String getDelFlag() {
		return get("delFlag");
	}

}
