package edu.dsideal.campus.framework.service;

/**
 * Created by dsideal-lee on 2016/10/10.
 */
public interface IRoleService {
    String getRolesByLoginId(String loginId, String domain);
}
