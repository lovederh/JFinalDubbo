package edu.dsideal.campus.framework.plugin.beetl;

import com.jfinal.render.Render;
import org.beetl.ext.jfinal3.JFinal3BeetlRender;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

public class MyBeetlRenderFactory extends JFinal3BeetlRenderFactory{
    @Override  
    public Render getRender(String view) {  
        JFinal3BeetlRender render=new JFinal3BeetlRender(groupTemplate, view);
        return render;  
    }  
}
