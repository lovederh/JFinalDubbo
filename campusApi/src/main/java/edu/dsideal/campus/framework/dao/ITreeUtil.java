package edu.dsideal.campus.framework.dao;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;

/**
 * Created by dsideal-lee on 2016/11/8.
 * treeGridJson :
 * {
 * colNames: [],
 * colModel: [{},{}],
 * ExpandColumn: "",
 * treeReader: {level_field: "menuLevel",
 * parent_id_field: "pmenuId",
 * leaf_field: "isLeaf",
 * expanded_field: "expanded"
 * }
 * }
 */
public interface ITreeUtil {
    /**
     * 构造树的配置
     *
     * @param strBuilder
     * @param record     //树配置信息t_sys_keyrule
     */
    void buildTreeReader(StringBuilder strBuilder, Record record);

    /**
     * 构造树的名称列ExpandColumn
     *
     * @param strBuilder
     * @param treeName   树名称列
     */
    void buildTreeName(StringBuilder strBuilder, String treeName);

    /**
     * 构造jqgrid的显示字段
     *
     * @param strBuilder
     * @param recordList 列配置表t_sys_boconfig
     */
    void buildColNames(StringBuilder strBuilder, List<Record> recordList);

    /**
     * 构造tree的列模型
     *
     * @param strBuilder
     * @param recordList 列配置表t_sys_boconfig
     */
    void buildColModel(StringBuilder strBuilder, List<Record> recordList, String keyCol);
}
