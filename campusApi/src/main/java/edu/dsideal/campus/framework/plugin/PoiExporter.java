package edu.dsideal.campus.framework.plugin;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import edu.dsideal.campus.common.CommonTools;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddressList;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

@SuppressWarnings("deprecation")
public class PoiExporter {

    public static final String VERSION_2003 = "2003";
    private static final int HEADER_ROW = 1;
    private static final int MAX_ROWS = 65535;
    private String version;
    private String showHeaders;
    private String[] sheetNames = new String[]{"sheet"};
    private int cellWidth = 8000;
    private int headerRow;
    private String[][] headers;
    private String[][] columns;
    private String[][] comments;
    private List<List<String>> selectList;
    private int listSize;
    private List<?>[] data;

    private void setTableHead(Workbook wb, Sheet sheet){        
    	//表头没有字段，返回
    	if (headers[0].length <= 0) {
    		return;
    	}
    	
    	Row row = sheet.createRow(0);
        Cell cell;
        if (headerRow <= 0) {
            headerRow = HEADER_ROW;
        }
        headerRow = Math.min(headerRow, MAX_ROWS);
        for (int h = 0, lenH = headers[0].length; h < lenH; h++) {
            if (cellWidth > 0) {
                sheet.setColumnWidth(h, cellWidth);
            }
            cell = row.createCell(h);
            cell.setCellValue(headers[0][h]);
            
            //设置注释
            setComments(sheet, cell, comments[0][h]);
            //设置表头格式
            CellStyle css0 = wb.createCellStyle();
            Font font = wb.createFont(); 
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);//粗体显示
			css0.setFont(font);//选择需要用到的字体格式
			css0.setFillForegroundColor(HSSFColor.YELLOW.index);
			css0.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cell.setCellStyle(css0);
            //设置单元格格式
            CellStyle css1 = wb.createCellStyle();
            DataFormat format = wb.createDataFormat();
            css1.setDataFormat(format.getFormat("@")); //格式为文本
            sheet.setDefaultColumnStyle(h,css1);
            //冻结第一行表头
            sheet.createFreezePane(0, 1, 0, 1);
            //隐藏非修改列，如主键id
            if(showHeaders.indexOf(","+headers[0][h]+",") == -1){
            	sheet.setColumnHidden(h, true);
            }
        }
    }
    
    private void setComments(Sheet sheet, Cell cell, String commentStr){
	    //设置注释
	    if(!CommonTools.isEmpty(commentStr)){
	    	Drawing draw = sheet.createDrawingPatriarch();
	    	Comment comment;
	    	if(VERSION_2003.equals(version)){
	    		comment = draw.createCellComment(new HSSFClientAnchor(0,0,0,0, (short)4, 2 ,(short) 8, 5));
	    		comment.setString(new HSSFRichTextString(commentStr));
	    	}else{
	    		comment = draw.createCellComment(new XSSFClientAnchor(0,0,0,0, (short)4, 2 ,(short) 8, 5));
	    		comment.setString(new XSSFRichTextString(commentStr));
	    	}
	    	cell.setCellComment(comment);
	    }
	}
    
    private void setSelectListSheet(Workbook wb, Sheet sheet, int i){
    	Sheet sheet2 = wb.createSheet(sheetNames[i] + "2");
        Row row2;
        Cell cell2;
        for (int j = 0; j < listSize; j++) {
        	row2 = sheet2.createRow(j);
        	for(int h = 0, lenH = headers[i].length; h < lenH; h++){
        		List<String> eachList = selectList.get(h);
        		if(eachList != null && j < eachList.size()){
            		cell2 = row2.createCell(h);
                    cell2.setCellValue(eachList.get(j) == null ? "" : eachList.get(j) + "");
            	}
            }
        }
    }
    
    // 根据当前列确定数据有效序列的引用位置
    private String creatExcelNameList(int col){
    	char column1 = 'A';
    	String column = "A";
    	if(col < 26){
    		column = (char)(column1+col) + "";
    	}else if(col/26 <= 26){
    		column = (char)(column1+col/26-1) + "" + (char)(column1+col%26);
    	}
    	return "$"+column+"$"+1+":$"+column+"$"+selectList.get(col).size();
    }
    
    private void creatExcelRefrence(Workbook wk, Sheet sheet, int i){
    	Name name;     
        name = wk.createName();     
        name.setNameName(sheetNames[i]);
        
    	for(int h = 0, lenH = headers[i].length; h < lenH; h++){
    		List<String> eachList = selectList.get(h);
    		if(eachList != null){
				CellRangeAddressList regions = new CellRangeAddressList(1, 65535,h, h);
    	        DVConstraint constraint = DVConstraint.createFormulaListConstraint(sheetNames[i]+"2!"+creatExcelNameList(h));
    	        HSSFDataValidation dataValidate = new HSSFDataValidation(regions, constraint);
    	        dataValidate.setSuppressDropDownArrow(false);
    			dataValidate.setErrorStyle(HSSFDataValidation.ErrorStyle.STOP);
    			dataValidate.createErrorBox("错误", "不允许手工录入，请在下拉框中选择！");
    			sheet.addValidationData(dataValidate);
        	}
        }
    }
    
    public PoiExporter(List<?>... data) {
        this.data = data;
    }

    public static PoiExporter data(List<?>... data) {
        return new PoiExporter(data);
    }

    public static List<List<?>> dice(List<?> num, int chunkSize) {
        int size = num.size();
        int chunk_num = size / chunkSize + (size % chunkSize == 0 ? 0 : 1);
        List<List<?>> result = Lists.newArrayList();
        for (int i = 0; i < chunk_num; i++) {
            result.add(Lists.newArrayList(num.subList(i * chunkSize, i == chunk_num - 1 ? size : (i + 1) * chunkSize)));
        }
        return result;
    }

    public Workbook export() {
        //Preconditions.checkNotNull(data, "data can not be null");
        Preconditions.checkNotNull(headers, "headers can not be null");
        Preconditions.checkNotNull(columns, "columns can not be null");
        Preconditions.checkArgument(data.length == sheetNames.length && sheetNames.length == headers.length
                && headers.length == columns.length, "data,sheetNames,headers and columns'length should be the same." +
                "(data:" + data.length + ",sheetNames:" + sheetNames.length + ",headers:" + headers.length + ",columns:" + columns.length + ")");
        Preconditions.checkArgument(cellWidth >= 0, "cellWidth can not be less than 0");
        Workbook wb;
        if (VERSION_2003.equals(version)) {
            wb = new HSSFWorkbook();
            if (data.length > 1) {
                for (int i = 0; i < data.length; i++) {
                    List<?> item = data[i];
                    Preconditions.checkArgument(item.size() < MAX_ROWS, "Data [" + i + "] is invalid:invalid data size (" + item.size() + ") outside allowable range (0..65535)");
                }
            } else if (data.length == 1 && data[0].size() > MAX_ROWS) {
                data = dice(data[0], MAX_ROWS).toArray(new List<?>[]{});
                String sheetName = sheetNames[0];
                sheetNames = new String[data.length];
                for (int i = 0; i < data.length; i++) {
                    sheetNames[i] = sheetName + (i == 0 ? "" : (i + 1));
                }
                String[] header = headers[0];
                headers = new String[data.length][];
                for (int i = 0; i < data.length; i++) {
                    headers[i] = header;
                }
                String[] column = columns[0];
                columns = new String[data.length][];
                for (int i = 0; i < data.length; i++) {
                    columns[i] = column;
                }
            }
        } else {
            wb = new XSSFWorkbook();
        }
        if (data.length == 0) {
        	//data为空时导出表头
        	Sheet sheet = wb.createSheet(sheetNames[0]);        	
            setTableHead(wb, sheet);
        	//
            return wb;
        }
        for (int i = 0; i < data.length; i++) {
            Sheet sheet = wb.createSheet(sheetNames[i]);
            Row row;
            setTableHead(wb, sheet);
            
            //导入模板创建关联表信息,此处对于同时有多张sheet导出时不支持,关联表会只有一个,如需要请修改关联表传入参数类型
            if(i == 0){
            	//创建关联表sheet
            	setSelectListSheet(wb, sheet, i);
            	//主表与关联表关联，此操作还没有实现支持2007
            	creatExcelRefrence(wb, sheet, i);
        	}
            
            for (int j = 0, len = data[i].size(); j < len; j++) {
                row = sheet.createRow(j + headerRow);
                Object obj = data[i].get(j);
                if (obj == null) {
                    continue;
                }
                if (obj instanceof Map) {
                    processAsMap(columns[i], row, obj);
                } else if (obj instanceof Model) {
                    processAsModel(columns[i], row, obj);
                } else if (obj instanceof Record) {
                    processAsRecord(columns[i], row, obj);
                } else {
                    throw new RuntimeException("Not support type[" + obj.getClass() + "]");
                }
            }
        }
        return wb;
    }

    @SuppressWarnings("unchecked")
    private static void processAsMap(String[] columns, Row row, Object obj) {
        Cell cell;
        Map<String, Object> map = (Map<String, Object>) obj;
        if (columns.length == 0) { // show all if column not specified
            Set<String> keys = map.keySet();
            int columnIndex = 0;
            for (String key : keys) {
                cell = row.createCell(columnIndex);
                cell.setCellValue(map.get(key) + "");
                columnIndex++;
            }
        } else {
            for (int j = 0, len = columns.length; j < len; j++) {
                cell = row.createCell(j);
                cell.setCellValue(map.get(columns[j]) == null ? "" : map.get(columns[j]) + "");
            }
        }
    }

    private static void processAsModel(String[] columns, Row row, Object obj) {
        Cell cell;
        Model<?> model = (Model<?>) obj;
        Set<Entry<String, Object>> entries = model._getAttrsEntrySet();
        if (columns.length == 0) { // show all if column not specified
            int columnIndex = 0;
            for (Entry<String, Object> entry : entries) {
                cell = row.createCell(columnIndex);
                cell.setCellValue(entry.getValue() + "");
                columnIndex++;
            }
        } else {
            for (int j = 0, len = columns.length; j < len; j++) {
                cell = row.createCell(j);
                cell.setCellValue(model.get(columns[j]) == null ? "" : model.get(columns[j]) + "");
            }
        }
    }

    private static void processAsRecord(String[] columns, Row row, Object obj) {
        Cell cell;
        Record record = (Record) obj;
        Map<String, Object> map = record.getColumns();
        if (columns.length == 0) { // show all if column not specified
            record.getColumns();
            Set<String> keys = map.keySet();
            int columnIndex = 0;
            for (String key : keys) {
                cell = row.createCell(columnIndex);
                cell.setCellValue(record.get(key) + "");
                columnIndex++;
            }
        } else {
            for (int j = 0, len = columns.length; j < len; j++) {
                cell = row.createCell(j);
                cell.setCellValue(map.get(columns[j]) == null ? "" : map.get(columns[j]) + "");
            }
        }
    }

    public PoiExporter version(String version) {
        this.version = version;
        return this;
    }

    public PoiExporter sheetName(String sheetName) {
        this.sheetNames = new String[]{sheetName};
        return this;
    }

    public PoiExporter sheetNames(String... sheetName) {
        this.sheetNames = sheetName;
        return this;
    }

    public PoiExporter cellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
        return this;
    }

    public PoiExporter headerRow(int headerRow) {
        this.headerRow = headerRow;
        return this;
    }

    public PoiExporter header(String... header) {
        this.headers = new String[][]{header};
        return this;
    }

    public PoiExporter headers(String[]... headers) {
        this.headers = headers;
        return this;
    }

    public PoiExporter column(String... column) {
        this.columns = new String[][]{column};
        return this;
    }

    public PoiExporter columns(String[]... columns) {
        this.columns = columns;
        return this;
    }
    
    public PoiExporter comment(String... comment) {
        this.comments = new String[][]{comment};
        return this;
    }
    
    public PoiExporter comments(String[]... comments) {
        this.comments = comments;
        return this;
    }
    
    public PoiExporter selectList(List<List<String>> selectList, int listSize) {
        this.selectList = selectList;
        this.listSize = listSize;
        return this;
    }
    
    public PoiExporter showHeaders(String showHeaders){
    	this.showHeaders = showHeaders;
    	return this;
    }
}

