package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 * 2016/11/4
 */
public interface IDictService {
    /**
     * 初始构造字典树数据
     */
    List<Record> initDictTree();

    /**
     * 根据字典编码, 删除当前字典及其子字典
     */
    void deleteDict(String delDictId);

    /**
     * 根据主键ID, 按表查询一条记录
     */
    Record findDictById(String id);

    /**
     * 保存方法
     */
    String saveDict(LoginUserObj loginUserObj, Map<String, String[]> paraMap);

    /**
     * 模糊查询
     */
    String searchDicts(String searchDictInput);

    /**
     * 列表查询
     */
    Page<Record> findChildrenDicts(String pageNumber, String pageSize, String dictId);
}
