package edu.dsideal.campus.framework.dao;

import edu.dsideal.campus.framework.plugin.generator.ModelGenerator;
import edu.dsideal.campus.framework.plugin.generator.TableMeta;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class MsModelGenerator extends ModelGenerator {
    protected String classDefineTemplate =
            "/**%n" +
            " * Generated by JFinal.%n" +
            " */%n" +
            "@SuppressWarnings(\"serial\")%n" +
            "public class %s extends %s {%n";
    
    private static final String DIALET = "Ms";
    
    public MsModelGenerator(String modelPackageName, String baseModelPackageName,
            String modelOutputDir) {
        super(modelPackageName, baseModelPackageName, modelOutputDir);
    }
    protected void genImport(TableMeta tableMeta, StringBuilder ret) {
        ret.append(String.format(importTemplate, modelPackageName, tableMeta.modelName));
    }
    protected void genClassDefine(TableMeta tableMeta, StringBuilder ret) {
        ret.append(String.format(classDefineTemplate, tableMeta.modelName+DIALET, tableMeta.modelName));
    }
    protected void genDao(TableMeta tableMeta, StringBuilder ret) {
    }

    protected void wirtToFile(TableMeta tableMeta) throws IOException {
        File dir = new File(modelOutputDir);
        if (!dir.exists())
            dir.mkdirs();
        
        String target = modelOutputDir + File.separator + tableMeta.modelName + DIALET + ".java";
        
        File file = new File(target);
        if (file.exists()) {
            return ;    // 若 Model 存在，不覆盖
        }
        
        FileWriter fw = new FileWriter(file);
        try {
            fw.write(tableMeta.modelContent);
        }
        finally {
            fw.close();
        }
    }
}
