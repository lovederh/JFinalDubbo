package edu.dsideal.campus.framework.service;

import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;

import java.util.List;
import java.util.Map;

/**
 * ztree的获得数据的api
 * Created by dsideal-lee on 2016/11/18.
 */
public interface ITreeService {
    /**
     * 查询ztree的数据
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> getTreeData(Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    /**
     * 初始化权限树数据
     * @param paraMap
     * @param loginUserObj
     * @return
     */
    List<Record> initRoleTree(Map<String, String[]> paraMap, LoginUserObj loginUserObj);

    boolean saveTreeRole(Map<String, String[]> paraMap, LoginUserObj loginUserObj);
}
