package edu.dsideal.campus.common;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.xwpf.converter.core.BasicURIResolver;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
public class PoiWord2Html {
    private String wordType;
    private String sourceFileName;
    private String targetFileName;
    private String imagePathStr;

    public PoiWord2Html(String wordType, String sourceFileName, String targetFileName, String imagePathStr) {
        this.wordType = wordType;
        this.sourceFileName = sourceFileName;
        this.targetFileName = targetFileName;
        this.imagePathStr = imagePathStr;
    }

    public void word2html(){
        try {
            if ("doc".equals(wordType)) {
                docToHtml();
            } else {
                docxToHtml();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    // doc转换为html
    private void docToHtml() throws Exception {
        HWPFDocument wordDocument = new HWPFDocument(new FileInputStream(sourceFileName));
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(document);
        // 保存图片，并返回图片的相对路径
        wordToHtmlConverter.setPicturesManager((content, pictureType, name, width, height) -> {
            File imageFile = new File(imagePathStr);
            if(! imageFile.exists()){
                imageFile.mkdirs();
            }

            String fileName = targetFileName.substring(targetFileName.lastIndexOf("\\") + 1);

            try(FileOutputStream out = new FileOutputStream(imagePathStr + fileName + name)){
                out.write(content);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "image/" + fileName + name;
        });
        wordToHtmlConverter.processDocument(wordDocument);
        Document htmlDocument = wordToHtmlConverter.getDocument();
        DOMSource domSource = new DOMSource(htmlDocument);
        StreamResult streamResult = new StreamResult(new File(targetFileName));

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();
        serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
        serializer.transform(domSource, streamResult);
    }
    // docx转换为html
    private void docxToHtml() throws Exception {
        OutputStreamWriter outputStreamWriter = null;
        try {
            XWPFDocument document = new XWPFDocument(new FileInputStream(sourceFileName));
            XHTMLOptions options = XHTMLOptions.create();
            // 存放图片的文件夹
            options.setExtractor(new FileImageExtractor(new File(imagePathStr)));
            // html中图片的路径
            options.URIResolver(new BasicURIResolver("image"));
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(targetFileName), "utf-8");
            XHTMLConverter xhtmlConverter = (XHTMLConverter) XHTMLConverter.getInstance();
            xhtmlConverter.convert(document, outputStreamWriter, options);
        } finally {
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
        }
    }
    public static void main(String[] args){
        PoiWord2Html poiWord2Html = new PoiWord2Html("docx", "E:\\万合共享资料\\数字化12管理平台v1.1.doc", "e:\\数字化校园管理平台v1.1.html", "e:\\image");
        poiWord2Html.word2html();
    }
}
