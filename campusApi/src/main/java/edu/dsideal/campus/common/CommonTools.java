package edu.dsideal.campus.common;

import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import org.dom4j.Document;
import org.dom4j.Element;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by dsideal-lee on 2016/10/15.
 */
public class CommonTools {
    /**
     * 通用消息方法
     * @param flag 成功标志 - 0 为成功，1为失败
     * @param msg  提示信息
     * @return
     */
    public static String getMsgJson(boolean flag, String msg){
        Map<String, String> msgMap = new HashMap<String, String>();
        if(flag){
            msgMap.put("status", ConstStatic.JSON_STATUS_SUCCESS);
            msgMap.put("msg", msg);
        }else {
            msgMap.put("status", ConstStatic.JSON_STATUS_FAIL);
            msgMap.put("msg", msg);
        }
        return JsonKit.toJson(msgMap);
    }

    public static String toUpperCaseFirstChar(String str){
        StringBuilder s = new StringBuilder(str);
        s.setCharAt(0, Character.toUpperCase(str.charAt(0)));
        return s.toString();
    }


    public static <T> T initIntercept(String bizIntercept) {
        if (!isEmpty(bizIntercept)) {
            try {
                // 实例化自定义拦截器
                return (T) Class.forName(bizIntercept).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * 获取当前系统时间字符串
     * 格式: yyyy-MM-dd
     */
    public static String getCurrentDateYmd(){
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(ConstStatic.DATE_FORMAT_YMD);
        return df.format(new Date()).toString();
    }

    /**
     * 获取当前系统时间字符串
     * 格式: yyyy-MM-dd HH:mm:ss
     */
    public static String getCurrentTimeYmdhms(){
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(ConstStatic.DATE_FORMAT_YMDHMS);
        return df.format(new Date()).toString();
    }
    /**
     * 获取当前系统时间字符串
     * 格式: yyyyMMddHHmmss
     * @return
     */
    public static String getCurrentTimeNoLine(){
        // 设置日期格式
        SimpleDateFormat df = new SimpleDateFormat(ConstStatic.DATE_FORMAT_YMDHMSS);
        return df.format(new Date()).toString();
    }
    public static String getChineseConvert(String oldString){
        String newString = "";
        try{
            newString = new String(oldString.getBytes(),"ISO-8859-1");
        }catch(Exception e){
            e.printStackTrace();
        }
        return newString;
    }

    /**
     * 判断字符串是否为非空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }

    /**
     * 判断字符串是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        return null==str || "null".equals(str) || "".equals(str.trim());
    }

    /**验证日期格式合法性
     * yyyyMMdd yyyy-MM-dd yyyy.MM.dd yyyy/MM/dd
     * @param str
     * @return
     */
    public static boolean validatDate(String str) {
        if (str.matches("^(\\d{4})(\\/|\\.|\\-)(0\\d{1}|1[0-2]|\\d{1})(\\/|\\.|\\-)(0\\d{1}|[12]\\d{1}|3[01]|\\d{1})$")
                || str.matches("^(\\d{8})$")) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * 验证传入的字符串是否是手机号码/座机号码
     * @param str
     * @return
     */
    public static boolean validatMobileOrlandlineNumber(String str){
        // 原正则表达式(Java代码需要转义): (^(\d{3,4}-)?\d{7,8})$|(0?[1][34578][0-9]{9})
        return str.matches("(^(\\d{3,4}-)?\\d{7,8})$|(0?[1][34578][0-9]{9})");
    }

    /**
     * 验证传入的字符串是否只是手机号码
     * @param str
     * @return
     */
    public static boolean validatMobileNumber(String str){
        return str.matches("^0?[1][34578][0-9]{9}$");
    }

    /**
     * 验证传入的字符串是否为有效网址格式
     * @param str
     * @return
     */
    public static boolean validatWebsiteURL(String str){
        // 原正则表达式(Java代码需要转义): ^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$
        return str.matches("^([hH][tT]{2}[pP]:\\/\\/|[hH][tT]{2}[pP][sS]:\\/\\/)(([A-Za-z0-9-~]+)\\.)+([A-Za-z0-9-~\\/])+$");
    }

    //检测是否为数字
    public static boolean isNumber(String num) {
        if (null == num || "".equals(num)) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]*");
        if (!pattern.matcher(num).matches()) {
            return false;
        }
        return true;
    }

    /**
     * 根据参数数组创建对象
     * @param array 数组格式{{key1,value1},{key2,value2}}
     * @return
     */
    public static Record createRecord(String[][]array){
        if( array == null  ){
            return null;
        }
        Record rd = new Record();
        for(int i = 0;i<array.length;i++){
            rd.set(array[i][0], array[i][1]);
        }
        return rd;
    }


    /**
     * 依据xPath获取节点配置的文本信息
     * @param document
     * @param xPath
     * @return
     */
    public static String findXmlConfigTextByXpath(Document document, String xPath){
        @SuppressWarnings("rawtypes")
        List nodeList = document.selectNodes(xPath);
        if(null==nodeList || nodeList.size()<=0){
            return null;
        }
        Element element = (Element)nodeList.get(0);
        String text = element.getTextTrim();
        return CommonTools.isEmpty(text)? null: text;
    }

    /**
     * 判断字符串是否在数组中
     * @param obj 字符串
     * @param arr 数组
     * @return
     */
    public static boolean strObjExistArray(String obj,String[] arr){
        if( obj == null || arr == null ){
            return false;
        }
        for( String s : arr ){
            if( obj.equals(s) ){
                return true;
            }
        }
        return false;
    }

    /**
     * 判断字符串是否在集合中
     * @param obj 字符串
     * @return
     */
    public static boolean strObjExistArray(String obj,List<String> list){
        if( obj == null || list == null ){
            return false;
        }
        for( String s : list ){
            if( obj.equals(s) ){
                return true;
            }
        }
        return false;
    }



    /**
     * 从请求参数集合中取出指定参数下的参数值, 若不存在则返回空字符串
     * 若为checkbox提交的数据, 则执行对参数数组内所有数据逗号分隔操作
     */
    public static String getValueFromParaMap (Map<String, String[]> paraMap, String paraName) {
        String[] paraArray = paraMap.get(paraName);
        if (paraArray!=null && paraArray.length>0) {
            int paraArrayLen = paraArray.length;
            if(paraArrayLen == 1){
                return paraArray[0];
            }
            StringBuilder paraValue = new StringBuilder();
            for(int i=0; i<paraArrayLen ; i++){
                if(i != 0){
                    paraValue.append(",");
                }
                paraValue.append(paraArray[i]);
            }
            return paraValue.toString();
        }
        return "";
    }

    /**
     * 请求url的拼接属性方法
     * @param baseUrl
     * @param options
     * @return
     */
    public static String appendUrlOptions(String baseUrl, String... options){
        if(isEmpty(baseUrl) || "#".equals(baseUrl) || null==options || options.length==0 || isEmpty(options[0])){
            return baseUrl;
        }
        // 构造拼接后最终的url
        StringBuilder finalUrl = new StringBuilder(baseUrl);
        String option;
        for(int i=0, len=options.length ; i<len ; i++){
            option = options[i];
            if(isEmpty(option)){
                continue;
            }
            if(i==0 && baseUrl.indexOf("?")<0){
                finalUrl.append("?");
            }else{
                finalUrl.append("&");
            }
            finalUrl.append(option);
        }
        return finalUrl.toString();
    }

    /**
     * 获取前端表单中指标参数值, 填充对应同字段有效的Record
     * @param formColumns 表单中存在的所有列
     * @param paraMap     前端表单参数
     * @param records      已经获取的Record, 如果不为null则本次继续完善即可, 否则新建一个Record
     * @return Record     对应同字段有效的Record
     */
    public static Record putRecordUseParams(String formColumns, Map<String, String[]> paraMap, Record... records) {
        // 填充的记录
        Record record = null==records||records.length==0||null==records[0]? new Record() : records[0];
        if (CommonTools.isEmpty(formColumns)) {
            return record;
        }
        // 各个字段依次赋值
        for (String colName : formColumns.split(",")) {
            if ("id".equals(colName)) {
                continue;
            }
            record.set(colName, CommonTools.getValueFromParaMap(paraMap, colName));
        }
        return record;
    }

    public static void buildJqFilter(StringBuilder sqlBuilder, String searchOper, String searchField, String searchString) {
        switch (searchOper) {
            case "eq": //equal
                sqlBuilder.append(searchField).append("= '").append(searchString).append("' ");
                break;
            case "ne": //not equal
                sqlBuilder.append(searchField).append("<> '").append(searchString).append("' ");
                break;
            case "bw": //begin with
                sqlBuilder.append(searchField).append(" like '").append(searchString).append("%' ");
                break;
            case "bn": //not begin with
                sqlBuilder.append(searchField).append(" not like '").append(searchString).append("%' ");
                break;
            case "ew": //ends with
                sqlBuilder.append(searchField).append(" like '%").append(searchString).append("' ");
                break;
            case "en": //not ends with
                sqlBuilder.append(searchField).append(" not like '%").append(searchString).append("' ");
                break;
            case "cn": //contains
                sqlBuilder.append(searchField).append(" like '%").append(searchString).append("%' ");
                break;
            case "nc": //not contains
                sqlBuilder.append(searchField).append(" not like '%").append(searchString).append("%' ");
                break;
            case "nu": //is null
                sqlBuilder.append(searchField).append(" is null ");
                break;
            case "nn": //not is null
                sqlBuilder.append(searchField).append(" is not null ");
                break;
            case "in": //in
                sqlBuilder.append(searchField).append(" in ('").append(searchString).append("') ");
                break;
            case "ni": //not in
                sqlBuilder.append(searchField).append(" not in ('").append(searchString).append("') ");
                break;
        }
    }
}
