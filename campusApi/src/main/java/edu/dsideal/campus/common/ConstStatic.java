package edu.dsideal.campus.common;

/**
 * Created by dsideal-lee on 2016/10/15.
 */
public class ConstStatic {
    /**
     * 开发者默认企业域 domain
     */
    public static final String ADMIN_DOMAIN = "domain";
    public static final String COPYRIGHT_DSIDEAL = "dsideal";
    /**
     * 日期时间格式化 yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_FORMAT_YMDHMS = "yyyy-MM-dd HH:mm:ss";
    /**
     * 日期时间格式化 yyyyMMddHHmmss
     */
    public static final String DATE_FORMAT_YMDHMSS = "yyyyMMddHHmmss";
    /**
     * 日期时间格式化 yyyy-MM-dd
     */
    public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
    /**
     * 系统操作成功标识
     */
    public static final String JSON_STATUS_SUCCESS = "200";
    /**
     * 系统操作失败标识
     */
    public static final String JSON_STATUS_FAIL = "400";

    /**
     * 操作成功提示
     */
    public static final String JSON_MSG_SUCCESS = "恭喜！操作成功:)";
    /**
     * 操作失败提示
     */
    public static final String JSON_MSG_FAIL = "对不起！操作失败，请联系管理员:(";
    /**
     * 表名唯一性
     */
    public static final String UNIQUE_MSG_FAIL = "对不起！唯一性校验失败，请联系管理员:(";

    /**
     * 系统定制logo默认是否启用(1启用; 0不启用)
     */
    public static final String DEFAULE_LOGO_IMG_USED = "1";

    /**
     * 系统风格定制 - 网页标题默认值
     */
    public static final String DEFAULE_WEB_PAGE_TITLE = "校园管理软件";
    /**
     * 系统风格定制 - 欢迎域主标题(默认名称)
     */
    public static String DEFAULE_SCHOOL_NAME_CN = null;

    /**
     * 主键生成规则配置字典表的静态数据1: 系统默认，2：UUID, 3：日期流水，4：默认开头字典流水，5：树结构
     */
    public static String[] KEY_RULE_CONF_DICT = {"1", "2", "3", "4", "5"};
}
