package edu.dsideal.campus.common;

import com.jfinal.log.Log;

/**
 * Created by Lee on 2016/12/31.
 */
public class Authorization {
    private static Log log = Log.getLog(Authorization.class);

    /**
     * 用go语言生成的公钥进行数据加密传给go服务端
     * @return
     */
    public static String getAuthorizationCode(){
        try {
            String publicKey = RSAUtils.PUBLIC_KEY_GO;
            String source = new StringBuilder(OSUtil.getOS()).append("@")
                    .append(OSUtil.getIpAddress()).append("@").append(OSUtil.getMacAddress()).toString();
            byte[] data = source.getBytes();
            //公钥加密
            byte[] encodedData = RSAUtils.encryptByPublicKey(data, publicKey);
            return Base64.encode(encodedData);
        }catch (Exception e){
            log.error("生成注册资源时报错："+e);
        }
        return "";
    }
}
