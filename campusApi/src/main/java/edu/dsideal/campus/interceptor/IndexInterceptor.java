package edu.dsideal.campus.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

import java.util.Arrays;

/**
 * 全局拦截器
 * 判断用户是否登陆状态及controllerKey是否有权限访问
 * Created by dsideal-lee on 2016/10/20.
 */
public class IndexInterceptor implements Interceptor {
    // 默认登录主页控制
    private static final String DEFAULT_MAIN_ACTION = "/login";//登录首页
    //基础开发平台的访问路径，不允许其非开发人员访问
    private static final String[] PERMISSION_CONTROLLER_KEY = {"/developer", "/meta", "/bo", "/dict", "/update", "/menu"};
    @Override
    public void intercept(Invocation inv) {
        String ctrlKey = inv.getControllerKey();
        if("/login".equals(ctrlKey)|| "/registerCode".equals(ctrlKey)
                || "/registSystem".equals(ctrlKey)){
            inv.invoke();
        }else if("/".equals(ctrlKey)){
            inv.getController().redirect(DEFAULT_MAIN_ACTION);
        }else{
            //验证系统是否注册
//            String isRegister = cache.get("isThisSystemRegister");
//            if(!"true".equals(isRegister)){
//                inv.getController().redirect("/registSystem");
//                return;
//            }
            if (inv.getController().getSessionAttr("loginUserObj") == null) {
                inv.getController().redirect(DEFAULT_MAIN_ACTION);
            }else{
                /*
                此处勿删，开发阶段使用， 方便调试，正式环境将些段注释解开
                String domain =((LoginUserObj) inv.getController().getSessionAttr("loginUserObj")).getDomain();
                //判断是否访问了开发人员才能访问的菜单
                if(isHavePermission(ctrlKey)){
                    //判断是否为开发人员
                    if("domain".equals("domain")) {
                        inv.invoke();
                    }else{
                        //非开发人员跳到登陆页面
                        inv.getController().redirect(DEFAULT_MAIN_ACTION);
                    }
                }*/
                inv.invoke();
            }
        }

    }

    private boolean isHavePermission(String ctrlKey){
        return Arrays.asList(PERMISSION_CONTROLLER_KEY).contains(ctrlKey);
    }
}
