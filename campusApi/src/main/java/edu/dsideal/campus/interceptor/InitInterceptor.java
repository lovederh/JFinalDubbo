package edu.dsideal.campus.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import edu.dsideal.campus.framework.dao.LoginUserObj;

/**
 * Created by dsideal-lee on 2016/11/8.
 */
public class InitInterceptor implements Interceptor{
    @Override
    public void intercept(Invocation inv) {
        Controller ctrl = inv.getController();
        ctrl.setAttr("domain", ((LoginUserObj)ctrl.getSessionAttr("loginUserObj")).getDomain());
        ctrl.setAttr("dtoName", ctrl.getPara("dtoName"));
        inv.invoke();
    }
}
