package fileservice.service;

import com.jfinal.plugin.activerecord.Record;

import java.io.File;

/**
 * 2017/2/8 0008
 */
public interface IFileService {

    /**
     * 保存上传文件的方法
     *
     * @param fileInfo
     * @param fileId
     * @param loginId          当前登陆人
     * @param fileType
     * @param saveSeverFile
     * @param file_name
     * @param real_name
     * @param selfRelativePath 自定义的文件上传路径
     * @return
     */
    Record saveUploadFile(Record fileInfo, String fileId, String loginId, String fileType, File saveSeverFile, String file_name, String real_name, String selfRelativePath);

    /**
     * 保存上传文件的方法, 同时在对应路径生成缩略图的方法
     *
     * @param fileInfo
     * @param fileId
     * @param loginId          当前登陆人
     * @param fileType
     * @param saveSeverFile
     * @param file_name
     * @param real_name
     * @param selfRelativePath 自定义的文件上传路径
     * @param thumbnailPath    缩略图存放路径
     * @param thumbnailWidth   生成缩略图宽度
     * @param thumbnailHeight  生成缩略图高度
     * @return
     */

    Record saveUploadFileAndCreateThumbnail(Record fileInfo, String fileId, String loginId, String fileType, File saveSeverFile, String file_name, String real_name, String selfRelativePath, String thumbnailPath, int thumbnailWidth, int thumbnailHeight);

    /**
     * 删除已上传文件
     *
     * @param id
     * @return
     */
    String delFile(String id);



    Record findDownloadFile(String id);
}
