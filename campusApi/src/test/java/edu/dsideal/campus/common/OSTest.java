package edu.dsideal.campus.common;

/**
 * Created by Lee on 2016/12/31.
 */
public class OSTest {
    public static void main(String[] args) throws Exception{
//        test();
//        testSign();
//        testEncode();
        testGO();
        testDeGo();
    }
    public static void test() throws Exception{
        String publicKey = RSAUtils.PUBLIC_KEY;
        String privateKey = RSAUtils.PRIVATE_KEY;
        System.out.println(OSUtil.getIpAddress());
        System.out.println(OSUtil.getMacAddress());
        System.out.println(OSUtil.getOS());
        String source = new StringBuilder(OSUtil.getOS()).append("@")
                .append(OSUtil.getIpAddress()).append("@").append(OSUtil.getMacAddress()).toString();
        byte[] data = source.getBytes();
        //公钥加密
        byte[] encodedData = RSAUtils.encryptByPublicKey(data, publicKey);
        System.out.println("加密后文字：\r\n" + new String(encodedData, "UTF-8"));
        //私钥解密
        byte[] decodedData = RSAUtils.decryptByPrivateKey(encodedData, privateKey);
        String target = new String(decodedData, "UTF-8");
        System.out.println("解密后文字: \r\n" + target);
    }

    static void testSign() throws Exception {
        String privateKey = RSAUtils.PRIVATE_KEY;
        String publicKey = RSAUtils.PUBLIC_KEY;
        System.err.println("私钥加密——公钥解密");
        String source = new StringBuilder(OSUtil.getOS()).append("@")
                .append(OSUtil.getIpAddress()).append("@")
                .append(OSUtil.getMacAddress()).append("@dsideal").toString();
        byte[] data = source.getBytes("UTF-8");
        //私钥加密
        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
        System.out.printf("加密后%s", new String(encodedData, "UTF-8"));
        //公钥解密
        byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
        String target = new String(decodedData, "UTF-8");
        System.out.println("解密后: \r\n" + target);

        System.err.println("私钥签名——公钥验证签名");
        String sign = RSAUtils.sign(encodedData, privateKey);
        System.err.println("签名:\r" + sign);
        boolean status = RSAUtils.verify(encodedData, publicKey, sign);
        System.err.println("验证结果:\r" + status);
    }

    public static void testEncode() throws Exception{
        String data = "HXeXiaN94Hns6OMu6DFBUiFcKjgJpMXd5/FEbH7oVTORUdguR4tdfOxoRvBnSi6zD6IR7JijN2nxBT3PemN645l/XgsQZwdmnJqi+AgpiYoR9aB+3zE3hHO/9Kp9Xj2YbTajlmXl/mhUAoRGOUkYAXq5+rYZktS731yYbPbie0k=";
        byte[] bd = data.getBytes();
        byte[] decodedData = RSAUtils.decryptByPublicKey(bd, RSAUtils.PUBLIC_KEY);
        String target = new String(decodedData);
        System.out.println("解密后: \r\n" + target);

    }

    public static void testGO() throws  Exception{
        String forEnctrptByGOPublicKeyStr = "Hello world!";
        byte[] forEnctrptData = forEnctrptByGOPublicKeyStr.getBytes();
        byte[] enctrptByGoPublicKey = RSAUtils.encryptByPublicKey(forEnctrptData, RSAUtils.PUBLIC_KEY_GO);
        System.out.println("go public key :"+ Base64.encode(enctrptByGoPublicKey));
    }

    public static void testDeGo() throws Exception{
        byte[] forDectrptData = Base64.decode("FPjWnTDm486HHm9ZRjwxH3TAx2uxeAyDlHCyjhb9dmnxQuhSRSMmbkfgdOCh+cKNBTF7vT6Z4w8X/5miuZAAi25J1xLCN+ekc1ioPTCZnrmRDzo3Mdp+nZuxdls9brTn6yUFIKNwOJ/ymSKvqp933knrxTqoLd3N3co5IvQPHJ0=");
        byte[] dectrptyByJavaPrivateKey = RSAUtils.decryptByPrivateKey(forDectrptData, RSAUtils.PRIVATE_KEY);
        System.out.println("java private Key:"+ new String(dectrptyByJavaPrivateKey));
    }
}
