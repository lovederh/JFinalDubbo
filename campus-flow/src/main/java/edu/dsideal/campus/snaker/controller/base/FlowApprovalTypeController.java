package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowApprovalTypeService;

import java.util.List;

/**
 * 审批类型工单处理特殊请求处理类
 * 2016/12/29
 */
@Before(IocInterceptor.class)
public class FlowApprovalTypeController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowApprovalTypeService flowApprovalTypeService;

    /**
     * 审批类型工单, 展示明细页面
     */
    public void openApprovalViewPage(){
        String viewUrl = CommonTools.appendUrlOptions("/flowUserForm/openApprovalViewPage", "showType=approvalTypeView");
        redirect(viewUrl, true);
    }

    /**
     * 只展示涉及审批环节的处理结果/处理备注列表
     * 不启用分页查询
     */
    public void approvalResultGridData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 流转的历史环节信息
        List<Record> approvalResultList = flowApprovalTypeService.approvalResultGridData(loginUserObj, getParaMap());
        if(approvalResultList != null){
            renderJson(approvalResultList);
        }else{
            renderNull();
        }
    }
}
