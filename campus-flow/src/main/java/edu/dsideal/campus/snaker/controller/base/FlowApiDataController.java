package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.INoticeService;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowApiDataService;

import java.util.List;

/**
 * 流程数据中, 后期需要从外部获取的请求处理
 * 如: 跨库的人员/角色/分组信息
 * 2017/2/27
 */
@Before(IocInterceptor.class)
public class FlowApiDataController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowApiDataService flowApiDataService;
    @Inject.BY_NAME
    private INoticeService noticeService;

    /**
     * 初始化处理者来源选取树
     */
    public void initPickTree(){
        String treeType = getPara("treeType");
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //树数据
        List<Record> treeData = null;
        switch (treeType) {
            case "role":
                //选择角色
                treeData = flowApiDataService.initDomainRoleTree(loginUserObj, getParaMap());
                break;
            case "teacher":
                //选择人员(教师)
                treeData = noticeService.teacherTree("t_sys_org", getParaMap(), loginUserObj);
                break;
        }
        renderJson(treeData);
    }

    /**
     * 点击树节点触发的事件, 加载节点下可供选择的数据
     */
    public void findPickData(){
        String treeType = getPara("treeType");
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //供选择的列表数据
        List<Record> pickData = null;
        switch (treeType) {
            case "role":
                //选择角色
                pickData = flowApiDataService.findAllDomainRole(loginUserObj, getParaMap());
                break;
            case "teacher":
                //选择人员(教师)
                pickData = flowApiDataService.findAllTeacherByOrgId(loginUserObj, getParaMap());
                break;
        }
        renderJson(pickData);
    }
}
