package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.interceptor.InitInterceptor;
import org.snaker.engine.entity.HistoryTask;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowQueryService;

import java.util.List;
import java.util.Map;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_ID;


/**
 * 流程查询
 * 附: 该类中的通用url
 * 	    我的待处理列表(包括待办和抄送给我的): /flowQuery/toMyDealGrid
 * 	    我的工单申请列表: /flowQuery/toMyStartOrders
 * 	    工单查询列表: /flowQuery/toAllOrderGrid
 * 	    查看流程图: /flowQuery/toFlowPicture
 * 	    展示已安装流程: /flowQuery/toInstalledFlows
 * 	    我的可申请流程列表: /flowQuery/iCanStartFlowsData
 */
@Before(IocInterceptor.class)
public class FlowQueryController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowQueryService flowQueryService;


    /*********************************** 流程分布企业域 ***********************************/
    /**
     * 跳转至: 流程对应的企业域安装情况页面
     */
    public void toSysFlowDomains(){
        //查询类型: install已经装过的/noInstall没有安装过的
        setAttr("type", getPara("type"));
        //系统流程文件全路径
        setAttr("fileResource", getPara("fileResource"));
        render("query/sysFlowDomains.html");
    }
    /**
     * 依据类型, 查询某指定流程对应的企业域安装情况
     */
    public void sysFlowDomainsData(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<Record> domainList = flowQueryService.sysFlowDomainsData(loginUserObj, getParaMap());
        if(domainList != null){
            renderJson(domainList);
        }else{
            renderNull();
        }
    }

    /********************************* 已安装流程列表 **********************************/
    /**
     * 跳转至查看已安装流程页面, 展示所有已装流程
     */
    @Before(InitInterceptor.class)
    public void toInstalledFlows() {
        render("query/installedFlows.html");
    }
    /**
     * 已安装的流程数据, 基于企业域进行过滤
     */
    public void installedFlowsData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        Page<Record> flows = flowQueryService.installedFlowsData(loginUserObj, getParaMap());
        if(flows != null){
            renderJson(flows);
        }else{
            renderNull();
        }
    }

    /**
     * 我的可申请流程集合数据, 用于申请时展示及下拉列表数据源
     */
    public void iCanStartFlowsData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<Record> flows = flowQueryService.iCanStartFlowsData(loginUserObj, getParaMap());
        setAttr("iCanStartFlows", flows);
        render("query/iCanStartFlows.html");
    }

    /************************************* 工单查询 **************************************/
    /**
     * 跳转至工单查询列表页面(所有类型工单)
     */
    @Before(InitInterceptor.class)
    public void toAllOrderGrid() {
        //供查询使用的申请类型下拉框
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<Record> flows = flowQueryService.iCanStartFlowsData(loginUserObj, getParaMap());
        setAttr("iCanStartFlows", flows);
        render("query/allOrderGrid.html");
    }
    /**
     * 请求工单查询的列表数据
     */
    public void allOrderGridData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        renderJson(flowQueryService.allOrderGridData(loginUserObj, getParaMap()));
    }

    /**
     * 由我发起的工单
     */
    @Before(InitInterceptor.class)
    public void toMyStartOrders() {
        //我可以启动的流程列表, 作为下拉框使用
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<Record> iCanStartFlows = flowQueryService.iCanStartFlowsData(loginUserObj, getParaMap());
        setAttr("iCanStartFlows", iCanStartFlows);
        render("query/myStartOrders.html");
    }
    /**
     * 请求由我发起的工单列表数据
     */
    public void myStartOrdersData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        renderJson(flowQueryService.myStartOrdersData(loginUserObj, getParaMap()));
    }

    /********************************** 我的待办工单处理 ***********************************/
    /**
     * 跳转至: 我的待办列表页面
     */
    @Before(InitInterceptor.class)
    public void toMyDealGrid() {
        //供查询使用的申请类型下拉框
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<Record> flows = flowQueryService.iCanStartFlowsData(loginUserObj, getParaMap());
        setAttr("iCanStartFlows", flows);
        render("query/myDealGrid.html");
    }
    /**
     * 请求我的待办列表, 包括待处理和抄送给我的工单, 不分页
     */
    public void myDealGridData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String[] userPriv = baseFlowDbService.initUserPrivs(loginUserObj);// 当前登陆人权限
        renderJson(flowQueryService.myDealGridData(loginUserObj, userPriv, getParaMap()));
    }

    /************************************ 工单历史环节展示 *************************************/
    /**
     * 请求任务节点的历史列表数据
     * 不启用分页查询
     */
    public void historyGridData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 流转的历史环节信息
        List<Record> historyTasks = flowQueryService.historyGridData(loginUserObj, getParaMap());
        if(historyTasks != null){
            renderJson(historyTasks);
        }else{
            renderNull();
        }
    }

    /**
     * 展开任务节点详情
     * 步骤: (1)根据指定的工单和任务, 查询配置的节点表单路径
     *      (2)对于找到的表单路径, 重定向之, 展示填报的具体信息
     */
    @Deprecated
    public void expandTaskForm(){
        String taskId = getPara(ATTR_TASK_ID);//任务ID
        // 当前任务节点使用的表单url
        HistoryTask history = baseFlowDbService.findHistoryTaskById(taskId);
        if(history == null){
            setAttr("errorMsg", "对不起, 任务环节历史数据缺失!");
            render("/WEB-INF/view/errorMsg.html");
            return;
        }
        // 构造最终跳转使用的url
        String actionUrl = history.getActionUrl();
        actionUrl = CommonTools.appendUrlOptions(actionUrl, "showType=view", "taskName=" + history.getTaskName());
        // 路径重定向之, 请求表单具体信息
        redirect(actionUrl, true);
    }

    /************************************ 流程图查看 *************************************/
    /**
     * 跳转至展示流程图Tab的请求
     * 页面会展示出:流转的历史环节和流程图
     */
    public void toFlowPicture() {
        String processId = getPara(ATTR_PROCESS_ID);//流程定义ID
        String orderId = getPara(ATTR_ORDER_ID);//工单ID
        setAttr(ATTR_PROCESS_ID, processId);//流程定义ID
        setAttr(ATTR_ORDER_ID, orderId);//工单ID
        render("query/flowPicture.html");
    }
    /**
     * 请求流程图数据展示的方法, 前端调用
     * 返回需要的json数据
     */
    public void flowPictureData() {
        String processId = getPara(ATTR_PROCESS_ID);//流程定义ID
        String orderId = getPara(ATTR_ORDER_ID);//工单ID
        // 请求流程图数据并返回
        Map<String, String> pictureData = flowQueryService.flowPictureData(processId, orderId);
        renderJson(pictureData);
    }
}
