package edu.dsideal.campus.snaker.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.ISysFormsService;

/**
 * 系统表单请求处理
 * 2016/12/26
 */
@Before(IocInterceptor.class)
public class SysFormsController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private ISysFormsService sysFormsService;

    /**
     * 请假申请-启动表单
     */
    public void ask4Leave(){
        // 展示类型: view展示明细; form: 展示表单
        String showType = getPara("showType");
        if("form".equals(showType)){
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            //请假类型下拉框
            setAttr("leaveTypeList", baseFlowDbService.findDictListByType(loginUserObj, "dict_leaveType"));
            //共享域设置当前时间
            setAttr("nowDate", CommonTools.getCurrentTimeYmdhms());

            render("ask4Leave/ask4Leave.html");
        }else{
            /*
            //明细时, 把需要额外处理的字段做一下处理
            Record bean = (Record)getAttr(BUSINESS_BEAN_KEY);
            bean = sysFormsService.viewAsk4LeaveBean(bean);
            setAttr(BUSINESS_BEAN_KEY, bean);
            */
            render("ask4Leave/ask4LeaveView.html");
        }
    }

    /**
     * 借款申请流程-启动表单
     */
    public void loanApply(){
        // 展示类型: view展示明细; form: 展示表单
        String showType = getPara("showType");
        if("form".equals(showType)){
            render("loan/loanApply.html");
        }else{
            render("loan/loanApplyView.html");
        }
    }
}
