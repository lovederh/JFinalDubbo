package edu.dsideal.campus.building.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IBuildingService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.List;

@Before(IocInterceptor.class)
public class BuildingController extends Controller {
    @Inject.BY_NAME
    IButtonService btnService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    IBuildingService buildingService;

    @Before(InitInterceptor.class)
    public void index() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //得到按钮配置信息
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        //得到列配置信息
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //列配置信息
        StringBuilder columns = new StringBuilder("[");
        columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        for (Record r : boConfList) {
            columns.append("{\"field\":\"").append(r.getStr("colName"))
                    .append("\",\"title\":\"").append(r.getStr("showName"));
            //根据字段类型添加字段的值
            columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
        }
        columns.deleteCharAt(columns.length() - 1).append("]");
        //列数据
        setAttr("columns", columns.toString());
        setAttr("businessTable", "t_sys_buildings");
        render("bizroot/building/building.html");
    }

    /**
     * 构造楼宇树结构
     */
    public void initBuildingTree() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(buildingService.initBuildingTree(loginUserObj, getParaMap()));
    }

    /**
     * 显示树结构节点对应的房间信息
     */
    public void queryBuildingGridData() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(buildingService.queryBuildingPager(loginUserObj, getParaMap()));
    }

    /**
     * 编辑/删除分组节点
     */
    public void buildingTreeNote() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(buildingService.buildingTreeNote(loginUserObj, getParaMap()));
    }
}
