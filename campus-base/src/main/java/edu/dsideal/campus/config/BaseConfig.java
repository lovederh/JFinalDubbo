package edu.dsideal.campus.config;

import com.jfinal.config.Routes;
import edu.dsideal.campus.building.controller.BuildingController;
import edu.dsideal.campus.dept.controller.DeptController;
import edu.dsideal.campus.emp.controller.EmpController;
import edu.dsideal.campus.student.controller.StudentController;

/**
 * Created by dsideal-lee on 2017/2/18.
 */
public class BaseConfig extends Routes {

    @Override
    public void config() {
        add("/dept",DeptController.class, "/WEB-INF/view");
        add("/emp", EmpController.class, "/WEB-INF/view");
        add("/building", BuildingController.class, "/WEB-INF/view");
        add("/student", StudentController.class, "/WEB-INF/view");
    }
}
