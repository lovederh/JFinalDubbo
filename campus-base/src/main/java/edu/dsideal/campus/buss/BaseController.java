package edu.dsideal.campus.buss;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.framework.service.IEmpService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.ArrayList;
import java.util.List;

@Before(IocInterceptor.class)
public class BaseController extends Controller{
    @Inject.BY_NAME
    IButtonService btnService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    IEmpService empService;
    @Before(InitInterceptor.class)
    public void treeTableModel(){
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //得到按钮配置信息
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        //得到列配置信息
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //设置查询条件
        List<Record> searchList = new ArrayList<Record>();
        //列配置信息
        StringBuilder columns = new StringBuilder("[");
        columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        for (Record r : boConfList) {
            columns.append("{\"field\":\"").append(r.getStr("colName"))
                    .append("\",\"title\":\"").append(r.getStr("showName"));
            //根据字段类型添加字段的值
            columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
            if("true".equals(r.getStr("search"))){
                searchList.add(r);
            }
        }
        columns.deleteCharAt(columns.length() - 1).append("]");
        //列数据
        setAttr("columns", columns.toString());
        //搜索栏
        setAttr("searchList", searchList);
        //下拉字典
        setAttr("dict", boService.getModelDict(getPara("dtoName"), loginUserObj.getDomain()));
        render("template/tree_tpl.html");
    }
}
