package edu.dsideal.campus.student.controller;

import com.jfinal.aop.Before;
import edu.dsideal.campus.buss.BaseController;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.framework.service.IEmpService;
import edu.dsideal.campus.interceptor.InitInterceptor;

/**
 * Created by dsideal-lee on 2017/2/23.
 */
public class StudentController extends BaseController {
    @Inject.BY_NAME
    IButtonService btnService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    IEmpService empService;
    @Before(InitInterceptor.class)
    public void index(){
        setAttr("boName", "t_sys_classes");
        super.treeTableModel();
    }
}
