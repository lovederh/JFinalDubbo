package edu.dsideal.campus.emp.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.buss.BaseController;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.framework.service.IEmpService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.List;

@Before(IocInterceptor.class)
public class EmpController extends BaseController {
    @Inject.BY_NAME
    IButtonService btnService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    IEmpService empService;

    @Before(InitInterceptor.class)
    public void index() {

        setAttr("boName", "t_dept_org");
        super.treeTableModel();
    }

    /**
     * 构造组织机构树
     */
    public void initDeptTree() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.initDeptTree(loginUserObj, getParaMap()));
    }

    /**
     * 显示选择节点机构的人员信息
     * 查询机构树列表右侧显示相对应的的人员信息列表
     */
    public void queryEmpGridData() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.queryEmpPager(loginUserObj, getParaMap()));
    }

    /**
     * 人员调动跳转弹窗方法
     */
    public void personMove() {
        setAttr("empId", getPara("ids"));
        setAttr("businessTable", getPara("businessTable"));
        setAttr("jsPath", "/resource/js/developer/emp/personMove.js");
        render("bizroot/emp/personMove.html");
    }

    /**
     * 人员调动保存方法
     */
    public void empMoveSave() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.empMoveSave(loginUserObj, getParaMap()));
    }

    /**
     * 人员状态变更按钮
     */
    public void empStatusChange() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        String empIds = getPara("ids");
        // 获取选中人员信息
        setAttr("empInfoList", empService.findEmpInfoList(loginUserObj, empIds));
        // 获取人员状态变更字典信息
        setAttr("empStatusDict", empService.findEmpStatusDict());
        setAttr("empIds", empIds);
        setAttr("jsPath", "/resource/js/developer/emp/empStatusChange.js");
        render("bizroot/emp/empStatusChange.html");
    }

    /**
     * 人员状态变更保存
     */
    public void empStatusSave() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.empStatusSave(loginUserObj, getParaMap()));
    }

    /**
     * 人员附表按钮方法
     */
    public void empMeter() {
        setAttr("empId", getPara("empId"));
        setAttr("jsPath", "/resource/js/developer/emp/empMeterTab.js");
        render("bizroot/emp/empMeterTab.html");
    }

    /**
     * 家庭成员信息表
     */
    public void familyInfoTable() {
        setAttr("empId", getPara("empId"));
        setAttr("jsPath", "/resource/js/developer/emp/familyInfoTable.js");
        render("bizroot/emp/familyInfoTable.html");
    }

    /**
     * 学历信息表
     */
    public void educationInfoTable() {
        setAttr("empId", getPara("empId"));
        setAttr("jsPath", "/resource/js/developer/emp/educationInfoTable.js");
        render("bizroot/emp/educationInfoTable.html");
    }

    /**
     * 工作简历信息表
     */
    public void resumeInfoTable() {
        setAttr("empId", getPara("empId"));
        setAttr("jsPath", "/resource/js/developer/emp/resumeInfoTable.js");
        render("bizroot/emp/resumeInfoTable.html");
    }

//************************************************家庭成员信息内容*******************************************************

    /**
     * 添加/编辑家庭成员保存方法
     */
    public void familyMemberSave() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.familyMemberSave(loginUserObj, getParaMap()));
    }

    /**
     * 家庭成员信息列表
     */
    public void familyMemberInfoList() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        List<Record> familyMemberList = empService.familyMemberList(loginUserObj, getParaMap());
        renderJson(JsonKit.toJson(familyMemberList));
    }

    /**
     * 加载家庭成员信息
     */
    public void loadInfoFamilyMember() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        String id = getPara("id");
        Record memberInfo = empService.loadInfoFamilyMember(loginUserObj, id);
        memberInfo = memberInfo == null ? new Record() : memberInfo;
        renderJson(memberInfo);
    }

    /**
     * 删除家庭成员按钮
     */
    public void delFamilyMember() {
        String ids = getPara("ids");
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson( empService.delFamilyMember(loginUserObj, ids));
    }

//****************************************************学历信息内容*******************************************************

    /**
     * 添加/编辑学历信息保存方法
     */
    public void educationSave() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.educationSave(loginUserObj, getParaMap()));
    }

    /**
     * 学历信息列表
     */
    public void educationInfoList() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        List<Record> educationList = empService.educationInfoList(loginUserObj, getParaMap());
        renderJson(JsonKit.toJson(educationList));
    }

    /**
     * 加载家庭成员信息
     */
    public void loadInfoEducation() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        String id = getPara("id");
        Record educationInfo = empService.loadInfoEducation(loginUserObj, id);
        educationInfo = educationInfo == null ? new Record() : educationInfo;
        renderJson(educationInfo);
    }


    /**
     * 删除学历信息按钮
     */
    public void delEducation() {
        String ids = getPara("ids");
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.delEducation(loginUserObj, ids));
    }

//************************************************工作简历信息内容*******************************************************

    /**
     * 添加/编辑工作简历保存方法
     */
    public void resumeSave() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.resumeSave(loginUserObj, getParaMap()));
    }

    /**
     * 工作简历信息列表
     */
    public void resumeInfoList() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        List<Record> resumeList = empService.resumeInfoList(loginUserObj, getParaMap());
        renderJson(JsonKit.toJson(resumeList));
    }

    /**
     * 加载工作简历信息
     */
    public void loadInfoResume() {
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        String id = getPara("id");
        Record resumeInfo = empService.loadInfoResume(loginUserObj, id);
        resumeInfo = resumeInfo == null ? new Record() : resumeInfo;
        renderJson(resumeInfo);
    }


    /**
     * 删除工作简历信息按钮
     */
    public void delResume() {
        String ids = getPara("ids");
        LoginUserObj loginUserObj = (LoginUserObj) getSessionAttr("loginUserObj");
        renderJson(empService.delResume(loginUserObj, ids));
    }


}
