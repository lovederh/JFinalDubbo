package edu.dsideal.campus.dept.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.*;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/15.
 */
@Before(IocInterceptor.class)
public class DeptController extends Controller {
    @Inject.BY_NAME
    IButtonService btnService;
    @Inject.BY_NAME
    ITreeService treeService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    private IDeptService deptService;
    @Inject.BY_NAME
    IKeyService keyService;

    @Before(InitInterceptor.class)
    public void index() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //得到按钮配置信息
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        ////得到列配置信息
        //List<Record> boConfList = boService.getBoConfig(getParaMap(), loginUserObj);
        ////列配置信息
        //StringBuilder columns = new StringBuilder("[");
        //columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        //for (Record r : boConfList){
        //    columns.append("{\"field\":\"").append(r.getStr("colName"))
        //            .append("\",\"title\":\"").append(r.getStr("showName"));
        //    //根据字段类型添加字段的值
        //    columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
        //}
        //columns.deleteCharAt(columns.length()-1).append("]");
        ////列数据
        //setAttr("columns", columns.toString());
        //render("bizroot/dept/dept.html");
        //setAttr("jsPath","/resource/js/developer/dept/deptGrid111.js");
        render("bizroot/dept/deptGrid.html");
    }

    /**
     *  查找组织机构信息(构造树列表结构)
     */
    public void deptTreeGrid(){
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        String nowNodeId = getPara("nodeid");//传入当前查询的节点ID
        String nowLevel = getPara("n_level");//当前菜单级别

        List<Record> treeData;
        if(CommonTools.isNotEmpty(nowNodeId) && CommonTools.isNotEmpty(nowLevel)){
            // 查询指定子机构集合
            treeData = deptService.findDeptChildren(loginUserObj,getParaMap(),nowNodeId, nowLevel);
        }else{
            // 查询所有
            treeData = deptService.findAllDeptDefine(loginUserObj,getParaMap());
        }
        renderJson("treeData", treeData);
    }

//    /**
//     *  构造组织机构树
//     */
//    public void initDeptTree(){
//        BaseTable model = BaseTable.createInstance(this, "framework");
////        renderJson(deptService.initDeptTree(model));
//    }

    /**
     *  显示选择节点机构的信息
     */
    public void queryDeptInfo(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String orgId = getPara("orgId");
        List<Record> deptInfo = deptService.queryDeptInfo(loginUserObj,orgId);
        renderJson(JsonKit.toJson(deptInfo));

    }
    /**
     *  增加选择节点子节点机构的信息
     */
    public void addDept(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String[] key = keyService.getKey(getParaMap());
        String deptDataKeyId = deptService.addDept(loginUserObj,key[1],getParaMap());
        Map<String, String> msgMap = new HashMap<String, String>();
        msgMap.put("status", ConstStatic.JSON_STATUS_SUCCESS);
        msgMap.put("msg", "新增节点成功!");
        msgMap.put("deptDataKeyId",deptDataKeyId);
        renderJson(JsonKit.toJson(msgMap));
    }

    /**
     *  删除所选择的节点
     */
    public void delDept(){
        String id = getPara("id");
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        renderJson(deptService.delDept(loginUserObj,id));
    }

    /**
     *  所选择的部门调整信息
     */
    public void deptAdjustInfo(){
        setAttr("oldOrgId", getPara("orgId"));
        setAttr("dtoName", getPara("dtoName"));
        setAttr("domain", getPara("domain"));
        setAttr("jsPath", "/resource/js/developer/dept/deptAdjustInfo.js");
        render("bizroot/dept/deptAdjustInfo.html");
    }

    /**
     *  部门调整方法
     */
    public void deptAdjustSave(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        renderJson(deptService.deptAdjustSave(loginUserObj, getParaMap()));
    }

}
