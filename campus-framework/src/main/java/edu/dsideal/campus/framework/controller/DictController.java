package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IDictService;

import java.util.List;

/**
 * 字典管理
 * 2016/11/4
 */
@Before(IocInterceptor.class)
public class DictController extends Controller {
    @Inject.BY_NAME
    IDictService dictService;

    /**************************************字典初始化********************************************/
    /**
     * 字典初始化页面请求
     */
    public void index(){
        setAttr("jsPath", "/resource/js/developer/dictIndex.js");
        render("developer/dict/dictIndex.html");
    }

    /**
     * 构造字典树数据
     */
    public void initDictTree(){
        List<Record> dictData = dictService.initDictTree();
        String dataJson = dictData==null? null: JsonKit.toJson(dictData);
        renderJson(dataJson);
    }

    /**************************************字典查询********************************************/
    /**
     * 模糊查询
     */
    public void searchDicts(){
        // 查询字典中文值
        String searchDictInput = getPara("searchDictInput");
        // 执行模糊查询, 返回满足条件的字典值ID
        renderText(dictService.searchDicts(searchDictInput));
    }

    /**
     * 显示当前字典及其子字典
     */
    public void findChildrenDicts(){
        // 字典编码ID
        String dictId = getPara("dictId");
        if(CommonTools.isEmpty(dictId)){
            renderNull();
            return;
        }
        //返回字典列表的json数据
        renderJson(dictService.findChildrenDicts(getPara("page"), getPara("rows"), dictId));
    }
    /**************************************字典增删改查********************************************/
    /**
     * 查询字典值定义信息
     */
    public void findDictById(){
        renderJson(dictService.findDictById(getPara("id")));
    }

    /**
     * 根据字典编码, 删除当前字典及其子字典
     */
    public void deleteDict(){
        dictService.deleteDict(getPara("dictId"));
        renderJson(CommonTools.getMsgJson(true, "删除成功!"));
    }

    /**
     * 保存方法(修改/新增)
     */
    public void saveDict(){
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        renderJson(dictService.saveDict(loginUserObj, getParaMap()));
    }
}
