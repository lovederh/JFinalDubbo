package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IUserService;

import java.util.HashMap;
import java.util.Map;

/**
 * 2016/11/3 0003
 */
@Before(IocInterceptor.class)
public class UserController extends Controller {
    @Inject.BY_NAME
    IUserService userService;
    /**
     * 个人资料保存
     */
    public void saveMyInfo(){
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        String loginId = loginUserObj.getLoginId();
        // 请求参数集合
        Map<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("id", getPara("id"));
        paramMap.put("loginId", loginId);
        paramMap.put("userName", getPara("userName"));
        paramMap.put("orgId", getPara("orgId"));
        paramMap.put("domain", getPara("domain"));
        paramMap.put("email", getPara("email"));
        paramMap.put("cardNo", getPara("cardNo"));
        paramMap.put("qq", getPara("qq"));
        paramMap.put("wexin", getPara("wexin"));
        paramMap.put("phone", getPara("phone"));
        paramMap.put("gender", getPara("gender"));
        // 更改session对象
        Record loginUser = userService.findUserNameByLoginId(loginId);
        setSessionAttr("userName", loginUser.get("userName"));
        setSessionAttr("loginUserObj", new LoginUserObj(loginUser));

        renderJson(userService.saveMyInfo(paramMap));
    }
}
