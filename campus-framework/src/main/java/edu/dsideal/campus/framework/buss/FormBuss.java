package edu.dsideal.campus.framework.buss;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.service.IBoService;

import java.util.List;

/**
 * Created by dsideal-lee on 2017/1/17.
 */
public class FormBuss {
    public static void showFormBuss(Controller ctrl, IBoService boService, String dtoName, LoginUserObj loginUserObj){
        List<Record> columns = boService.getFormShowCol(dtoName, loginUserObj);
        ctrl.setAttr("columns", columns);
        //下拉字典
        ctrl.setAttr("dict", boService.getModelDict(dtoName, loginUserObj.getDomain()));
        //校验
        ctrl.setAttr("validateConf", boService.getValidate(dtoName));
    }
}
