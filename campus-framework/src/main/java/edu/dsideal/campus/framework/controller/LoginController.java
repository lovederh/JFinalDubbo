package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.ILoginService;
import edu.dsideal.campus.framework.service.IRoleService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Before(IocInterceptor.class)
public class LoginController extends Controller {
    @Inject.BY_NAME
    ILoginService loginService;
    @Inject.BY_NAME
    IRoleService roleService;

    public void index() {
        setAttr("jsPath", "/resource/js/login/login.js");
        render("login.html");
    }

    /**
     * 用户登录时的后台校验方法
     * 校验项: 1.用户账号loginId是否存在;
     * 2.用户账号是否可用(isUsed='1'可用);
     * 3.密码是否正确;
     * 4.用户账号的domain字段,如果domain字段的值是domain,该账号为超级管理员,否则为普通用户.
     */
    public void check() {
        //进行登录时的后台校验, 返回登录人对象
        LoginUserObj loginUserObj = loginService.checkLoginUser(getParaMap());
        //判断用户状态标识(是否可用)
        if (!loginUserObj.isUserStatus()) {
            HashMap<String, Object> result = new HashMap<String, Object>();
            result.put("status", false);
            result.put("message", loginUserObj.getUserStatusMsg());
            renderJson(result);
            return;
        }

        // session域中存放登陆用户头像缩略图路径thumbnailPath
        if (CommonTools.isEmpty(loginUserObj.getThumbnailPath())) {
            // 如果登陆用户头像缩略图路径thumbnailPath为空使用默认头像
            loginUserObj.setThumbnailPath("/resource/third/hplus/img/profile_small.jpg");
        }
        setSessionAttr("thumbnailPath", loginUserObj.getThumbnailPath());

        // session域中存放登陆用户对象loginUserObj
        setSessionAttr("loginUserObj", loginUserObj);
        setSessionAttr("domain", loginUserObj.getDomain());
        // 设置cookie
        setCookie("loginId", loginUserObj.getLoginId(), 1 << 31 - 1);

        //控制页面跳转(超级管理员跳转至/developer; 普通用户跳转至/welcome)
        String redirect2Route = "domain".equals(loginUserObj.getDomain()) ? "developer" : "welcome";
        renderJson("{\"status\":200,\"redirectTo\":\"" + redirect2Route + "\"}");
    }

    /**
     * 跳转到上传头像页面
     */
    public void forwardUploadHead() {
        //setAttr("jsPath", "/resource/js/login/uploadHead.js");
        render("uploadHead.html");
    }

    /**
     * 跳转到个人资料页面
     */
    public void forwardPersonalInfo() {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        Record loginUser = loginService.findUserRecordByLoginId(loginUserObj.getLoginId());
        setAttr("loginUserObj", loginUserObj);
        // session域中存放登陆用户头像路径
        if (CommonTools.isEmpty(loginUser.get("path"))) {
            setAttr("path", "/resource/third/hplus/img/a5.jpg");
        } else {
            setAttr("path", loginUser.get("path"));
        }
        setAttr("loginUser", loginUser);
        render("personalInfo.html");

    }

    /**
     * 跳转到注册页面
     */
    public void register() {
        render("registerSchool.html");
    }

    /**
     * 执行注册操作
     */
    public void registerControl() {
        //注册结果提示信息
        String registerMsg = "";
        boolean registerResult;//注册结果
        boolean isAllWrite = true;
        String loginID = getPara("loginID");
        String loginName = loginService.checkRegisterLoginID(loginID);
        //判断用户名是否已经存在
        if (!("".equals(loginName))) {
            registerResult = false;
            registerMsg = "用户名已存在";
            isAllWrite = false;
        }
        String companyName = getPara("companyName");
        String companyAdd = getPara("companyAdd");
        String tel = getPara("tel");
        if (!("".equals(tel))) {
            Pattern pattern = Pattern.compile("[0-9]*");
            Matcher isNum = pattern.matcher(tel);
            if (tel.length() != 11 && !isNum.matches()) {
                registerResult = false;
                registerMsg += "电话号码错误";
            }
            isAllWrite = false;
        }
        String corporation = getPara("corporation");
        String faxNum = getPara("faxNum");
        String emailAdd = getPara("emailAdd");
        String hasEmailAdd = loginService.checkRegisterEmailAdd(emailAdd);
        //判断电子邮箱地址是否存在
        if (!("".equals(hasEmailAdd))) {
            registerResult = false;
            registerMsg = "电子邮箱地址已存在";
            isAllWrite = false;
        }
        if (!("".equals(emailAdd))) {
            Pattern pattern = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$");
            Matcher matcher = pattern.matcher(emailAdd);
            if (!matcher.matches()) {
                registerMsg += "电子邮箱地址错误";
            }
            isAllWrite = matcher.matches();
        }
        if (("".equals(loginID)) || ("".equals(companyName)) || ("".equals(companyAdd)) || ("".equals(tel)) || ("".equals(corporation)) || ("".equals(faxNum)) || ("".equals(emailAdd))) {
            isAllWrite = false;
        }
        String uuid = UUID.randomUUID().toString();//主键UUID
        // 发送申请注册时间
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//可以方便地修改日期格式
        String applyTime = dateFormat.format(now);

        if (validateCaptcha("identifyingCode")) {
            if (isAllWrite == false) {
                registerResult = false;
                renderJson(CommonTools.getMsgJson(registerResult, registerMsg));
                return;
            }
            ArrayList<String> registerValue = new ArrayList<String>();
            registerValue.add(companyName);
            registerValue.add(companyAdd);
            registerValue.add(tel);
            registerValue.add(corporation);
            registerValue.add(faxNum);
            registerValue.add(emailAdd);
            registerValue.add(uuid);
            registerValue.add(loginID);
            registerValue.add(applyTime);
            renderJson(loginService.saveRegisterValue(registerValue));
            registerResult = true;
            registerMsg = "注册成功, 点击确定前往登录！";
        } else {
            registerResult = false;
            registerMsg += "验证码错误，请重新输入";
        }
        renderJson(CommonTools.getMsgJson(registerResult, registerMsg));
    }
}
