package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IDoService;
import edu.dsideal.campus.framework.service.IValidateService;

/**
 * Created by dsideal-lee on 2016/10/15.
 */
@Before(IocInterceptor.class)
public class MetaController extends Controller {
    @Inject.BY_NAME
    IDoService doService;
    @Inject.BY_NAME
    IValidateService validateService;
    public void index() {
        //根据菜单自动选择配置的业务模型
        setAttr("modelMaster", "t_sys_tables");
        setAttr("modelSlave", "t_sys_columns");
        //设置用户js路径
        setAttr("jsPath", "/resource/js/template/jqMasterSlave.js");
        render("template/jqgrid_master_slave.html");
    }

    /**
     * 表结构
     */
    public void model() {
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        if (model == null) {
            renderJson(CommonTools.getMsgJson(false,  ConstStatic.JSON_MSG_FAIL));
        }
        //设置Model的查询条件
        StringBuilder where = new StringBuilder();
        if("true".equals(getPara("_search"))){
            where.append(" where ");
            CommonTools.buildJqFilter(where, getPara("searchOper"), getPara("searchField"),getPara("searchString"));
        }else{
            where.append(" where 1=1 ");
        }

        //设置排序条件
        String order = " order by id desc ";
        //查询model的列表数据
        Page<?> modelList = doService.show(model, getParaToInt("page"), getParaToInt("rows"), where.toString(), order);
        //返回model的json数据
        renderJson(modelList);
    }

    /**
     * 字段信息
     */
    public void fields() {
        //获得参数
        int pId = getParaToInt("pId");
        setAttr("pId", pId);
        //根据id获得表的tableName
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
//        模型为空则加载失败
        if (model == null) {
            renderJson(CommonTools.getMsgJson(false,  ConstStatic.JSON_MSG_FAIL));
        }
//        设定where条件
        String where = new StringBuilder(" where pId = ").append(pId).toString();
//        设置排序条件
        String order = " order by id asc ";
//        查询列表数据
        Page<?> modelList = doService.show(model, getParaToInt("page"), getParaToInt("rows"), where, order);
//        返回json数据
        renderJson(modelList);
    }

    /**
     * 保存方法
     */
    public void save() throws Exception {
        //得到操作类型 add  edit del
        String oper = getPara("oper");
        //生成模型对象 得到当前类所在包的根包名this.getClass().getPackage().getName().split(".")[0]
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        //操作结果标记
        boolean flag = false;
        //编辑
        if ("edit".equals(oper)) {
            flag = doService.update(model);
        } else if ("add".equals(oper)) { //新增
            //新增方法移除id字段，让数据库自动生成
            model.remove("id");
            //保存方法
            if(!validateService.checkUnique(model)){
                renderJson(CommonTools.getMsgJson(false,  ConstStatic.UNIQUE_MSG_FAIL));
                return;
            }
            flag = doService.save(model);
        } else if ("del".equals(oper)) { //删除
            //获得选中的列，用逗号分隔
            String[] ids = getPara("id").split(",");
            flag = doService.deleteById(model, ids);
        }
        if (flag) {
            //操作成功提示
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_SUCCESS));
        } else {
            //操作失败提示
            renderJson(CommonTools.getMsgJson(flag,  ConstStatic.JSON_MSG_FAIL));
        }
    }

    public void rebuild() throws Exception{
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        boolean flag = doService.rebuild(model);
        if (flag) {
            //操作成功提示
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_SUCCESS));
        } else {
            //操作失败提示
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_FAIL));
        }
    }
}
