package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.ISystemConfigService;

@Before(IocInterceptor.class)
public class DeveloperController extends Controller{
    @Inject.BY_NAME
    ISystemConfigService systemConfigService;

    public void index(){
        setAttr("jsPath", "/resource/js/developer/main.js");
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        // 通过domain获取到用户的配置信息
        String domain = loginUserObj.getDomain();
        setAttr("loginUserObj",loginUserObj);
        //用户头像缩略图路径thumbnailPath
        setAttr("thumbnailPath",getSessionAttr("thumbnailPath"));
        // 根据用户的domain查找到系统配置信息
        setAttr("systemConfigInfo",systemConfigService.findSystemConfigInfo(domain));
        render("developer/developer.html");
    }
    public void mainPage(){
        render("developer/main_page.html");
    }

    public void basicTable(){
        setAttr("jsPath", "/resource/js/template/jqGridTemplate.js");
        render("template/jqgrid_formedit.html");
    }

    public void testGrid(){
        setAttr("jsPath", "/resource/js/template/jqGridFormEdit.js");
        render("template/jqgrid_formedit.html");
    }

}
