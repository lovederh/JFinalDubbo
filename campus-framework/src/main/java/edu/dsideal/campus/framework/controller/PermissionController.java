package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.List;

/**
 * Created by dsideal-lee on 2016/11/1.
 */
@Before(IocInterceptor.class)
public class PermissionController extends Controller {
    @Inject.BY_NAME
    private IBoService boService;
    @Inject.BY_NAME
    private IButtonService btnService;
    /**
     * 角色初始化功能，此功能开始进入通用功能开发
     */
    public void grantRole(){
        String domain = ((LoginUserObj)getSessionAttr("loginUserObj")).getDomain();
        setAttr("dtoName", "t_sys_role");
        setAttr("domain", domain);
        render("developer/role/role.html");
    }

    public void useToChild(){

        render("developer/role/use2child.html");
    }
}
