package edu.dsideal.campus.framework.controller;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.plugin.PoiRender;

import java.util.ArrayList;

/**
 * Created by dsideal-lee on 2017/3/4.
 */
public class ExcelController extends Controller{
    public void importExcel(){

    }
    public void exportExcel(){
        PoiRender poiRender = PoiRender.me(new ArrayList<Record>());
        poiRender.sheetName("dd").headers(new String[]{"1","2","3"}).columns(new String[]{"4","5","6"}).comments(new String[]{"7","8"})
                .version("2003").showHeaders("1,2,3");
        render(poiRender);
    }
}
