package edu.dsideal.campus.framework.controller;

import edu.dsideal.campus.framework.buss.FormBuss;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.framework.service.IDoService;
import edu.dsideal.campus.framework.service.IKeyService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/4.
 */
@Before(IocInterceptor.class)
public class PageController extends Controller {
    @Inject.BY_NAME
    private IBoService boService;
    @Inject.BY_NAME
    private IButtonService btnService;
    @Inject.BY_NAME
    private IDoService doService;
    @Inject.BY_NAME
    private IKeyService keyService;
    private LoginUserObj getLoginUserObj() {
        return getSessionAttr("loginUserObj");
    }
    @Before(InitInterceptor.class)
    public void index() {
        LoginUserObj loginUserObj = getLoginUserObj();
        //得到按钮配置信息
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        //列配置信息
        StringBuilder columns = new StringBuilder("[");
        columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        //得到列配置信息
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //设置查询条件
        List<Record> searchList = new ArrayList<Record>();
        //构造toolbarBtn
        for (Record r : boConfList){
            if("true".equals(r.getStr("search"))){
                searchList.add(r);
            }
            columns.append("{\"field\":\"").append(r.getStr("colName"))
                    .append("\",\"title\":\"").append(r.getStr("showName"));
            //根据字段类型添加字段的值
            columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
            //判断列的类型r.getStr("edittype") text select checkbox radio ;
        }
        columns.deleteCharAt(columns.length()-1).append("]");
        //列数据
        setAttr("columns", columns.toString());
        //搜索栏
        setAttr("searchList", searchList);
        //按钮栏
        setAttr("toolbarBtn", JsonKit.toJson(btnList));
        //下拉字典
        setAttr("dict", boService.getModelDict(getPara("dtoName"), loginUserObj.getDomain()));
        render("template/basic_table.html");
    }
    /**
     * 列表初始化加载数据
     */
    public void getListInitData() {
        Map<String, Object> dataMap = boService.getDoByPage(getParaMap(), getLoginUserObj());
        renderJson(JsonKit.toJson(dataMap));
    }

    /**
     * 显示表单
     */
    public void showForm(){
        String dtoName = getPara("dtoName");
        LoginUserObj loginUserObj = getLoginUserObj();
        FormBuss.showFormBuss(this, boService, dtoName, loginUserObj);
        String paraOpt = getPara("paraOpt");
        if("edit".equals(paraOpt)){
            setAttr("formData", JsonKit.toJson(boService.getDoDataById(getParaMap(), loginUserObj)));
        }
        render("template/form_tpl.html");
    }

    /**
     * 通用保存方法
     * 所有表主键id为自动增加
     * 树表包含codeId，按照树结构设置生成编码
     * 日期流水和以字母开头的表也同样包含codeId，按照相应规则生成编码
     * @throws Exception
     */
    public void save() throws Exception{
        BaseTable model = BaseTable.createInstance(getParaMap(), "edu.dsideal.campus.framework");
        model.set("loginUserObj", getSessionAttr("loginUserObj"));
        String paraOpt = getPara("paraOpt");
        boolean flag = false;
        if("add".equals(paraOpt)){
            model.remove("id");
            //新增方法
            String[] key = keyService.getKey(getParaMap());
            if(key != null && key[1] != null){
                model.set(key[0], key[1]);
            }
            flag = doService.save(model);
        }else if("edit".equals(paraOpt)){
            //修改方法
            flag = doService.update(model);
        }else if("del".equals(paraOpt)){
            //删除方法
            flag = doService.deleteById(model, getPara("ids").split(","));
        }
        if(flag){
            renderJson(CommonTools.getMsgJson(true, ConstStatic.JSON_MSG_SUCCESS));
            return;
        }
        renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_FAIL));
    }
}
