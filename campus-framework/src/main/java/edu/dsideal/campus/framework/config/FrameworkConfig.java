package edu.dsideal.campus.framework.config;

import edu.dsideal.campus.framework.controller.*;
import com.jfinal.config.Routes;

/**
 * Created by dsideal-lee on 2016/10/9.
 */
public class FrameworkConfig extends Routes {

    @Override
    public void config() {
        add("/", LoginController.class, "WEB-INF/view/login");
        add("/registSystem", RegistSystemController.class, "WEB-INF/view");
        add("/login", LoginController.class, "WEB-INF/view/login");
        add("/logout", LogoutController.class);
        add("/welcome", WelcomeController.class, "WEB-INF/view");
        add("/developer", DeveloperController.class, "WEB-INF/view");
        add("/permission", PermissionController.class, "WEB-INF/view");
        add("/meta", MetaController.class, "WEB-INF/view");
        add("/bo", BoController.class, "WEB-INF/view");
        add("/grid", GridController.class, "WEB-INF/view");
        add("/tree", TreeController.class, "WEB-INF/view");
        add("/page", PageController.class, "WEB-INF/view");
        add("/excel", ExcelController.class, "WEB-INF/view");
        // 菜单管理
        add("/menu", MenuController.class, "WEB-INF/view");
        // 字典管理
        add("/dict", DictController.class, "WEB-INF/view");
        // 用户信息管理
        add("/user", UserController.class,"WEB-INF/view");
        // 系统升级
        add("/update", UpdateController.class,"WEB-INF/view");
        // 系统配置
        add("/systemConfig", SystemConfigController.class,"WEB-INF/view");
        // 统计图表--ECharts
        add("/echarts", EchartsController.class,"WEB-INF/view");
        //注册验证码
        add("/registerCode",RegisterController.class,"WEB-INF/view");
    }
}
