package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IKeyService;
import edu.dsideal.campus.framework.service.ISystemConfigService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 2016/11/17 0017
 */
@Before(IocInterceptor.class)
public class SystemConfigController extends Controller {
    @Inject.BY_NAME
    ISystemConfigService systemConfigService;
    @Inject.BY_NAME
    IKeyService keyService;

    @Before(InitInterceptor.class)
    public void index() {
        // 找到系统配置信息
        setAttr("systemConfigInfo", systemConfigService.findSystemConfigInfo(getSessionAttr("domain")));
        setAttr("jsPath", "/resource/js/developer/systemconfig/systemConfig.js");
        render("developer/systemconfig/systemConfig.html");
    }

    /**
     * 查询学校配置信息
     */
    public void queryMembersConfigInfo() {
        renderJson(systemConfigService.queryMembersConfigInfo());
    }

    /**
     * 添加配置信息功能
     */
    public void addConfig() {
        // 网站标签标题
        String webTitle = getPara("webTitle");
        // 版权所有信息
        String mainTitle = getPara("mainTitle");

        // 随机生成配置信息ID
        String configId = UUID.randomUUID().toString();
        if (CommonTools.isEmpty(webTitle)) {
            renderJson(CommonTools.getMsgJson(false,"网站标签标题不能为空"));
            return;
        }
        if (CommonTools.isEmpty(mainTitle)) {
            renderJson(CommonTools.getMsgJson(false,"版权所有信息不能为空"));
            return;
        }
        renderJson(systemConfigService.addConfig(configId, webTitle, mainTitle));
    }

    /**
     * 保存配置信息
     */
    public void saveConfig() {
        // 网站标签标题
        String webTitle = getPara("webTitle");
        // 版权所有信息
        String mainTitle = getPara("mainTitle");
        // 配置信息ID
        String configId = getPara("id");
        // 配置信息isPass
        String isPass = getPara("isPass");

        renderJson(systemConfigService.saveConfig(configId, webTitle, mainTitle, isPass));
    }

    /**
     * 删除配置信息按钮功能
     */
    public void delConfig() {
        String[] ids = getParaValues("id");
        renderJson(systemConfigService.delConfig(ids));
    }

    /**
     * 通过按钮功能
     */
    public void passConfig() {
        String[] ids = getParaValues("id");
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        model.set("pId", "101");
        model.set("level", "1");
        model.set("isLeaf", "true");
        model.set("expanded", "true");
        model.set("ord", "1");
        model.set("delFlag", 0);
        Map<String, String[]> paraMap = getParaMap();
        Map<String, String[]> tempMap = new HashMap<String, String[]>();
        tempMap.put("pId", new String[]{"101"});//手动把pId赋值进去, 用于接下来的生成规则判断
        tempMap.putAll(paraMap);
        String[] key = keyService.getKey(tempMap);
        model.set("id", key[1]);
        renderJson(systemConfigService.passConfig(ids, model));
    }

    /**
     * 通过按钮功能
     */
    public void notPassConfig() {
        String[] ids = getParaValues("id");
        renderJson(systemConfigService.notPassConfig(ids));
    }
}
