package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IUpdateService;

import java.util.List;
import java.util.Map;

/**
 * 系统升级
 * 2016/11/11 0011
 */
@Before(IocInterceptor.class)
public class UpdateController extends Controller {
    @Inject.BY_NAME
    IUpdateService updateService;
    /**
     * 系统升级初始化页面请求
     */
    public void index(){
        setAttr("jsPath", "/resource/js/developer/update/update.js");
        render("developer/update/update.html");
    }

    /**
     *  构造升级模块信息树
     */
    public void initUpdateTree(){
        List<Record> updateData = updateService.initUpdateTree();
        String dataJson = updateData==null? null: JsonKit.toJson(updateData);
        renderJson(dataJson);
    }

    /**
     *  查找升级信息
     */
    public void findUpdateList(){
        // 升级表表名
        String updateTableName = getPara("updateTableName");
        if(CommonTools.isEmpty(updateTableName)){
            renderNull();
            return;
        }
        // 升级列表
        List<Map<String, String>> findUpdateList = updateService.findUpdate(updateTableName);
        renderJson("updateDataList", findUpdateList);
    }

    /**
     *  系统升级按钮功能
     */
    public void updateSelectedData(){
        // 批量升级选中行信息
        String rows = getPara("rows");
        renderJson(updateService.updateSelectedData(rows));
    }
}
