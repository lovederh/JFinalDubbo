package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IValidateService;

@Before(IocInterceptor.class)
public class BoController extends Controller {
    @Inject.BY_NAME
    private IBoService boService;

    @Inject.BY_NAME
    private IValidateService validateService;

    public void index(){
        setAttr("modelMaster", "t_sys_bo");
        setAttr("modelSlave", "t_sys_boconfig");
        setAttr("tableNameValue", boService.getContactTableData("t_sys_tables",null, "domain"));
        setAttr("requirValue", boService.getContactTableData("t_sys_dict", "dict_validate", "domain"));
        setAttr("jsPath", "/resource/js/developer/bo.js");
        render("developer/bo/bo.html");
    }

    public void basicTableData(){
        setAttr("jsPath", "/resource/js/template/btable/btable.js");
        render("template/basic_table.html");
    }

    public void user(){
        setAttr("jsPath", "/resource/js/template/btable/btable.js");
        render("template/basic_table.html");
    }
    //业务模型保存功能
    public void save(){
        String oper = getPara("oper");
        BaseTable model = getBaseTable();
        boolean flag = false;
        switch(oper){
            case "edit":
                flag = boService.update(model);
                break;
            case "add":
                if(!validateService.checkUnique(model)){
                    renderJson(CommonTools.getMsgJson(false, "校验唯一性不通过！"));
                    return;
                }
                //新增方法移除id字段，让数据库自动生成
                model.remove("id");
                try {
                    flag = boService.save(model);
                }catch (Exception e){
                    e.printStackTrace();
                    renderJson(CommonTools.getMsgJson(flag, "操作失败，请检查表管理中模型设置是否正确！"));
                }
                break;
            case "del":
                //获得选中的列，用逗号分隔
                String[] ids = getPara("id").split(",");
                flag = boService.deleteById(model, ids);
                break;
            default:
                break;
        }
        if (flag) {
            //操作成功提示
            renderJson(CommonTools.getMsgJson(flag, "恭喜，操作成功！"));
        } else {
            //操作失败提示
            renderJson(CommonTools.getMsgJson(flag, "操作失败，请联系管理员！"));
        }
    }

    public void configKeyRule(){
        Record keyRule = boService.getKeyRuleData(getBaseTable());
        setAttr("keyrule", keyRule);
        setAttr("jsPath", "/resource/js/developer/keyrule.js");
        render("developer/bo/key_rule.html");
    }

    public void saveKeyRuleConfig(){
        boolean flag = boService.saveKeyRuleConfig(getBaseTable());
        if(flag){
            renderJson(CommonTools.getMsgJson(true, "保存成功！"));
        }else{
            renderJson(CommonTools.getMsgJson(false, "对不起， 保存失败！"));
        }
    }

    private BaseTable getBaseTable() {
        return BaseTable.createInstance(this, "edu.dsideal.campus.framework");
    }

    public void keyConfig(){
        setAttr("jsPath", "/resource/js/developer/keyConfig.js");
        setAttr("dtoName", "t_sys_ruleconfig");
        render("developer/bo/key_config.html");
    }
}
