package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IMenuService;

import java.util.List;

/**
 * 菜单管理
 * 2016/10/31 0031
 */
@Before(IocInterceptor.class)
public class MenuController extends Controller {
    @Inject.BY_NAME
    IMenuService menuService;

    /**************************************菜单树表格********************************************/
    /**
     * 菜单定义页面跳转
     */
    public void menuDefine(){
        setAttr("jsPath", "/resource/js/developer/menu/menuDefine.js");
        render("developer/menu/menuDefine.html");
    }

    /**
     * 查找菜单定义信息(构造树列表结构)
     */
    public void findMenuDefines(){
        String nowNodeId = getPara("nodeid");//传入当前查询的节点ID
        String nowLevel = getPara("n_level");//当前菜单级别

        List<Record> treeData;
        if(CommonTools.isNotEmpty(nowNodeId) && CommonTools.isNotEmpty(nowLevel)){
            // 查询指定子菜单集合
            treeData = menuService.findMenuDefineChildren(nowNodeId, nowLevel);
        }else{
            // 查询所有
            treeData = menuService.findAllMenuDefine();
        }
        renderJson("treeData", treeData);
    }

    /**************************************菜单定义弹窗********************************************/
    /**
     * 打开菜单定义弹窗(修改时调用)
     */
    public void updateMenuDialog(){
        // 菜单表主键ID
        String primaryId = getPara("primaryId");
        // 取出菜单定义信息并传递给前端
        setAttr("menuDefine", menuService.findMenuDefineById(primaryId));
        // 获取到关联业务模型传递给前端下拉框
        setAttr("relatedBoList", menuService.getRelatedBoData());
        // 操作类型为修改
        setAttr("operateType", "update");
        setAttr("jsPath", "/resource/js/developer/menu/menuDefineDialog.js");
        render("developer/menu/menuDefineDialog.html");
    }

    /**
     * 新增子菜单弹窗
     */
    public void insertMenuDialog(){
        // 父菜单主键ID
        String parentMenuId = getPara("parentMenuId");
        // 父菜单级别
        String parentLevel = getPara("parentLevel");
        // 新菜单级别
        int menuLevel;

        if(CommonTools.isNotEmpty(parentMenuId) && CommonTools.isNotEmpty(parentLevel)){
            menuLevel = Integer.valueOf(parentLevel) + 1;
        }else{
            // 新菜单的父菜单为根节点
            menuLevel = 1;
            parentMenuId = "0";//新菜单的父编码
        }
        Record menuDefine = new Record();
        menuDefine.set("level", menuLevel);//新菜单级别
        menuDefine.set("pId", parentMenuId);//新菜单的父编码
        setAttr("menuDefine", menuDefine);
        // 获取到关联业务模型传递给前端下拉框
        setAttr("relatedBoList", menuService.getRelatedBoData());
        // 操作类型为新增
        setAttr("operateType", "insert");
        setAttr("jsPath", "/resource/js/developer/menu/menuDefineDialog.js");
        render("developer/menu/menuDefineDialog.html");
    }

    /**************************************菜单定义增删改********************************************/
    /**
     * 根据菜单编码, 删除当前菜单及其子菜单
     */
    public void deleteMenuDefine(){
        menuService.deleteMenuDefine(getPara("menuId"));
        renderJson(CommonTools.getMsgJson(true, "删除成功!"));
    }

    /**
     * 保存方法(修改/新增)
     */
    public void saveMenuDefine(){
        renderJson(menuService.saveMenuDefine(getParaMap()));
    }

    public void menuBtn(){

    }
}
