package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IEchartsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2016/11/23 0023
 */
@Before(IocInterceptor.class)
public class EchartsController extends Controller{
    @Inject.BY_NAME
    IEchartsService echartsService;

    public void index() {
        setAttr("jsPath","/resource/js/developer/echarts/echartsTree.js");
        render("developer/echarts/echartsTree.html");
    }

    /**
     *  构造图表信息树
     */
    public void initEchartsTree(){
        // 图表信息列表
        List<Record> echartsData = echartsService.initEchartsTree();
        String dataJson = echartsData==null? null: JsonKit.toJson(echartsData);
        renderJson(dataJson);
    }



    /**
     * 单击图表项维护树叶子节点时触发的请求, 展示节点对应的图表项维护信息
     */
    public void showEchartForm(){
        // 当前选中树节点ID(图表项ID)
        String echartId = getPara("echartId");
        if(CommonTools.isEmpty(echartId)){
            echartId = "0";
        }
        // 判断当前操作类型
        String type = getPara("type");
        //返回前端的图表配置对象
        Record echartInfo;
        if ("add".equals(type)){
            echartInfo = new Record();
            echartInfo.set("pId", echartId);
        }else{
            // 查找一条图表项配置信息并返回
            echartInfo = echartsService.findOne(echartId);
        }
        setAttr("echartInfo", echartInfo);
        // 构造图表类型下拉框
        setAttr("echartsTypeList", echartsService.findEchartsTypeList());
        setAttr("jsPath","/resource/js/developer/echarts/echartsForm.js");
        // 前端页面跳转
        render("developer/echarts/echartsForm.html");
    }

    /**
     * 图表项维护信息的保存方法
     */
    public void saveEchartForm(){
        Map<String, String> resultMap = new HashMap<String, String>();
        try {
            // 先保存图表项维护信息
            Record echartRecord = echartsService.saveEchartInfo(getParaMap());

            resultMap.put("optionData", echartsService.createOptionData(echartRecord));
            resultMap.put("status", ConstStatic.JSON_STATUS_SUCCESS);
            resultMap.put("msg", "保存成功!");
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("status", ConstStatic.JSON_STATUS_FAIL);
            resultMap.put("msg", "后台处理异常, 请检查图表项配置!");
        }
        renderJson(resultMap);
    }

    /**
     *  删除选中图表
     */
    public void deleteEcharts(){
        echartsService.deleteEcharts(getPara("echartId"));
        renderJson(CommonTools.getMsgJson(true, "删除成功!"));
    }
}
