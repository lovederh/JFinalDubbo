package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.ITreeUtil;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.dao.TreeFactory;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/7.
 */
@Before(IocInterceptor.class)
public class GridController extends Controller {
    @Inject.BY_NAME
    IBoService boService;

    public void index() {
        render("template/jqgrid_treegrid.html");
    }

    public void getGridConfig() {
        //生成业务模型
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        //查找配置列
        List<Record> gridConfigList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //查找主键配置
        model.set("boName", getPara("dtoName"));
        Record keyRuleConfigRecord = boService.getKeyRuleData(model);
        if(keyRuleConfigRecord == null){
            throw new RuntimeException("树结构配置不正确！请检查业务模型keyRule配置！");
        }
        StringBuilder gridConfig = new StringBuilder("{");
        if("tree".equals(getPara("condition"))) {
            ITreeUtil treeService = TreeFactory.getTreeService("jqgrid");
            //构建tree名称json
            treeService.buildTreeName(gridConfig, keyRuleConfigRecord.getStr("treeNameCol"));
            gridConfig.append(",");
            //构建树的treeReader配置
            treeService.buildTreeReader(gridConfig, keyRuleConfigRecord);
            gridConfig.append(",");
            //构建列名称
            treeService.buildColNames(gridConfig, gridConfigList);
            gridConfig.append(",");
            //构建列模型
            treeService.buildColModel(gridConfig, gridConfigList, keyRuleConfigRecord.getStr("keyCol"));
        }
        gridConfig.append("}");
        renderJson(JsonKit.toJson(gridConfig.toString()));
    }

    public void getGridData() {
        String dtoName = getPara("dtoName");
        String domain = getPara("domain");
        List<Record> gridData = boService.getDoDataList(dtoName, domain);
        Map<String, Object> gridDataMap = new HashMap<String, Object>();
        if("tree".equals(getPara("condition"))) {
            for(Record r : gridData){
                if("0".equals(r.getStr("level")))
                    r.set("expanded", true);
                else
                    r.set("expanded", false);
            }
            gridDataMap.put("gridData", gridData);
        }else{
            gridData = boService.getDoDataList(dtoName, domain);
        }
        renderJson(gridDataMap);
    }
}
