package edu.dsideal.campus.framework.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IMenuService;
import edu.dsideal.campus.framework.service.ISystemConfigService;

/**
 * Created by dsideal-lee on 2016/10/9.
 */
@Before(IocInterceptor.class)
public class WelcomeController extends Controller{
    @Inject.BY_NAME
    IMenuService menuService;
    @Inject.BY_NAME
    ISystemConfigService systemConfigService;

    public void index(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 获取登录用户的角色权限
        String loginRoles = loginUserObj.getRoles();
        // 通过domain获取用户的配置信息ID
        String configId = loginUserObj.getDomain();
        // 通过configId找到登录用户的系统配置信息
        setAttr("systemConfigInfo",systemConfigService.findSystemConfigInfo(configId));
        // 获取通过角色查询到的菜单
        setAttr("menuList", menuService.findRoleMenuList(loginRoles));
        setAttr("loginUserObj",loginUserObj);
        // 用户头像缩略图路径thumbnailPath
        setAttr("thumbnailPath",getSessionAttr("thumbnailPath"));
        setAttr("jsPath", "/resouce/js/welcome/welcome.js");
        render("welcome/welcome.html");
    }

    public void mainPage(){
        setAttr("jsPath", "/resouce/js/welcome/main.js");
        render("welcome/main_page.html");
    }
}
