package edu.dsideal.campus.framework.controller;

import edu.dsideal.campus.framework.buss.FormBuss;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.framework.base.BaseTable;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IBoService;
import edu.dsideal.campus.framework.service.IButtonService;
import edu.dsideal.campus.framework.service.IKeyService;
import edu.dsideal.campus.framework.service.ITreeService;
import edu.dsideal.campus.interceptor.InitInterceptor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsideal-lee on 2016/11/18.
 */
@Before(IocInterceptor.class)
public class TreeController extends Controller {
    @Inject.BY_NAME
    ITreeService treeService;
    @Inject.BY_NAME
    IBoService boService;
    @Inject.BY_NAME
    IKeyService keyService;
    @Inject.BY_NAME
    IButtonService btnService;

    private LoginUserObj getLoginUserObj() {
        return getSessionAttr("loginUserObj");
    }

    @Before(InitInterceptor.class)
    public void index() {
        //得到按钮配置信息
        LoginUserObj loginUserObj = getLoginUserObj();
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        //得到列配置信息
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //列配置信息
        StringBuilder columns = new StringBuilder("[");
        columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        for (Record r : boConfList) {
            columns.append("{\"field\":\"").append(r.getStr("colName"))
                    .append("\",\"title\":\"").append(r.getStr("showName"));
            //根据字段类型添加字段的值
            columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
            //判断列的类型r.getStr("edittype") text select checkbox radio ;
        }
        columns.deleteCharAt(columns.length() - 1).append("]");
        //列数据
        setAttr("columns", columns.toString());
        render("template/tree_tpl.html");
    }

    /**
     * 初始化普通树
     */
    public void initTree() {
        renderJson(treeService.getTreeData(getParaMap(), getLoginUserObj()));
    }

    /**
     * 查询列表数据
     */
    public void queryGridData() {
        renderJson(boService.getDoByPage(getParaMap(), getLoginUserObj()));
    }

    /**
     * 树新增结点方法
     *
     * @throws Exception
     */
    public void addNode() throws Exception {
        BaseTable model = BaseTable.createInstance(this, "edu.dsideal.campus.framework");
        String[] key = keyService.getKey(getParaMap());
        model.set("nodeKey", key[1]);
        Record node = boService.saveTreeNode(model);
        node.set("newTreeId", key);
        node.set("msg", ConstStatic.JSON_MSG_SUCCESS);
        renderJson(node);
    }

    /**
     * 删除树结点方法
     */
    public void delNode() throws Exception {
        boolean flag = boService.delTreeNodeById(getParaMap());
        if (flag) {
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_SUCCESS));
            return;
        }
        renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_FAIL));
    }

    /**
     * 初始化权限树方法
     */
    public void initRoleTree() {
        //根据不同的参数初始化不同的service
        Map<String, String[]> paraMap = initBeanByTreeId();
        renderJson(treeService.initRoleTree(paraMap, getLoginUserObj()));
    }

    /**
     * 根据不同的树ID构造相应的树构造所需参数
     *
     * @return paraMap 参数map
     */
    private Map<String, String[]> initBeanByTreeId() {
        String[] dtoName = new String[1];
        dtoName[0] = getDtoNameByPara(getPara("paraTreeId"));
        Map<String, String[]> paraMap = new HashMap<>();
        paraMap.putAll(getParaMap());
        paraMap.put("dtoName", dtoName);
        return paraMap;
    }

    /**
     * 不同的树对应不同的表结构
     */
    private String getDtoNameByPara(String treeId) {
        String dtoName = null;
        switch (treeId) {
            case "js_tree_t1":
                dtoName = "t_dept_org";
                break;
            case "js_tree_t2":
                dtoName = "t_sys_menu";
                break;
            case "js_tree_t3":
                dtoName = "t_sys_menubtn";
                break;
            case "js_tree_t4":
                dtoName = "t_sys_boconfig";
                break;
            case "js_tree_t5":
                dtoName = "t_dept_org";
                break;
            case "js_tree_t6":
                dtoName = "t_dept_org";
                break;
            case "js_tree_childrole":
                dtoName = "t_sys_role";
                break;
            default:
                break;
        }
        return dtoName;
    }

    /**
     * 保存权限数据
     */
    public void saveRoleData() {
        Map<String, String[]> paraMap = initBeanByTreeId();
        boolean flag = treeService.saveTreeRole(paraMap, getLoginUserObj());
        if (flag) {
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_SUCCESS));
            return;
        } else {
            renderJson(CommonTools.getMsgJson(flag, ConstStatic.JSON_MSG_FAIL));
            return;
        }
    }

    /**
     * treegrid通用初始化方法
     */
    @Before(InitInterceptor.class)
    public void initTreeGrid() {
        LoginUserObj loginUserObj = getLoginUserObj();
        //得到按钮配置信息
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //设置查询条件
        List<Record> searchList = new ArrayList<Record>();
        //构造toolbarBtn
        for (Record r : boConfList) {
            if ("true".equals(r.getStr("search"))) {
                searchList.add(r);
            }
        }
        //搜索栏
        setAttr("searchList", searchList);
        //下拉字典
        setAttr("dict", boService.getModelDict(getPara("dtoName"), loginUserObj.getDomain()));
        render("template/jqgrid_treegrid.html");
    }

    /**
     * 通用增加同级，增加子级树结点和修改结点弹出表单的方法
     */
    public void getTreeNodeDetailForm() {
        String paraOpt = getPara("paraOpt");
        LoginUserObj loginUserObj = getLoginUserObj();
        Record formData = new Record();
        //增加同级
        if ("addPeer".equals(paraOpt)) {
            convert2TreeNode(formData, getPara("level"));
            //增加子级
        } else if ("addChild".equals(paraOpt)) {
            convert2TreeNode(formData, new BigDecimal(getPara("level")).add(BigDecimal.ONE).toString());
            //编辑
        } else if ("edit".equals(paraOpt)) {
            formData = boService.getDoDataById(getParaMap(), loginUserObj);
        }
        setAttr("formData", JsonKit.toJson(formData));
        String dtoName = getPara("dtoName");
        FormBuss.showFormBuss(this, boService, dtoName, loginUserObj);
        render("template/form_tpl.html");
    }

    private void convert2TreeNode(Record formData, String level) {
        formData.set("level", level);
        formData.set("pId", getPara("pId"));
        formData.set("isLeaf", "true");
        formData.set("expanded", "false");
    }

    @Before(InitInterceptor.class)
    public void eidtTree() {
        //得到按钮配置信息
        LoginUserObj loginUserObj = getLoginUserObj();
        List<Record> btnList = btnService.getBtns(getParaMap(), loginUserObj);
        //设置按钮
        setAttr("btnList", btnList);
        //得到列配置信息
        List<Record> boConfList = boService.getBoConfig(getPara("dtoName"), loginUserObj);
        //列配置信息
        StringBuilder columns = new StringBuilder("[");
        columns.append("{\"checkbox\":true, \"field\":\"state\"},");
        for (Record r : boConfList) {
            columns.append("{\"field\":\"").append(r.getStr("colName"))
                    .append("\",\"title\":\"").append(r.getStr("showName"));
            //根据字段类型添加字段的值
            columns.append("\",\"visible\":").append(r.getStr("hidden")).append("},");
            //判断列的类型r.getStr("edittype") text select checkbox radio ;
        }
        columns.deleteCharAt(columns.length() - 1).append("]");
        //列数据
        setAttr("columns", columns.toString());
        render("template/etree_grid_tpl.html");
    }
}
