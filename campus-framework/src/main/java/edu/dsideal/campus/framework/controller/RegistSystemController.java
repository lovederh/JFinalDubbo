package edu.dsideal.campus.framework.controller;

import com.jfinal.core.Controller;
import edu.dsideal.campus.common.Authorization;

/**
 * Created by Lee on 2016/12/31.
 */
public class RegistSystemController extends Controller{
    public void index(){
        String encodedData = Authorization.getAuthorizationCode();
        setAttr("encodeData", encodedData);
        render("regist/registSystem.html");
    }
}
