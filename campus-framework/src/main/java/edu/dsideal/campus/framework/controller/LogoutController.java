package edu.dsideal.campus.framework.controller;

import com.jfinal.core.Controller;

/**安全退出
 * 2016/11/4 0004
 */
public class LogoutController extends Controller {
    public void index(){
        this.removeSessionAttr("loginUserObj");
        this.removeSessionAttr("thumbnailPath");
        this.removeSessionAttr("domain");
        this.removeCookie("loginId");

        renderHtml("<script>parent.window.location.href='/login';</script>");
    }
}
