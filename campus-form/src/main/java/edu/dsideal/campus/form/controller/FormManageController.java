package edu.dsideal.campus.form.controller;


import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.*;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.framework.service.IFormManageService;

import javax.xml.ws.Action;
import java.util.List;

/**
 * 自定义表单管理Controller
 * Created by Lu on 2017/3/2.
 */
@Before(IocInterceptor.class)
public class FormManageController extends Controller {
    /** 自定义表单管理Service */
    @Inject.BY_NAME
    IFormManageService formManageService;

    /**
     * 打开自定义表单管理页面
     */
    public void openFormManage(){
        render("queryForms.html");
    }

    /**
     * 查询自定义表单管理列表数据
     */
    public void queryFormList(){
        int pageSize = Integer.MAX_VALUE;
        int pageNumber = 1;
        String title = getPara("searchTitle");
        if (getPara("pageSize") != null){
            pageSize = Integer.parseInt(getPara("pageSize"));
        }
        if (getPara("pageNumber") != null){
            pageNumber = Integer.parseInt(getPara("pageNumber"));
        }
        Page<Record> results = formManageService.queryForms(pageNumber, pageSize, title);
        // 查询结果返回Json
        renderJson(results);
    }

    /**
     * 自定义表单列表中，其中某一条数据的“编辑”查询
     */
    public void queryFormForUpd(){
        String id = getPara("id");

        renderText("");
//        setAttr("order", order);
//        render("deal/viewTaskTab.html");
    }

    /**
     * 自定义表单列表中，其中某一条数据的“预览”查询
     */
    public void queryFormForPre(){
        String id = getPara("id");

        renderText("");
    }


    /**
     * 保存自定义表单
     */
    public void saveForm(){
//        String fields
    }

    /**
     * 查询自定义表单
     */
    public void queryForm(){
        render("form_builder.html");
    }
}
