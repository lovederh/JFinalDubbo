package edu.dsideal.campus.form.config;

import com.jfinal.config.Routes;
import edu.dsideal.campus.form.controller.FormManageController;

/**
 * Created by Lu on 2017/3/3.
 */
public class FormRoutes extends Routes {
    @Override
    public void config() {
        add("/formManage", FormManageController.class, "WEB-INF/view/form");
    }
}
