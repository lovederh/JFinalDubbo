define(function (require) {
    var winTop = window.top;
    var $ = require('jquery'),
        bootstrap = require('bootstrap');


    require('jqSlimscroll');
    //
    require('mPageInfo');
    //

    var $notice = $('#js_modNotice'),
        $message = $('#js_modMessage'),
        $recentFile = $('#js_modRecentFile');

    $notice.find('.jc-mod-info-ct ').slimScroll({
        height: '690',
        railOpacity: 0.9,
        alwaysVisible: false
    });
    $message.find('.jc-mod-info-ct ').slimScroll({
        height: '690',
        railOpacity: 0.9,
        alwaysVisible: false
    });
    $recentFile.find('.jc-mod-info-ct ').slimScroll({
        height: '690',
        railOpacity: 0.9,
        alwaysVisible: false
    });

});


