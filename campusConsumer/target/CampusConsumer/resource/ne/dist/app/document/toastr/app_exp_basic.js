define(function (require) {
    'use strict';
    require('mConfig');

    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    /**
     * 页面级 页面参数对象
     * @namespace
     *
     *@param {Object}
     */
    var nePageArgum = {};


    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {

        /**
         * 全局页面事件对象
         * @private
         **/
        event : function () {
            Nenu.context.event = {

                ease  : function () {
                    Nenu.toastr.success('实例1!');
                },
                ease1 : function () {


                    Nenu.toastr.success(
                        {
                            msg     : '实例2文本文本文本'
                            , title : '实例2标题!'
                            ,
                            show    : function () {
                                console.log('显示');
                            },
                            hide    : function () {

                            }
                        });


                },

                ease2 : function () {
                    Nenu.toastr.success('实例3!');
                }
                ,
                ease3 : function () {
                    Nenu.toastr.success('实例4!', true);
                }
            };
        }


        ,
        /**
         * 业务逻辑 函数
         * @private
         **/
        business : function () {
            Nenu.toastr.options({
                show : function () {
                    console.log('全局显示');
                },
                hide : function () {
                    console.log('全局隐藏');
                }
            });
        }
        ,
        /**
         * 初始化
         * @private
         **/
        init     : function () {
            this.event();
            this.business();

        }
    };

    nePageLogic.init();

});