define ( function ( require ) {
    var $ = require ( 'jquery' );

    //加载组件
    require ( 'nvUEditor' );

    //在编辑器外面套一层高度百分百的容器
    var $neFit = $ ( '.ne-fit' );

    //当前编辑器的toobar
    var $ueToolbar;
    //当前编辑器的bootom
    var $ueBottom;

    //定义获取最大高度的方法
    var getMaxHeight = function () {

        console.log ( $neFit.height () );
        console.log ( $ueToolbar.height () );
        console.log ( $ueBottom.height () );

        return $neFit.height () - $ueToolbar.height () - $ueBottom.height ();
    };

    //初始化
    var ue = UE.getEditor ( 'editor' );

    //先隐藏,为了不让编辑器设置高度的时候有个停顿现象
    $neFit.hide ();

    ue.ready ( function () {
        //加载完毕,获取元素
        $ueToolbar = $ ( '.edui-editor-toolbarbox' , $neFit );
        $ueBottom = $ ( '.edui-editor-bottomContainer' , $neFit );

        //显示
        $neFit.show ();

        //设置高度
        ue.setHeight ( getMaxHeight () );

    } );

    //当窗口大小变动时,重新设置高度
    var timer;
    $ ( window ).on ( 'resize.ueditorHeight' , function () {
        if ( timer ) {
            clearTimeout ( timer );
        }
        timer = setTimeout ( function () {
            ue.setHeight ( getMaxHeight () );
        } , 200 );

    } );

} );