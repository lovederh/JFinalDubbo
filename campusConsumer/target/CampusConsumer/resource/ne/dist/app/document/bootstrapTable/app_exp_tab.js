define(function (require) {
    var $ = require('jquery');
    require('bootstrap');
    require('jqBootstrapTable');

    var $grid = $('#js_grid');
    var jsonPath = Nenu.CONSTANT.PATH.JSON;

  /*  var jottedTools = require('md');*/


    $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
    $(window).on('resize.neTableHt', function () {
        setTimeout(function () {
            $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
        }, 200);
    });


  /*  //初始化
    jottedTools.init({
            pane: 'html',
            height: '100%',
            runScripts: false,
            prefix: 'bootstrapTable/',
            list: [
                {
                    el: 'js_jotted',
                    tpl: {
                        html: 'exp_basic',
                        css: 'exp_basic_css',
                        js: 'exp_basic_js'
                    }
                }
            ]
        }
    );*/

    $grid.neTable({

        //nenu 本系统定义的参数对象
        nenu: {

            query: '#js_grid_search_form',

            toobarBtn: {

                search: {
                    switch: true
                },

                add: {
                    switch: true,
                    callBack: function () {
                        console.log('添加按钮');
                    }
                },

                delete: {
                    switch: true,
                    callBack: function (e, arrySelectDataByID) {
                        console.log('删除按钮');
                    },
                    checkBack: function (arrySelectDataByID) {

                    }
                },

                edit: {
                    switch: true,
                    callBack: function () {
                        console.log('修改按钮');
                    }
                }

            },
            rowBtn: {
                list: [
                    {
                        title: '添加',
                        text: '添加',
                        icon: 'glyphicon glyphicon-plus',
                        cls: 'add',
                        callBack: function (e, value, row, index) {
                            console.log('添加');
                        }
                    },
                    {
                        title: '修改按钮喔!',
                        text: '修改',
                        icon: 'glyphicon glyphicon-pencil',
                        cls: 'edit',
                        callBack: function (e, value, row, index) {
                            console.log('修改');
                        }
                    }
                ]
            }

        },

        table: {

            url:jsonPath+ "bootstrapTable/base.json", //ajax url

            columns: [
                {
                    checkbox: true,
                    field: 'state'
                }, {
                    field: 'id',
                    title: 'ID'

                }, {
                    field: 'name',
                    title: '名称'

                }, {
                    field: 'price',
                    title: '价格'
                }
            ]
        }

    });

});