define(function (require) {
    require('mConfig');
    //校验组件
    require('jqValidate');

    //城市三级联动组件
    require('jqCitys');

    //日期组件
    require('neDate');
    //通知组件
    require('neToastr');


    //全局JSON
    var gJSON = require('mJson');

    //获取dom
    var $form = $('#js_student_info_form'),
        $nation = $('#js_student_info_form_select_nation'),
        $citys = $('#js_student_info_form_citys_citys'),
        $location = $('#js_student_info_form_citys_location');


    var fnInitCitys = function () {
        $citys.citys({ dataUrl : gJSON.citys, province : '吉林省', city : '长春市', area : '南关区' });
        $location.citys({ dataUrl : gJSON.citys, province : '吉林省', city : '长春市', area : '南关区' });
    }

    //民族
    $nation.html($.map(gJSON.nation, function (n) {
        return '<option value="' + n.id + '">' + n.name + '</option>';
    }).join(''));

    //三级联动
    fnInitCitys();

    //日期控件
    Nenu.date.default({ elem : '#js_student_info_form_date_birthday' });


    //校验
    $form.neValidate({
        ajaxBtn : function () {

            return {
                type : 'get',
                url  : Nenu.CONSTANT.PATH.JSON + 'ajax.json',

                success : function (data) {
                    Nenu.toastr.success(data.success);
                }
            }
        }
    });

    //重置
    $.neOn({
        sel   : '#js_student_info_form_btn_reset',
        eFunc : function (e) {
            $form.neResetForm();
            fnInitCitys();
        }
    });

});