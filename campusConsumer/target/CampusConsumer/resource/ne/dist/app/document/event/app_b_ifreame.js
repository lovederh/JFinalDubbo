/**
 * @file  b_ifreame.html 入口文件
 * @author 冯泽民
 *
 */

define(function (require) {
    'use strict';

    require('mConfig');
    //加载event核心文件
    require('neEvent');

    require('jqSlimscroll');

    require('jqLayout');

    /**
     * 页面级 页面参数对象
     * @namespace
     *
     *@param {Object} $body  jq-body 对象
     *@param {Object} $scroll  jq-scroll 对象 需要添加滚动条的dom对象
     * @param {Object} data  父页面传递过来的数据对象
     */
    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        option  : Nenu.open.data.child

    };

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {

        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        }
        ,

        /**
         * 布局插件
         * @private
         **/
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },

        /**
         * 全局页面事件对象
         * @private
         **/

        event : function () {
            Nenu.context.event = {
                fnOpen  : {
                    url    : 'document/event/b_ifreame_three.html',
                    width  : '1200px',
                    height : '700px',
                    neData : {
                        aaa : '第二层父亲'
                    }
                },
                fnSure  : function () {
                    alert('确定');
                },
                fnReset : function () {
                    alert('重置');
                },
                fnClose : function () {

                    nePageArgum.option.setParentData({
                        aaa : '第二层给第一层传值'
                    });

                    nePageArgum.option.close();
                }
            };
        },

        /**
         * 子页面关闭回调函数
         * @private
         **/
        end : function () {
            Nenu.open.end = function () {
                console.log(Nenu.open.data.parent);
            };
        },

        /**
         * 业务逻辑 函数
         * @private
         **/
        business : function () {
            // 父页面传递的参数
            console.log(nePageArgum.option);
        }

        ,
        /**
         * 初始化
         * @private
         **/
        init : function () {
            this.pLayout();
            this.event();
            this.end();
            this.business();
        }
    };

    nePageLogic.init();
  


});