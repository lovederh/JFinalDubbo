/**
 * 全局通知提示组件
 */

define(function (require) {
    'use strict';
    require('jqLayerDate');

    var format = {
        ymd    : 'YYYY-MM-DD',
        ymdhms : 'YYYY-MM-DD hh:mm:ss',
        hms    : 'hh:mm:ss'
    };

    var defaults = {
        elem    : '#js_student_info_form_date_birthday',
        format  : format['ymd'],
        isclear : false,
        min     : laydate.now(),
        istime  : true,
        start   : laydate.now(),
        choose  : function (dates) {
            $(this.elem).focus();
        }
    };

    Nenu.date = {
        /**
         * 默认格式
         *
         * @param option   layerDate参数
         * @param str      日期格式化字符串 默认 ymd
         */
        default : function (option, str) {
            option.format = format[str || 'ymd'];
            laydate($.extend({}, defaults, option));
        }
    }
});



