/*TMODJS:{"version":2,"md5":"82859373d7f4c1f2e2781377ed868bdb"}*/
template('validate/ajax_js',function($data,$filename
/**/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$escape=$utils.$escape,$out='';$out+='\r\n    var $sub = $(\'#js_form\');\r\n\r\n    $sub.neValidate({\r\n        rules: {\r\n            username: {\r\n                required: true,\r\n                minlength: 6,\r\n                isSole: {}\r\n            }\r\n        },\r\n        messages: {\r\n            username: {\r\n                required: "请输入用户名",\r\n                minlength: "用户名为6个字符"\r\n            }\r\n        }\r\n        ,\r\n        ajaxBtn: {\r\n           type:\'get\',\r\n            url: \'';
$out+=$escape($data.filePath);
$out+='/json/ajax.json\',\r\n            success: function (data) {\r\n                  $(\'#js_result\').html(data.success);\r\n            }\r\n        }\r\n\r\n    });\r\n\r\n\r\n\r\n\r\n\r\n\r\n';
return new String($out);
});