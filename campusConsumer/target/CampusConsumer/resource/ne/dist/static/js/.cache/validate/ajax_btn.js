/*TMODJS:{"version":3,"md5":"3b108864c8220169137c59b123125f0f"}*/
template('validate/ajax_btn',function($data,$filename
/**/) {
'use strict';var $utils=this,$helpers=$utils.$helpers,$escape=$utils.$escape,$out='';$out+='<!--需要的样式-->\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/css/bootstrap.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/css/font-awesome.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/css/font-nenu.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/css/style.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/css/style_expand.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/layerDate/1.1/need/laydate.css">\r\n<link rel="stylesheet" href="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/layerDate/1.1/skins/default/laydate.css">\r\n\r\n\r\n<!--表单 -->\r\n\r\n<div class="alert alert-info">\r\n    通过ajax校验用户名是否唯一，且使用 jq.form ajaxForm 方法提交\r\n\r\n    <div id="js_result" style="font-weight: bold ; font-size: 20px;  color: #3c763d;"></div>\r\n</div>\r\n\r\n<form class="ne-form form-horizontal" id="js_form">\r\n\r\n    <div class="form-group">\r\n        <div class="jc-validate-box  has-feedback">\r\n            <label class=" col-xs-2 control-label">用户名：</label>\r\n            <div class="js-validate-choice  col-xs-10 ">\r\n                <input class="form-control" name="username" type="text">\r\n                <div class="jc-message  help-block">用户名必须唯一</div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div class="form-group has-feedback">\r\n        <div class="col-xs-offset-2">\r\n            <!--如果使用自定义btn 则在ajaxBtn中使用btn属性: 否则 请使用 submit按钮提交-->\r\n            <button class="btn btn-primary" id="js_form_submit_btn" type="button">提交</button>\r\n        </div>\r\n    </div>\r\n\r\n</form>\r\n<!--表单  -->\r\n\r\n<!--需要的js-->\r\n<!--需要的js-->\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/ba_nenu.js"></script>\r\n\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/2.1.4/jquery.min.js"></script>\r\n\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/form/3.51/jquery.form.js"></script>\r\n\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/validate/1.15.0/jquery.validate.js"></script>\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/validate/1.15.0/messages_zh.min.js"></script>\r\n<!--<script src="';
$out+=$escape($data.filePath);
$out+='/static/js/lib/jquery/plugin/validate/1.15.0/additional-methods.js"></script>-->\r\n<script src="';
$out+=$escape($data.filePath);
$out+='static/js/lib/jquery/plugin/validate/1.15.0/jquery.validate.extend.js"></script>';
return new String($out);
});