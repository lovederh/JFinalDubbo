define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    //使用按钮事件组件
    var nePageLogic = {
        event: function () {
            Nenu.context.event = {

            };
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {

            };
        },
        //初始化
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();
});