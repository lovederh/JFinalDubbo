define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //日期组件
    require('neDate');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['标题','申请类型','发起时间','发起人','状态','操作','processId','orderId','taskInfoId'];
    var colModel = [
        {name: 'orderTitle', index: 'orderTitle', width: 200, align: "center"},
        {name: 'processDisplay', index: 'processDisplay', width: 100, align: "center"},
        {name: 'createTime', index: 'createTime', width: 100, align: "center"},
        {name: 'creatorDisplay', index: 'creatorDisplay', width: 100, align: "center"},
        {name: 'orderState', index: 'orderState', width: 100, align: "center", formatter: function (cellValue, options, rowObject) {
            return util_displayGridOrderState(cellValue, true);
        }},
        {name: 'operate', width: 200, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn;
            if(-1 == rowObject.orderState){
                //当前为草稿状态, 则按钮为草稿派发/删除草稿
                operateBtn =
                    '<button class="btn btn-primary btn-outline btn-sm" ' +
                    '   onclick="draftStart(\''+ rowObject.processId +'\',\''+ rowObject.taskInfoId +'\')" >' +
                    '<i class="glyphicon glyphicon-send"></i>&nbsp;提交</button>' +
                    '<span class="ne-nbsp"></span>' +
                    '<button class="btn btn-primary btn-outline btn-sm deleteDraft" ' +
                    '   taskInfoId="'+ rowObject.taskInfoId +'" >' +
                    '<i class="glyphicon glyphicon-remove-circle"></i>&nbsp;删除草稿</button>';
            }else{
                operateBtn =
                    '<button class="btn btn-primary btn-outline btn-sm" ' +
                    '   onclick="util_viewOrder(\''+ rowObject.processId +'\',\''+ rowObject.orderId +'\')" >' +
                    '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;查看</button>';
            }
            return operateBtn;
        }},
        {name: 'processId', hidden: true},
        {name: 'orderId', hidden: true},
        {name: 'taskInfoId', hidden: true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: '/flowQuery/myStartOrdersData',
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "10",
                rowList: [5,10,20,50,100],
                pager: '#js_gridPager',
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: false,
                jsonReader: {
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                searchGridDataBtn: searchGridDataFunc,
                startNewOrderBtn: startNewOrderFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();//当前页列表刷新
            };
        },
        init: function () {
            Nenu.date.default({elem:'#searchCreateTimeStart',istime:false});
            Nenu.date.default({elem:'#searchCreateTimeEnd',istime:false});
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    //Grid行删除草稿按钮绑定操作
    $(document).on("click", '.deleteDraft', function(){
        var taskInfoId = $(this).attr("taskInfoId");
        util_deleteDraft(taskInfoId, reloadGrid);
    });

    //查询按钮
    function searchGridDataFunc(){
        //确认时间框选择的值是否有效
        var searchCreateTimeStart = $('#searchCreateTimeStart').val();
        var searchCreateTimeEnd = $('#searchCreateTimeEnd').val();
        if(searchCreateTimeEnd < searchCreateTimeStart){
            Nenu.toastr.warning('发起时间查询: 截至时间不能在起始时间之前!');
            return;
        }
        $grid.jqGrid('setGridParam',{
            //发送查询条数据
            postData: $('#js_gridSearchBar').serializeArray(),
            page: 1
        }).trigger("reloadGrid");
    }

    //发起新流程申请按钮
    function startNewOrderFunc(){
        return {
            title: '新申请',
            url: '/flowQuery/iCanStartFlowsData?loadImgFlag=true',
            height: '1500px',
            width: '2000px'
        };
    }

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});

/**
 * 草稿派发
 */
function draftStart(processId, taskInfoId){
    Nenu.event.ifreame({
        url : '/flowDeal/instanceProcessTab?processId=' + processId + '&taskInfoId=' + taskInfoId,
        height: '1500px',
        width: '2000px'
    });
}
