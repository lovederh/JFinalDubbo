/**
 * 流程处理工具Js
 * zhaoyou 2017.1.9
 */

/**
 * 传入dom元素ID, 将其渲染为查询条内使用的时间控件
 * @param elemDomId 渲染为时间控件的dom元素ID
 * @returns {{elem: string, isclear: boolean, init: boolean, istime: boolean}}
 */
function util_initSearchDateDom(elemDomId){
    return {
        elem: '#' + elemDomId,
        isclear: true,//显示清空按钮
        init: false,//初始化时不赋值
        istime: false//关闭时间选择
    };
}

/**
 * 通用的格式化grid中工单状态字段的方法, 展示不同颜色
 * @param orderState 工单状态值
 * @param coloFlag 是否启用颜色标识
 */
function util_displayGridOrderState(orderState, coloFlag){
    var orderStateDisplay = "";
    if(0 == orderState){
        orderStateDisplay = "<label style='color:green;'>已归档</label>";
    }else if(2 == orderState){
        orderStateDisplay = "<label style='color:blue;'>强制归档</label>";
    }else if(1 == orderState){
        orderStateDisplay = "<label style='color:orange;'>流转中</label>";
    }else if(-1 == orderState){
        orderStateDisplay = "<label style='color:red;'>未提交</label>";
    }
    if(! coloFlag){
        //如果不启用颜色标识, 则利用正则表达式匹配替换为默认颜色
        orderStateDisplay = orderStateDisplay.replace(/color:([\s\S]*);/, "color:#676A6C;");
    }
    return orderStateDisplay;
}

/**
 * 角色/教师双选器选择后, 父页面数据对象处理
 * 根据选择的类型, 依次遍历生成字符串, 用于前端展示
 * @param actorsData 已经选好的所有数据
 */
function util_initPickActorsObj(actorsData){
    //遍历当前所选处理者, 进行各变量重新赋值
    var teacherDisplay = [];
    var teacherValue = [];
    var roleDisplay = [];
    var roleValue = [];
    $.each(actorsData, function(index, item){
        if('1' == item.actorType){
            teacherDisplay.push(item.actorDisplay);
            teacherValue.push(item.actorValue);
        }else if('role' == item.actorType){
            roleDisplay.push(item.actorDisplay);
            roleValue.push(item.actorValue);
        }
    });
    return {
        role: {
            actorValueStr: roleValue.join(','),
            actorDisplayStr: roleDisplay.join(',')
        },
        '1': {
            actorValueStr: teacherValue.join(','),
            actorDisplayStr: teacherDisplay.join(',')
        }
    }
}

/**
 * 通用的格式化业务标识字段的方法
 * @param businessType 业务标识值(sys:系统默认; user:用户定制)
 */
function util_displayBusinessType(businessType){
    if('sys' == businessType){
        return "系统默认";
    }else{
        return "用户定制";
    }
}

/**
 * 打开查看工单弹窗
 * @param processId
 * @param orderId
 * @param isCcOrderFlag 是否为抄送给我的工单标识
 */
function util_viewOrder(processId, orderId, isCcOrderFlag){
    Nenu.event.ifreame({
        url : '/flowDeal/toViewTaskTab?dealFlag=false&isCcOrder=' + isCcOrderFlag + '&processId=' + processId + '&orderId=' + orderId,
        height: '1500px',
        width: '2000px'
    });
}

/**
 * 打开通用的处理工单弹窗
 * @param processId
 * @param orderId
 * @param taskId
 * @param taskName
 */
function util_dealTask(processId, orderId, taskId){
    Nenu.event.ifreame({
        url : '/flowDeal/toViewTaskTab?dealFlag=true&processId=' + processId + '&orderId=' + orderId + '&taskId=' + taskId,
        height: '1500px',
        width: '2000px'
    });
}


/**
 * 批量标记请求
 * @param orderIds 要标记的工单ID, 以逗号分隔
 * @param callBackFunc 成功回调方法
 */
function util_ccOrdersRead(orderIds, callBackFunc){
    //发送批量标记请求
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/flowDeal/ccOrdersRead',
        data: {
            orderIds : orderIds
        },
        success:function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                util_alertOkDone(jsonData.msg, callBackFunc);
            }else{
                //操作失败后弹出错误提示窗, 执行回调方法
                Nenu.toastr.error(jsonData.msg);
            }
        }
    });
}

/**
 * 强制归档工单
 * @param orderId
 * @param callBackFunc
 */
function util_terminateOrder(orderId, callBackFunc){
    Nenu.event.confirm({
        title : '是否强制归档工单，该操作不可恢复?',
        execute : function (close){
            close();
            // 关闭后发送强制归档请求
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/flowDeal/terminateOrder',
                data: {
                    orderId : orderId
                },
                success:function(jsonData){
                    util_alertOkDone(jsonData.msg, callBackFunc);
                }
            });
        }
    });
}

/**
 * 工单删除, 只删除一个工单, 不批量
 * @param orderId
 * @param callBackFunc
 */
function util_deletePhysicalOrder(orderId, callBackFunc){
    Nenu.event.confirm({
        title : '是否确认删除工单，该操作不可恢复?',
        execute : function (close){
            close();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/flowDeal/deletePhysicalOrder',
                data: {
                    orderId : orderId
                },
                success:function(jsonData){
                    var status = jsonData.status;
                    var msg = jsonData.msg;
                    if('200' == status){
                        util_alertOkDone(jsonData.msg, callBackFunc);
                    }else{
                        //操作失败后弹出错误提示窗, 执行回调方法
                        Nenu.toastr.error(jsonData.msg);
                    }
                }
            });
        }
    });
}

/**
 * 草稿删除方法
 * @param taskInfoId 草稿对应的taskInfoId
 * @param callBackFunc
 */
function util_deleteDraft(taskInfoId, callBackFunc){
    Nenu.event.confirm({
        title : '是否确认删除，删除后将无法恢复?',
        execute : function (close){
            close();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/flowDeal/deleteDraft',
                data: {
                    taskInfoId : taskInfoId
                },
                success:function(jsonData){
                    var status = jsonData.status;
                    var msg = jsonData.msg;
                    if('200' == status){
                        Nenu.toastr.success(msg);
                        (callBackFunc && typeof(callBackFunc)==="function") && callBackFunc();
                    }else{
                        //操作失败后弹出错误提示窗, 执行回调方法
                        Nenu.toastr.error(jsonData.msg);
                    }
                }
            });
        }
    });
}

/**
 * 流程文件删除, 每次只删除一个文件, 同时删除相应配置, 不批量
 * @param fileResource
 * @param processSetId
 * @param callBackFunc
 */
function util_deleteFlowFile(fileResource, processSetId, callBackFunc) {
    $.ajax({
        type: 'POST',
        url: '/flowSet/deleteFlowFile',
        dataType: 'json',
        async: false,
        data: {
            fileResource : fileResource,
            processSetId : processSetId
        },
        success: function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                Nenu.toastr.success(msg);
                (callBackFunc && typeof(callBackFunc)==="function") && callBackFunc();
            }else{
                Nenu.toastr.error(msg);
            }
        }
    });
}

/**
 * 安装流程的请求, 对于系统流程, 需要传入企业域信息
 * @param fileResource
 * @param processType
 * @param callBackFunc
 */
function util_deployFlow(fileResource, processType, callBackFunc){
    //发送安装请求
    $.ajax({
        type: 'POST',
        url: '/flowSet/deployFlow',
        dataType: 'json',
        async: false,
        data: {
            fileResource : fileResource,
            processType : processType
        },
        success: function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                Nenu.toastr.success(msg);
                (callBackFunc && typeof(callBackFunc)==="function") && callBackFunc();
            }else{
                Nenu.toastr.error(msg);
            }
        }
    });
}

/**
 * 操作成功后, 弹出确定框, 点击确定进行下一步操作
 * @param message
 * @param callBackFunc
 */
function util_alertOkDone(message, callBackFunc){
    //操作完成后弹出提示窗, 并执行回调方法
    Nenu.event.confirm({
        title : message,
        isColse: false,
        execute : function (close){
            close();
            (callBackFunc && typeof(callBackFunc)==="function") && callBackFunc();
        }
    });
}