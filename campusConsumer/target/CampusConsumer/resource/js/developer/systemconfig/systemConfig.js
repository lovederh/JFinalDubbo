define(function (require) {
    require("mConfig");
    require('bootstrap');
    require('jqBootstrapTable');
    //通知组件
    require('neToastr');

    var $grid = $('#configId');
    $grid.neTable({
        table: {
            url: "/systemConfig/queryMembersConfigInfo", //ajax url
            dataType: 'json',
            striped: true,
            idField: "id",  //标识哪个字段为id主键
            columns: [
                {checkbox: true, field: 'state'},
                {field: 'id', name: 'id', title: 'ID'},
                {field: 'webTitle', name: 'webTitle', title: '网站标签标题'},
                {field: 'mainTitle', name: 'mainTitle', title: '版权所有信息'},
                {field: 'loginID', name: 'loginID', title: '用户名'},
                {field: 'companyName', name: 'companyName', title: '企业名称'},
                {field: 'companyAdd', name: 'companyAdd', title: '单位地址'},
                {field: 'tel', name: 'tel', title: '联系电话'},
                {field: 'corporation', name: 'corporation', title: '法人'},
                {field: 'faxNum', name: 'faxNum', title: '传真号'},
                {field: 'emailAdd', name: 'emailAdd', title: '电子邮箱地址'},
                {field: 'isPass', name: 'isPass', title: '是否通过'},
                {field: 'applyTime', name: 'applyTime', title: '申请时间'},
                {field: 'passTime', name: 'passTime', title: '通过时间'}
            ]
        }

    });
});

/**
 * 增加配置信息按钮
 */
function add() {
    //构造虚拟表单,获取表单中的name值
    var formData = $('#addFormId').serializeArray();
    $.ajax({
        type: 'POST',
        url: '/systemConfig/addConfig',
        async: false,
        data: formData,
        success: function (jsonData) {
            var status = jsonData.status;
            var msg = jsonData.msg;
            if ('200' == status) {
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                });
            } else {
                layer.msg(msg, {
                    icon: 5,
                    time: 2000,//2s后自动关闭
                    btn: ['确定']
                });
            }
        }
    })
}

/**
 *  选中编辑行
 */
function editRow() {
    var rows = $('#configId').bootstrapTable('getSelections');
    $('#editRow').removeAttr("href");
    if (rows.length <= 0) {
        alertErrorLayer('请选择至少一条数据!');
    } else if (rows.length > 1) {
        alertErrorLayer('请选择一条数据!');
    } else {
        $('#editRow').attr("href", "systemConfig.html#edit-form");
        $('#id').val(rows[0].id);
        $('#webTitle').val(rows[0].webTitle);
        $('#mainTitle').val(rows[0].mainTitle);
    }
}

/**
 *  编辑保存按钮
 */
function save() {
    //构造虚拟表单,获取表单中的name值
    var formData = $('#editFormId').serializeArray();
    $.ajax({
        type: 'POST',
        url: '/systemConfig/saveConfig',
        async: false,
        data: formData,
        success: function (jsonData) {
            var status = jsonData.status;
            var msg = jsonData.msg;
            if ('200' == status) {
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                });
            } else {
                layer.msg(msg, {
                    icon: 5,
                    time: 2000,//2s后自动关闭
                    btn: ['确定']
                });
            }
        }
    })

}

/**
 * 删除配置信息按钮功能
 */
function delRows() {
    var rows = $('#configId').bootstrapTable('getSelections');
    var ids = new Array();
    for (var i = 0; i < rows.length; i++) {
        ids.push(rows[i].id);
        console.log(rows[i].loginID);
    }
    $.ajax({
        type: 'POST',
        url: '/systemConfig/delConfig?id=' + ids,
        async: false,
        success: function (jsonData) {
            var status = jsonData.status;
            var msg = jsonData.msg;
            if ('200' == status) {
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                }, function () {
                    // 表格刷新
                    $("#configId").bootstrapTable('refresh');
                });
            } else {
                layer.msg(msg, {
                    icon: 5,
                    time: 2000,//2s后自动关闭
                    btn: ['确定']
                });
            }
        }
    })
}

/**
 * 通过按钮功能
 */
function passRows() {
    var rows = $('#configId').bootstrapTable('getSelections');
    if (rows.length <= 0) {
        Nenu.toastr.warning('请选择至少一条数据!', true);
    } else {
        var ids = new Array();
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].id);
        }
        $.ajax({
            type: 'POST',
            url: '/systemConfig/passConfig?id=' + ids + "&" + $.param(formObj),
            async: false,
            success: function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    Nenu.toastr.success(msg);
                    // 表格刷新
                    $("#configId").bootstrapTable('refresh');
                } else {
                    Nenu.toastr.error(msg);
                }
            }
        });
    }
}

/**
 * 不通过按钮功能
 */
function notPassRows() {
    var rows = $('#configId').bootstrapTable('getSelections');
    if (rows.length <= 0) {
        Nenu.toastr.warning('请选择至少一条数据!', true);
    } else {
        var ids = new Array();
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].id);
        }
        $.ajax({
            type: 'POST',
            url: '/systemConfig/notPassConfig?id=' + ids,
            async: false,
            success: function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    Nenu.toastr.success(msg, true);
                    // 表格刷新
                    $("#configId").bootstrapTable('refresh');
                } else {
                    Nenu.toastr.error(msg, true);
                }
            }
        });
    }
}

/**
 * 弹出错误警告框
 */
function alertErrorLayer(msg) {
    layer.msg(msg, {
        icon: 5,
        time: 2000,//2s后自动关闭
        btn: ['确定']
    });
}