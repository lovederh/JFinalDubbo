define(function (require) {
    'use strict';
    require("mConfig");
    require('jqGrid');
    require('jqZtree');
    require('jqSlimscroll');
    require('jqLayout');
    require('neEvent');
    require('neToastr');
    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID', 'loginId', '人员姓名', '联系方式', '部门'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'loginId', hidden: true},
        {name: 'userName', index: 'userName', width: 100, align: "center"},
        {name: 'phone', index: 'phone', width: 100, align: "center"},
        {name: 'orgId', index: 'orgId', width: 100, align: "center"}
    ];

    //第一次进入则加载已经选择过的人员
    var pickUsersStr = "";
    if (pickUsersStr) {
        pickUsersStr = "," + pickUsersStr + ",";
    }

    //第二次进入加载已经选择过的人员
    if (nativePickUsersStr) {
        pickUsersStr = nativePickUsersStr + ",";
    }


    function addHoverDom(treeId, treeNode) {
    }

    var groupUserSelected='';

    var rows;


    // var data = Nenu.open.data.parent;
    // groupUserSelected = data.groupUserSelected;
    // alert(groupUserSelected);

    //alert(groupUserSelected);

    /**
     * 页面级 页面参数对象
     * @namespace
     *@param {Object} $body  jq-body 对象
     *@param {Object} $scroll  jq-scroll 对象 需要添加滚动条的dom对象
     * @param {Object} data  父页面传递过来的数据对象
     */
    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll'),
        option: Nenu.open.data.child
    };

    var nePageLogic = {
        selectTab: "js_tree_selectGroup",
        options: Nenu.open.data.child,
        initTree: function (treeId) {
            if (!treeId) {
                treeId = "js_tree_selectGroup";
                formObj.paraTreeId = treeId;
                nePageLogic.selectTab = treeId;
            }
            if (treeId == 'js_tree_selectGroup') {
                initGroupTree();
            } else {
                initTeacherTree();
            }
        },

        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        fnTable: function () {
            //选择分组左侧人员列表
            $('#js_groupUser_grid').jqGrid({
                //url: '/notice/queryEmpGridData?' + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_groupUserPager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });

            //选择教师左侧人员列表
            $('#js_teacher_grid').jqGrid({
                url: '/notice/queryEmpGridData?' + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_teacherpager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });

            $('#js_selectGrid').jqGrid({
                url: '/notice/querySelectedStaff?pickUsersStr=' + nativePickUsersStr + "&" + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                height: "auto",
                pager: '#js_selectGridpager_grid',//pager在使用树表格时禁用
                autowidth: true,
                rowNum: "10",//显示全部记录
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.method.bind(['jc-click|click.tab|true']);
            var newNum = 1;
            Nenu.context.event = {
                clickTab: function () {
                    var treeId = "js_tree_" + $(this).attr("id");
                    formObj.paraTreeId = treeId;
                    nePageLogic.selectTab = treeId;
                    nePageLogic.initTree(treeId);
                },

                //往右侧选择
                rightSelected: function () {
                    rows = $("#js_teacher_grid").jqGrid('getGridParam', 'selarrrow');
                    $.each(rows, function (i, item) {
                        var rowData = $("#js_teacher_grid").jqGrid('getRowData', rows[i]);
                        if (pickUsersStr.lastIndexOf(rowData.id + ",") < 0) {
                            pickUsersStr += rowData.id + ",";
                            $("#js_selectGrid").jqGrid("addRowData", rows[i], rowData, "last");
                        }
                    });

                    rows = $("#js_groupUser_grid").jqGrid('getGridParam', 'selarrrow');
                    $.each(rows, function (i, item) {
                        var rowData = $("#js_groupUser_grid").jqGrid('getRowData', rows[i]);
                        if (pickUsersStr.lastIndexOf(rowData.id + ",") < 0) {
                            pickUsersStr += rowData.id + ",";
                            $("#js_selectGrid").jqGrid("addRowData", rows[i], rowData, "last");
                        }
                    });

                },

                //往左侧选择
                leftSelected: function () {
                    rows = $("#js_selectGrid").jqGrid("getGridParam", "selarrrow");
                    for (var i = 0; i < rows.length; i++) {
                        var rowData = $("#js_selectGrid").jqGrid('getRowData', rows[i]);
                        pickUsersStr = pickUsersStr.replace(rowData.id + ",", "");
                        $("#js_selectGrid").jqGrid('delRowData', rows[i]);
                    }
                },

                //选择教师方式的搜索查询
                searchTeacherInput: function () {
                    var searchTeacherInput = $('#searchTeacherInput').val();
                    if (!searchTeacherInput) {
                        return;
                    }
                    $('#js_teacher_grid').jqGrid('setGridParam', {
                        //发送查询条数据
                        url: '/notice/queryEmpGridData?searchInput=' + searchTeacherInput,
                        page: 1
                    }).trigger("reloadGrid");
                },

                //右侧已选择人员搜索查询
                searchInputSelected: function () {
                    var searchInputSelected = $('#searchInputSelected').val();
                    $('#js_selectGrid').jqGrid('setGridParam', {
                        //发送查询条数据
                        url: '/notice/queryEmpGridData?searchInput=' + searchInputSelected,
                        page: 1
                    }).trigger("reloadGrid");
                },

                //添加分组
                addGroupNote: function () {
                    // 机构树选择的机构节点
                    var nodes = $.fn.zTree.getZTreeObj('js_tree_selectGroup').getSelectedNodes();
                    var selectNode = nodes[0];
                    if (!nodes.length) {
                        Nenu.toastr.warning("请选择根节点进行添加分组!");
                        return;
                    }
                    if (selectNode.level != '1') {
                        var nodeName = '新增分组' + (newNum++);
                        $.ajax({
                            url: '/notice/addGroup',
                            data: {
                                name: nodeName,
                                pId: selectNode.id
                            },
                            type: "post",
                            async: false,
                            success: function (jsonData) {
                                var newNode = {
                                    id: jsonData.groupId,
                                    pId: selectNode.id,
                                    name: nodeName
                                };
                                var addedNode = $.fn.zTree.getZTreeObj('js_tree_selectGroup').addNodes(selectNode, newNode);
                                $.fn.zTree.getZTreeObj('js_tree_selectGroup').selectNode(addedNode[0]);
                            }
                        });
                    } else {
                        Nenu.toastr.warning("不允许在分组节点下创建新的分组,请选择根节点进行添加分组!");
                        return;
                    }
                },

                groupManage: function () {
                    var nodes = $.fn.zTree.getZTreeObj('js_tree_selectGroup').getSelectedNodes();
                    var selectNode = nodes[0];
                    if (!nodes.length) {
                        Nenu.toastr.warning("请选择子节点进行组员添加和维护!");
                        return;
                    }
                    if (selectNode.id == '0') {
                        Nenu.toastr.warning("请选择子节点进行组员添加和维护!");
                        return;
                    }
                    // alert(selectNode.id)
                    Nenu.event.ifreame({
                        title: '组员维护',
                        url: '/notice/editGroupUser?groupId=' + selectNode.id + '&' + $.param(formObj),
                        height: '1000px',
                        width: '1200px'
                    });
                },

                //确定按钮
                fnSure: function () {
                    nePageArgum.option.setParentData({
                        pickUsersStr: pickUsersStr
                    });
                    nePageArgum.option.close();
                }
            }
        },
        end: function () {
            Nenu.open.end = function () {
                var data = Nenu.open.data.parent;   //子页面往父页面返回的数据
                groupUserSelected = data.groupUserSelected;
            }
        },

        init: function () {
            this.pLayout();
            this.initTree();
            this.fnTable();
            this.event();
            this.end();
        }
    };

    nePageLogic.init();

    function initGroupTree() {
        var $selectGroupTree = $('#js_tree_selectGroup').neTree({
            setting: {
                view: {
                    addHoverDom: addHoverDom,
                    selectedMulti: false
                },
                edit: {
                    enable: true,
                    editNameSelectAll: true,
                    showRenameBtn: showEditOrDelBtn,
                    showRemoveBtn: showEditOrDelBtn,
                    removeTitle: '删除',
                    renameTitle: '重命名'
                },
                async: {
                    enable: true,
                    url: '/notice/initTree?' + $.param(formObj)
                },
                check: {
                    enable: false
                },
                callback: {
                    onRemove: onRemove,//删除节点后触发，用户后台操作
                    onRename: onRename,//编辑后触发，用于操作后台
                    onClick:function (event, treeId, treeNode) {
                        nePageLogic.selectedTreeId = treeNode.id;
                        $('#js_groupUser_grid').jqGrid('setGridParam', {
                            //发送查询条数据
                            url: '/notice/queryGroupUser?groupId=' + nePageLogic.selectedTreeId,
                            page: 1
                        }).trigger("reloadGrid");
                    }

                }
            }
        });
    }

    function initTeacherTree() {
        var $selectTeacherTree = $('#js_tree_selectTeacher').neTree({
            setting: {
                edit: {
                    enable: false
                },
                async: {
                    enable: true,
                    url: '/notice/initTree?' + $.param(formObj)
                },
                check: {
                    enable: false
                },
                callback: {
                    onClick: function (event, treeId, treeNode) {
                        nePageLogic.selectedTreeId = treeNode.id;
                        $('#js_teacher_grid').jqGrid('setGridParam', {
                            //发送查询条数据
                            url: '/notice/queryEmpGridData?orgId=' + nePageLogic.selectedTreeId,
                            page: 1
                        }).trigger("reloadGrid");
                    }
                }
            }
        });
    }

    function onRename(e, treeId, treeNode, isCancel) {
        $.ajax({
            url: '/notice/editGroupNote',
            data: {
                opt: "edit",
                id: treeNode.id,
                name: treeNode.name
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    function onRemove(e, treeId, treeNode) {
        $.ajax({
            url: '/notice/editGroupNote',
            data: {
                opt: "del",
                id: treeNode.id
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    // 判断根节点是否可以编辑
    function showEditOrDelBtn(treeId, treeNode) {
        if (treeNode.isParent) {
            return false;
        }
        return true;
    }


});