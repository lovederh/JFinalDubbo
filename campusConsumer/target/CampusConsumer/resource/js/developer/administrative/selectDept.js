define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqZtree');
    //加载event核心文件
    require('neEvent');

    require('jqBootstrapTable');
    //通知组件
    require('neToastr');
    require('jqSlimscroll');
    require('jqLayout');


    /**
     * 页面级 页面参数对象
     *  @namespace
     *  @param {Object} $body  jq-body 对象
     *  @param {Object} $scroll  jq-scroll 对象 需要添加滚动条的dom对象
     *  @param {Object} data  父页面传递过来的数据对象
     */
    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll'),
        option: window.parent.Nenu.open.data.child

    };

    var nePageLogic = {
        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        fnTable: function () {
            var $tree = $('#js_deptTree').neTree({
                setting: {
                    async: {
                        enable: true,
                        url: '/notice/deptUserTree?' + $.param(formObj)
                    },
                    check: {
                        enable: true
                    },
                    callback: {}
                }
            });


        },
        event: function () {
            Nenu.context.event = {
                searchDeptOrUser: function () {
                    var ztreeObj = $.fn.zTree.getZTreeObj('js_deptTree');
                    var searchInput = $('#searchInput').val();
                    if (!searchInput) {
                        return;
                    }
                    // 存在有效的查询值, 则请求后台
                    $.ajax({
                        type: 'post',
                        url: '/notice/searchDeptAndUser',
                        dataType: 'text',
                        data: {
                            searchInput: searchInput
                        },
                        success: function (dataText) {
                            if (dataText) {
                                // 循环展开
                                var searchDicts = dataText.split(',');
                                $.each(searchDicts, function (i, value) {
                                    // 找到对应节点, 并展开
                                    var searchNode = ztreeObj.getNodeByParam('id', value, null);
                                    ztreeObj.expandNode(searchNode, true, false, false);
                                    ztreeObj.selectNode(searchNode);
                                });
                            }
                        }
                    });
                },

                fnSureDeptUser: function () {
                    var treeObj = $.fn.zTree.getZTreeObj('js_deptTree');
                    var nodes = treeObj.getCheckedNodes(true);

                    if (nodes) {

                        var selectedStaffIds = '';

                        for (var node in nodes) {
                            if (nodes[node].id.substring(0, 3) == 'emp') {

                                var staffId = nodes[node].id;
                                selectedStaffIds += (staffId.substring(staffId.lastIndexOf('_') + 1, staffId.length)) + ',';
                            }
                        }

                        if (selectedStaffIds) {
                            //截掉最后一个多余的逗号
                            selectedStaffIds = selectedStaffIds.substring(0, selectedStaffIds.length - 1);
                        }

                        nePageArgum.option.setParentData({
                            selectedStaffIds: selectedStaffIds
                        });

                        nePageArgum.option.close();
                    }
                }

            }
        },

        end: function () {
        },

        init: function () {
            this.pLayout();
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();
});
