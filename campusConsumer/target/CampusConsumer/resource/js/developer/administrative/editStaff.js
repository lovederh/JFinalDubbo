define(function (require) {
    // 'use strict';
    //加载组件
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //日期组件
    require('neDate');
    //通知组件
    require('neToastr');
    require('jqSlimscroll');
    require('jqLayout');
    require('jqGrid');

    var $grid = $('#js_grid');

    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID', 'loginId', '人员姓名', '联系方式', '部门'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'loginId', hidden: true},
        {name: 'userName', index: 'userName', width: 100, align: "center"},
        {name: 'phone', index: 'phone', width: 100, align: "center"},
        {name: 'orgId', index: 'orgId', width: 100, align: "center"}
    ];

    var pickUsersStr = ',' + nativePickUsersStr + ',';

    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll'),
        option: Nenu.open.data.child
    };
    var nePageLogic = {
        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        fnTable: function () {
            $grid.jqGrid({
                url: '/notice/querySelectedStaff?pickUsersStr=' + nativePickUsersStr + "&" + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_pager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {        //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }

            });
        },
        event: function () {
            Nenu.context.event = {
                //添加人员
                addPersonal:function () {
                    var url = '/notice/selectMaintainStaff?pickStaffType=addStaff&' + $.param(formObj);
                    Nenu.event.ifreame({
                        url: url,
                        height: '1000px',
                        width: '1200px'
                    });
                },

                // 批量删除
                deleteSelected: function () {
                    var rows = $("#js_grid").jqGrid("getGridParam", "selarrrow");
                    if (rows.length > 0) {
                        for (var i = 0; i < rows.length; i++) {
                            var rowData = $("#js_grid").jqGrid('getRowData', rows[i]);
                            pickUsersStr = pickUsersStr.replace("," + rowData.id + ",", ",");
                            $("#js_grid").jqGrid("delRowData", rows[i]);
                        }
                    } else {
                        Nenu.toastr.info('请选择一行数据！');
                        return;
                    }
                },

                // 搜索按钮
                searchSelectedStaff: function () {
                    // 序列化表单数据后提交
                    var formObj = $('#js_search_form').serializeArray();
                    $grid.jqGrid('setGridParam', {
                        url: "/notice/querySelectedStaff",
                        postData: formObj, //发送数据
                        page: 1
                    }).trigger("reloadGrid");
                },
                fnSure: function () {
                    nePageArgum.option.setParentData({
                        pickUsersStr: pickUsersStr
                    });
                    nePageArgum.option.close();
                }
            }
        },
        end:function () {
            // Nenu.open.end = function () {
            //     var data = Nenu.open.data.parent;   //子页面往父页面返回的数据
            //     pickUsersStr = data.pickUsersStr;
            // }
        },
        init: function () {
            this.pLayout();
            this.fnTable();
            this.event();
        }
    };
    nePageLogic.init();
});
