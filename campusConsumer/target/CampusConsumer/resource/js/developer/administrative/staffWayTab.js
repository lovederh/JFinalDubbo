define(function (require) {
    var $ = require('jquery');
    require("mConfig");
    require('jqZtree');

    require('jqLayout');
    require('neEvent');
    require('neToastr');

    require('jqDualListTable');
    require('jqBootstrapTableGroupBy');

    var $dualList;

    var editMemberData = [];

    var selectedGroupId;

    function addHoverDom(treeId, treeNode) {
    }

    // 判断根节点是否可以编辑
    function showEditOrDelBtn(treeId, treeNode) {
        if (treeNode.isParent) {
            return false;
        }
        return true;
    }

    function onRename(e, treeId, treeNode, isCancel) {
        $.ajax({
            url: '/notice/editGroupNote',
            data: {
                opt: "edit",
                id: treeNode.id,
                name: treeNode.name
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    function onRemove(e, treeId, treeNode) {
        $.ajax({
            url: '/notice/editGroupNote',
            data: {
                opt: "del",
                id: treeNode.id
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    var nePageArgum = {
        option: Nenu.open.data.child,
        aType: {
            Group: '分组',
            Teacher: '教师',
            Student: '学生',
            Parents: '家长'
        }
    };

    var nePageLogic = {
        event: function () {
            var newNum = 1;
            Nenu.context.event = {

                // 确定按钮
                fnSure: function () {
                    var data = $dualList.neDualListTable('getData');
                    nePageArgum.option.setParentData(data);
                    nePageArgum.option.close();
                },

                //取消按钮
                fnCancel: function () {
                    nePageArgum.option.close();
                },

                //添加分组按钮
                addGroupNote: function () {
                    // 机构树选择的机构节点
                    var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();

                    var selectNode = nodes[0];

                    if (!nodes.length) {
                        Nenu.toastr.warning("请选择根节点进行添加分组!");
                        return;
                    }

                    if (selectNode.level != '1') {
                        var nodeName = '新增分组' + (newNum++);
                        $.ajax({
                            url: '/notice/addGroup',
                            data: {
                                name: nodeName,
                                pId: selectNode.id
                            },
                            type: "post",
                            async: false,
                            success: function (jsonData) {
                                var newNode = {
                                    id: jsonData.groupId,
                                    pId: selectNode.id,
                                    name: nodeName
                                };
                                var addedNode = $.fn.zTree.getZTreeObj('js_tree').addNodes(selectNode, newNode);
                                $.fn.zTree.getZTreeObj('js_tree').selectNode(addedNode[0]);
                            }
                        });
                    } else {
                        Nenu.toastr.warning("不允许在分组节点下创建新的分组,请选择根节点进行添加分组!");
                        return;
                    }
                },

                //组员维护按钮
                groupManage: function () {

                    var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();

                    var selectNode = nodes[0];

                    if (!nodes.length) {
                        Nenu.toastr.warning("请选择子节点进行组员添加和维护!");
                        return;
                    }

                    if (selectNode.id == '00') {
                        Nenu.toastr.warning("请选择子节点进行组员添加和维护!");
                        return;
                    }

                    selectedGroupId = selectNode.id;

                    Nenu.event.ifreame({
                        title: '组员维护',
                        url: '/notice/editGroupMember?groupId=' + selectNode.id,
                        height: '1000px',
                        width: '1200px',
                        neData: editMemberData
                    });
                }
            }
        },
        end: function () {
            Nenu.open.end = function () {

                editMemberData = Nenu.open.data.parent;

                var rightMemberId = [];
                for (var i = 0, length = editMemberData.length; i < length; i++) {
                    var user = editMemberData[i];
                    rightMemberId.push(user.id);
                }

                $.ajax({
                    type: 'POST',
                    url: "/notice/saveGroupMember",
                    dataType: 'json',
                    async: false,
                    data: {
                        rightMemberId: rightMemberId.toString(),
                        selectedGroupId: selectedGroupId
                    },
                    success: function (jsonData) {
                        $dualList.neDualListTable('setData', {data: jsonData});
                    }
                });
            }
        },

        init: function () {
            this.event();
            this.end();
        }
    };

    nePageLogic.init();

    function fnLoad() {
        var $tabs = $('.jc-tabs');
        var aryBtns = $('li', $tabs);
        var $tree;
        // 参数type为页面页签类型
        var fnTree = function (url, type) {

            if (type == 'Group') {

                $tree = $('#js_tree').neTree({

                    setting: {

                        view: {addHoverDom: addHoverDom, selectedMulti: false},

                        edit: {
                            enable: true,
                            editNameSelectAll: true,
                            showRenameBtn: showEditOrDelBtn,
                            showRemoveBtn: showEditOrDelBtn,
                            removeTitle: '删除',
                            renameTitle: '重命名'
                        },

                        async: {enable: true, url: url + $.param(formObj)},

                        check: {enable: false},

                        callback: {
                            onRemove: onRemove,//删除节点后触发，用户后台操作
                            onRename: onRename,//编辑后触发，用于操作后台
                            onClick: function (event, treeId, treeNode) {
                                $.ajax({
                                    url: '/notice/query' + type + 'Data',
                                    data: {id: treeNode.id},
                                    dataType: 'json',
                                    type: "post",
                                    async: false,
                                    success: function (jsonData) {
                                        editMemberData = jsonData;
                                        $dualList.neDualListTable('setData', {data: jsonData});
                                    }
                                });
                            }
                        }
                    },
                    data: [{"id": "00", "name": nePageArgum.aType[type], "isParent": true}]
                });
            } else {
                //初始化tree
                $tree = $('#js_tree').neTree({

                    setting: {

                        async: {enable: true, url: url + $.param(formObj)},

                        check: {enable: false},

                        callback: {
                            onClick: function (event, treeId, treeNode) {
                                $.ajax({
                                    url: '/notice/query' + type + 'Data',
                                    data: {id: treeNode.id},
                                    dataType: 'json',
                                    type: "post",
                                    async: false,
                                    success: function (jsonData) {
                                        $dualList.neDualListTable('setData', {data: jsonData});
                                    }
                                });
                            }
                        }
                    },
                    data: [{"id": "00", "name": nePageArgum.aType[type], "isParent": true}]
                });
            }
        };

        //定义tabls按钮点击事件
        $tabs.on('click', 'a', function (e) {
            e.stopPropagation();

            var $this = $(this);

            var treeType = $this.data().type;

            if (treeType == 'Group') {
                $('#treeDiv').append(
                    '<button class="btn-sm btn-white btn-bitbucket jc-btn" data-btn="addGroupNote"><i class="glyphicon glyphicon-plus"></i>添加分组</button>' +
                    '<div class="ne-nbsp"></div>' +
                    '<button class="btn-sm btn-white btn-bitbucket jc-btn" data-btn="groupManage"><i class="glyphicon glyphicon-user"></i>组员维护</button>');
            } else {
                $('#treeDiv').empty();
            }

            formObj.paraTreeId = treeType;

            aryBtns.each(function () {
                $(this).removeClass('active');
            });

            $this.parent().addClass('active');

            fnTree('/notice/initTree?', treeType);
        });

        //初始化neDualListTable
        $dualList = $('#js_dual_list_table').neDualListTable({

            rightData: nePageArgum.option.neData,

            neTable: {
                table: {
                    columns: [
                        //列设置
                        {checkbox: true, field: 'state'},
                        {field: 'userName', title: '姓名', name: 'userName'},
                        {field: 'phone', title: '电话', name: 'phone'},
                        {field: 'orgId', title: '部门', name: 'orgId'}
                    ],
                    //行数据加载时候的回调 , 这个使用默认即可
                    rowAttributes: function (row, index) {
                        return row.state = false;
                    }

                }
            }
        })
        ;
        //默认加载分组
        $('a', aryBtns[0]).trigger('click');
        // $dualList.neDualListTable('leftMethod','getData',true);
    }

    //初始化layout
    $('body .tab-pane .panel-body ').layout({
        name: 'manLayout',
        west: {size: 250},
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: fnLoad
    });

});