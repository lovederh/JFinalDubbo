define(function (require) {
    'use strict';
    require("mConfig");
    require('jqGrid');
    require('jqZtree');
    require('jqSlimscroll');
    require('jqLayout');
    require('neEvent');
    require('neToastr');
    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID', 'loginId', '人员姓名', '联系方式', '部门'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'loginId', hidden: true},
        {name: 'userName', index: 'userName', width: 100, align: "center"},
        {name: 'phone', index: 'phone', width: 100, align: "center"},
        {name: 'orgId', index: 'orgId', width: 100, align: "center"}
    ];

    //第一次进入则加载已经选择过的人员
    var groupUserSelected = "";
    if (groupUserSelected) {
        groupUserSelected = "," + groupUserSelected + ",";
    }

    var groupUser;

    /**
     * 页面级 页面参数对象
     * @namespace
     *@param {Object} $body  jq-body 对象
     *@param {Object} $scroll  jq-scroll 对象 需要添加滚动条的dom对象
     * @param {Object} data  父页面传递过来的数据对象
     */
    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll'),
        option: Nenu.open.data.child
    };

    var nePageLogic = {
        selectTab: "js_tree_selectTeacher",
        options: Nenu.open.data.child,
        initTree: function (treeId) {
            if (!treeId) {
                treeId = "js_tree_selectTeacher";
                formObj.paraTreeId = treeId;
                nePageLogic.selectTab = treeId;
            }
            if (treeId == 'js_tree_selectTeacher') {
                initTeacherTree();
            } else {

            }
        },

        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        fnTable: function () {
            //选择教师左侧人员列表
            $('#js_teacher_grid').jqGrid({
                url: '/notice/queryEmpGridData?' + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_teacherpager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });

            $('#js_selectGrid').jqGrid({
                url: '/notice/queryGroupUser?groupId=' + groupId + "&" + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                height: "auto",
                pager: '#js_selectGridpager_grid',//pager在使用树表格时禁用
                autowidth: true,
                rowNum: "10",//显示全部记录
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },

        event: function () {
            Nenu.method.bind(['jc-click|click.tab|true']);
            Nenu.context.event = {
                clickTab: function () {
                    var treeId = "js_tree_" + $(this).attr("id");
                    formObj.paraTreeId = treeId;
                    nePageLogic.selectTab = treeId;
                    nePageLogic.initTree(treeId);
                },

                //往右侧选择
                rightSelected: function () {
                    var rows = $("#js_teacher_grid").jqGrid('getGridParam', 'selarrrow');
                    $.each(rows, function (i, item) {
                        var rowData = $("#js_teacher_grid").jqGrid('getRowData', rows[i]);
                        if (groupUserSelected.lastIndexOf(rowData.id + ",") < 0) {
                            groupUserSelected += rowData.id + ",";
                            $("#js_selectGrid").jqGrid("addRowData", rows[i], rowData, "last");
                        }
                    });
                },

                //往左侧选择
                leftSelected: function () {
                    var rows = $("#js_selectGrid").jqGrid("getGridParam", "selarrrow");
                    for (var i = 0; i < rows.length; i++) {
                        var rowData = $("#js_selectGrid").jqGrid('getRowData', rows[i]);
                        groupUserSelected = groupUserSelected.replace(rowData.id + ",", "");
                        $("#js_selectGrid").jqGrid('delRowData', rows[i]);

                    }
                },

                //选择教师方式的搜索查询
                searchTeacherInput: function () {
                    var searchTeacherInput = $('#searchTeacherInput').val();
                    if (!searchTeacherInput) {
                        return;
                    }
                    $('#js_teacher_grid').jqGrid('setGridParam', {
                        //发送查询条数据
                        url: '/notice/queryEmpGridData?searchInput=' + searchTeacherInput,
                        page: 1
                    }).trigger("reloadGrid");
                },

                //右侧已选择人员搜索查询
                searchInputSelected: function () {
                    var searchInputSelected = $('#searchInputSelected').val();
                    $('#js_selectGrid').jqGrid('setGridParam', {
                        //发送查询条数据
                        url: '/notice/queryEmpGridData?searchInput=' + searchInputSelected,
                        page: 1
                    }).trigger("reloadGrid");
                },

                //确定按钮
                fnSure: function () {
                    $.ajax({
                        url: '/notice/addGroupUser',
                        data: {
                            groupUserSelected: groupUserSelected.substring(0, groupUserSelected.length - 1),
                            groupId: groupId
                        },
                        type: "post",
                        async: false,
                        success: function (jsonData) {
                            nePageArgum.option.setParentData({
                                groupUserSelected: groupUserSelected
                            });
                            nePageArgum.option.close();
                        }
                    });
                }
            }
        },

        init: function () {
            this.pLayout();
            this.initTree();
            this.fnTable();
            this.event();
        }
    };

    nePageLogic.init();

    function initTeacherTree() {
        var $selectTeacherTree = $('#js_tree_selectTeacher').neTree({
            setting: {
                edit: {
                    enable: false
                },
                async: {
                    enable: true,
                    url: '/notice/initTree?' + $.param(formObj)
                },
                check: {
                    enable: false
                },
                callback: {
                    onClick: function (event, treeId, treeNode) {
                        nePageLogic.selectedTreeId = treeNode.id;
                        $('#js_teacher_grid').jqGrid('setGridParam', {
                            //发送查询条数据
                            url: '/notice/queryEmpGridData?orgId=' + nePageLogic.selectedTreeId,
                            page: 1
                        }).trigger("reloadGrid");
                    }
                }
            }
        });
    }


});