define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var $resumeGrid = $('#js_resume_grid');

    var nePageLogic = {
        fnTable : function () {
            $resumeGrid.neTable({
                table:{
                    url: '/emp/resumeInfoList?empId='+$('#empId').val(),
                    columns: [
                        {field: 'state',checkbox: true},
                        {field: 'workTime',title: '成员姓名'},
                        {field: 'workUnit',title: '人员关系'},
                        {field: 'workDirection',title: '工作职务'}
                    ]
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            Nenu.context.event = {
                // 新增工作简历信息按钮
                addResume : function () {
                    $('#resumeForm').show();
                    $('#resumeId').val("");
                    $('#workTime').val("");
                    $('#workUnit').val("");
                    $('#workDirection').val("");
                },

                resumeCancel : function () {
                    $('#resumeId').val("");
                    $('#workTime').val("");
                    $('#workUnit').val("");
                    $('#workDirection').val("");
                    $('#resumeForm').hide();
                },

                // 编辑工作简历信息按钮
                editResume : function () {
                    var selRows = $resumeGrid.bootstrapTable('getSelections');
                    if(selRows.length > 0){
                        if (selRows.length > 1){
                            Nenu.toastr.info('请选择一条数据进行编辑!');
                            return;
                        }
                        $('#resumeForm').show();
                        $.ajax({
                            type: 'post',
                            url: '/emp/loadInfoResume?id='+selRows[0].id,
                            dataType: 'json',
                            async: false,
                            success: function(jsonData){
                                $('#resumeId').val(jsonData.id);
                                $('#workTime').val(jsonData.workTime);
                                $('#workUnit').val(jsonData.workUnit);
                                $('#workDirection').val(jsonData.workDirection);
                            }
                        });
                    }else{
                        Nenu.toastr.info('请选择一条要编辑的数据!');
                        return;
                    }
                },

                // 添加/编辑工作简历信息保存按钮
                resumeSave : function(){
                    // 序列化表单数据后提交
                    var resumeForm = $('#resumeForm').serializeArray();
                    $.ajax({
                        type: 'post',
                        url: '/emp/resumeSave',
                        dataType: 'json',
                        async: false,
                        data: resumeForm,
                        success: function(jsonData){
                            Nenu.toastr.success(jsonData.msg);
                            $('#resumeForm').hide();
                            $resumeGrid.bootstrapTable('refresh');
                        }
                    });
                },

                // 删除工作简历按钮
                delResume : function () {
                    var selRows = $resumeGrid.bootstrapTable('getSelections');
                    if (selRows.length > 0) {
                        Nenu.event.confirm({
                            isClose : false,             // 是否自动关闭窗口
                            title   : '确定要删除所选的记录吗？',        // 窗口标题
                            execute : function (close) {  // 确定按钮执行的事件 参数 close 传入的关闭函数
                                var id = new Array();
                                for (var i = 0; i < selRows.length; i++) {
                                    id.push(selRows[i].id);
                                }
                                $.ajax({
                                    type     : 'POST',
                                    url      : "/emp/delResume",
                                    dataType : 'json',
                                    async    : false,
                                    data     : {
                                        ids : id.join(',')
                                    },
                                    success  : function (jsonData) {
                                        Nenu.toastr.success(jsonData.msg);
                                        $resumeGrid.bootstrapTable('refresh');
                                    }
                                });
                                close();
                            }
                        });
                    } else {
                        Nenu.toastr.info('请选择一行数据！');
                        return {};
                    }

                }
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.fnTable();
            this.event();
        }
    };
    nePageLogic.init();
});