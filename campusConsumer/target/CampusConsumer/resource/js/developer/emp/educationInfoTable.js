define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var $educationGrid = $('#js_education_grid');


    var nePageLogic = {
        fnTable : function () {
            $educationGrid.neTable({
                table:{
                    url: '/emp/educationInfoList?empId='+$('#empId').val(),
                    columns: [
                        {field: 'state',checkbox: true},
                        {field: 'graduateTime',title: '毕业时间'},
                        {field: 'graduateSchool',title: '毕业院校'},
                        {field: 'educationBackground',title: '学历学位'},
                        {field: 'major',title: '主修专业'},
                        {field: 'educationFile',title: '证书文件'}
                    ]
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            Nenu.context.event = {
                // 学历信息新增按钮
                addEducation : function () {
                    $('#educationForm').show();
                    $('#educationId').val("");
                    $('#graduateTime').val("");
                    $('#graduateSchool').val("");
                    $('#educationBackground').val("");
                    $('#major').val("");
                    $('#educationFile').val("");
                },

                educationCancel : function () {
                    $('#educationId').val("");
                    $('#graduateTime').val("");
                    $('#graduateSchool').val("");
                    $('#educationBackground').val("");
                    $('#major').val("");
                    $('#educationFile').val("");
                    $('#educationForm').hide();
                },

                // 编辑学历信息按钮
                editEducation : function () {
                    var selRows = $educationGrid.bootstrapTable('getSelections');
                    if(selRows.length > 0){
                        if (selRows.length > 1){
                            Nenu.toastr.info('请选择一条数据进行编辑!');
                            return;
                        }
                        $('#educationForm').show();
                        $.ajax({
                            type: 'post',
                            url: '/emp/loadInfoEducation?id='+selRows[0].id,
                            dataType: 'json',
                            async: false,
                            success: function(jsonData){
                                $('#educationId').val(jsonData.id);
                                $('#graduateTime').val(jsonData.graduateTime);
                                $('#graduateSchool').val(jsonData.graduateSchool);
                                $('#educationBackground').val(jsonData.educationBackground);
                                $('#major').val(jsonData.major);
                                $('#educationFile').val(jsonData.educationFile);
                            }
                        });
                    }else{
                        Nenu.toastr.info('请选择一条要编辑的数据!');
                        return;
                    }
                },

                // 添加/编辑学历信息保存按钮
                educationSave : function(){
                    // 序列化表单数据后提交
                    var educationForm = $('#educationForm').serializeArray();
                    $.ajax({
                        type: 'post',
                        url: '/emp/educationSave',
                        dataType: 'json',
                        async: false,
                        data: educationForm,
                        success: function(jsonData){
                            Nenu.toastr.success(jsonData.msg);
                            $('#educationForm').hide();
                            $educationGrid.bootstrapTable('refresh');
                        }
                    });
                },

                // 删除家庭成员按钮
                delEducation : function () {
                    var selRows = $educationGrid.bootstrapTable('getSelections');
                    if (selRows.length > 0) {
                        Nenu.event.confirm({
                            isClose : false,             // 是否自动关闭窗口
                            title   : '确定要删除所选的记录吗？',        // 窗口标题
                            execute : function (close) {  // 确定按钮执行的事件 参数 close 传入的关闭函数
                                var id = new Array();
                                for (var i = 0; i < selRows.length; i++) {
                                    id.push(selRows[i].id);
                                }
                                $.ajax({
                                    type     : 'POST',
                                    url      : "/emp/delEducation",
                                    dataType : 'json',
                                    async    : false,
                                    data     : {
                                        ids : id.join(',')
                                    },
                                    success  : function (jsonData) {
                                        Nenu.toastr.success(jsonData.msg);
                                        $educationGrid.bootstrapTable('refresh');
                                    }
                                });
                                close();
                            }
                        });
                    } else {
                        Nenu.toastr.info('请选择一行数据！');
                        return {};
                    }

                }
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.fnTable();
            this.event();
        }
    };
    nePageLogic.init();
});