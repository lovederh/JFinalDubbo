define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var nePageLogic = {
        event : function () {
            var option = Nenu.open.data.child;
            Nenu.context.event = {
                save:function(){
                    $.ajax({
                        type: 'POST',
                        url: '/emp/empStatusSave',
                        dataType: 'json',
                        async: false,
                        data: {
                            empIds:formObj.empIds,
                            empStatus:$('#js_default_form_select_empStatus').val()
                        },
                        success: function (jsonData) {
                            Nenu.toastr.success(jsonData.msg);
                        }
                    })
                },
                fnClose: function () {
                    option.close();
                }
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
        }
    };
    nePageLogic.init();
});

