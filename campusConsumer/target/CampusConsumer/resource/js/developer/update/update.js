// 全局树对象
var ztreeObj;

$(document).ready(function () {
    // 初始化字典表格
    $.jgrid.defaults.styleUI = 'Bootstrap';
    // 初始构造字典树
    initUpdateTree();
    initUpdateGrid();
});

/******************************页面初始化************************************/

// 请求后台构造树
function initUpdateTree(){
    // zTree属性设置
    var treeSetting = {
        view: {
            selectedMulti: false//不再支持多选
        },
        callback: {
            // 单击事件(显示当前可升级信息)
            onClick: function(event, treeId, treeNode) {
                // 刷新表数据
                $('#updateGrid').setGridParam({
                    postData : {
                        updateTableName : treeNode.tableName
                    }
                }).trigger("reloadGrid");
            }
        }
    };
    // 请求数据初始化树
    $.ajax({
        type : 'post',
        url : '/update/initUpdateTree',
        dataType : 'json',
        success : function(jsonData){
            // 构建树
            $.fn.zTree.init($('#updateTree'), treeSetting, jsonData);
            // 全局对象赋值
            ztreeObj = $.fn.zTree.getZTreeObj('updateTree');
            // 默认选中根节点
            defaultSelectRoot();
        }
    });
}

/**
 * 初始化字典表格
 */
function initUpdateGrid(){
    //列配置
    var colNames = ['名称', '更新人', '版本号', '时间', '语句','isMax'];
    var colModel = [
        {name: 'tableId', index: 'tableId', width: 100, align: "center"},
        {name: 'loginId', index: 'loginId', width: 100, align: "center"},
        {name: 'version', index: 'version', width: 100, align: "center"},
        {name: 'time', index: 'time', width: 100, align: "center"},
        {name: 'sql', index: 'sql', width: 100, align: "center"},
        {name: 'isMax',hidden: true}
    ];

    //表格生成声明
    $("#updateGrid").jqGrid({
        url: '/update/findUpdateList',
        datatype: 'json',
        postData: { updateTableName : "" },
        colNames: colNames,//列名
        colModel: colModel,//列字段配置
        jsonReader: {
            root: "updateDataList",
            repeatitems : false
        },
        rowNum : "-1",//显示全部记录
        autowidth: true,
        shrinkToFit:true,
        height: 200,//grid的高度
        pager: "#updatePager",//工具栏的元素
        caption: "升级表信息",
        toolbar:[true,"both"],
        multiselect:true // 支持多项选择
    });
    //  升级按钮
    $("#t_updateGrid").append(
        '<table cellspacing="0" cellpadding="0" border="0" style="float:left;table-layout:auto;margin-top:2px" class="ui-pg-table topnavtable">' +
        '   <tr>' +
        '       <td>' +
        '           <button id="saveBtn" class="btn btn-primary" onclick="upgrade()"><i class="fa fa-upload"></i>&nbsp;升级</button>' +
        '       </td>' +
        '   </tr>' +
        '</table>');
}

/**
 * 默认选中根节点
 */
function defaultSelectRoot(){
    var nodes = ztreeObj.getNodes();
    if(nodes.length){
        ztreeObj.selectNode(nodes[0]);
    }
}

/**
 *  升级按钮
 */
function upgrade() {
    // 使用多选属性selarrrow
    var upgradeSelect = $("#updateGrid").jqGrid('getGridParam','selarrrow');
    if(upgradeSelect.length <= 0 ){
        alertErrorLayer('请选择至少一条数据!');
    }else{
        // 批量升级
        var upgradeRow;
        var obj;
        var array = new Array();
        for(var i=0; i<upgradeSelect.length; i++) {
            upgradeRow = $("#updateGrid").jqGrid('getRowData', upgradeSelect[i]);
            obj = new Object();
            obj.tableId = upgradeRow.tableId;
            obj.loginId = upgradeRow.loginId;
            obj.version = upgradeRow.version;
            obj.time = upgradeRow.time;
            obj.sql = upgradeRow.sql;
            obj.isMax = upgradeRow.isMax;
            array.push(obj);
        }
        $.ajax({
            type : 'post',
            url : '/update/updateSelectedData',
            async: false,
            data : { rows : JSON.stringify(array) },
            success : function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if('200' == status){
                    layer.msg(msg, {
                        icon: 1,
                        btn: ['确定']
                    });
                }else{
                    layer.msg(msg, {
                        icon: 5,
                        time: 2000,//2s后自动关闭
                        btn: ['确定']
                    });
                }
                // 表格刷新
                $("#updateGrid").trigger("reloadGrid");
            }
        })
    }
}

/**
 * 弹出错误警告框
 */
function alertErrorLayer(msg){
    layer.msg(msg, {
        icon: 5,
        time: 2000,//2s后自动关闭
        btn: ['确定']
    });
}
