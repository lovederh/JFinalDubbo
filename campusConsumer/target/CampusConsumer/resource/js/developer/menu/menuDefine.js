$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    //列配置
    var colNames = ['主键ID', '菜单名称', '菜单编码', '菜单路径', '业务类型', '是否启用', '关联业务模型', '排序号码', '是否叶子菜单', '父菜单编码', '菜单级别', '是否展开'];
    var colModel = [
        {name: 'primaryId', hidden: true},
        {name: 'menuName', index: 'menuName', width: 100, align: "center"},
        {name: 'menuId', index: 'menuId', width: 100, align: "right", key: true},
        {name: 'menuUrl', index: 'menuUrl', width: 100, align: "center"},
        {name: 'businessType', index: 'businessType', width: 100, align: "center"},
        {name: 'isEnabled', index: 'isEnabled', width: 100, align: "center"},
        {name: 'relatedBo', index: 'relatedBo', width: 100, align: "center"},
        {name: 'orderNo', index: 'orderNo', width: 100, align: "center"},
        {name: 'isLeaf', index: 'isLeaf', width: 100, align: "center"},
        {name: 'pId', hidden: true},
        {name: 'level', hidden: true},
        {name: 'expanded', hidden: true}
    ];

    //树形表格生成声明
    $("#menuDefineGrid").jqGrid({
        url: '/menu/findMenuDefines',
        datatype: 'json',
        colNames: colNames,//列名
        colModel: colModel,//列字段配置
        //启用树形表格
        treeGrid: true,
        treeGridModel: 'adjacency',
        ExpandColumn : 'menuName',
        jsonReader: {
            root: "treeData",
            repeatitems : false
        },
        treeReader : {
            level_field: "level",
            parent_id_field: "pId",
            leaf_field: "isLeaf",
            expanded_field: "expanded"
        },
        pager: false,//pager在使用树表格时禁用
        height: "auto",
        rowNum : "-1",//显示全部记录
        autowidth: true,
        shrinkToFit:true,
        // 双击对选中菜单操作: rowid为行ID; iRow行索引; iCol为列索引; e事件对象
        ondblClickRow : function(rowid, iRow, iCol, e){
            var rowData = $(this).jqGrid('getRowData', rowid);
            layer.open({
                type: 2,
                title: '菜单定义信息',
                shadeClose: true,
                shade: 0.8,
                area: ['800px', '90%'],
                content: '/menu/updateMenuDialog?primaryId=' + rowData.primaryId
            });
        }
    });
});

/**
 * 重新加载当前表格
 */
function reloadGrid(){
    $("#menuDefineGrid").trigger('reloadGrid');
}
