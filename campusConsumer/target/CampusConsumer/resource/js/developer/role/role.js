/**
 * Created by dsideal-lee on 2016/12/19.
 */
define(function (require) {
    'use strict';
    require("mConfig");
    require('jqZtree');
    require('neEvent');
    require('neToastr');
    var nePageLogic = {
        selectTab: "js_tree_t1",
        options: Nenu.open.data.child,
        initTree: function (treeId) {
            if(!treeId){
                treeId = "js_tree_t1";
                formObj.paraTreeId = treeId;
                nePageLogic.selectTab = treeId;
                formObj.roleId = nePageLogic.options.neData.roleId;
            }
            var $tree = $('#'+treeId).neTree({
                setting: {
                    async: {
                        enable: true,
                        url: '/tree/initRoleTree?' + $.param(formObj)
                    },
                    check: {
                        enable: true
                    },
                    callback: {
                    }
                }
            });
        },
        event: function () {
            Nenu.method.bind(['jc-click|click.tab|true']);
            Nenu.context.event = {
                saveRole: function() {
                    var treeObj = $.fn.zTree.getZTreeObj(nePageLogic.selectTab);
                    var nodes = treeObj.getCheckedNodes(true);
                    if(nodes) {
                        var ids = new Array();
                        for (var node in nodes) {
                            ids.push(nodes[node].key);
                        }
                        formObj.ids = ids.toString();
                        //是否应用到子级
                        var isUsedToChild = $("#js_form_radio_yes").val();
                        if(isUsedToChild){
                            formObj.roleId = nePageLogic.options.neData.roleId;
                            return {
                                title: "新增信息",
                                url    : '/permission/useToChild?'+ $.param(formObj),
                                neData : formObj,
                                height : '750px',
                                width  : '800px'
                            };
                        }else{
                            nePageLogic.commSaveRole(nePageLogic.options.neData.roleId);
                        }
                    }else{
                        Nenu.toastr.error("请给点权限再保存！");
                    }
                },
                clickTab: function(){
                    var treeId = "js_tree_"+$(this).attr("id");
                    formObj.paraTreeId = treeId;
                    nePageLogic.selectTab = treeId;
                    nePageLogic.initTree(treeId);
                }
            }
        },
        commSaveRole: function(roleIds){
            formObj.roleId = roleIds;
            $.ajax({
                url: "/tree/saveRoleData",
                data: formObj,
                type: "POST",
                async: false,
                success: function (jsonData) {
                    Nenu.toastr.success(jsonData.msg);
                }
            });
        },
        end: function(){
            Nenu.open.end = function () {
                //弹出授权子菜单的方法
                var data = Nenu.open.data.parent;
                var roleIds = data.roleIds;
                this.commSaveRole(roleIds);
            };
        },
        business: function () {
        },
        init: function () {
            this.initTree();
            this.event();
            this.business();
        }
    };
    nePageLogic.init();
});