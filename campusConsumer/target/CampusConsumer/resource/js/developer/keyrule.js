/**
 * Created by dsideal-lee on 2016/10/28.
 */
function operateSave(){
    // 保存按钮不再可用, 防止重复提交
    var $saveBtn = $('#saveBtn');
    $saveBtn.attr('disabled', true);
    // 序列化表单数据后提交
    var formData = $('#formID').serializeArray();
    $.ajax({
        type: 'POST',
        url: '/bo/saveKeyRuleConfig',
        dataType: 'json',
        async: false,
        data: formData,
        success: function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //关闭layer弹出的iframe
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    parent.layer.close(index);
                });
            }else{
                layer.msg(msg, {
                    icon: 5,
                    time: 2000,//2s后自动关闭
                    btn: ['确定']
                });
            }
        }
    });
    $saveBtn.attr('disabled', false);
}