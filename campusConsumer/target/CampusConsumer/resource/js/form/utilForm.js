/**
 * 自定义表单列表页-列表中其中某一条数据的“编辑”功能处理
 * @param id 表单的Id
 */
function formUpdate(id){
    Nenu.event.ifreame({
        url : '/formManage/queryFormForUpd?id=' + id,
        height: '1500px',
        width: '2000px'
    });
}

/**
 * 自定义表单列表页-列表中其中某一条数据的“预览”功能处理
 * @param id 表单的Id
 */
function formPreview(id){
    Nenu.event.ifreame({
        url : '/formManage/queryFormForPre?id=' + id,
        height: '1500px',
        width: '2000px'
    });
}