define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //日期组件
    require('neDate');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['id','标题','类型','应用模块','创建时间','操作'];
    var colModel = [
        {name: 'id', index: 'id', hidden: true},
        {name: 'formDisplay', index: 'formDisplay', width: 200, align: "center"},
        {name: 'formType', index: 'formType', align: "center", hidden: true},
        {name: 'usedModule', index: 'usedModule', width: 100, align: "center"},
        {name: 'createTime', index: 'createTime', width: 100, align: "center"},
        {name: 'operate', width: 100, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn =
                '<button class="btn btn-primary btn-outline btn-sm" ' +
                '   onclick="formUpdate(\''+ rowObject.id +'\')" >' +
                '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;编辑</button>' +
                '<span class="ne-nbsp"></span>' +
                '<button class="btn btn-primary btn-outline btn-sm" ' +
                '   onclick="formPreview(\''+ rowObject.id +'\')" >' +
                '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;预览</button>';
            return operateBtn;
        }}
    ];

    var formsLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: '/formManage/queryFormList',
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "10",
                rowList: [5,10,20,50,100],
                pager: '#js_gridPager',
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: false,
                jsonReader: {
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                searchGridDataBtn: searchGridDataFunc,
                //deletePhysicalOrderBtn: deletePhysicalOrderFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {

            };
        },
        init: function () {
            this.fnTable();
            this.event();
            this.end();
        }
    };
    formsLogic.init();

    /**
     * 查询方法
     */
    function searchGridDataFunc(){
        // 标题检索条件
        var searchTitle = $('#searchTitle').val();

        $grid.jqGrid('setGridParam',{
            //发送查询条数据
            postData: $('#js_gridSearchBar').serializeArray(),
            page: 1
        }).trigger("reloadGrid");
    }

    //彻底删除
    /*function deletePhysicalOrderFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个已归档工单进行删除!');
            return;
        }
        var rowData = $grid.jqGrid('getRowData', rows[0]);
        if(1 == rowData.orderState){
            Nenu.toastr.warning('请选择一个已归档工单进行删除!');
            return;
        }
        util_deletePhysicalOrder(rowData.orderId, reloadGrid);
    }*/

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});


