define(function(require){
    require("mConfig");
    require("jqGrid");
    require("neEvent");
    require('neToastr');
    var neTreeConfig = {
        treeReader: {
            level_field: "level",
            parent_id_field: "pId",
            leaf_field: "isLeaf",
            expanded_field: "expanded"
        },
        ExpandColumn: '',
        colNames: '', //列名
        colModel: '', //列配置
        init: function(){
            $.ajax({
                url: "/grid/getGridConfig?condition=tree",
                type: "GET",
                data: formObj,
                async: false,
                success: function (resData) {
                    var jsonData = JSON.parse(resData);
                    neTreeConfig.colNames = jsonData.colNames;
                    neTreeConfig.colModel = jsonData.colModel;
                    neTreeConfig.treeReader = jsonData.treeReader;
                    neTreeConfig.ExpandColumn = jsonData.ExpandColumn;
                }
            });
            console.log(neTreeConfig);
        }
    };
    neTreeConfig.init();
    $.jgrid.defaults.styleUI = 'Bootstrap';
    //树形表格生成声明
    var selRowData;
    var nePageLogic = {
        serializeObject: function(arr){
            var o = {};
            $.each(arr, function() {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [ o[this.name] ];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        },
        addNode: function(dtoFormObj, title){
            return {
                title: title,
                url: '/tree/getTreeNodeDetailForm?'+ $.param(dtoFormObj),
                neData: formObj,
                height: '400px',
                width: '800px',
                index: 1,
                close: nePageLogic.reloadGrid
            }
        },
        event: function(){
            Nenu.context.event = {
                doSearch: function(){
                    var postData = nePageLogic.serializeObject($("#js_query_form").serializeArray());
                    postData.isSearch = "true";
                    postData.dtoName = formObj.dtoName;
                    postData.domain = formObj.domain;
                    $("#grid_list").jqGrid('setPostData',postData);
                    nePageLogic.reloadGrid();
                },
                addPeerLevel: function(){
                    if(selRowData == null || selRowData == undefined){
                        Nenu.toastr.info("请选择一条记录!");
                        return;
                    }
                    var dtoFormObj = {
                        dtoName: formObj.dtoName,
                        paraOpt: 'addPeer',
                        pId: selRowData.pId,
                        level: selRowData.level
                    }
                    formObj.paraOpt = "add";
                    formObj.formWrapper = "addPeer";
                    return nePageLogic.addNode(dtoFormObj, "增加同级");
                },
                addChildLevel: function(){
                    if(selRowData == null || selRowData == undefined){
                        Nenu.toastr.info("请选择一条记录!");
                        return;
                    }
                    var dtoFormObj = {
                        dtoName: formObj.dtoName,
                        paraOpt: 'addChild',
                        pId: selRowData.codeId,
                        level: selRowData.level
                    }
                    formObj.paraOpt = "add";
                    formObj.formWrapper = "addChild";
                    return nePageLogic.addNode(dtoFormObj, "增加子级");
                },
                edit: function(){
                    if(selRowData == null || selRowData == undefined){
                        Nenu.toastr.info("请选择一条记录!");
                        return;
                    }
                    formObj.id = selRowData.id;
                    formObj.paraOpt = "edit";
                    return nePageLogic.addNode(formObj, "修改");
                },
                delData: function(){
                    if(selRowData == null || selRowData == undefined){
                        Nenu.toastr.info("请选择一条记录!");
                        return;
                    }
                    if(selRowData.pId == "-1"){
                        Nenu.toastr.info("根结点不允许删除!");
                        return;
                    }
                    var delMsg = "确定要删除结点";
                    if(selRowData.isLeaf == 'false'){
                        delMsg += "及其子结点"
                    }
                    delMsg += "吗？";
                    Nenu.event.confirm ({
                        isClose   : false,             // 是否自动关闭窗口
                            title     : delMsg,        // 窗口标题
                            execute   : function (close){  // 确定按钮执行的事件 参数 close 传入的关闭函数
                                formObj.ids = selRowData.codeId;
                                $.ajax({
                                    type     : 'POST',
                                    url      : "/tree/delNode",
                                    dataType : 'json',
                                    async    : false,
                                    data     : formObj,
                                    success  : function (jsonData) {
                                        nePageLogic.reloadGrid();
                                    }
                                });
                            close();
                        }
                    });
                },
                grantRole: function(){
                    if (selRowData) {
                        formObj.roleId = selRowData.id;
                        return {
                            title  : "授权界面",
                            url    : '/permission/grantRole?id=' + selRowData.id + '&' + $.param(formObj),
                            neData : formObj,
                            height : '750px',
                            width  : '800px'
                        }
                    } else {
                        Nenu.toastr.info('请选择一行数据！');
                        return {};
                    }
                }
            }
        },
        end: function(){
            Nenu.open.end = function(){
                var data = Nenu.open.data.parent;
                if(data){

                }
                nePageLogic.reloadGrid();

            }
        },
        initTreeGrid: function(){
            $("#grid_list").jqGrid({
                //data: data,
                url: '/grid/getGridData?condition=tree',
                datatype: 'json',
                postData: formObj,
                colNames: neTreeConfig.colNames,//列名
                colModel: neTreeConfig.colModel,//列字段配置
                //启用树形表格
                treeGrid: true,
                treeGridModel: 'adjacency',
                ExpandColumn: neTreeConfig.ExpandColumn,
                jsonReader: {
                    root: "gridData",
                    repeatitems: false
                },
                treeReader: neTreeConfig.treeReader,
                pager: false,//pager在使用树表格时禁用
                rowNum : "-1",//显示全部记录
                height: "auto",
                autowidth: true,
                shrinkToFit: true,
                onSelectRow: function (rowid) {
                    selRowData = $(this).jqGrid('getRowData', rowid);
                }
            });
        },
        reloadGrid: function(){
            $("#grid_list").trigger('reloadGrid');
        },
        init: function(){
            this.initTreeGrid();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    $('.ui-jqgrid-bdiv').height($('.wrapper').outerHeight(true) - $('.ui-jqgrid-hdiv').offset().top - 36 - 39 - 2);
    $(window).bind('resize', function () {
        var width = $('.jc_wrapper').width();
        $('.ui-jqgrid-bdiv').height($('.wrapper').outerHeight(true)- $('.ui-jqgrid-hdiv').offset().top - 36 - 39 - 2);
        $table.setGridWidth(width);
    });
});


