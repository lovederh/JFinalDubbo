$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    //主grid
    var colModelMaster = [                                    //列配置
        {
            name: 'id',
            index: 'id',
            editable: false,
            width: 60,
            sorttype: "int",
            search: true,
            hidden: true
        },
        {
            name: 'tableName',
            index: 'tableName',
            editable: true,
            width: 100
        },
        {
            name: 'showName',
            index: 'showName',
            editable: true,
            width: 90
        },
        {
            name: 'tableType',
            index: 'tableType',
            editable: true,
            width: 90,
            edittype:"select",
            formatter:"select",
            editoptions:{value:"1:存储表;2:树表"}
        },
        {
            name: 'isCreate',
            index: 'isCreate',
            editable: false,
            width: 80,
            hidden: true,
            align: "right"
        }
    ];
    $("#grid_master").jqGrid({
        url: '/meta/model',                            //从服务端获取数据的url
        datatype: "json",                              //返回数据的解析格式
        height: 150,                                   //grid的高度
        postData: formObjMaster,                       //向服务端传递的数据
        autowidth: true,                               //自动宽度
        shrinkToFit: true,                             //宽度自动调整
        rowNum: 20,                                     //每页条数
        rowList: [10, 20, 30],                          //每页条数列表
        colNames: ['序号', '表名', '显示名', '表类型', '创建否'], //列名
        colModel: colModelMaster,                            //列配置
        pager: "#pager_master",                        //工具栏的元素
        jsonReader: {                                   //解析的jsonReader
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: false
        },
        viewrecords: true,                             //查看数据
        caption: "表信息",                             // 表格的名称信息
        hidegrid: false,                               //是否允许隐藏
        editurl: "/meta/save",                          //增 删 改提交时的url
        onSelectRow: function (ids) {                  //选中某行触发事件
            // 获得选中行的数据
            var rowData = $("#grid_master").jqGrid('getRowData', ids);
            //获得表名
            var tableName = rowData.tableName;
            //设置子表的pId为父表的id
            formObjSlave.pId = ids;
            //设置子表的tableName为父表的tableName
            formObjSlave.tableName = tableName;
            if (ids == null) {
                ids = 0;
                if (jQuery("#grid_slave").jqGrid('getGridParam', 'records') > 0) {
                    jQuery("#grid_slave").jqGrid('setGridParam', {url: "/meta/fields?pId=" + ids, page: 1});
                    jQuery("#grid_slave").jqGrid('setCaption', "字段表: " + ids)
                        .trigger('reloadGrid');
                }
            } else {
                jQuery("#grid_slave").jqGrid('setGridParam', {url: "/meta/fields?pId=" + ids, page: 1});
                jQuery("#grid_slave").jqGrid('setCaption', "字段表: " + ids).trigger('reloadGrid');
            }
        }
    });
    // Setup buttons
    $("#grid_master").jqGrid('navGrid', '#pager_master', {
            edit: true,
            add: true,
            del: true,
            search: true,
            refresh: false
        }, {                                                        //设置编辑按钮的配置
            editCaption: "编辑",
            recreateForm: true,
            checkOnUpdate: true,
            checkOnSubmit: false,                                 //是否弹出数据改变确认提示框
            closeAfterEdit: true,
            reloadAfterSubmit: true,
            resize: true,
            editData: formObjMaster,                              //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置新增按钮的配置
            closeAfterAdd: true,
            recreateForm: true,
            editData: formObjMaster,                            //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置删除按钮的配置
            delData: formObjMaster,                            //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        });
    //重构按钮
    jQuery("#grid_master").jqGrid("navButtonAdd", "#pager_master", {
            caption: "重构",
            buttonicon: "ui-icon-add",
            onClickButton: function(){
                var gsr = jQuery("#grid_master").jqGrid('getGridParam','selrow');
                if(gsr){
                    var rowData = jQuery("#grid_master").jqGrid("getRowData", gsr);
                    $.ajax({
                        type: "POST",
                        url: "/meta/rebuild",
                        data: {dtoName: rowData.tableName},
                        dataType: "json",
                        async: false,
                        success: function(jsonData){
                            layer.alert(jsonData.msg);
                        }
                    });
                } else {
                    layer.alert("请选择要重构的表！");
                }
            },
            position: "last",
            title: "重构表结构",
            cursor: "pointer"
        });
    //子grid
    var colNameSlave = ['主键', '列字段', '列名', '类型', '字段长度', '主键否', '所属表'];
    var colModelSlave =[
        {name: 'id', index: 'id', editable: false, width: 55},
        {name: 'colName', index: 'colName', editable: true, width: 180},
        {name: 'showName', index: 'showName', editable: true, width: 80, align: "right"},
        {name: 'type', index: 'type', width: 80, editable: true, align: "right",
            edittype: "select",
            formatter: "select",
            editoptions: {value:"int:int;varchar:varchar;varchar2:varchar2;unsigned:unsigned;clob:clob"}},
        {
            name: 'colLen',
            index: 'colLen',
            editable: true,
            width: 80,
            align: "right",
            exittype: "number",
            sortable: false,
            search: false
        },
        {
            name: 'isKey',
            index: 'isKey',
            editable: true,
            width: 80,
            align: "right",
            edittype: "checkbox",
            formatter: "checkbox",
            editoptions: {value:"true:false"},
            sortable: false,
            search: false
        },
        {
            name: 'tableName',
            index: 'tableName',
            editable: false,
            width: 150,
            align: "right",
            sortable: false,
            search: false
        }
    ];
    jQuery("#grid_slave").jqGrid({
        url: '/meta/fields?id=0&pId=0',
        datatype: "json",
        postData: formObjSlave,                       //向服务端传递的数据
        colNames: colNameSlave,
        colModel: colModelSlave,
        autowidth: true,
        height: 250,
        shrinkToFit: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: '#pager_slave',
        sortname: 'item',
        viewrecords: true,
        sortorder: "asc",
        multiselect: true,
        hidegrid: false,
        editurl: "/meta/save",
        jsonReader: {
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: true
        },
        caption: "表字段信息"
    });
    $("#grid_slave").jqGrid('navGrid', '#pager_slave', {
            edit: true,
            add: true,
            del: true,
            search: true,
            refresh: false
        }, {//设置编辑按钮的配置
            editCaption: "编辑",
            recreateForm: true,
            checkOnUpdate: true,
            checkOnSubmit: false, //是否弹出数据改变确认提示框
            closeAfterEdit: true,
            reloadAfterSubmit: true,
            resize: true,
            editData: formObjSlave,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置新增按钮的配置
            closeAfterAdd: true,
            recreateForm: true,
            editData: formObjSlave,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置删除按钮的配置
            delData: formObjSlave,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        });

    //jQuery("#ms1").click(function () {
    //    var s;
    //    s = jQuery("#list10_d").jqGrid('getGridParam', 'selarrrow');
    //    alert(s);
    //});

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper').width();
        $('#grid_master').setGridWidth(width);
        $('#grid_slave').setGridWidth(width);
    });
});