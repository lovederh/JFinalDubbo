/**
 * Created by dsideal-lee on 2016/11/1.
 */

define(function (require) {
    var $ = require('jquery');
    require('jqBootstrapTable');

    var columns = [{
        field: "aa",
        title: "kbkb",
        align: "left",
        sortable: true,
        order: "asc"
    }, {
        field: "bb",
        title: "kbkb",
        align: "left",
        sortable: true,
        order: "asc"
    }];
    var data = [{aa: "3333", bb: "5555"}, {"aa": "2222", bb: "4444"}];
    $("#tableList").bootstrapTable({
        columns: columns,
        data: data,
        striped: true,
        sortname: "aa",   //排序列
        sortOrder: "asc",
        method: "get",
        url: "",
        contentType: "application/json",
        dataType: "json",
        ajaxOptions: {},
        pagination: true, //是否显示分页条
        iconSize: 'outline',
        icons: {
            columns: 'glyphicon-list'
        }
    });
});