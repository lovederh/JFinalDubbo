package edu.dsideal.campus.framework;

import java.util.Arrays;

/**
 * Created by dsideal-lee on 2016/11/28.
 */
public class IndexInterceptorTest {
    private static final String[] PERMISSION_CONTROLLER_KEY = {"/developer", "/meta", "/bo", "/dict", "/update", "/menu"};
    public static void main(String[] args){
        System.out.println(isHavePermission(""));
    }
    private static boolean isHavePermission(String ctrlKey){
        return Arrays.asList(PERMISSION_CONTROLLER_KEY).contains(ctrlKey);
    }
}
