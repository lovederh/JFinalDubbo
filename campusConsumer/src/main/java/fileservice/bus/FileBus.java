package fileservice.bus;

import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import edu.dsideal.campus.common.CommonTools;
import net.coobird.thumbnailator.Thumbnails;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 文件上传操作
 * @author zhaoyou
 */
public class FileBus {
	// 初始化项目路径
	private static String webContextPath = JFinal.me().getServletContext().getRealPath("/");

	/**
	 * 保存上传文件的方法
	 * @param uploadFile	文件对象(构造t_sys_file表数据模型)
	 * @param loginId		当前登陆人
	 * @param selfRelativePath	自定义的文件上传路径
	 * @return
	 */
	public static Record saveUploadFile(UploadFile uploadFile, String loginId, String selfRelativePath) {
		// 文件名处理
		String originalFileName = uploadFile.getOriginalFileName();
		// 文件真实名称
		String real_name = uploadFile.getFileName();
		String file_name = "";
		if(CommonTools.isNotEmpty(real_name)){
			// 文件类型
			String fileType = "";
			if(originalFileName.indexOf(".") >= 0){
				fileType = originalFileName.substring(originalFileName.lastIndexOf(".")+1);
			}
			// 文件进行重命名后结果
			file_name = rename(originalFileName);
			// 文件全路径(项目+相对路径)
			String fileRootPath = webContextPath + selfRelativePath;
			File fileRootFile = new File(fileRootPath);
			if(!fileRootFile.exists()){
				fileRootFile.mkdirs();
			}
			// 生成文件的全名, 存到服务器中
			String qualifiedName = fileRootPath + "\\" + file_name;
			File saveSeverFile = new File(qualifiedName);
			// 完成文件上传操作
			uploadFile.getFile().renameTo(saveSeverFile);
			
			// 文件信息存入数据库
			String fileId = UUID.randomUUID().toString();
			Record fileInfo = new Record();
			fileInfo.set("id", fileId);
			fileInfo.set("username", loginId);
			fileInfo.set("upload_date", new Date());// 上传时间
			fileInfo.set("file_type", fileType);// 文件类型
			fileInfo.set("file_size", saveSeverFile.length());// 文件大小
			fileInfo.set("file_name", file_name);// 保存在服务器中的文件名
			// 去除文件名中所有空格, 防止出现下载时文件名转码问题
    		real_name = real_name.replace(" ", "");
			fileInfo.set("real_name", real_name);// 文件本名
			fileInfo.set("path", selfRelativePath);
			fileInfo.set("del_flag", "0");
			Db.save("t_sys_file", "id", fileInfo);
			return fileInfo;
		}
		return null;
	}
	
	/**
	 * 保存上传文件的方法, 同时在对应路径生成缩略图的方法
	 * @param uploadFile	文件对象(构造t_sys_file表数据模型)
	 * @param loginId		当前登陆人
	 * @param selfRelativePath	自定义的文件上传路径
	 * @param thumbnailWidth	生成缩略图宽度
	 * @param thumbnailHeight	生成缩略图高度
	 * @return
	 */
	public static Record saveUploadFileAndCreateThumbnail(UploadFile uploadFile, String loginId, String selfRelativePath, 
			int thumbnailWidth, int thumbnailHeight) {
		try {
			// 文件名处理
			String originalFileName = uploadFile.getOriginalFileName();
			// 文件真实名称
			String real_name = uploadFile.getFileName();
			String file_name = "";
			if(CommonTools.isNotEmpty(real_name)){
				// 文件类型
				String fileType = "";
				if(originalFileName.indexOf(".") >= 0){
					fileType = originalFileName.substring(originalFileName.lastIndexOf(".")+1);
				}
				// 文件进行重命名后结果
				file_name = rename(originalFileName);
				// 文件全路径(项目+相对路径)
				String fileRootPath = webContextPath + selfRelativePath;
				File fileRootFile = new File(fileRootPath);
				if(!fileRootFile.exists()){
					fileRootFile.mkdirs();
				}
				// 生成文件的全名
				String qualifiedName = fileRootPath + "\\" + file_name;
				// 完成文件上传操作
				File saveQualifiedFile = new File(qualifiedName);
				uploadFile.getFile().renameTo(saveQualifiedFile);
				
				// 执行生成缩略图操作
				thumbnailWidth = thumbnailWidth==0?  150: thumbnailWidth;
				thumbnailHeight = thumbnailHeight==0?  150: thumbnailHeight;
				// 缩略图存放路径
				String thumbnailPath = selfRelativePath + "/thumbs";
				File thumbnailRootFile = new File(webContextPath + thumbnailPath);
				if (!thumbnailRootFile.exists()) {
					thumbnailRootFile.mkdirs();
				}
				// 生成缩略图
				String thumbnailQualifiedName = webContextPath + thumbnailPath + "\\" + file_name;
				Thumbnails.of(saveQualifiedFile).size(thumbnailWidth, thumbnailHeight).toFile(thumbnailQualifiedName);
				
				// 文件信息存入数据库
				String fileId = UUID.randomUUID().toString();
				Record fileInfo = new Record();
				fileInfo.set("id", fileId);
				fileInfo.set("username", loginId);
				fileInfo.set("upload_date", new Date());
				fileInfo.set("file_type", fileType);
				fileInfo.set("file_size", saveQualifiedFile.length());// 文件大小
				fileInfo.set("file_name", file_name);
				// 去除文件名中所有空格, 防止出现下载时文件名转码问题
	    		real_name = real_name.replace(" ", "");
				fileInfo.set("real_name", real_name);
				fileInfo.set("path", selfRelativePath);
				fileInfo.set("thumbnail_path", thumbnailPath);
				fileInfo.set("del_flag", "0");
				Db.save("t_sys_file", "id", fileInfo);
				return fileInfo;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 上传文件后下载请求
	 */
    public static String download(HttpServletResponse response, String id) {
        if(CommonTools.isEmpty(id)){
    		return "请选择您要下载的文件!";
        }
    	Record fileInfo = Db.findById("t_sys_file", "id", id);
    	if(fileInfo == null){
    		return "对不起, 您下载的文件已丢失!";
    	}
    	String file_name = fileInfo.getStr("file_name");
    	String real_name = fileInfo.getStr("real_name");
    	// 相对路径
    	String selfRelativePath = fileInfo.getStr("path");
		if(CommonTools.isEmpty(file_name) || CommonTools.isEmpty(real_name)){
			return "对不起, 您下载的文件已丢失!";
		}
    	
		FileInputStream in = null;
		OutputStream out = null;
    	try {
    		// 文件全路径(项目+相对路径)
    		String fileRootPath = webContextPath + selfRelativePath;
    		File file = new File(fileRootPath + "\\" + file_name);
    		if(file==null || file.isDirectory() || !file.exists()){
    			return "对不起, 您下载的文件已丢失!";
			}
    		// 设置输出的格式
    		response.reset();
    		// 防止浏览器兼容性问题, 声明内容类型为zip型
    		response.setContentType("application/zip");
    		response.setCharacterEncoding("UTF-8");
    		// 去除文件名中所有空格, 防止出现下载时文件名转码问题
    		real_name = real_name.replace(" ", "");
    		response.setHeader("Content-Disposition", "attachment; filename=" + new String(real_name.getBytes("GBK"), "iso8859-1"));
			
    		in = new FileInputStream(file);
			out = response.getOutputStream();
			
			byte[] buffer = new byte[1024];
			int len=0;
			while((len=in.read(buffer))>=0){
				out.write(buffer, 0, len);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
    		return "对不起, 文件下载处理出错, 请重新下载!";
		}finally{
			if(null != in){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally{
					in = null;
				}
			}
			if(null != out){
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally{
					out = null;
				}
			}
		}
    }
    
    /**
	 * 删除单个文件的方法
	 * @param id		要删除的文件ID
	 * @return
	 */
	public static void delSingleFile(String id){
		if(CommonTools.isEmpty(id)){
    		return;
        }
		// 根据ID, 查找一条记录
		Record fileInfo = Db.findById("t_sys_file", "id", id);
		if(null==fileInfo){
			return;
		}
		delFileByFileInfo(fileInfo);
	}
	
	/**
	 * 根据文件信息Record, 删除服务器中指定文件
	 */
	public static void delFileByFileInfo(Record fileInfo){
		String file_name = fileInfo.getStr("file_name");
    	// 相对路径
    	String selfRelativePath = fileInfo.getStr("path");
    	// 删除原文件
		if(CommonTools.isNotEmpty(file_name) && CommonTools.isNotEmpty(selfRelativePath)){
			// 文件全路径(项目+相对路径)
    		String fileRootPath = webContextPath + selfRelativePath;
    		File file = new File(fileRootPath + "\\" + file_name);
    		// 删除
			if(file.exists() && file.isFile()){
				file.delete();
			}
		}
		// 删除缩略图文件
		String thumbnail_path = fileInfo.getStr("thumbnail_path");
		if(CommonTools.isNotEmpty(file_name) && CommonTools.isNotEmpty(thumbnail_path)){
			// 缩略图文件全路径(项目+缩略图相对路径)
    		String thumbnailRootFile = webContextPath + thumbnail_path;
    		File thumbnail = new File(thumbnailRootFile + "\\" + file_name);
    		// 删除
			if(thumbnail.exists() && thumbnail.isFile()){
				thumbnail.delete();
			}
		}
		// 最终删除掉文件数据表中的数据
		Db.delete("t_sys_file", fileInfo);
	}
	
	/**
	 * 将上传的文件进行重命名
	 */
	private static String rename(String originalFilename) {
		Long now = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		Long random = (long) (Math.random() * now);
		String fileName = now + "_" + random;
		
		if (originalFilename.indexOf(".") != -1) {
			fileName += originalFilename.substring(originalFilename.lastIndexOf("."));
		}
		return fileName;
	}


}
