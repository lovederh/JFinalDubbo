package fileservice.config;

import com.jfinal.config.Routes;
import fileservice.controller.FileController;
import fileservice.controller.UeditorController;

/**
 * 2017/1/14 0014
 */
public class FileServiceConfig extends Routes {
    @Override
    public void config() {
        add("/ueditor", UeditorController.class, "/WEB-INF/view");
        add("/file", FileController.class, "/WEB-INF/view");
    }
}
