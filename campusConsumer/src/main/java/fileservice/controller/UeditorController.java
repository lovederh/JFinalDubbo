package fileservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.render.JsonRender;
import com.jfinal.upload.UploadFile;
import fileservice.bus.FileBus;
import fileservice.plugin.ueditor.MyConfigManager;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ueditor使用支撑
 * 来源: 项目移植
 * @author zhaoyou
 */
public class UeditorController extends Controller {
	// 相对路径
	private static final String selfRelativePath = "/upload/ueditor";
	
	public static final int CONFIG = 0;
	public static final int UPLOAD_IMAGE = 1;
	public static final int UPLOAD_SCRAWL = 2;
	public static final int UPLOAD_VIDEO = 3;
	public static final int UPLOAD_FILE = 4;
	public static final int CATCH_IMAGE = 5;
	public static final int LIST_FILE = 6;
	public static final int LIST_IMAGE = 7;
	
	public static final Map<String, Integer> mapping;
	
	static {
		mapping = new HashMap<String, Integer>();
		mapping.put("config", CONFIG);
		mapping.put("uploadimage", UPLOAD_IMAGE);
		mapping.put("uploadscrawl", UPLOAD_SCRAWL);
		mapping.put("uploadvideo", UPLOAD_VIDEO);
		mapping.put("uploadfile", UPLOAD_FILE);
		mapping.put("catchimage", CATCH_IMAGE);
		mapping.put("listfile", LIST_FILE);
		mapping.put("listimage", LIST_IMAGE);
	}
	
	// 实例化编辑器对象时, 使用个性化serverUrl时发送的请求
	public void server() {
		String action = getPara("action");
		int actionCode = mapping.get(action);
		
		HttpServletRequest request = getRequest();
		// 这个是处理jsonp的形式, 一般都是不跨域的
	    String callbackName = getPara("callback");
	    if (callbackName != null){
	    	if (!validCallbackName(callbackName)) {
	    		JSONObject json = new JSONObject();
	    		json.put("state", "callback参数名不合法");
	    		render(new JsonRender(json.toJSONString()).forIE());
	    	}  
	    	render(new JsonRender(callbackName + "(" + actionjson(actionCode,request) + ");").forIE());
	    }
	    render(new JsonRender(actionjson(actionCode,request)).forIE());
    }
	
	
	public String actionjson(int actionCode,HttpServletRequest request) {
		// 当前登陆人ID
		String loginId = getSessionAttr("loginId");
		// 返回的json结果
		JSONObject json = new JSONObject();
		switch (actionCode) {
			case 0:{
				String rootPath = request.getContextPath();
				String contextPath = request.getContextPath();
				String uri = request.getRequestURI();
				MyConfigManager myConfigManager = MyConfigManager.getInstance(rootPath, contextPath, uri);
				return myConfigManager.getAllConfig().toString();
			}
			case 1:
			case 2:
			case 3:
			case 4:{
				List<UploadFile> uploadFileList = this.getFiles();
				for(UploadFile uploadFile : uploadFileList){
					String fileId = FileBus.saveUploadFile(uploadFile, loginId, selfRelativePath).get("id");
					// 返回结果
			    	if(null != fileId){
			    		String fileName = uploadFile.getFileName();
			    		json.put("state", "SUCCESS");
			    		json.put("url", request.getContextPath() + "/ueditor/download?id=" + fileId);
			    		json.put("title", fileName);
			    		json.put("original", fileName);
			    		return json.toJSONString();
			    	}
				}
			}
			case 5:
				break;
			case 6:
			case 7:
		}
		return json.toJSONString();
	}
	
    /**
     * 打开富文本编辑器对应页面操作
     */
	public void beginEdit(){
		render("/bizroot/sys/common/ueditorPage.jsp");
	}
    
	private boolean validCallbackName(String name){
		if (name.matches("^[a-zA-Z_]+[\\w0-9_]*$")) {
			return true;
		}
		return false;
	}
}
