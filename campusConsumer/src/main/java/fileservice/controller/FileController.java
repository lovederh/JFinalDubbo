package fileservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import fileservice.service.IFileService;
import net.coobird.thumbnailator.Thumbnails;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 2017/2/8 0008
 */
@Before(IocInterceptor.class)
public class FileController extends Controller {
    @Inject.BY_NAME
    IFileService fileService;
    // 初始化项目路径
    private static String webContextPath = JFinal.me().getServletContext().getRealPath("/");

    private static String PHOTO_TYPE = "jpg,gif,jpeg,jpe,png,bmp";

    /**
     * 文件下载请求(根据文件表中数据ID下载)
     * url中请求参数:	id 文件表中主键ID
     */
    public void downloadbyId() {
        String id = this.getPara("id");
        // 调用下载方法
        String resultMsg = null;

        HttpServletResponse response = this.getResponse();

        Record fileInfo = fileService.findDownloadFile(id);
        if (fileInfo != null) {
            String file_name = fileInfo.getStr("fileName");
            String real_name = fileInfo.getStr("realName");
            // 相对路径
            String selfRelativePath = fileInfo.getStr("path");
            if (CommonTools.isNotEmpty(file_name) && CommonTools.isNotEmpty(real_name)) {
                FileInputStream in = null;
                OutputStream out = null;
                try {
                    // 文件全路径(项目+相对路径)
                    String fileRootPath = webContextPath + selfRelativePath;
                    File file = new File(fileRootPath + "\\" + file_name);
                    if (file.isFile() && file.exists()) {
                        // 设置输出的格式
                        response.reset();
                        // 防止浏览器兼容性问题, 声明内容类型为zip型
                        response.setContentType("application/zip");
                        response.setCharacterEncoding("UTF-8");
                        // 去除文件名中所有空格, 防止出现下载时文件名转码问题
                        real_name = real_name.replace(" ", "");
                        response.setHeader("Content-Disposition", "attachment; filename=" + new String(real_name.getBytes("GBK"), "iso8859-1"));

                        in = new FileInputStream(file);
                        out = response.getOutputStream();

                        byte[] buffer = new byte[1024];
                        int len = 0;
                        while ((len = in.read(buffer)) >= 0) {
                            out.write(buffer, 0, len);
                        }
                        renderNull();
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    resultMsg = "对不起, 文件下载处理出错, 请重新下载!";
                } finally {
                    if (null != in) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            in = null;
                        }
                    }
                    if (null != out) {
                        try {
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            out = null;
                        }
                    }
                }

                resultMsg = "对不起, 您下载的文件已丢失!";

                // 未找到相应文件, 以错误形式返回
                setAttr("errorMsg", resultMsg);
                setAttr("errorDesc", "可能原因：<br/>（1）文件上传时出错；<br/>（2）文件意外丢失；<br/>（3）你没有下载文件的权限。");
                render("/WEB-INF/view/errorMsg.html");
            }
        }
    }


    /**
     * 批量上传文件, 前端uploadify插件支持
     * 依据表名, 是否生成缩略图标识(缩略图宽度/缩略图高度), 完成文件上传保存
     * 返回关键信息: 文件ID, 文件前端展示的src
     */
    public void uploadifyFileList() throws IOException {
        LoginUserObj loginUserObj = getSessionAttr("loginUserObj");
        // 获取提交的所有数据
        List<UploadFile> uploadFileList = this.getFiles();
        // 返回的json结果
        JSONObject json = new JSONObject();
        // 当前登陆人ID
        String loginId = loginUserObj.getLoginId();
        String selfRelativePath = "upload";
        // 项目路径, 用于拼接前端img标签的src属性
        String ctx = getRequest().getContextPath();
        // 原图片对应src
        StringBuilder natureSrcSb;
        // 缩略图对应src
        StringBuilder thumbnailSrcSb;
        // 文件信息Record
        Record fileInfo;
        // 文件名处理
        String originalFileName;
        // 文件真实名称
        String real_name;
        String file_name;
        String fileId;
        String fileType = "";
        // 文件全路径(项目+相对路径)
        String fileRootPath = webContextPath + selfRelativePath;
        for (UploadFile uploadFile : uploadFileList) {
            originalFileName = uploadFile.getOriginalFileName();
            real_name = uploadFile.getFileName();
            if (CommonTools.isNotEmpty(real_name)) {
                // 文件类型
                if (originalFileName.indexOf(".") >= 0) {
                    fileType = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
                }
                // 文件进行重命名后结果
                file_name = rename(originalFileName);

                File fileRootFile = new File(fileRootPath);
                if (!fileRootFile.exists()) {
                    fileRootFile.mkdirs();
                }
                // 生成文件的全名, 存到服务器中
                String qualifiedName = fileRootPath + "\\" + file_name;
                File saveSeverFile = new File(qualifiedName);
                // 完成文件上传操作
                uploadFile.getFile().renameTo(saveSeverFile);
                fileId = UUID.randomUUID().toString();
                fileInfo = new Record();

                if (PHOTO_TYPE.contains(fileType)) {
                    int width = 150;
                    int height = 150;
                    // 完成文件上传操作
                    File saveQualifiedFile = new File(qualifiedName);
                    uploadFile.getFile().renameTo(saveQualifiedFile);

                    // 缩略图存放路径
                    String thumbnailPath = selfRelativePath + "/thumbs";
                    File thumbnailRootFile = new File(webContextPath + thumbnailPath);
                    if (!thumbnailRootFile.exists()) {
                        thumbnailRootFile.mkdirs();
                    }
                    // 生成缩略图
                    String thumbnailQualifiedName = webContextPath + thumbnailPath + "\\" + file_name;
                    Thumbnails.of(saveQualifiedFile).size(width, height).toFile(thumbnailQualifiedName);
                    fileInfo = fileService.saveUploadFileAndCreateThumbnail(fileInfo, fileId, loginId, fileType, saveSeverFile, file_name, real_name, selfRelativePath, thumbnailPath, width, height);
                } else {
                    fileInfo = fileService.saveUploadFile(fileInfo, fileId, loginId, fileType, saveSeverFile, file_name, real_name, selfRelativePath);
                }

                // 返回结果
                if (null != fileInfo) {
                    // 文件ID
                    json.put("id", fileInfo.get("id"));
                    // 原图片对应src拼接
                    //String fileName = fileInfo.getStr("fileName");
                    //natureSrcSb = new StringBuilder(ctx);
                    //natureSrcSb.append(fileInfo.getStr("path")).append("/");
                    //natureSrcSb.append(fileName);
                    //json.put("natureSrc", natureSrcSb.toString());
                    // 缩略图对应src拼接
                    //thumbnailSrcSb = new StringBuilder(ctx);
                    //thumbnailSrcSb.append(fileInfo.getStr("thumbnailPath")).append("/");
                    //thumbnailSrcSb.append(fileName);
                    //json.put("thumbnailSrc", thumbnailSrcSb.toString());
                    //json.put("fileName", fileName);
                    break;
                }
            }
        }
        renderJson(json);
    }

    /**
     * 将上传的文件进行重命名
     */
    private static String rename(String originalFilename) {
        Long now = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
        Long random = (long) (Math.random() * now);
        String fileName = now + "_" + random;

        if (originalFilename.indexOf(".") != -1) {
            fileName += originalFilename.substring(originalFilename.lastIndexOf("."));
        }
        return fileName;
    }


    /**
     * 删除已上传文件
     */
    public void delFile() {
        renderJson(fileService.delFile(getPara("id")));
    }

}
