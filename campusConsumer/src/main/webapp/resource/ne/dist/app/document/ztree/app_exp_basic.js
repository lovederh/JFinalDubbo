define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');

    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');


    var $tree, $grid = $('#js_grid');

    var jsonPath = Nenu.CONSTANT.PATH.JSON;


    var fnTable = function () {


        /*async: {
         enable: true,
         type: 'get',
         contentType: 'application/json',
         url: '/json/ztree/getRootData.json',
         autoParam: ['id']
         },*/

        /* $tree = $('#treeDemo').neTree({
         setting: {
         async: {
         enable: true,
         url: '/json/ztree/getRootData.json'
         },
         callback: {
         onExpand: function () {
         console.log(1);
         },
         onClick: function () {
         console.log(222);

         }
         }
         },
         data: [{
         "id": "00",
         "name": "根目录",
         "isParent": true
         }]

         });*/


        $tree = $('#js_tree').neTree({

            // ztree的配置参数 setting 完全按照ztree原生组件的参数设置即可
            setting : {

                /**
                 * 异步加载树
                 *    enable : boolean  默认   false   不开启异步加载
                 *       url : string   默认   ''      数据的地址

                 注: 其他参数请参考官方文档
                 */
                async : {
                    enable : true,
                    url    : jsonPath+'ztree/getRootData.json'
                },
                check: {
                    enable: true
                },
                //回调函数参数对象
                callback : {
                    /**
                     * 单击节点事件
                     *    event : object    默认   eventObject   标准的 js event 对象
                     *   treeId : string    默认   ''            对应 zTree 的 treeId，便于用户操控
                     treeNode : JSON      默认   jsonObject    被点击的节点 JSON 数据对象

                     */
                    onClick : function (event, treeId, treeNode) {
                        console.log('单击事件触发!');

                    }
                }
            },

            //自定义参数,ztree使用的数据非ajax
            data : [{
                "id"       : "00",
                "name"     : "根目录",
                "isParent" : true
            }]

        });


        $.neOn([
            {
                sel   : '#js_tree_refresh',
                eFunc : function () {
                    $tree.neTree('refreshRoot');
                }
            }

        ]);


        $grid.neTable({

            nenu  : {

                query : '#js_grid_search_form',

                toobarBtn : {

                    search : {
                        switch : true
                    },

                    add : {
                        switch   : true,
                        callBack : function () {
                            console.log('添加按钮');
                        }
                    },

                    delete : {
                        switch    : true,
                        callBack  : function (e, arrySelectDataByID) {
                            console.log('删除按钮');
                        },
                        checkBack : function (arrySelectDataByID) {

                        }
                    },

                    edit : {
                        switch   : true,
                        callBack : function () {
                            console.log('修改按钮');
                        }
                    }

                },
                rowBtn    : {
                    list : [
                        {
                            title    : '添加',
                            text     : '添加',
                            icon     : 'glyphicon glyphicon-plus',
                            cls      : 'add',
                            callBack : function (e, value, row, index) {
                                console.log('添加');
                            }
                        },
                        {
                            title    : '修改按钮喔!',
                            text     : '修改',
                            icon     : 'glyphicon glyphicon-pencil',
                            cls      : 'edit',
                            callBack : function (e, value, row, index) {
                                console.log('修改');
                            }
                        }
                    ]
                }

            },
            table : {

                url : jsonPath+"bootstrapTable/base.json", //ajax url

                columns : [
                    {
                        checkbox : true,
                        field    : 'state'
                    }, {
                        field : 'id',
                        title : 'ID'

                    }, {
                        field : 'name',
                        title : '名称'

                    }, {
                        field : 'price',
                        title : '价格'
                    }
                ]
            }
        });
    };


    $('body').layout({
        name            : 'manLayout',
        west            : {
            size     : 250,
            children : {
                name : 'treeLayout'

            }
        },
        applyDemoStyles : false,
        closable        : false,
        spacing_open    : 15,
        onresize_end    : function () {

        },
        onload_end      : fnTable

    });

});