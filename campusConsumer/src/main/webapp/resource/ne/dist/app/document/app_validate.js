define(function (require) {

    var $ = require('jquery');
    require('jqValidate');


    var jottedTools = require('md');
    //初始化
    jottedTools.init({
            height     : '40em',
            prefix     : 'validate/',
            filePath   : Nenu.CONSTANT.PATH.BASE
            , rootPath : Nenu.CONSTANT.PATH
            , list     : [
                {
                    el  : 'js_rules',
                    tpl : {
                        html : 'rules',
                        css  : 'rules_css',
                        js   : 'rules_js'
                    }
                },
                {
                    el  : 'js_ajax',
                    tpl : {
                        html : 'ajax',
                        css  : 'ajax_css',
                        js   : 'ajax_js'
                    }
                },
                {
                    el  : 'js_ajax2',
                    tpl : {
                        html : 'ajax_btn',
                        css  : 'ajax_btn_css',
                        js   : 'ajax_btn_js'
                    }
                }
            ]
        }
    );


});










