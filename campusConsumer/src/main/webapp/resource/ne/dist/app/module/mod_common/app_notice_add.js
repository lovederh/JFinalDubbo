define(function (require) {
    var winTop = window.top;
    var $ = require('jquery'),
        bootstrap = require('bootstrap');

   
    require('jqValidate');

    require('jqLayerDate');

    //  console.log(Nenu.getParameterData());
    laydate({
        elem: '#js_nAddDate',
        format: 'YYYY-MM-DD hh:mm:ss',
        min: laydate.now(),
        istime: true,
        start: laydate.now()
    });


    $('#js_noticeAddForm').validate();


});


