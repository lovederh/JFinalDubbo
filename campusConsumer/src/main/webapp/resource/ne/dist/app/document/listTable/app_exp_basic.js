define ( function ( require ) {
    var $ = require ( 'jquery' );
    require ( 'bootstrap' );

    require ( 'jqDualListTable' );

    var jsonPath = Nenu.CONSTANT.PATH.JSON;

    var json = require ( 'json!jsonPath/bootstrapTable/base.json' );

    $ ( '#js_dual_list_table' ).neDualListTable ({
        data : json
        ,
        neTable:{
            table:{
                columns         : [

                    {
                        field : 'name' ,
                        title : '名称'
                    }

                ]
            }
        }
    } );
    
} );