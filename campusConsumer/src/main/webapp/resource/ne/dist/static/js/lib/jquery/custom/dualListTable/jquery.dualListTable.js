/*
 * @extends jquery.1.11 +
 * @extends bootstrap table 1.11.0
 * @fileOverview neTable
 * @author FZM
 * @version 1.11.2
 * @date 2017-02-09
 * Copyright (c) 2012 FZM
 */
!(function ( root , factory ) {
    'use strict';
    if ( typeof define === 'function' ) {
        if ( define.amd ) {
            // AMD
            define ( ['jquery' , 'lodash' , 'jqBootstrapTable'] , factory );
        }
        if ( define.cmd ) {
            // CMD
            define ( factory );
        }

    } else if ( typeof exports === 'object' ) {
        // Node, CommonJS之类的
        //   module.exports = factory(require('jquery'), require('underscore'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory ( root.jQuery );
    }
} ( this , function ( $ , _ ) {
    'use strict';
    var datakey = 'ne_dualListTable' ,

        columns = [
            {
                checkbox : true ,
                field    : 'state'
            }
        ] ,

    // 默认参数
        defaults = {
            data      : [] ,
            leftData  : [] ,
            rightData : [] ,
            filter    : 'id' ,
            neTable   : {

                nenu  : {
                    fit : 'position'
                } ,
                table : {
                    pagination      : false ,
                    showColumns     : false ,
                    search          : true
                    , rowAttributes : function ( row , index ) {
                        return row.state = false;
                    }
                }
            }
        };

    $.fn.neDualListTable = function ( settings ) {

        var run = $.type ( settings ) === 'string' ,
            args = [].slice.call ( arguments , 1 ) ,
            options = $.extend ( true , {} , defaults ) ,
            $element ,
            instance;

        if ( run && run[0] !== '_' ) {
            if ( !this.length ) return;
            $element = $ ( this[0] );
            instance = $element.data ( datakey );
            if ( !instance ) $element.data ( datakey , instance = new Constructor ( $element[0] , options )._init () );
            return Constructor.prototype[settings] ? Constructor.prototype[settings].apply ( instance , args ) : undefined;
        }
        else if ( !run ) {

            options = $.extend ( true , options , settings );

            var isCheck = _.findIndex ( _.get ( settings , 'neTable.table.columns' ) , function ( o ) {
                return o.field === 'state';
            } );
            if ( isCheck < 0 ) {
                _.set ( options , 'neTable.table.columns' , _.concat ( columns , _.get ( options , 'neTable.table.columns' ) ) );
            }
        }

        return this.each ( function () {
            var element = this ,
                instance = $ ( element ).data ( datakey );
            if ( !instance ) {
                $ ( element ).data ( datakey , instance = new Constructor ( element , options )._init () );
            }
        } );
    };

// 暴露插件的默认配置
    $.fn.neDualListTable.defaults = defaults;

// 构造函数
    function Constructor ( element , options ) {
        var the = this;
        the.$el = $ ( element );
        the.id = element.id;
        the.options = options;
    }

// 原型方法，驼峰写法
    Constructor.prototype = {
        /**
         * 初始化
         * @return this
         * @version 1.0
         */
        _init : function () {

            var _this = this;
            var $this = this.$el;
            var opt = this.options;
            var dom = this.dom = {
                left    : {
                    $el : $this.find ( '.jc-dual-list-left' )
                }
                , right : {
                    $el : $this.find ( '.jc-dual-list-right' )
                }
                , arrw  : {
                    $el : $this.find ( '.jc-dual-list-arrw' )
                }
            };
            //
            dom.left.$resetBtn = dom.left.$el.find ( '.jc-reset-btn' );
            dom.left.$query = dom.left.$el.find ( '.jc-query' );
            dom.left.$table = dom.left.$el.find ( '.jc-table' );
            //
            dom.right.$resetBtn = dom.right.$el.find ( '.jc-reset-btn' );
            dom.right.$query = dom.right.$el.find ( '.jc-query' );
            dom.right.$table = dom.right.$el.find ( '.jc-table' );
            //
            dom.arrw.$rightBtn = dom.arrw.$el.find ( '.jc-right-btn' );
            dom.arrw.$leftBtn = dom.arrw.$el.find ( '.jc-left-btn' );

            dom.left.$table.neTable ( (opt.neTable.table.toolbar = '#' + _this.id + ' .jc-dual-list-left .jc-table-toobar' , opt.neTable) );
            dom.right.$table.neTable ( (opt.neTable.table.toolbar = '#' + _this.id + ' .jc-dual-list-right .jc-table-toobar' , opt.neTable) );

            var cache = this.cache = {
                data  : opt.data ,
                left  : _.isEmpty ( opt.leftData ) ? _this._getFillterData ( opt.data , opt.rightData ) : opt.leftData

            };

            cache.right =_.isEmpty ( opt.rightData ) ? _this._getFillterData ( opt.data , cache.left ) : opt.rightData;


            this.leftMethod ( 'load' , cache.left );
            this.rightMethod ( 'load' , cache.right );

            //左边设置到右边
            dom.arrw.$rightBtn.on ( 'click' , function () {
                /* var checkData = _this.leftMethod ( 'getSelections' );
                 if ( _.isEmpty ( checkData ) )return;
                 //
                 cache.right = _this._getConcatData ( cache.right , checkData );
                 cache.left = _this._getFillterData ( _.isEmpty ( cache.data ) ? cache.left : cache.data , cache.right );
                 //
                 _this.leftMethod ( 'load' , cache.left );
                 _this.rightMethod ( 'load' , cache.right );*/
                _this._moveData ( 'left' );

            } );

            //右边设置到左边
            dom.arrw.$leftBtn.on ( 'click' , function () {

                /*  var checkData = _this.rightMethod ( 'getSelections' );
                 if ( _.isEmpty ( checkData ) )return;
                 //
                 cache.left = _this._getConcatData ( cache.left , checkData );
                 cache.right = _this._getFillterData ( _.isEmpty ( cache.data ) ? cache.right : cache.data , cache.left );
                 //
                 _this.leftMethod ( 'load' , cache.left );
                 _this.rightMethod ( 'load' , cache.right );*/
                _this._moveData ( 'right' );
            } );

            //重置按钮
            dom.left.$resetBtn.on ( 'click' , function () {
                _this.leftMethod ( 'resetSearch' , '' );
            } );
            dom.right.$resetBtn.on ( 'click' , function () {
                _this.rightMethod ( 'resetSearch' , '' );
            } );
            return this;
        }

        /**
         * 获取过滤的数据
         * @param data
         * @param pullData
         * @returns {*}
         * @private
         */
        , _getFillterData : function ( data , pullData ) {
            var filter = this.options.filter;
            return _.sortBy ( _.pullAllBy ( _.cloneDeep ( data ) , pullData , filter ) , [
                function ( o ) {
                    return o[filter];
                }
            ] );
        }

        /**
         * 获取组合过的数据
         * @returns {*}
         * @private
         */
        , _getConcatData : function () {
            var filter = this.options.filter;
            return _.sortBy ( _.concat.apply ( null , arguments ) , [
                function ( o ) {
                    return o[filter];
                }
            ] );
        }

        /**
         * 移动数据
         * @param type    String: left,right
         * @private
         */
        , _moveData   : function ( type ) {

            var oType = {
                left    : ['left' , 'right']
                , right : ['right' , 'left']
            };
            if ( !(oType = oType[type]) )return;
            var cache = this.cache;

            var checkData = this[oType[0] + 'Method'] ( 'getSelections' );
            if ( _.isEmpty ( checkData ) )return;
            //
            cache[oType[1]] = this._getConcatData ( cache[oType[1]] , checkData );
            cache[oType[0]] = this._getFillterData ( _.isEmpty ( cache.data ) ? cache[oType[0]] : cache.data , cache[oType[1]] );
            //
            this.leftMethod ( 'load' , cache.left );
            this.rightMethod ( 'load' , cache.right );

        }
        /**
         * 左侧bootstrap table 使用官方组件提供的方法
         * @returns {*}
         */
        , leftMethod  : function () {
            var neTable = this.dom.left.$table.data ( 'ne_table' );
            return neTable['method'].apply ( neTable , arguments );

        }
        /**
         * 右侧bootstrap table 使用官方组件提供的方法
         * @returns {*}
         */
        , rightMethod : function () {
            var neTable = this.dom.right.$table.data ( 'ne_table' );
            return neTable['method'].apply ( neTable , arguments );
        }
        /**
         * 设置数据
         * @param object
         *  {
         *  data:  全部数据
         *  right: 右边数据
         *  left:  左边数据
         *  }
         */
        , setData     : function ( object ) {

            var cache = this.cache;
            var opt = this.options;
            //
            cache.data = this.options.data = object.data || [];
            cache.right = this.options.rightData = object.right || cache.right;
            cache.left = _.pullAllBy ( _.cloneDeep ( cache.data ) , cache.right , opt.filter );

            //
            this.leftMethod ( 'resetSearch' , '' );
            this.rightMethod ( 'resetSearch' , '' );
            this.leftMethod ( 'load' , cache.left );
            this.rightMethod ( 'load' , cache.right );

        }

        /**
         * 获取当前数据
         * @param type  left,right
         * @returns {Array|*}
         */
        , getData : function ( type ) {
            var type = type || 'right';
            var data = this.cache.right;
            if ( type == 'left' ) {
                data = this.cache.left;
            }
            return data;
        }

    };

} ))
;