/**
 * Created by dsideal-lee on 2016/11/17.
 */
;!function (window, undefined) {
window.layUtil = {
    layFrame: function (url,title, width, height) {
        var defWidth = width ? width : '600px';
        var defHeight = height ? height : '500px';
        layer.open({
            type: 2,
            title: title,
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: [defWidth, defHeight],
            content: url,
            end: function () {
                console.log("关闭后执行的？");
            }
        });
    }
    ,
    layAjax: function (url, formData) {
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            async: false,
            data: formData,
            success: function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    layer.msg(msg, {
                        icon: 1,
                        btn: ['确定']
                    }, function () {
                        //关闭layer弹出的iframe
                        var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                        parent.layer.close(index);
                    });
                } else {
                    layer.msg(msg, {
                        icon: 5,
                        time: 2000,//2s后自动关闭
                        btn: ['确定']
                    });
                }
            }
        });
    },
    // 处理成功后, 弹出消息提示, 执行回调方法
    layerAlertInfo: function(infoMsg, callBack, funcData){
        layer.msg(infoMsg, {
            icon: 1,
            btn: ['确定']
        }, function(){
            (callBack && typeof(callBack)==="function") && callBack(funcData);
        });
    },
    // 处理失败/错误的点击后, 弹出消息提示, 执行回调方法
    layerAlertError: function(errorMsg, callBack, funcData){
        layer.msg(errorMsg, {
            icon: 5,
            time: 2000,//2s后自动关闭
            btn: ['确定']
        }, function(){
            (callBack && typeof(callBack)==="function") && callBack(funcData);
        });
    },
    // 发送ajax请求, 根据请求结果, 弹出不同类型的结果提示, 并执行相应的回调方法
    layerAjaxCallBack: function (url, urlData, successCallBack, failCallBack) {
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            async: false,
            data: urlData,
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if('200' == status){
                    //操作成功后弹出成功提示窗, 并执行回调方法
                    layUtil.layerAlertInfo(msg, successCallBack, jsonData);
                }else{
                    //操作失败后弹出错误提示窗, 执行回调方法
                    layUtil.layerAlertError(msg, failCallBack, jsonData);
                }
            }
        });
    }
}
}(window);