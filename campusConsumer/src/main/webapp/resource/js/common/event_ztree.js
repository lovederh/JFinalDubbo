/**
 * ztree event
 */


define(function (require) {

    'use strict';

    if (!formObj) {
        $.error('没有定义formObj,请检查!!!');
    }

    //加载组件
    require('jquery');
    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');


    /**
     * options
     *   isExtend boolean 是否合并event对象  默认 true
     *   ztreeID string  ztreeid
     *   grid    jqObj   jquery对象
     */
    return function (options) {
        var isExtend = options.isExtend || true;
        var ztreeID = options.ztree;
        var $grid = options.grid;
        var newCount = 1;
        var oEvent = {

            // 新增节点按钮方法
            operateInsert : function () {
                // 机构树选择的机构节点
                var nodes = $.fn.zTree.getZTreeObj(ztreeID).getSelectedNodes();
                var selectNode = nodes[0];
                if (!nodes.length) {
                    Nenu.toastr.warning("请选择要新增数据的父节点!");
                    return;
                }
                var newNode = {
                    id       : (100 + newCount),
                    pId      : selectNode.id,
                    name     : "新增" + (newCount++),
                    level    : selectNode.level + 1,
                    open     : false,
                    isParent : false,
                    isLeaf   : true
                };
                var addedNode = $.fn.zTree.getZTreeObj(ztreeID).addNodes(selectNode, newNode);
                $.ajax({
                    url     : '/tree/addNode?' + $.param(formObj) + "&" + $.param(newNode),
                    type    : "post",
                    async   : false,
                    success : function (jsonData) {
                        Nenu.toastr.success(jsonData.msg);
                        addedNode[0].id = jsonData.newTreeId;
                        $.fn.zTree.getZTreeObj(ztreeID).updateNode(addedNode[0]);
                        $.fn.zTree.getZTreeObj(ztreeID).selectNode(addedNode[0]);
                        $grid.bootstrapTable('refresh', { url : '/tree/queryGridData?id=' + addedNode[0].id + "&" + $.param(formObj) });
                    }
                });
            }
            // 删除节点按钮方法
            ,
            operateDelete : function () {
                // 机构树选择的机构节点
                var nodes = $.fn.zTree.getZTreeObj(ztreeID).getSelectedNodes();
                // 当前节点
                var selectNode = nodes[0];
                if (!nodes.length) {
                    Nenu.toastr.warning("请选择一个要删除的节点!");
                    return;
                }
                // selectNode.id的值为'0'的话该节点为根节点
                if (selectNode.id == '0') {
                    Nenu.toastr.warning("该节点为根节点不可以删除!");
                    return;
                }
                $.ajax({
                    url     : 'tree/delNode',
                    data    : { dtoName : formObj.dtoName, id : selectNode.id },
                    type    : "post",
                    async   : false,
                    success : function (jsonData) {
                        Nenu.toastr.success(jsonData.msg);
                        $.fn.zTree.getZTreeObj(ztreeID).removeNode(selectNode);
                        $grid.bootstrapTable('refresh', { url : '/tree/queryGridData?id=' + selectNode.id + "&" + $.param(formObj) });
                    }
                });
            },

            // 组织结构管理--新增节点按钮方法
            addNote:function () {
                var newCount = 1;
                var newNum=1;
                // 机构树选择的机构节点
                var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();
                var selectNode = nodes[0];
                if(! nodes.length){
                    Nenu.toastr.warning("请选择要新增数据的父节点!");
                    return;
                }
                var newNode = {
                    id: (100 + newCount),
                    pId: selectNode.id,
                    name: "新增" + (newCount++)
                };
                var addedNode = $.fn.zTree.getZTreeObj('js_tree').addNodes(selectNode, newNode);
                $.ajax({
                    url: '/dept/addDept',
                    data: {
                        dtoName : 't_dept_org',
                        name : '新增'+(newNum++),
                        pId : selectNode.id
                    },
                    type: "post",
                    async: false,
                    success: function (jsonData) {
                        Nenu.toastr.success(jsonData.msg);
                        addedNode[0].id=jsonData.deptDataKeyId;// 等于orgId
                        $.fn.zTree.getZTreeObj('js_tree').updateNode(addedNode[0]);
                        $.fn.zTree.getZTreeObj('js_tree').selectNode(addedNode[0]);
                        $grid.bootstrapTable('refresh',{url:'/dept/queryDeptInfo?orgId='+addedNode[0].id});
                    }
                });
            },
            // 组织结构管理--删除节点按钮方法
            delNote:function () {
                // 机构树选择的机构节点
                var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();
                // 当前节点
                var selectNode = nodes[0];
                if(! nodes.length){
                    Nenu.toastr.warning("请选择要一个节点!");
                    return;
                }
                // selectNode.id的值为'0'的话该节点为根节点
                if(selectNode.pId == '0'){
                    Nenu.toastr.warning("该节点为根节点不可以删除!");
                    return;
                }
                $.ajax({
                    url: '/dept/delDept',
                    data: {dtoName : 't_dept_org', id: selectNode.id},
                    type: "post",
                    async: false,
                    success: function (jsonData) {
                        Nenu.toastr.success(jsonData.msg);
                        $.fn.zTree.getZTreeObj('js_tree').removeNode(selectNode);
                        $grid.bootstrapTable('refresh',{url:'/dept/queryDeptInfo?deptId='+selectNode.id});
                    }
                });
            },

        };
        Nenu.context.event = isExtend ? $.extend({}, Nenu.context.event, oEvent) : oEvent;

    };


});

