define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var onGrid = require("/resource/js/common/event_grid.js");

    var onZtree = require("/resource/js/common/event_ztree.js");

    var $grid = $('#js_grid');
    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        $ztree: '',
        selectedTreeId: '',
        tree: {
            addHoverDom: function () {
                if ($("#diyBtn_"+treeNode.id).length>0) return;
                var editStr = "<span id='diyBtn_space_" +treeNode.id+ "' >&nbsp;</span><select class='selDemo ' id='diyBtn_" +treeNode.id+ "'><option value=1>1</option><option value=2>2</option><option value=3>3</option></select>";
                aObj.after(editStr);
                var btn = $("#diyBtn_"+treeNode.id);
                if (btn) btn.bind("change", function(){alert("diy Select value="+btn.attr("value")+" for " + treeNode.name);});
            },
            removeHoverDom: function () {
                if (treeNode.parentTId && treeNode.getParentNode().id!=1) return;
                if (treeNode.id == 15) {
                    $("#diyBtn1_"+treeNode.id).unbind().remove();
                    $("#diyBtn2_"+treeNode.id).unbind().remove();
                } else {
                    $("#diyBtn_"+treeNode.id).unbind().remove();
                    $("#diyBtn_space_" +treeNode.id).unbind().remove();
                }
            },
            addDiyDom: function () {
                var editStr = "<select class='selDemo' id='diyBtn_" +treeNode.id+ "'><option value=1>1</option><option value=2>2</option><option value=3>3</option></select>";
                aObj.after(editStr);
                var btn = $("#diyBtn_"+treeNode.id);
                if (btn) btn.bind("change", function(){alert("diy Select value="+btn.attr("value")+" for " + treeNode.name);});
            }
        },
        initTreeAndTable: function () {
            var $ztree = $('#js_tree').neTree({
                setting: {
                    async: {
                        enable: true,
                        url: '/tree/initTree?' + $.param(formObj)
                    },
                    view: {
                        addHoverDom: nePageLogic.tree.addHoverDom,
                        removeHoverDom: nePageLogic.tree.removeHoverDom,
                        addDiyDom: nePageLogic.tree.addDiyDom
                    },
                    callback: {
                        onClick: function (event, treeId, treeNode) {
                            nePageLogic.selectedTreeId = treeNode.id;
                            $grid.bootstrapTable('refresh', {url: '/tree/queryGridData?id=' + nePageLogic.selectedTreeId + "&" + $.param(formObj)});
                        }
                    }
                }
            });
            $grid.neTable({
                table: {
                    url: '/tree/queryGridData?id=' + nePageLogic.selectedTreeId + "&" + $.param(formObj), //ajax url
                    method: "get",//请求方式
                    queryParams: function (params) {
                        return {limit: params.limit, offset: params.offset}
                    },
                    dataField: "rows",//服务端返回数据键值 就是说记录放的键值是rows，分页时使用总记录数的键值为total
                    pageNumber: 1,
                    pageSize: 10,
                    dataType: "json",//期待返回数据类型
                    pagination: true,//是否分页
                    sidePagination: "server",
                    pageList: [10, 20, 50, 100, 200, 500],
                    striped: true,
                    columns: JSON.parse(extraObj.columns)
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            onZtree({
                grid: $grid,
                ztree: 'js_tree'
            });
            onGrid({
                grid: $grid
            });
        },
        end: function () {
            Nenu.open.end = function () {
                //父页面表格刷新
                $('#js_grid').bootstrapTable("refresh", {url: '/tree/queryGridData?id=' + nePageLogic.selectedTreeId + "&" + $.param(formObj)});
            };
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();
    $('body').layout({
        name: 'manLayout',
        west: {
            size: 250,
            children: {
                name: 'treeLayout'

            }
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: nePageLogic.initTreeAndTable
    });
});