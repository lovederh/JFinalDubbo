define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var onGrid = require("/resource/js/common/event_grid.js");

    var onZtree = require("/resource/js/common/event_ztree.js");

    var $grid = $('#js_grid');

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        selectedTreeId: -1,
        dtoObj : {
            dtoName: formObj.boName,
            domain: formObj.domain
        },
        fnTable: function () {


            var $tree = $('#js_tree').neTree({

                setting: {
                    async: {
                        enable: true,
                        url: '/tree/initTree?' + $.param(nePageLogic.dtoObj)
                    },
                    callback: {
                        onClick: function (event, treeId, treeNode) {
                            nePageLogic.selectedTreeId = treeNode.id;
                            $grid.bootstrapTable('refresh', {url: '/tree/queryGridData?id=' + nePageLogic.selectedTreeId + "&" + $.param(formObj)});
                        }
                    }
                }
            });

            $grid.neTable({
                //nenu 本系统定义的参数对象
                nenu: {
                    toobarBtn: {
                        search: {
                            switch: true,
                            sel: '#js_query_form_btn_search',
                            callBack: function () {
                                //使用方式
                                $('#js_grid').bootstrapTable("refresh",
                                    {url: '/tree/queryGridData?id=' +nePageLogic.selectedTreeId
                                    +"&"+$.param(formObj)
                                    +"&"+$("#js_query_form").serialize()});
                            }
                        }
                    }
                },
                table: {
                    url: '/tree/queryGridData?id=' + nePageLogic.selectedTreeId+"&"+$.param(formObj), //ajax url
                    method: "get",//请求方式
                    queryParams: function (params) {
                        return {limit: params.limit, offset: params.offset}
                    },
                    dataField: "rows",//服务端返回数据键值 就是说记录放的键值是rows，分页时使用总记录数的键值为total
                    pageNumber: 1,
                    pageSize: 10,
                    dataType: "json",//期待返回数据类型
                    pagination: true,//是否分页
                    sidePagination: "server",
                    pageList: [10, 20, 50, 100, 200, 500],
                    striped: true,
                    columns: JSON.parse(extraObj.columns)
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            onZtree({
                grid:$grid,
                ztree:'js_tree'
            });
            onGrid({
                grid:$grid,
                nePageLogic: nePageLogic
            });
        },
        end: function (flag) {
            Nenu.open.end = function () {
                //父页面表格刷新
                $('#js_grid').bootstrapTable("refresh",{url: '/tree/queryGridData?id=' +nePageLogic.selectedTreeId+"&"+$.param(formObj)});
                flag = false;
            };
            if(flag){
                $('#js_grid').bootstrapTable("refresh",{url: '/tree/queryGridData?id=' +nePageLogic.selectedTreeId+"&"+$.param(formObj)});
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
            this.end(false);
        }
    };
    nePageLogic.init();
    $('body').layout({
        name: 'manLayout',
        west: {
            size: 250,
            children: {
                name: 'treeLayout'

            }
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: nePageLogic.fnTable
    });

});