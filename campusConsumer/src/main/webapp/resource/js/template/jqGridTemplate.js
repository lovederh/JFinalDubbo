/**
 * Created by dsideal-lee on 2016/10/12.
 */
$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    var lastsel3;
    //日期格式化
    var datePicker = {
        dataInit: function (e) {
            var id = $(e).attr("id");
            laydate({
                elem: '#'+id, //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
                event: 'focus' //响应事件。如果没有传入event，则按照默认的click
            });
        }
    };

    var autoComplete = {
        // dataInit is the client-side event that fires upon initializing the toolbar search field for a column
        // use it to place a third party control to customize the toolbar
        dataInit: function (element) {
            $(element).attr("autocomplete", "off").typeahead({
                appendTo: "body",
                source: function (query, proxy) {
                    $.ajax({
                        url: 'http://trirand.com/blog/phpjqgrid/examples/jsonp/autocompletepbs.php?callback=?&acelem=ShipName',
                        dataType: "json",
                        data: {term: query},
                        success: proxy
                    });
                }
            })
        }
    };
    var gridCaption = "表格名称";
    jQuery("#table_list_2").jqGrid({
        datatype: "local",
        height: 500,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames:['ID Number','Last Sales','Name', 'Stock', 'Ship via','Notes','department'],
        colModel:[
            {name:'id',index:'id', width:90, sorttype:"int", editable: true},
            {
                name: 'sdate', index: 'sdate', width: 90, editable: true, sorttype: "date", editoptions: datePicker
            },
            {name:'name',index:'name', width:150,editable: true,editoptions:{size:"20",maxlength:"30"}},
            {name:'stock',index:'stock', width:60, editable: true,edittype:"checkbox",editoptions: {value:"Yes:No"}},
            {name:'ship',index:'ship', width:90, editable: true,edittype:"select",editoptions:{value:"FE:FedEx;IN:InTime;TN:TNT;AR:ARAMEX"}},
            {name:'note',index:'note', width:200, sortable:false,editable: true,edittype:"textarea", editoptions:{rows:"2",cols:"10"}},
            {name:'department',index:'department', width:200, sortable:false,editable: true,edittype: 'select'}
        ],
        //onSelectRow: function(id){
        //    if(id && id!==lastsel3){
        //        jQuery('#table_list_2').jqGrid('restoreRow',lastsel3);
        //        //jQuery('#table_list_2').jqGrid('editRow',id,true,pickdates);
        //        lastsel3=id;
        //    }
        //},
        //toolbar:[true, "top"],
        //celledit: true,
        add: true,
        edit: true,
        viewrecords: true,
        caption: gridCaption,
        hidegrid: false,
        pager: "#pager_list_2"

    });
    //$("#t_table_list_2").append("<input type='button' value='Click Me' style='height:20px;font-size:-3'/>");
    //$("input","#t_table_list_2").click(function(){
    //    alert("Hi! I'm added button at this toolbar");
    //});
    var mydata3 = [
        {id:"12345",name:"Desktop Computer",note:"note",stock:"Yes",ship:"FedEx", sdate:"2007-12-03"},
        {id:"23456",name:"Laptop",note:"Long text ",stock:"Yes",ship:"InTime",sdate:"2007-12-03"}
    ];
    //$("#table_list_2").setSelection(4, true);
    //jQuery("#table_list_2").jqGrid('navGrid',"#pager_list_2",{edit:false,add:false,del:false});
    //jQuery("#table_list_2").jqGrid('inlineNav',"#pager_list_2");
    jQuery("#table_list_2").jqGrid('navGrid','#pager_list_2' );
    //设置显示自定义按钮
    jQuery("#table_list_2").jqGrid('navButtonAdd','#pager_list_2',{
        caption:"",
        title: "Reorder Columns",
        onClickButton : function (){
            jQuery("#table_list_2").jqGrid('columnChooser');
            return false;
        }
    });
    //是否显示查询
    jQuery("#table_list_2").jqGrid('navButtonAdd','#pager_list_2',{
        caption:"",
        title: "显示查询条",
        onClickButton : function (){
            jQuery("#table_list_2").jqGrid('filterToolbar',{searchOperators : true});
            return false;
        }
    });
    for(var i=0;i < mydata3.length;i++)
        jQuery("#table_list_2").jqGrid('addRowData',mydata3[i].id,mydata3[i]);

    function pickdates(id){
        laydate({
            elem: '#'+id+'_sdate', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
        });
    }
    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper').width();
        $('#table_list_2').setGridWidth(width);
    });
});
