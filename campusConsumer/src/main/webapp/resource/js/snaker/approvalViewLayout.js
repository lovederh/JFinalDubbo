define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqGrid');
    require('jqSlimscroll');
    require('jqLayout');


    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        //取自身的父界面属性作为弹窗属性
        option  : window.parent.Nenu.open.data.child
    };


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['任务处理人','任务环节','任务创建时间','任务完成时间','处理结果','处理备注','id','task_Name'];
    var colModel = [
        {name: 'operatorName', index: 'operatorName', width: 100, align: "center"},
        {name: 'taskDisplay', index: 'taskDisplay', width: 100, align: "center"},
        {name: 'create_Time', index: 'create_Time', width: 200, align: "center"},
        {name: 'finish_Time', index: 'finish_Time', width: 200, align: "center"},
        {name: 'approvalResult', index: 'approvalResult', width: 100, align: "center",formatter: function(cellValue, options, rowObject){
            var approvalResult = ""
            if('agree' == cellValue){
                approvalResult = "<label style='color:green;'>通过</label>";
            }else if('disagree' == cellValue){
                approvalResult = "<label style='color:red;'>不通过</label>";
            }
            return approvalResult;
        }},
        {name: 'approvalRemark', index: 'approvalRemark', width: 300, align: "center"},
        {name: 'id', hidden: true},
        {name: 'task_Name', hidden: true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: "/flowApprovalType/approvalResultGridData?" + $.param(utilPageParam),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "-1",//显示全部记录
                pager: false,
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: false,
                jsonReader: {
                    repeatitems: false
                },
                caption: '审批详情'
            });
        },
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },
        event: function () {
            Nenu.context.event = {
                applyAgainBtn : applyAgainFunc,
                terminateOrderBtn : terminateOrderFunc,
                deleteOrderBtn : deleteOrderFunc,
                acceptOrderTaskBtn : acceptOrderTaskFunc,
                approvalPassBtn : approvalPassFunc,
                approvalNotPassBtn : approvalNotPassFunc,
                ccOrdersReadBtn: ccOrdersReadFunc,
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                }
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                //弹窗处理任务标识为成功, 则关闭整个弹窗
                var dealSuccess = Nenu.open.data.parent.dealSuccess;
                if(dealSuccess && 'true'==dealSuccess){
                    nePageArgum.option.close();
                }
            };
        },
        init: function () {
            this.pLayout();
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    //强制归档
    function terminateOrderFunc(){
        util_terminateOrder(utilPageParam.orderId, function(){
            nePageArgum.option.close();
        });
    }

    //工单删除
    function deleteOrderFunc(){
        util_deletePhysicalOrder(utilPageParam.orderId, function(){
            nePageArgum.option.close();
        });
    }

    //重新派发
    function applyAgainFunc(){
        return {
            title: '重新派发',
            url: '/flowDeal/openDealTaskForm?' + $.param(utilPageParam),
            height: '1500px',
            width: '1000px'
        };
    }

    //提取任务
    function acceptOrderTaskFunc(){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/flowDeal/acceptOrderTask',
            data: {
                taskId : utilPageParam.taskId
            },
            success:function(jsonData){
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    //审批通过按钮, 打开表单弹窗
    function approvalPassFunc(){
        return {
            title: '审批通过',
            url: '/flowDeal/openDealTaskForm?approvalResult=agree&' + $.param(utilPageParam),
            height: '1500px',
            width: '1000px'
        };
    }

    //审批不通过按钮, 打开表单弹窗
    function approvalNotPassFunc(){
        return {
            title: '审批不通过',
            url: '/flowDeal/openDealTaskForm?approvalResult=disagree&' + $.param(utilPageParam),
            height: '1500px',
            width: '1000px'
        };
    }

    // 标记已阅
    function ccOrdersReadFunc(){
        util_ccOrdersRead(utilPageParam.orderId)
    }
});

