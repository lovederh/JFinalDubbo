define(function (require) {
    require('mConfig');

    //require('jqBootstrapTable');// 加载bootstrapTable

    require('snaker');// 加载snaker流程图展示使用的脚本
    require('/resource/third/snaker/raphael-min.js');

    $(function(){
        //initHistoryGrid();// 初始化环节节点历史信息

        // 发送后台请求, 请求流程图构造数据
        $.ajax({
            type: "get",
            dataType: "json",
            url: "/flowQuery/flowPictureData",
            data: pageParam,
            async: false,
            globle:false,
            success: function(data){
                showPicture(data.process, data.state);
            }
        });
    });

    // 流程图展示方法, 注意路径设置
    function showPicture(process, state) {
        $('#js_flowPicture').snakerflow($.extend(true, {
                basePath: "/resource/third/snaker/",
                orderId: pageParam.orderId,
                restore: eval("(" + process + ")"),
                editable: false
            }, eval("(" + state + ")")
        ));
    }

    /*
    function initHistoryGrid(){
        //使用bootstrapTable构造流转的历史记录
        var $grid = $('#js_historyGrid');
        $grid.neTable({
            table: {
                url: "/flowQuery/historyGridData?includeActive=true&" + $.param(pageParam),
                pagination: false,//不分页
                showColumns: false,//不显示列下拉框
                height: 400,
                columns: [{
                    checkbox: true,
                    width: '40px'
                }, {
                    field: 'display_Name',
                    title: '任务名称',
                    width: '200px'
                }, {
                    field: 'create_Time',
                    title: '任务创建时间',
                    width: '200px'
                }, {
                    field: 'finish_Time',
                    title: '任务完成时间',
                    width: '200px'
                }, {
                    field: 'operatorName',
                    title: '任务处理人',
                    width: '200px'
                }, {
                    field: 'task_State',
                    title: '任务状态',
                    width: '200px',
                    formatter: function(value, row, index){
                        //0：已结束；1：活跃
                        return '0'==value? '已结束': '活跃';
                    }
                }]
            }
        });
    }
    */
});
