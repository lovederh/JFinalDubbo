define(function (require) {
    var $ = require('jquery');
    require("mConfig");
    require('jqZtree');

    require('jqLayout');
    require('neEvent');
    require('neToastr');

    require('jqDualListTable');
    require('jqBootstrapTableGroupBy');

    var $dualList;

    var nePageArgum = {
        option: Nenu.open.data.child
    };

    var nePageLogic = {
        event: function () {
            Nenu.context.event = {
                // 确定按钮
                fnSure: function () {
                    var data = $dualList.neDualListTable('getData');
                    nePageArgum.option.setParentData({
                        pickSureFlag : 'true',
                        actorsData: data
                    });
                    nePageArgum.option.close();
                },
                //取消按钮
                fnCancel: function () {
                    nePageArgum.option.close();
                }
            }
        },
        end: function () {
            Nenu.open.end = function () {

            }
        },
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    function fnLoad() {
        var $tabs = $('.jc-tabs');
        var aryBtns = $('li', $tabs);

        //初始化neDualListTable
        $dualList = $('#js_dual_list_table').neDualListTable({
            rightData: nePageArgum.option.neData,
            filter: 'id',//多选功能的过滤条件
            neTable: {
                table: {
                    columns: [
                        //列设置
                        {checkbox: true, field: 'state'},
                        {field: 'actorDisplay', title: '处理者名称', name: 'actorDisplay'},
                        {field: 'actorTypeDisplay', title: '处理者类型', name: 'actorTypeDisplay'},
                        {field: 'phone', title: '电话', name: 'phone'},
                        {field: 'orgName', title: '部门', name: 'orgName'},
                        {field: 'id', name: 'id', visible: false},
                        {field: 'actorValue', name: 'actorValue', visible: false},
                        {field: 'actorType', name: 'actorType', visible: false}
                    ],
                    //行数据加载时候的回调 , 这个使用默认即可
                    rowAttributes: function (row, index) {
                        return row.state = false;
                    }
                }
            }
        });

        //定义tabls按钮点击事件
        $tabs.on('click', 'a', function (e) {
            e.stopPropagation();
            var $this = $(this);

            //执行标签切换的方法
            aryBtns.each(function () {
                $this.removeClass('active');
            });
            $this.parent().addClass('active');

            //清空双选器左侧数据
            $dualList.neDualListTable ('setData',{
                left: []
            });

            //请求选择树数据
            $('#js_treeDiv').empty();
            var treeType = $this.data().type;
            initPickTree(treeType);
        });
        //默认加载角色(第一个标签)
        $('a', aryBtns[0]).trigger('click');
    }

    //初始化layout
    $('body .tab-pane .panel-body ').layout({
        name: 'actorsLayout',
        west: {
            size: 250
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: fnLoad
    });


    //初始化构造选择树(如总角色树, 教师部门树)
    function initPickTree(treeType) {
        var $tree = $('#js_tree').neTree({
            setting: {
                view: {
                    selectedMulti: false//不再支持多选
                },
                async: {
                    enable: true,
                    url: '/flowApiData/initPickTree?treeType=' + treeType
                },
                check: {
                    enable: false
                },
                callback: {
                    //加载结束的事件, 默认选中根节点
                    onAsyncSuccess: function(){
                        //获取ztree原生对象, 执行方法扩展
                        var ztreeObj  = $tree.neTree('getTree');
                        var nodes = ztreeObj.getNodes();
                        if(nodes.length){
                            ztreeObj.selectNode(nodes[0]);
                            ztreeObj.setting.callback.onClick(null, null, nodes[0]);
                        }
                    },
                    onClick: function (event, treeId, treeNode) {
                        $.ajax({
                            url: '/flowApiData/findPickData',
                            data: {
                                treeType: treeType,
                                treeId: treeNode.id
                            },
                            dataType: 'json',
                            type: "post",
                            async: false,
                            success: function (jsonData) {
                                $dualList.neDualListTable('setData', {data: jsonData});
                            }
                        });
                    }
                }
            }
        });
    }
});