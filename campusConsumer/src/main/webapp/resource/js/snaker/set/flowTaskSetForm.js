define(function (require) {
    require('mConfig');
    //校验组件
    require('jqValidate');
    //通知组件
    require('neToastr');
    //加载event核心文件
    require('neEvent');

    require('jqSlimscroll');
    require('jqLayout');


    var $rolesHidden = $('#js_roleDataStr');
    var $teachersHidden = $('#js_teacherDataStr');
    var $roleScope = $('#js_taskRoleScope');
    var $teacherScope = $('#js_taskTeacherScope');

    //初始化获取环节下已选处理者
    var actorsAllData = [];

    $.ajax({
        url: '/flowSet/findProcessTaskActors',
        dataType: 'json',
        type: "post",
        data: {
            taskSetId: pageParam.taskSetId
        },
        async: false,
        success: function (jsonData) {
            var roleActors = jsonData.actorMap.role;
            var teacherActors = jsonData.actorMap['1'];
            //依次赋值
            actorsAllData = jsonData.actorsAllData;//已选全部数据
            $rolesHidden.val(roleActors.actorValueStr);//角色真实值串
            $teachersHidden.val(teacherActors.actorValueStr);//教师真实值串
            $roleScope.html(roleActors.actorDisplayStr);//角色展示值串
            $teacherScope.html(teacherActors.actorDisplayStr);//教师展示值串
        }
    });


    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        option  : Nenu.open.data.child
    };

    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },
        event: function () {
            Nenu.context.event = {
                pickTaskActorsBtn: pickTaskActorsFunc
            };
        },
        end : function () {
            Nenu.open.end = function () {
                //弹出的人员选择窗关闭时, 在当前页处理所选的人员
                var pickSureFlag = Nenu.open.data.parent.pickSureFlag;
                if(!pickSureFlag || 'true'!=pickSureFlag){
                    return;
                }
                actorsAllData = Nenu.open.data.parent.actorsData;
                var pickActorsObj = util_initPickActorsObj(actorsAllData);
                var roleActors = pickActorsObj.role;
                var teacherActors = pickActorsObj['1'];
                $rolesHidden.val(roleActors.actorValueStr);//角色真实值串
                $teachersHidden.val(teacherActors.actorValueStr);//教师真实值串
                $roleScope.html(roleActors.actorDisplayStr);//角色展示值串
                $teacherScope.html(teacherActors.actorDisplayStr);//教师展示值串
            };
        },
        init : function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //处理者选择按钮
    function pickTaskActorsFunc(){
        return {
            title: '选择处理者',
            url: '/flowSet/toPickTaskActors',
            height: '1500px',
            width: '2000px',
            neData: actorsAllData
        };
    }


    //表单验证, 环节配置提交保存(包括基本配置和处理者)
    var $form = $('#js_flowTaskSetForm');
    $form.neValidate({
        ajaxBtn : {
            btn : $('#js_submitBtn'),//按钮对应的dom元素
            type : 'post',
            url : '/flowSet/updateFlowTaskSet',
            success : function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    Nenu.toastr.success(msg);
                } else {
                    Nenu.toastr.error(msg);
                }
            }
        }
    });
});