define(function (require) {
    require('mConfig');
    //校验组件
    require('jqValidate');
    //通知组件
    require('neToastr');
    //加载event核心文件
    require('neEvent');

    require('jqSlimscroll');
    require('jqLayout');



    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        option  : Nenu.open.data.child
    };

    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },
        event: function () {
            Nenu.context.event = {
                batchDeployFlowBtn: batchDeployFlowFunc,
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                }
            };
        },
        end : function () {
            Nenu.open.end = function () {

            };
        },
        initDeployInfo : function(){
            //安装的企业域
            var domains = nePageArgum.option.neData.domains;
            var domainNames = nePageArgum.option.neData.domainNames;
            if(!domains && !domainNames){
                $('#js_domainNames').html('全部');
            }else{
                $('#js_domainNames').html(domainNames);
            }
            //安装的流程文件
            $('#js_fileResource').html(nePageArgum.option.neData.fileResource);
        },
        init : function () {
            this.pLayout();
            this.event();
            this.end();

            this.initDeployInfo();
        }
    };
    nePageLogic.init();


    /**
     * 取出填入的流程拦截器, 执行批量安装
     */
    function batchDeployFlowFunc(){
        var fileResource = nePageArgum.option.neData.fileResource;
        var domains = nePageArgum.option.neData.domains;
        var flowInterceptor = $('#flowInterceptor').val();
        batchDeployFlow(fileResource, domains, flowInterceptor);
    }


    function batchDeployFlow(fileResource, domains, flowInterceptor){
        $.ajax({
            type: 'POST',
            url: '/flowSet/batchDeploySysFlow',
            dataType: 'json',
            async: false,
            data: {
                fileResource : fileResource,
                domains : domains,
                flowInterceptor : flowInterceptor
            },
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if('200' == status){
                    util_alertOkDone(msg, function(){
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                }else{
                    Nenu.toastr.error(msg);
                }
            }
        });
    }

});