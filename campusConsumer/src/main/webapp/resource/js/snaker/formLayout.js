/**
 * 共通化的工单处理方法
 * 注: 通过新建工单标识确定该页面展示方式, 新建派发情况下则不涉及弹窗,
 *     其它处理环节为弹窗展示, 则注意调用关闭弹窗的方法
 * 2016/12/24
 */
define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //校验组件
    require('jqValidate');
    //日期组件
    require('neDate');

    require('jqSlimscroll');
    require('jqLayout');

    var pageOpenOption;
    if('true' == utilPageParam.newOrderFlag){
        //新建工单情况下, 表单是嵌到iframe中的, 其父页面为弹窗对象
        pageOpenOption = window.parent.Nenu.open.data.child;
    }else{
        pageOpenOption = Nenu.open.data.child;
    }

    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        //取自身的父界面属性作为弹窗属性
        option  : pageOpenOption
    };


    //抄送者数据
    var ccOperatorAll = [];
    var $rolesCcHidden = $('#js_roleCcOperators');
    var $teachersCcHidden = $('#js_teacherCcOperators');
    var $roleCcScope = $('#js_roleCcScope');
    var $teacherCcScope = $('#js_teacherCcScope');


    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },

        event: function () {
            Nenu.context.event = {
                pickCcOperatorsBtn: pickCcOperatorsFunc,
                saveDraftBtn : saveDraftFunc,
                saveTemplateBtn : saveTemplateFunc,
                useTemplateBtn : useTemplateFunc,
                goBackBtn : function(){//返回至流程类型选择
                    history.go(-1);
                },
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                }
            };
        },
        end : function () {
            Nenu.open.end = function () {
                //弹出的人员选择窗关闭时, 在当前页处理所选的人员
                var pickSureFlag = Nenu.open.data.parent.pickSureFlag;
                if(!pickSureFlag || 'true'!=pickSureFlag){
                    return;
                }
                ccOperatorAll = Nenu.open.data.parent.actorsData;
                var pickActorsObj = util_initPickActorsObj(ccOperatorAll);
                var roleActors = pickActorsObj.role;
                var teacherActors = pickActorsObj['1'];
                $rolesCcHidden.val(roleActors.actorValueStr);//角色真实值串
                $teachersCcHidden.val(teacherActors.actorValueStr);//教师真实值串
                $roleCcScope.html(roleActors.actorDisplayStr);//角色展示值串
                $teacherCcScope.html(teacherActors.actorDisplayStr);//教师展示值串
            };
        },
        init: function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    //任务表单验证并提交
    var $form = $('#js_dealTaskForm');
    $form.neValidate({
        ajaxBtn : {
            btn : $('#js_submitBtn'),//按钮对应的dom元素
            type : 'post',
            url : '/flowDeal/dealTaskSaveBusiness',
            success : function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    util_alertOkDone(msg, function(){

                        nePageArgum.option.setParentData({
                            dealSuccess : 'true'
                        });
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                } else {
                    Nenu.toastr.error(msg);
                }
            }
        }
    });

    //选择当前环节抄送对象
    function pickCcOperatorsFunc(){
        return {
            title: '选择抄送对象',
            url: '/flowSet/toPickTaskActors',
            height: '1500px',
            width: '2000px',
            neData: ccOperatorAll
        };
    }

    //存为草稿
    function saveDraftFunc(){
        //保存草稿不校验表单
        var $saveDraftBtn = $('#js_saveDraftBtn');
        $saveDraftBtn.attr('disabled', true);
        // 序列化表单数据后提交
        var formData = $('#js_dealTaskForm').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/flowDeal/saveDraftData',
            async: false,
            data: formData,
            dataType: 'json',
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    util_alertOkDone(msg, function(){

                        nePageArgum.option.setParentData({
                            dealSuccess : 'true'
                        });
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                } else {
                    Nenu.toastr.error(msg);
                    $saveDraftBtn.attr('disabled', false);
                }
            }
        });
    }

    //存为模板
    function saveTemplateFunc(){

    }

    //引用模板
    function useTemplateFunc(){

    }
});
