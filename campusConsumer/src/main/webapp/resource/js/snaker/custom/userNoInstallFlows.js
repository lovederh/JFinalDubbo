define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['申请类型','版本号','启动表单类型','启动表单名称','流程文件','操作','fileResource','processSetId'];
    var colModel = [
        {name: 'processDisplay', index: 'processDisplay', width: 200, align: "center"},
        {name: 'customVersion', index: 'customVersion', width: 100, align: "center"},
        {name: 'formType', index: 'formType', width: 100, align: "center", formatter: function (cellValue, options, rowObject) {
            return util_displayBusinessType(cellValue, false);
        }},
        {name: 'formDisplay', index: 'formDisplay', width: 200, align: "center"},
        {name: 'fileName', index: 'fileName', width: 200, align: "center"},
        {name: 'operate', width: 100, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn =
                '<button class="btn btn-primary btn-outline btn-sm deployFlow" ' +
                '   fileResource="'+ rowObject.fileResource +'" >' +
                '<i class="glyphicon glyphicon-cog"></i>&nbsp;安装</button>';
            return operateBtn;
        }},
        {name: 'fileResource', hidden: true},
        {name: 'processSetId', hidden: true}
    ];

    //使用按钮事件组件
    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: "/flowCustom/userNoInstallData",
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "-1",//显示全部记录
                pager: false,
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: true,
                jsonReader: {
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                addCustomFlowSetBtn: addCustomFlowSetFunc,
                editCustomFlowSetBtn: editCustomFlowSetFunc,
                deleteFlowFileBtn: deleteFlowFileFunc
            };
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();//当前页列表刷新
            };
        },
        //初始化
        init: function () {
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    //Grid行按钮绑定安装操作
    $(document).on("click", '.deployFlow', function(){
        var fileResource = $(this).attr("fileResource");
        util_deployFlow(fileResource, 'user', reloadGrid);
    });


    //新定制一个流程
    function addCustomFlowSetFunc(){
        return {
            title: '新定制',
            url: '/flowCustom/openCustomFlowForm',
            height: '500px',
            width: '800px'
        };
    }

    //修改已有的定制流程
    function editCustomFlowSetFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行修改!', true);
            return;
        }
        var processSetId = $grid.jqGrid('getRowData', rows[0]).processSetId;
        return {
            title: '修改定制',
            url: '/flowCustom/openCustomFlowForm?processType=user&processSetId=' + processSetId,
            height: '500px',
            width: '800px'
        };
    }

    //删除流程文件, 下次不会出现在待安装列表
    function deleteFlowFileFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行删除!', true);
            return;
        }
        Nenu.event.confirm({
            title : '是否确认删除，删除后将无法恢复?',
            execute : function (close){
                close();
                // 关闭后发送删除请求
                var rowData = $grid.jqGrid('getRowData', rows[0]);
                util_deleteFlowFile(rowData.fileResource, rowData.processSetId, reloadGrid);
            }
        });
    }

    // 列表重新刷新
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});
