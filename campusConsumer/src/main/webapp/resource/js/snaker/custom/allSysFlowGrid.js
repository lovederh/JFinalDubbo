define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqGrid');


    //Grid表头
    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['流程信息','流程文件名','文件路径','操作'];
    var colModel = [
        {name: 'processDisplay', index: 'processDisplay', width: 200, align: "center"},
        {name: 'fileName', index: 'fileName', width: 200, align: "center"},
        {name: 'fileResource', index: 'fileResource', width: 400, align: "center"},
        {name: 'operate', width: 100, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn =
                '<button class="btn btn-primary btn-outline btn-sm deleteFlowFile" ' +
                '   fileResource="'+ rowObject.fileResource +'" >' +
                '<i class="glyphicon glyphicon-remove"></i>&nbsp;删除文件</button>';
            return operateBtn;
        }}
    ];


    //Grid行按钮绑定删除事件
    $(document).on("click", '.deleteFlowFile', function(){
        var fileResource = $(this).attr("fileResource");
        if(fileResource){
            util_deleteFlowFile(fileResource, '', reloadGrid);
        }
    });


    //使用按钮事件组件
    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: "/flowCustom/allSysFlowGridData",
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "-1",//显示全部记录
                pager: false,
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: true,
                jsonReader: {
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                deployDomainFlowBtn: deployDomainFlowFunc,
                undeployDomainFlowBtn: undeployDomainFlowFunc
            };
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();//当前页列表刷新
            };
        },
        //初始化
        init: function () {
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //多企业域安装
    function deployDomainFlowFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行企业域安装!', true);
            return;
        }
        var fileResource = $grid.jqGrid('getRowData', rows[0]).fileResource;
        return {
            title: '企业域安装',
            url: '/flowQuery/toSysFlowDomains?type=noInstall&fileResource=' + fileResource,
            height: '800px',
            width: '1200px'
        };
    }

    //控制流程禁用(统一卸载)
    function undeployDomainFlowFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个要卸载的流程!', true);
            return;
        }
        var fileResource = $grid.jqGrid('getRowData', rows[0]).fileResource;
        return {
            title: '企业域卸载',
            url: '/flowQuery/toSysFlowDomains?type=install&fileResource=' + fileResource,
            height: '800px',
            width: '1200px'
        };
    }

    // 列表重新刷新
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});
