define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    var $familyGrid = $('#js_family_grid');


    var nePageLogic = {
        fnTable : function () {
            $familyGrid.neTable({
                table:{
                    url: '/emp/familyMemberInfoList?empId='+$('#empId').val(),
                    columns: [
                        {field: 'state',checkbox: true},
                        {field: 'memberName',title: '成员姓名'},
                        {field: 'relation',title: '人员关系'},
                        {field: 'work',title: '工作职务'},
                        {field: 'workUnit',title: '工作单位'},
                        {field: 'phone',title: '电话'}
                    ]
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            Nenu.context.event = {
                // 家庭成员新增按钮
                addFamily : function () {
                    $('#familyForm').show();
                    $('#familyId').val("");
                    $('#memberName').val("");
                    $('#relation').val("");
                    $('#work').val("");
                    $('#workUnit').val("");
                    $('#phone').val("");
                },

                familyCancel : function () {
                    $('#familyId').val("");
                    $('#memberName').val("");
                    $('#relation').val("");
                    $('#work').val("");
                    $('#workUnit').val("");
                    $('#phone').val("");
                    $('#familyForm').hide();
                },

                // 编辑家庭成员按钮
                editFamily : function () {
                    var selRows = $familyGrid.bootstrapTable('getSelections');
                    if(selRows.length > 0){
                        if (selRows.length > 1){
                            Nenu.toastr.info('请选择一条数据进行编辑!');
                            return;
                        }
                        $('#familyForm').show();
                        $.ajax({
                            type: 'post',
                            url: '/emp/loadInfoFamilyMember?id='+selRows[0].id,
                            dataType: 'json',
                            async: false,
                            success: function(jsonData){
                                $('#familyId').val(jsonData.id);
                                $('#memberName').val(jsonData.memberName);
                                $('#relation').val(jsonData.relation);
                                $('#work').val(jsonData.work);
                                $('#workUnit').val(jsonData.workUnit);
                                $('#phone').val(jsonData.phone);
                            }
                        });
                    }else{
                        Nenu.toastr.info('请选择一条要编辑的数据!');
                        return;
                    }
                },

                // 添加/编辑家庭成员保存按钮
                familySave : function(){
                    // 序列化表单数据后提交
                    var familyForm = $('#familyForm').serializeArray();
                    $.ajax({
                        type: 'post',
                        url: '/emp/familyMemberSave',
                        dataType: 'json',
                        async: false,
                        data: familyForm,
                        success: function(jsonData){
                            Nenu.toastr.success(jsonData.msg);
                            $('#familyForm').hide();
                            $familyGrid.bootstrapTable('refresh');
                        }
                    });
                },

                // 删除家庭成员按钮
                delFamily : function () {
                    var selRows = $familyGrid.bootstrapTable('getSelections');
                    if (selRows.length > 0) {
                        Nenu.event.confirm({
                            isClose : false,             // 是否自动关闭窗口
                            title   : '确定要删除所选的记录吗？',        // 窗口标题
                            execute : function (close) {  // 确定按钮执行的事件 参数 close 传入的关闭函数
                                var id = new Array();
                                for (var i = 0; i < selRows.length; i++) {
                                    id.push(selRows[i].id);
                                }
                                $.ajax({
                                    type     : 'POST',
                                    url      : "/emp/delFamilyMember",
                                    dataType : 'json',
                                    async    : false,
                                    data     : {
                                        ids : id.join(',')
                                    },
                                    success  : function (jsonData) {
                                        Nenu.toastr.success(jsonData.msg);
                                        $familyGrid.bootstrapTable('refresh');
                                    }
                                });
                                close();
                            }
                        });
                    } else {
                        Nenu.toastr.info('请选择一行数据！');
                        return {};
                    }

                }
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.fnTable();
            this.event();
        }
    };
    nePageLogic.init();

});