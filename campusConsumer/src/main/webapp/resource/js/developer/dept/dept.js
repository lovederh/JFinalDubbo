define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    var onGrid = require("/resource/js/common/event_grid.js");
    var onZtree = require("/resource/js/common/event_ztree.js");
    var $grid = $('#js_grid');

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        treeNode:'',
        fnTable : function () {
            var orgId = 0;
            // 生成组织机构树
            var $tree = $('#js_tree').neTree({
                // ztree的配置参数 setting 完全按照ztree原生组件的参数设置即可
                setting: {
                    async: {
                        enable: true,
                        url: '/dept/initDeptTree?' + $.param(formObj)
                    },
                    //回调函数参数对象
                    callback: {
                        onClick: function (event, treeId, treeNode) {
                            nePageLogic.treeNode =treeNode;
                            $grid.bootstrapTable('refresh', {url: '/dept/queryDeptInfo?orgId=' + treeNode.id});
                        }
                    }
                },
            });

            $grid.neTable({
                table: {
                    url: "/dept/queryDeptInfo?orgId=" + orgId, //ajax url
                    columns: JSON.parse(extraObj.columns)
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            onZtree({
                grid:$grid,
                ztree:'js_tree'
            });
            onGrid({
                grid:$grid
            });
        },
        end: function () {
            Nenu.open.end = function () {
                //父页面表格刷新
                $('#js_grid').bootstrapTable("refresh",{url: '/dept/queryDeptInfo?orgId=' + nePageLogic.treeNode.id});
            };
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();
    $('body').layout({
        name: 'manLayout',
        west: {
            size: 250,
            children: {
                name: 'treeLayout'

            }
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: nePageLogic.fnTable
    });
});

