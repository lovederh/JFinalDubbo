define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqGrid');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    var onGrid = require("/resource/js/common/event_grid.js");
    var $grid = $('#orgGrid');

    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID','机构名称','部门ID','父机构ID','级数','叶子否','排序号','企业域','机构类别','展开否','delFlag'];
    var colModel = [
        {name : 'dataId',hidden : true},
        {name : 'orgName', index : 'orgName', width : 100, align : "center"},
        {name : 'orgId', index : 'orgId', width : 100, align : "right", key : true},
        {name : 'pId', hidden : true},
        {name : 'level', hidden : true},
        {name : 'isLeaf', index : 'isLeaf', width : 100, align : "center"},
        {name : 'ord', index : 'ord', width : 100, align : "center"},
        {name : 'domain', hidden : true},
        {name : 'orgType', index : 'orgType', width : 100, align : "center"},
        {name : 'expanded', hidden : true},
        {name : 'delFlag', hidden : true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $("#orgGrid").jqGrid({
                url: '/dept/deptTreeGrid?'+ $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                //启用树形表格
                treeGrid: true,
                treeGridModel: 'adjacency',
                ExpandColumn : 'orgName',
                jsonReader: {
                    root: "treeData",
                    repeatitems : false
                },
                treeReader : {
                    level_field: "level",
                    parent_id_field: "pId",
                    leaf_field: "isLeaf",
                    expanded_field: "expanded"
                },
                pager: false,//pager在使用树表格时禁用
                height: "auto",
                rowNum : "-1",//显示全部记录
                autowidth: true,
                shrinkToFit:true
            });
        },
        event: function () {
            onGrid({
                grid:$grid
            });

        },
        init: function () {
            this.fnTable();
            this.event();
        }
    };
    nePageLogic.init();
});

