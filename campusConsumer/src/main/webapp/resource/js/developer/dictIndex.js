// 全局树对象
var ztreeObj;

$(document).ready(function () {
    // 初始化字典表格
    $.jgrid.defaults.styleUI = 'Bootstrap';
    initDictGrid();
    // 初始构造字典树
    initDictTree();
});

/******************************页面初始化************************************/
/**
 * 初始化字典表格
 */
function initDictGrid(){
    //列配置
    var colNames = ['主键ID', '字典编码', '字典名称', '父字典编码', '英文标记', '字典排序', '字典真实值'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'dictId', index: 'dictId', width: 100, align: "center"},
        {name: 'dictName', index: 'dictName', width: 100, align: "center"},
        {name: 'pdictId', index: 'pdictId', width: 100, align: "center"},
        {name: 'dictType', index: 'dictType', width: 100, align: "center"},
        {name: 'orderNo', index: 'orderNo', width: 100, align: "center"},
        {name: 'realValue', index: 'realValue', width: 100, align: "center"}
    ];

    //树形表格生成声明
    $("#dictGrid").jqGrid({
        url: '/dict/findChildrenDicts',
        datatype: 'json',
        postData: {
            dictId : "root"
        },
        colNames: colNames,//列名
        colModel: colModel,//列字段配置
        jsonReader: {
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: false
        },
        rowNum: 10,//每页条数
        rowList: [10, 20, 30],//每页条数列表
        autowidth: true,
        shrinkToFit:true,
        height: 200,//grid的高度
        pager: "#dictPager",//工具栏的元素
        caption: "子字典信息"
    });
}

// 请求后台构造字典树
function initDictTree(){
    // zTree属性设置
    var treeSetting = {
        view: {
            selectedMulti: false//不再支持多选
        },
        callback: {
            // 单击事件(显示当前字典配置信息)
            onClick: function(event, treeId, treeNode) {
                // 加载字典配置信息
                showDictData(treeNode);
                // 刷新字典表数据
                $('#dictGrid').setGridParam({
                    postData : {
                        dictId : treeNode.id
                    }
                }) .trigger("reloadGrid");
            }
        }
    };
    // 请求数据初始化树
    $.ajax({
        type : 'post',
        url : '/dict/initDictTree',
        dataType : 'json',
        success : function(jsonData){
            // 构建字典树
            $.fn.zTree.init($('#dictTree'), treeSetting, jsonData);
            // 全局对象赋值
            ztreeObj = $.fn.zTree.getZTreeObj('dictTree');
            // 默认选中根节点
            defaultSelectRoot();
        }
    });
}

/**
 * 默认选中根节点
 */
function defaultSelectRoot(){
    var nodes = ztreeObj.getNodes();
    if(nodes.length){
        ztreeObj.selectNode(nodes[0]);
    }
}

/**
 * 检验是否为根节点
 */
function checkIsRootNode(node){
    var nodeId = node.id;
    return 'root' == nodeId;
}

/******************************字典数据操作************************************/
/**
 * 点击新增按钮方法, 新增一条字典记录
 */
function operateInsert(){
    var nodes = ztreeObj.getSelectedNodes();
    if(! nodes.length){
        alertErrorLayer('请选择要新增字典的父字典!');
        return;
    }
    var pdictNode = nodes[0];
    var insertData = {
        pdictId: pdictNode.id,//父字典编码
        dictType: pdictNode.dictType//字典大类
    };
    loadDictForm(insertData);
}

/**
 * 删除选中字典及其子字典
 */
function operateDelete(){
    // 获取当前选中的字典节点
    var nodes = ztreeObj.getSelectedNodes();
    if(! nodes.length){
        alertErrorLayer('请选择要删除的字典!');
        return;
    }
    var selectNode = nodes[0];
    if(checkIsRootNode(selectNode)){
        alertErrorLayer('不能删除根节点!');
        return;
    }

    //询问框
    layer.confirm('将删除当前字典及其子字典, 是否确认?', {
        btn: ['确定', '取消']
    }, function(){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/dict/deleteDict',
            data: {
                dictId: selectNode.id
            },
            success:function(data){
                layer.msg(data.msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //关闭后的操作
                    ztreeObj.removeNode(selectNode);//移除所选节点
                    defaultSelectRoot();//选中根节点
                });
            }
        });
    });
}

/**
 * 保存
 */
function operateSave(){
    // 保存按钮不再可用, 防止重复提交
    var $saveBtn = $('#saveBtn');
    $saveBtn.attr('disabled', true);
    // 序列化表单数据后提交
    var formData = $('#dictForm').serializeArray();
    $.ajax({
        type: 'post',
        url: '/dict/saveDict',
        dataType: 'json',
        async: false,
        data: formData,
        success: function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //重新加载整个字典树
                    initDictTree();
                });
            }else{
                alertErrorLayer(msg);
            }
        }
    });
    $saveBtn.attr('disabled', false);
}

/******************************字典树点击事件************************************/
/**
 * 单击字典树节点, 查看选中节点的信息
 */
function showDictData(treeNode){
    // 判断当前选中的节点是否为根节点
    if(checkIsRootNode(treeNode)){
        // 根节点则隐藏表单
        fillInFormData({});
        $('#dictForm').hide();
    }else{
        $.ajax({
            type : 'post',
            url : '/dict/findDictById?id=' + treeNode.primaryId,
            dataType : 'json',
            success : function(data) {
                loadDictForm(data);
            }
        });
    }
}

/**
 * 表单中呈现字典配置数据
 */
function loadDictForm(data){
    // 先清除表单中所有数据
    fillInFormData({});
    // 渲染后台请求到的数据
    fillInFormData(data);
    // 控制表单重新展示
    $('#dictForm').show();
}

/**
 * 表单中所有数据依照传入的对象进行渲染
 */
function fillInFormData(data){
    // 渲染数据
    $('#id').val(data.id);
    $('#pdictId').val(data.pdictId);
    $('#dictName').val(data.dictName);
    $('#dictType').val(data.dictType);
    $('#orderNo').val(data.orderNo);
    $('#realValue').val(data.realValue);
    // 字典编码赋值
    $('#dictIdDiv').html(data.dictId? data.dictId: '');
}

/******************************字典查询************************************/
/**
 * 模糊查询
 */
function searchDicts(){
    var searchDictInput = $('#searchDictInput').val();
    if(!searchDictInput){
        return;
    }
    // 存在有效的查询值, 则请求后台
    $.ajax({
        type : 'post',
        url : '/dict/searchDicts',
        dataType : 'text',
        data: {
            searchDictInput : searchDictInput
        },
        success : function(dataText){
            // 先折叠全部节点再展开根节点
            ztreeObj.expandAll(false);
            ztreeObj.expandNode(ztreeObj.getNodeByParam('id', 'root', null), true, false, false);
            if(dataText){
                // 循环展开
                var searchDicts = dataText.split(',');
                $.each(searchDicts, function (i, value) {
                    // 找到对应节点, 并展开
                    var searchNode = ztreeObj.getNodeByParam('primaryId', value, null);
                    ztreeObj.expandNode(searchNode, true, false, false);
                    ztreeObj.selectNode(searchNode);
                });
            }
        }
    });
}

/**
 * 弹出错误警告框
 */
function alertErrorLayer(msg){
    layer.msg(msg, {
        icon: 5,
        time: 2000,//2s后自动关闭
        btn: ['确定']
    });
}