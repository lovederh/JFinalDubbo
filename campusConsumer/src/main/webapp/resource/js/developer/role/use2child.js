define(function(require){
    'use strict'
    require("mConfig");
    require('jqZtree');
    require('neEvent');
    require('neToastr');

    var nePageLogic = {
        options: Nenu.open.data.child,
        initTree: function () {
            var $tree = $('#'+treeId).neTree({
                setting: {
                    async: {
                        enable: true,
                        url: '/tree/initRoleTree?roleId=' + $.param(options.formObj)
                    },
                    check: {
                        enable: true
                    },
                    callback: {
                    }
                }
            });
        },
        event: function(){
            Nenu.context.event = {
                ensured: function(){

                }
            }
        },
        init: function(){
            this.initTree();
            this.event();
        }

    };
    nePageLogic.init();

});