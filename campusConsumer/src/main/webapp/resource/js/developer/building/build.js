/**
 * Created by admin on 2017/2/14.
 */
define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqLayout');
    require('jqZtree');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    var onGrid = require("/resource/js/common/event_grid.js");
    var $grid = $('#js_grid');
    var newCount = 1;
    function addHoverDom(treeId, treeNode) {
        // var sObj = $("#" + treeNode.tId + "_span");
        // if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
        // var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='add node' onfocus='this.blur();'></span>";
        // sObj.after(addStr);
        // var btn = $("#addBtn_"+treeNode.tId);
        // if (btn) btn.bind("click", function(){
        //     var zTree = $.fn.zTree.getZTreeObj("js_tree_building");
        //     zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
        //     return false;
        // });

    }

    // 判断根节点是否可以编辑
    function showEditOrDelBtn(treeId, treeNode) {
        // if (treeNode.isParent) {
        //     return false;
        // }
        return true;
    }

    function onRename(e, treeId, treeNode, isCancel) {
        $.ajax({
            url: '/building/buildingTreeNote',
            data: {
                opt: "edit",
                codeId: treeNode.id,
                name: treeNode.name
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    function onRemove(e, treeId, treeNode) {
        $.ajax({
            url: '/building/buildingTreeNote',
            data: {
                opt: "del",
                codeId: treeNode.id
            },
            type: "post",
            async: false,
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);
            }
        });
    }

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        selectedTreeId: -1,
        fnTable: function () {
            var $tree = $('#js_tree_building').neTree({
                setting: {
                    view: {addHoverDom: addHoverDom, selectedMulti: false},

                    edit: {
                        enable: true,
                        editNameSelectAll: true,
                        showRenameBtn: showEditOrDelBtn,
                        showRemoveBtn: showEditOrDelBtn,
                        removeTitle: '删除',
                        renameTitle: '重命名'
                    },

                    async: {
                        enable: true,
                        url: '/building/initBuildingTree?businessTable=' + extraObj.businessTable
                    },

                    callback: {
                        onRemove: onRemove,//删除节点后触发，用户后台操作
                        onRename: onRename,//编辑后触发，用于操作后台
                        onClick: function (event, treeId, treeNode) {
                            nePageLogic.selectedTreeId = treeNode.id;
                            $grid.bootstrapTable('refresh', {url: '/building/queryBuildingGridData?buildId=' +nePageLogic.selectedTreeId});

                        }
                    }
                }
            });

            $grid.neTable({
                table: {
                    url: '/building/queryBuildingGridData?id=' + nePageLogic.selectedTreeId, //ajax url
                    method: "get",//请求方式
                    dataField: "rows",//服务端返回数据键值 就是说记录放的键值是rows，分页时使用总记录数的键值为total
                    pageNumber: 1,
                    pageSize: 10,
                    dataType: "json",//期待返回数据类型
                    pagination: true,//是否分页
                    sidePagination: "server",
                    pageList: [10, 20, 50, 100, 200, 500],
                    striped: true,
                    columns: JSON.parse(extraObj.columns)
                }
            });
        },
        /**
         * 全局页面事件对象e
         * @private
         **/
        event: function () {
            onGrid({
                grid:$grid
            });
        },
        end: function () {
            Nenu.open.end = function () {
                //父页面表格刷新
                $('#js_grid').bootstrapTable("refresh",{url: '/building/queryBuildingGridData?id=' +nePageLogic.selectedTreeId+"&"+$.param(formObj)});
            };
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    $('body').layout({
        name: 'manLayout',
        west: {
            size: 250,
            children: {
                name: 'treeLayout'

            }
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: nePageLogic.fnTable
    });

});
