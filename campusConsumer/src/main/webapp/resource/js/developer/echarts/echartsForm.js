$(document).ready(function () {
    // 注册主题
    var myTheme = new Theme();
    echarts.registerTheme('myTheme', myTheme.getTheme('myTheme'));
});

// 保存图表项信息, 并返回图表预览
function operateSave(){
    var $echartForm = $('#echartForm');
    var $saveBtn = $('#saveBtn');
    $saveBtn.attr('disabled', true);
    // 序列化表单数据后提交
    var echartData = $echartForm.serializeArray();
    $.ajax({
        type: 'POST',
        url: '/echarts/saveEchartForm',
        async: false,
        data: echartData,
        success: function(jsonData){
            if(jsonData.status == '200'){
                // 保存并生成图表项option成功, 则展示预览效果
                var optionData = jsonData.optionData;
                var option = eval('('+ optionData +')');
                // 生成图表
                var previewEchart = echarts.init(document.getElementById('previewEchart'),'myTheme');
                previewEchart.setOption(option);
                alertErrorLayer(jsonData.msg);
            }else{
                // 操作失败, 给出提示
                alertErrorLayer(jsonData.msg);
            }
        }
    });
    $saveBtn.attr('disabled', false);
}


/**
 * 弹出错误警告框
 */
function alertErrorLayer(msg){
    layer.msg(msg, {
        icon: 5,
        time: 2000,//2s后自动关闭
        btn: ['确定']
    });
}