define(function (require) {
    // 'use strict';
    //加载组件
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqGrid');
    var $grid = $('#js_grid');

    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID', 'loginId', '人员姓名', '联系方式'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'loginId', hidden: true},
        {name: 'userName', index: 'userName', width: 100, align: "center"},
        {name: 'phone', index: 'phone', width: 100, align: "center"}
    ];

    var nePageLogic = {
        fnTable: function () {
            $grid.jqGrid({
                url: '/notice/queryStaff?' + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_pager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {

            }
        },
        init: function () {
            this.fnTable();

            this.event();
        }
    };
    nePageLogic.init();
});
