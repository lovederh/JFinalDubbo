define(function (require) {
    var $ = require('jquery');
    require("mConfig");
    require('jqZtree');

    require('jqLayout');
    require('neEvent');
    require('neToastr');
    require('jqDualListTable');

    var $dualList;

    var nePageArgum = {
        option: Nenu.open.data.child,
        aType: {
            Group: '分组',
            Teacher: '教师',
            Student: '学生',
            Parents: '家长'
        }
    };

    var nePageLogic = {
        event: function () {
            var newNum = 1;
            Nenu.context.event = {

                fnSure: function () {
                    var data = $dualList.neDualListTable('getData');
                    nePageArgum.option.setParentData(data);
                    nePageArgum.option.close();
                }
            }
        },

        init: function () {
            this.event();
        }
    };

    nePageLogic.init();

    function fnLoad() {
        // var $dualList;
        var $tabs = $('.jc-tabs');
        var aryBtns = $('li', $tabs);
        // 参数type为页面页签类型
        var fnTree = function (url, type) {
            //初始化tree
            var $tree = $('#js_tree').neTree({

                setting: {

                    async: {enable: true, url: url + $.param(formObj)},

                    check: {enable: true},

                    callback: {
                        onClick: function (event, treeId, treeNode) {
                            $.ajax({
                                url: '/notice/query' + type + 'Data',
                                data: {id: treeNode.id},
                                dataType: 'json',
                                type: "post",
                                async: false,
                                success: function (jsonData) {
                                    $dualList.neDualListTable('setData', {data: jsonData});
                                }
                            });
                        }
                    }
                },
                data: [{"id": "00", "name": nePageArgum.aType[type], "isParent": true}]
            });
        };

        //定义tabls按钮点击事件
        $tabs.on('click', 'a', function (e) {

            e.stopPropagation();

            var $this = $(this);

            var treeType = $this.data().type;

            formObj.paraTreeId = treeType;

            aryBtns.each(function () {
                $(this).removeClass('active');
            });

            $this.parent().addClass('active');

            fnTree('/notice/initTree?', treeType);
        });

        //初始化neDualListTable
        $dualList = $('#js_dual_list_table').neDualListTable({

            rightData: nePageArgum.option.neData,

            neTable: {
                table: {
                    columns: [        //列设置
                        {checkbox: true, field: 'state'},
                        {field: 'userName', title: '姓名', name: 'userName'},
                        {field: 'phone', title: '电话', name: 'phone'},
                        {field: 'orgId', title: '部门', name: 'orgId'}],//行数据加载时候的回调 , 这个使用默认即可
                    rowAttributes: function (row, index) {
                        return row.state = false;
                    }

                }
            }
        })
        ;
        //默认加载分组
        $('a', aryBtns[0]).trigger('click');
    }

    //初始化layout
    $('body .tab-pane .panel-body ').layout({
        name: 'manLayout',
        west: {size: 250},
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: fnLoad
    });

});