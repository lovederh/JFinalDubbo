/**
 * Created by dsideal-lee on 2016/10/9.
 */

$(document).ready(function(){
   document.getElementById("loginId").focus();

   $("#loginId").keydown(function (event) {
       if(event.keyCode == 13){
           if($("#loginId").val()){
               $("#passwd").focus();
           }
       }
   });
   $("#passwd").keydown(function(event){
       if(event.keyCode==13){
          login();
       }
   });
});

function login() {
    var errorMsg = "";
    var loginId = $("#loginId").val();
    var passwd = $("#passwd").base64encode();
    var errorFlag = false;

    if( !loginId){
        errorMsg += "&nbsp;&nbsp;用户名不能为空!";
        errorFlag = true;
    }
    if( !errorFlag && !passwd){
        errorMsg += "&nbsp;&nbsp;密码不能为空!";
        errorFlag = true;
    }
    if( errorFlag ){
        $(".login_info").html(errorMsg);
        $(".login_info").show();
    }
    else{
        $(".login_info").show();
        $(".login_info").html("&nbsp;&nbsp;正在登录中...");
        $.post("/login/check",{loginId: loginId, passwd: passwd},
            function(jsonData){
                if(jsonData.status == false){
                    $(".login_info").html(jsonData.message);
                }
                if(jsonData.status == 200){
                    window.location = jsonData.redirectTo;
                }
            },"json");
    }
}



