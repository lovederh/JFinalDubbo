package fileservice.controller;

import edu.dsideal.campus.common.PoiWord2Html;

/**
 * 2017/2/21 0021
 */
public class Word2HtmlRunnable implements Runnable {


    private String file_name;
    private String qualifiedName;
    private String fileRootPath;

    private String fileType;


    public Word2HtmlRunnable(String fileType, String file_name, String qualifiedName, String fileRootPath) {
        this.fileType = fileType;
        this.file_name = file_name;
        this.qualifiedName = qualifiedName;
        this.fileRootPath = fileRootPath;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);

            String finalHtmlName = file_name + ".html";
            //Word2Html.wordToHtml(qualifiedName, fileRootPath + "\\" + finalHtmlName);

            PoiWord2Html poiWord2Html = new PoiWord2Html(fileType, qualifiedName, fileRootPath + "\\" + finalHtmlName, fileRootPath + "\\image\\");
            poiWord2Html.word2html();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
}
