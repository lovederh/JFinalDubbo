package edu.dsideal.campus.config;

import com.jfinal.plugin.activerecord.Db;

import java.util.List;

/**
 * 2017/2/23
 */
public class MysqlJdbcHelper {

    public static boolean checkAszjcUser(String loginUser, String loginUserName){
        long l = Db.queryLong("select count(*) from t_sys_user where loginId=? and userName=? ", loginUser, loginUserName);
        return l >= 1;
    }

    public static List<Object> findAszjcUserRole(String loginUser){
        List<Object> rs = Db.query("select roleId from t_sys_user_role where loginId=? ", loginUser);
        return rs;
    }
}
