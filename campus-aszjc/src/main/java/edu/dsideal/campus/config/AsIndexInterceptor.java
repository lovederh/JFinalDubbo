package edu.dsideal.campus.config;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.snaker.service.aszjc.IFlowAszjcService;
import org.springframework.context.ApplicationContext;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 全局拦截器
 * 判断用户是否登陆状态及controllerKey是否有权限访问
 * Created by dsideal-lee on 2016/10/20.
 */
public class AsIndexInterceptor implements Interceptor {
    // 默认登录主页控制
    private static final String DEFAULT_MAIN_ACTION = "http://221.203.37.131:8081/zjgl/rrlogin.action";//登录首页
    //基础开发平台的访问路径，不允许其非开发人员访问
    private static final String[] PERMISSION_CONTROLLER_KEY = {"/developer", "/meta", "/bo", "/dict", "/update", "/menu"};

    @Override
    public void intercept(Invocation inv) {
        String ctrlKey = inv.getControllerKey();


        Controller controller = inv.getController();
        String loginUserType = controller.getPara("loginUserType");

        if("/login".equals(ctrlKey)|| "/registerCode".equals(ctrlKey) || "/registSystem".equals(ctrlKey)){
            inv.invoke();
        }else if("/".equals(ctrlKey)){
            inv.getController().redirect(DEFAULT_MAIN_ACTION);
        }else if("/welcome".equals(ctrlKey) && "aszjc".equals(loginUserType)){

            //构造鞍山职教城的登录用户
            String loginUser = controller.getPara("loginUser");
            String loginUserName = "";
            try {
                loginUserName = URLDecoder.decode(controller.getPara("loginUserName"), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            //校验登陆用户是否在user表存在
            boolean result = MysqlJdbcHelper.checkAszjcUser(loginUser, loginUserName);
            if(result){

                //获取人员角色
                List<Object> roleList = MysqlJdbcHelper.findAszjcUserRole(loginUser);
                if(roleList==null || roleList.isEmpty()){
                    inv.getController().redirect(DEFAULT_MAIN_ACTION);
                    return;
                }

                String roles = "";
                for(int i=0, size=roleList.size(); i<size ; i++){
                    if(i != 0){
                        roles += ",";
                    }
                    roles += roleList.get(i);
                }

                LoginUserObj loginUserObj = new LoginUserObj();
                loginUserObj.setLoginId(loginUser);
                loginUserObj.setDomain("aszjc");
                loginUserObj.setUserName(loginUserName);

                loginUserObj.setRoles(roles);

                // session域中存放登陆用户对象loginUserObj
                controller.setSessionAttr("loginUserObj", loginUserObj);

                inv.invoke();
            }else{
                inv.getController().redirect(DEFAULT_MAIN_ACTION);
            }

        }else{
            if (inv.getController().getSessionAttr("loginUserObj") == null) {
                inv.getController().redirect(DEFAULT_MAIN_ACTION);
            }else{
                String domain =((LoginUserObj) inv.getController().getSessionAttr("loginUserObj")).getDomain();
                //判断是否访问了开发人员才能访问的菜单
                if(isHavePermission(ctrlKey)){
                    //判断是否为开发人员
                    if("domain".equals(domain)) {
                        inv.invoke();
                    }else{
                        //非开发人员跳到登陆页面
                        inv.getController().redirect(DEFAULT_MAIN_ACTION);
                    }
                }
                inv.invoke();
            }
        }
    }

    private boolean isHavePermission(String ctrlKey){
        return Arrays.asList(PERMISSION_CONTROLLER_KEY).contains(ctrlKey);
    }
}
