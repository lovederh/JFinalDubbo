package edu.dsideal.campus.config;

import com.jfinal.core.Const;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import edu.dsideal.campus.framework.config.FrameworkConfig;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.log.Log;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import edu.dsideal.campus.common.Base64;
import edu.dsideal.campus.common.ConstStatic;
import edu.dsideal.campus.common.OSUtil;
import edu.dsideal.campus.common.RSAUtils;
import edu.dsideal.campus.snaker.config.SnakerRoutes;
import fileservice.config.FileServiceConfig;
import edu.dsideal.campus.framework.plugin.spring.SpringPlugin;
import edu.dsideal.campus.interceptor.IndexInterceptor;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

public class MainConfig extends JFinalConfig {
    private static Log log = Log.getLog(MainConfig.class);
    private GroupTemplate groupTemplate;

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(true);
        JFinal3BeetlRenderFactory rf = new JFinal3BeetlRenderFactory();
        rf.config();
        me.setRenderFactory(rf);
        GroupTemplate gt = rf.groupTemplate;
        me.setEncoding("UTF-8");
        //默认10M,此处设置为最大1000M
        me.setMaxPostSize(100 * Const.DEFAULT_MAX_POST_SIZE);
    }

    @Override
    public void configHandler(Handlers me) {
    }

    @Override
    public void configInterceptor(Interceptors me) {
        //全局拦截器
        me.add(new AsIndexInterceptor());
    }

    @Override
    public void configPlugin(Plugins me) {
        me.add(new SpringPlugin());
        //配置redis缓存插件
        RedisPlugin redisPlugin = new RedisPlugin("dssm", "127.0.0.1", 6379, 100000);
        redisPlugin.getJedisPoolConfig().setMaxIdle(20);
        me.add(redisPlugin);



        // 定义数据库连接信息
        loadPropertyFile("jdbc.properties");
        String jdbcUrl = getProperty("jdbcUrl");
        String user = getProperty("user");
        String password = getProperty("password");
        String driverClass = getProperty("driver");
        C3p0Plugin cp = new C3p0Plugin(jdbcUrl, user, password, driverClass);
        me.add(cp);

        ActiveRecordPlugin arp = new ActiveRecordPlugin(cp);
        me.add(arp);
        // 配置Mysql方言
        arp.setDialect(new MysqlDialect());
        // 是否显示sql
        arp.setShowSql(true);
    }

    @Override
    public void configRoute(Routes me) {
        me.add(new FrameworkConfig());
        me.add(new FileServiceConfig()); // 行政管理
        // 流程处理请求路由
        me.add(new SnakerRoutes());
    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void afterJFinalStart() {
        System.out.println("Demo consumer for Dubbo启动完成");
        try {
            isRegistSystem();
        } catch (Exception e) {
            log.error("判断是否注册出现异常:" + e);
        }
    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8091, "/", 5);
    }

    public GroupTemplate getGroupTemplate() {
        return groupTemplate;
    }

    public void setGroupTemplate(GroupTemplate groupTemplate) {
        this.groupTemplate = groupTemplate;
    }

    //regist
    private static void isRegistSystem() throws Exception {
//        String privateKey = RSAUtils.PRIVATE_KEY;
        String publicKey = RSAUtils.PUBLIC_KEY;
//        System.err.println("私钥加密——公钥解密");
//        String source = new StringBuilder(OSUtil.getOS()).append("@")
//                .append(OSUtil.getIpAddress()).append("@")
//                .append(OSUtil.getMacAddress()).append("@dsideal").toString();
//        byte[] data = source.getBytes("UTF-8");
//        //私钥加密
//        byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
//        String encodeStr = Base64.encode(encodedData);
//        System.out.println(encodeStr);
        String encodeStr = "HXeXiaN94Hns6OMu6DFBUiFcKjgJpMXd5/FEbH7oVTORUdguR4tdfOxoRvBnSi6zD6IR7JijN2nxBT3PemN645l/XgsQZwdmnJqi+AgpiYoR9aB+3zE3hHO/9Kp9Xj2YbTajlmXl/mhUAoRGOUkYAXq5+rYZktS731yYbPbie0k=";
//        String encodeStr = "HXeXiaN94Hns6OMu6DFBUiFcKjgJpMXd5/FEbH7oVTORUdguR4tdfOxoRvBnSi6zD6IR7JijN2nxBT3PemN645l/XgsQZwdmnJqi+AgpiYoR9aB+3zE3hHO/9Kp9Xj2YbTajlmXl/mhUAoRGOUkYAXq5+rYZktS731yYbPbie0k";
        try {
            byte[] decodedData = RSAUtils.decryptByPublicKey(Base64.decode(encodeStr), publicKey);
            String authorizeCode = new String(decodedData);
            String[] config = authorizeCode.split("@");
            if (OSUtil.getOS().equals(config[0]) && OSUtil.getIpAddress().equals(config[1])
                    && OSUtil.getMacAddress().equals(config[2])
                    && ConstStatic.COPYRIGHT_DSIDEAL.equals(config[3])) {
                Redis.use("dssm").set("isThisSystemRegister", "true");
            } else {
                Redis.use("dssm").set("isThisSystemRegister", "false");
            }
        } catch (Exception e) {
            log.error("判断是否注册出现异常:" + e);
            Redis.use("dssm").set("isThisSystemRegister", "false");
        }
    }
}

