package edu.dsideal.campus.snaker.config;

import com.jfinal.config.Routes;
import edu.dsideal.campus.snaker.controller.SysFormsController;
import edu.dsideal.campus.snaker.controller.base.*;

/**
 * 涉及流程的请求路由
 * 2016/11/21
 */
public class SnakerRoutes extends Routes {
    /**
     * 为了将流程请求与普通请求区分开, 建议: 涉及流程的处理类, 路由名中包含flow
     */
    @Override
    public void config() {
        /**********************通用请求处理**************************/
        //流程查询
        add("/flowCustom", FlowCustomController.class, "WEB-INF/view/snaker");
        //流程处理
        add("/flowDeal", FlowDealController.class, "WEB-INF/view/snaker");
        //流程查询
        add("/flowQuery", FlowQueryController.class, "WEB-INF/view/snaker");
        //已安装流程配置
        add("/flowSet", FlowSetController.class, "WEB-INF/view/snaker");
        //流程使用表单(即业务表单)请求
        add("/flowUserForm", FlowUserFormController.class, "WEB-INF/view/snaker");

        //审批类型工单处理特殊请求处理
        add("/flowApprovalType", FlowApprovalTypeController.class, "WEB-INF/view/snaker");
        //流程数据中, 后期需要从外部获取的请求处理
        add("/flowApiData", FlowApiDataController.class, "WEB-INF/view/snaker");

        /**********************个性化流程处理**************************/
        //所有的系统表单请求
        add("/sysForms", SysFormsController.class, "WEB-INF/view/snaker/sysForms");
    }
}
