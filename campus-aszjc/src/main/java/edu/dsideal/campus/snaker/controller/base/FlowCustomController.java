package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.interceptor.InitInterceptor;
import edu.dsideal.campus.snaker.bean.FlowSource;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowCustomService;

import java.util.List;

/**
 * 流程定制处理类
 * 附: 该类中的通用url
 *      /flowCustom/toAllSysFlowGrid 所有系统流程列表
 *      /flowCustom/toUserNoInstallFlows (用户流程)待安装列表
 */
@Before(IocInterceptor.class)
public class FlowCustomController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowCustomService flowCustomService;

    /********************************* 待安装列表 *********************************/
    /**
     * 系统流程 - 所有系统流程列表
     */
    @Before(InitInterceptor.class)
    public void toAllSysFlowGrid() {
        render("custom/allSysFlowGrid.html");
    }
    /**
     * 所有的系统流程列表数据
     */
    public void allSysFlowGridData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<FlowSource> flows = flowCustomService.allSysFlowGridData(loginUserObj, getParaMap());
        if(flows != null){
            renderJson(flows);
        }else{
            renderNull();
        }
    }

    /**
     * 用户定制流程 - 待安装
     */
    @Before(InitInterceptor.class)
    public void toUserNoInstallFlows() {
        render("custom/userNoInstallFlows.html");
    }
    /**
     * 所有的待安装的, 用户定制流程列表
     */
    public void userNoInstallData() {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        List<FlowSource> flows = flowCustomService.userNoInstallData(loginUserObj, getParaMap());
        if(flows != null){
            renderJson(flows);
        }else{
            renderNull();
        }
    }

    /*********************************** 流程定制 ***********************************/
    /**
     * 新定制/修改定制流程功能, 流程类型为用户定制
     */
    public void openCustomFlowForm(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 流程配置对象
        String processSetId = getPara("processSetId");
        Record processSet = baseFlowDbService.findProcessSetById(processSetId);
        processSet = null==processSet? new Record(): processSet;
        processSet.set("processType", getPara("processType"));
        setAttr("processSet", processSet);
        // 表单类型下拉框
        setAttr("userFormList", baseFlowDbService.findUserFormList(loginUserObj));
        // 流程定制类型下拉框
        setAttr("customTypeList", baseFlowDbService.findDictListByType(loginUserObj, "dict_customType"));
        setAttr("processType", "user");
        render("custom/customFlowForm.html");
    }
    /**
     * 保存用户定制的流程
     */
    public void saveCustomFlow() throws Exception{
        boolean result;
        String msg;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowCustomService.saveCustomFlow(loginUserObj, getParaMap());
            result = true;
            msg = "流程定制已保存!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 流程定制保存失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }
}
