package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowDealService;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;
import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_TASK_ID;


/**
 * 流程处理
 * 附: 该类中的通用url
 *      查看流程: /flowDeal/toViewTaskTab?dealFlag=false&...
 * 	    处理流程环节: /flowDeal/toViewTaskTab?dealFlag=true&...
 * 	    删除工单: /flowDeal/deleteOrder
 */
@Before(IocInterceptor.class)
public class FlowDealController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowDealService flowDealService;

    /**
     * 标记抄送的流程实例为已读
     */
    public void ccOrdersRead() throws Exception{
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String[] userPriv = baseFlowDbService.initUserPrivs(loginUserObj);// 当前登陆人权限
        renderJson(flowDealService.ccOrdersRead(loginUserObj, userPriv, getParaMap()));
    }

    /**
     * 确认受理工单某环节(提取任务), 传入当前环节ID即可
     */
    public void acceptOrderTask() throws Exception {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");// 当前登陆人
        flowDealService.acceptOrderTask(loginUserObj, getParaMap());
        renderJson(CommonTools.getMsgJson(true, "任务提取成功, 请及时处理!"));
    }

    /**
     * 强制归档指定工单, 传入工单ID即可
     */
    public void terminateOrder() throws Exception {
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");// 当前登陆人
        renderJson(flowDealService.terminateOrder(loginUserObj, getParaMap()));
    }

    /**
     * 彻底删除指定工单, 传入工单ID即可
     */
    public void deletePhysicalOrder() {
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");// 当前登陆人
            flowDealService.physicalDeleteOrder(loginUserObj, getParaMap());
            result = true;
            msg = "工单已经彻底删除!";
        }catch (Exception e){
            result = false;
            msg = "工单删除操作失败, 请重试!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 通过流程名称启动流程, 打开初始tab页
     * 根据企业域过滤, 查找该流程名下最新版本, 控制流程启动
     */
    public void instanceProcessTab(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");// 当前登陆人
        //获取流程名下最新版本配置信息
        Record processSet = flowDealService.findProcessSetByPara(loginUserObj, getParaMap());
        if(processSet == null){
            // 未找到相应流程, 以错误形式返回
            setAttr("errorMsg", "对不起，流程启动失败。");
            setAttr("errorDesc", "可能原因：<br/>（1）流程尚未安装；<br/>（2）流程配置错误；<br/>（3）你还没有启动流程的权限。");
            render("/WEB-INF/view/errorMsg.html");
            return;
        }
        setAttr("processSet", processSet);
        setAttr(ATTR_PROCESS_ID, processSet.get(ATTR_PROCESS_ID));
        setAttr("taskInfoId", getPara("taskInfoId"));//草稿派发情况
        render("deal/startProcessTab.html");
    }

    /**
     * 处理流程/查看流程的通用请求, 打开存在两个tab页的主界面
     * 依据dealFlag(处理标识)判断请求方式, dealFlag为真则进行环节处理, 否则只查看
     * 注: 处理与查看体现在功能按钮上, 处理时会显示审批(不)通过/重新派发等按钮
     */
    public void toViewTaskTab(){
        //保留请求参数, 包括: processId/orderId/taskId/dealFlag/isCcOrder
        keepPara();
        String orderId = getPara(ATTR_ORDER_ID);//工单ID
        //查询到流程实例(工单)信息
        Record order = baseFlowDbService.findHistoryOrderById(orderId);
        setAttr("order", order);

        render("deal/viewTaskTab.html");
    }

    /**
     * 处理工单请求, 弹出环节表单
     * 前提: 工单已经存在, 此时获取流程图配置时使用的节点url
     */
    public void openDealTaskForm(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String taskFormUrl = flowDealService.findTaskFormUrlByTaskId(loginUserObj, getPara(ATTR_PROCESS_ID), getPara(ATTR_TASK_ID));
        //路径重新定向, 请求当前环节表单url
        redirect(taskFormUrl, true);
    }

    /**
     * 执行任务节点处理, 同时保存业务数据
     */
    public void dealTaskSaveBusiness() {
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowDealService.dealTaskSaveBusiness(loginUserObj, getParaMap());
            result = true;
            msg = "恭喜, 处理成功!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 处理失败, 请重试!";
            e.printStackTrace();
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 保存草稿数据, 不启动工单
     */
    public void saveDraftData(){
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowDealService.saveDraftData(loginUserObj, getParaMap());
            result = true;
            msg = "草稿保存成功!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 草稿保存失败, 请重试!";
            e.printStackTrace();
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 删除草稿
     */
    public void deleteDraft(){
        flowDealService.deleteDraft(getPara("taskInfoId"));
        renderJson(CommonTools.getMsgJson(true, "草稿已经删除!"));
    }
}
