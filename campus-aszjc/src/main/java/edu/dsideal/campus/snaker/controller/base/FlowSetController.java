package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.bean.ProcessTaskAllActorsObj;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowSetService;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_PROCESS_ID;

/**
 * 已安装流程配置
 * 附: 该类中的通用url
 *      /flowSet/deployFlow 执行流程安装
 */
@Before(IocInterceptor.class)
public class FlowSetController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowSetService flowSetService;

    /********************************* 卸载与流程文件删除 *********************************/
    /**
     * 流程文件删除
     */
    public void deleteFlowFile(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        renderJson(flowSetService.deleteFlowFile(loginUserObj, getParaMap()));
    }

    /**
     * 控制已安装过的系统流程不再可用(批量卸载)
     * 操作: 对于所有安装该流程的企业域, 均执行该流程的卸载操作
     */
    public void batchUndeployFlow() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowSetService.batchUndeployFlow(loginUserObj, getParaMap());
            result = true;
            msg = "企业域流程批量卸载完毕!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 批量卸载失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 流程卸载
     */
    public void undeployFlow() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowSetService.undeployFlow(loginUserObj, getParaMap());
            result = true;
            msg = "流程已经卸载完毕!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 流程卸载失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /********************************** 流程安装处理 ***********************************/
    /**
     * 流程安装
     */
    public void deployFlow() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowSetService.deployFlow(loginUserObj, getParaMap());
            result = true;
            msg = "流程已经安装完毕!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 流程安装失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 系统流程的批量安装
     */
    public void batchDeploySysFlow() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowSetService.batchDeploySysFlow(loginUserObj, getParaMap());
            result = true;
            msg = "企业域流程批量安装完毕!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 批量安装失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 系统流程安装时, 跳转至可填入通用配置的页面(常用的有拦截器属性配置)
     * 目的: 为本次安装的企业域设置相同的配置, 以后再要改只能去数据库改
     */
    public void toDeployInputCommonSet(){
        //查询类型: install已经装过的/noInstall没有安装过的
        setAttr("type", getPara("type"));
        //系统流程文件全路径
        setAttr("fileResource", getPara("fileResource"));
        render("set/deployInputCommonSet.html");
    }

    /*********************************** 已安装流程修改配置 ***********************************/
    /**
     * 已经安装过的流程进行整体配置时, 不再展示那些会使流程图发生改变的配置项
     */
    public void openFlowSetForm(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String processId = getPara(ATTR_PROCESS_ID);
        Record processSet = baseFlowDbService.findProcessSetByProcessId(processId);
        if(null == processSet){
            //未找到流程配置, 以错误形式返回
            setAttr("errorMsg", "对不起，未找到指定流程，流程可能已损坏。");
            render("/WEB-INF/view/errorMsg.html");
            return;
        }
        setAttr("processSet", processSet);
        // 表单类型下拉框
        setAttr("userFormList", baseFlowDbService.findUserFormList(loginUserObj));
        render("set/flowSetForm.html");
    }
    /**
     * 流程配置更新, 针对已经安装过的流程, 本次保存不会让流程图重置
     */
    public void updateFlowSet() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            flowSetService.updateFlowSet(loginUserObj, getParaMap());
            result = true;
            msg = "流程配置已修改!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 流程配置保存失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /*********************************** 节点环节配置 ***********************************/
    /**
     * 流程环节配置的更新方法, 主要包含环节表单配置和审批人配置
     */
    public void updateFlowTaskSet() throws Exception{
        String msg;
        boolean result;
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            //flowAszjcService.updateFlowTaskSet(loginUserObj, getParaMap());
            flowSetService.updateFlowTaskSet(loginUserObj, getParaMap());
            result = true;
            msg = "环节节点配置信息已保存!";
        }catch (Exception e){
            result = false;
            msg = "对不起, 配置信息保存失败!";
        }
        renderJson(CommonTools.getMsgJson(result, msg));
    }

    /**
     * 打开流程环节配置主页面(已安装过的流程), 包含三个tab, 内部嵌套iframe
     */
    public void openFlowTaskSetTab(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        String processId = getPara(ATTR_PROCESS_ID);
        Record processSet = baseFlowDbService.findProcessSetByProcessId(processId);
        if(null == processSet){
            //未找到流程配置, 以错误形式返回
            setAttr("errorMsg", "对不起，未找到指定流程，流程可能已损坏。");
            render("/WEB-INF/view/errorMsg.html");
            return;
        }
        setAttr("processSet", processSet);
        setAttr("taskSetObjs", flowSetService.findBaseFlowTaskSets(loginUserObj, getParaMap()));
        render("set/flowTaskSetTab.html");
    }
    /**
     * 打开流程环节配置表单页面
     */
    public void openFlowTaskSetForm(){
        keepPara();
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 表单类型下拉框
        setAttr("userFormList", baseFlowDbService.findUserFormList(loginUserObj));
        //处理环节基本配置
        setAttr("taskSetObj", flowSetService.findOneFlowTaskSetObj(loginUserObj, getParaMap()));
        render("set/flowTaskSetForm.html");
    }


    /**
     * 查询获取处理环节处理者配置
     */
    public void findProcessTaskActors(){
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        ProcessTaskAllActorsObj taskActors = flowSetService.findProcessTaskActors(loginUserObj, getParaMap());
        renderJson(taskActors);
    }
    /**
     * 打开选择环节处理者的双选器页面
     * 目前支持: 人员选择; 角色选择
     */
    public void toPickTaskActors(){
        keepPara();
        render("set/pickTaskActors.html");
    }
}
