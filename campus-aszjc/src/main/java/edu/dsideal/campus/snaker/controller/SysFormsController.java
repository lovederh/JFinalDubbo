package edu.dsideal.campus.snaker.controller;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import edu.dsideal.campus.common.CommonTools;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.ISysFormsService;
import fileservice.service.IFileService;

import static edu.dsideal.campus.snaker.FlowConstDefine.BUSINESS_BEAN_KEY;

/**
 * 系统表单请求处理
 * 2016/12/26
 */
@Before(IocInterceptor.class)
public class SysFormsController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private ISysFormsService sysFormsService;

    @Inject.BY_NAME
    IFileService fileService;


    /**
     * 政府或上级部门文件审阅
     */
    public void govtFile(){
        // 当前登陆人
        LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
        // 展示类型: view展示明细; form: 展示表单
        String showType = getPara("showType");
        if("form".equals(showType)){
            //设置当前用户为上传人
            setAttr("uploadUser", loginUserObj.getUserName());
            //共享域设置当前时间(上传时间)
            setAttr("uploadDate", CommonTools.getCurrentTimeYmdhms());

            render("aszjc/govtFile.html");
        }else{
            Record bean = (Record)getAttr(BUSINESS_BEAN_KEY);
            //拿到上传的文件对象
            Record fileInfo = fileService.findDownloadFile(bean.get("uploadFile"));
            setAttr("fileInfo", fileInfo);

            //根据文件类型, 判断文件是否需要直观呈现, 返回iframe的Url
            String iframeUrl = sysFormsService.initIframeFileUrl(loginUserObj, getParaMap(), fileInfo);
            if(CommonTools.isNotEmpty(iframeUrl)){
                setAttr("iframeUrl", iframeUrl);
            }

            render("aszjc/govtFileView.html");
        }
    }
}
