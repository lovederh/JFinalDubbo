package edu.dsideal.campus.snaker.controller.base;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import edu.dsideal.campus.framework.dao.LoginUserObj;
import edu.dsideal.campus.framework.plugin.spring.Inject;
import edu.dsideal.campus.framework.plugin.spring.IocInterceptor;
import edu.dsideal.campus.snaker.bean.TaskFormAttr;
import edu.dsideal.campus.snaker.service.IBaseFlowDbService;
import edu.dsideal.campus.snaker.service.IFlowUserFormService;

import static edu.dsideal.campus.snaker.FlowConstDefine.ATTR_ORDER_TITLE;
import static edu.dsideal.campus.snaker.FlowConstDefine.BUSINESS_BEAN_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.BUSINESS_TASK_INFO_KEY;
import static edu.dsideal.campus.snaker.FlowConstDefine.FORM_HTML_OBJ_KEY;


/**
 * 流程使用的用户表单(即业务表单)请求
 * 附: 该类中的通用url
 *      流程环节通用的表单url: /flowUserForm/initTaskForm
 * 2016/12/22
 */
@Before(IocInterceptor.class)
public class FlowUserFormController extends Controller {
    @Inject.BY_NAME
    private IBaseFlowDbService baseFlowDbService;
    @Inject.BY_NAME
    private IFlowUserFormService flowUserFormService;

    /**
     * 基于配置表, 通用的打开环节表单的请求
     * 同时展示业务数据(明细时查看环节数据, 重复环节时加载最近一次填报的)
     * 同时判断审批结果, 若为审批通过则展示配置的表单; 审批不通过则只展示备注项
     */
    public void initTaskForm() {
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            TaskFormAttr formAttr = flowUserFormService.initTaskFormAttr(loginUserObj, getParaMap());
            if(null == formAttr){
                throw new RuntimeException("任务环节表单对象, 无法实例化!");
            }

            //请求参数共享attribute域
            keepPara();
            //TaskFormAttr内部属性置于attribute域
            setAttr("isDraftStart", formAttr.getIsDraftStart());//草稿派发标识(新建工单有效)
            setAttr(ATTR_ORDER_TITLE, formAttr.getOrderTitle());//工单主题(新建工单有效)
            setAttr("processTaskSet", formAttr.getProcessTaskSet());//流程与任务节点配置
            setAttr("approvalResult", formAttr.getApprovalResult());//审批结果
            setAttr(BUSINESS_TASK_INFO_KEY, formAttr.getTaskInfo());//任务节点基本信息
            setAttr(BUSINESS_BEAN_KEY, formAttr.getBusinessBean());//节点业务数据(用户表单数据)
            setAttr(FORM_HTML_OBJ_KEY, formAttr.getFormHtmlObj());//节点表单对应的html字符串
            setAttr("nextTaskActors", formAttr.getNextTaskActors());//紧邻当前环节的处理环节下, 处理者集合
            if(null != formAttr.getOtherData()){
                setAttr("otherData", JsonKit.toJson(formAttr.getOtherData()));//节点表单对应的html字符串
            }

            //涉及新的url, 则执行forward跳转(注意: 配置的跳转url一定不要处于当前controller中)
            if("forward".equals(formAttr.getFormUrlType())){
                forwardAction(formAttr.getFormUrl());
            }else{
                //返回渲染html页面, 则使用render
                render(formAttr.getFormUrl());
            }
        }catch (Exception e){
            e.printStackTrace();
            setAttr("errorMsg", "对不起, 流程配置信息缺失, 无法进行操作!");
            render("/WEB-INF/view/errorMsg.html");
        }
    }

    /**
     * 流程类型为审批流程情况下, 工单详情展示
     * 各任务环节的展示页面统一: 上面展示第一环节填报的表单(最近一次的), 下面展示审批列表
     * 本次请求即处理第一环节填报表单的展示
     */
    public void openApprovalViewPage() {
        try {
            LoginUserObj loginUserObj = (LoginUserObj)getSessionAttr("loginUserObj");
            TaskFormAttr formAttr = flowUserFormService.initTaskFormAttr(loginUserObj, getParaMap());
            if(null == formAttr){
                throw new RuntimeException("任务环节表单对象, 无法实例化!");
            }

            //请求参数共享attribute域
            keepPara();
            //TaskFormAttr内部属性置于attribute域
            setAttr("processTaskSet", formAttr.getProcessTaskSet());//流程与任务节点配置
            setAttr(BUSINESS_TASK_INFO_KEY, formAttr.getTaskInfo());//任务节点基本信息
            setAttr(BUSINESS_BEAN_KEY, formAttr.getBusinessBean());//节点业务数据(用户表单数据)
            setAttr(FORM_HTML_OBJ_KEY, formAttr.getFormHtmlObj());//节点表单对应的html字符串
            if(null != formAttr.getOtherData()){
                setAttr("otherData", JsonKit.toJson(formAttr.getOtherData()));//节点表单对应的html字符串
            }

            //涉及新的url, 则执行forward跳转(注意: 配置的跳转url一定不要处于当前controller中)
            if("forward".equals(formAttr.getFormUrlType())){
                forwardAction(formAttr.getFormUrl());
            }else{
                //返回渲染html页面, 则使用render
                render(formAttr.getFormUrl());
            }
        }catch (Exception e){
            e.printStackTrace();
            setAttr("errorMsg", "对不起, 流程配置信息缺失, 无法进行操作!");
            render("/WEB-INF/view/errorMsg.html");
        }
    }
}
