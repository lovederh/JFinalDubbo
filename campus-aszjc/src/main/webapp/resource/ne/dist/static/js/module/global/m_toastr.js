/**
 * 全局通知提示组件
 */

!(function (root, factory) {
    'use strict';
    if (typeof define === 'function') {
        if (define.amd) {
            // AMD
            define(['jqToastr'], factory);
        }
        if (define.cmd) {
            // CMD
            define(factory);
        }

    } else if (typeof exports === 'object') {
        // Node, CommonJS之类的
        //   module.exports = factory(require('jquery'), require('underscore'));
    } else {
        // 浏览器全局变量(root 即 window)
        root.returnExports = factory(root.jQuery);
    }
}(this, function (toastr) {
    'use strict';
    toastr.options = {
        "closeButton"     : false,
        "debug"           : false,
        "progressBar"     : true,
        "positionClass"   : "toast-top-right",
        "onclick"         : null,
        "showDuration"    : "400",
        "hideDuration"    : "1000",
        "timeOut"         : "2000",
        "extendedTimeOut" : "1000",
        "showEasing"      : "swing",
        "hideEasing"      : "linear",
        "showMethod"      : "fadeIn",
        "hideMethod"      : "fadeOut"
    };

    Nenu.toastr = {
        info    : function (msg) {
            toastr.info(msg);
        },
        warning : function (msg) {
            toastr.warning(msg);
        },
        success : function (msg) {
            toastr.success(msg);
        },
        error   : function (msg) {
            toastr.error(msg);
        }
    };
}));
