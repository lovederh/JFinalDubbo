define(function (require) {
    require('mConfig');
    require('jqBootstrapTable');

    var $grid = $('#js_grid');
    var jsonPath = Nenu.CONSTANT.PATH.JSON;
    $grid.neTable({

        //nenu 本系统定义的参数对象
        nenu : {

            query : '#js_query_form',

            toobarBtn : {

                search : {
                    switch : true,
                    sel    : '#js_query_form_btn_search'
                }
            },

            rowBtn : {
                list : [
                    {
                        title    : '添加',
                        text     : '添加',
                        icon     : 'glyphicon glyphicon-plus',
                        cls      : 'add',
                        callBack : function (e, value, row, index) {
                            console.log('添加');
                        }
                    },
                    {
                        title    : '修改按钮喔!',
                        text     : '修改',
                        icon     : 'glyphicon glyphicon-pencil',
                        cls      : 'edit',
                        callBack : function (e, value, row, index) {
                            console.log('修改');
                        }
                    }
                ]
            }

        },

        table : {
            toolbar : '#js_toolbar_grid',
            url     : jsonPath + "bootstrapTable/base.json", //ajax url

            columns : [
                {
                    checkbox : true,
                    field    : 'state'
                }, {
                    field : 'id',
                    title : 'ID'

                }, {
                    field : 'name',
                    title : '名称'

                }, {
                    field : 'price',
                    title : '价格'
                }
            ]
        }

    });


    $.neOn([
        {
            sel   : '#js_toolbar_grid_btn_add',
            eFunc : function (e) {
                alert('增加');

            }
        },
        {
            sel   : '#js_toolbar_grid_btn_edit',
            eFunc : function (e) {
                alert('修改');
            }
        }

    ]);

});