define ( function ( require ) {
    var $ = require ( 'jquery' );

    require ( 'jqLayout' );
    require ( 'jqZtree' );
    require ( 'jqDualListTable' );

    var jsonPath = Nenu.CONSTANT.PATH.JSON;

    var gridData = {
        gridGroup   : require ( 'json!jsonPath/dualListTable/gridGroup.json' ) ,
        gridParents : require ( 'json!jsonPath/dualListTable/gridParents.json' ) ,
        gridStudent : require ( 'json!jsonPath/dualListTable/gridStudent.json' ) ,
        gridTeacher : require ( 'json!jsonPath/dualListTable/gridTeacher.json' )
    };

    var rootName = {
        Group   : '分组' ,
        Teacher : '教师' ,
        Student : '学生' ,
        Parents : '家长'
    };

    function fnLoad () {

        var $dualList;
        var $tabs = $ ( '.jc-tabs' );
        var aryBtns = $ ( 'li' , $tabs );

        var fnTree = function ( url , type ) {



            //初始化tree
          var $tree =   $ ( '#js_tree' ).neTree ( {
                setting : {
                    async    : {
                        enable : true ,
                        url    : url
                    } ,
                    check    : {
                        enable : true
                    } ,
                    callback : {
                        onClick : function ( event , treeId , treeNode ) {
                            if ( treeNode.id == '00' )return;
                            $dualList.neDualListTable ( 'setData' , { data : gridData['grid' + type] } );
                        }
                    }
                } ,
                data    : [
                    {
                        "id"       : "00" ,
                        "name"     : rootName[type] ,
                        "isParent" : true
                    }
                ]

            } );

           // $tree.neTree('expand',true);


        };

        //定义tabls按钮点击事件
        $tabs.on ( 'click' , 'a' , function ( e ) {
            e.stopPropagation ();
            var $this = $ ( this );

            aryBtns.each ( function () {
                $ ( this ).removeClass ( 'active' );
            } );

            $this.parent ().addClass ( 'active' );
            
            fnTree ( jsonPath + 'dualListTable/get' + $this.data ().type + '.json' , $this.data ().type );
        } );

        //初始化neDualListTable
        $dualList = $ ( '#js_dual_list_table' ).neDualListTable ( {
            data : []
            ,
            neTable:{
                table:{
                    columns         : [

                        {
                            field : 'name' ,
                            title : '名称1'
                        },  
                        {
                            field : 'name' ,
                            title : '名称2'
                        }
                        , {
                            field : 'name' ,
                            title : '名称3'
                        }
                        , {
                            field : 'name' ,
                            title : '名称4'
                        }
                    ]
                }
            }
        } );



        //默认加载分组
        $ ( 'a' , aryBtns[0] ).trigger ( 'click' );

    }

    //初始化layout
    $ ( 'body .tab-pane .panel-body ' ).layout ( {
        name            : 'manLayout' ,
        west            : {
            size : 250
        } ,
        applyDemoStyles : false ,
        closable        : false ,
        spacing_open    : 15 ,
        onresize_end    : function () {
        } ,
        onload_end      : fnLoad

    } );

} );