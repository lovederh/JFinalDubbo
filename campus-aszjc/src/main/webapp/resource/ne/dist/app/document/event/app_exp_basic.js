define(function (require) {
    'use strict';
    require('mConfig');

    //加载event核心文件
    require('neEvent');

    require('jqLayer');


    /**
     * 页面级 页面参数对象
     * @namespace
     *
     *@param {Object}
     */
    var nePageArgum = {};


    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {

        /**
         * 全局页面事件对象
         * @private
         **/
        event : function () {
            Nenu.context.event = {

                fnOpenOne : {
                    title  : '弹出层',
                    url    : 'document/event/b_ifreame.html',
                    neData : { aaa : '第一层父亲' },
                    height : '800px',
                    width  : '1500px'
                }

                , fnOpenTow : function () {
                    //Todo 自定义操作

                    return {
                        height : '600px',
                        width  : '600px'
                    }
                }

                , ease : function () {  // 默认  不写这个函数是执行的空函数不会报错
                    alert('ease');
                }

                , ease2 : function () {
                    alert('ease2');
                }

                , ease3 : function () {
                    alert('ease3');
                }

                , confirm1 : {
                    isClose : false, title : '添加提示!', execute : function (close) {
                        console.log(this);
                    }
                }

                , confirm2 : function () {
                    //Todo 自定义操作

                    return {
                        execute : function (close) {
                            console.log(this);
                        }

                    }

                }


                , change : function (e) {
                    console.log(this);
                    console.log(e);
                },

                /**
                 * 直接调用 ifreame
                 * @param e
                 */
                directIfreame   : function (e) {

                    Nenu.event.ifreame({
                        url    : 'document/event/b_ifreame.html',
                        neData : { aaa : '第一层父亲' },
                        height : '800px',
                        width  : '1500px'
                    });

                }
                , directConfirm : function (e) {
                    Nenu.event.confirm(e);
                },
                directAlert     : function () {
                    Nenu.event.alert();

                },
                directMsg       : function () {
                    Nenu.event.msg();
                }
                , directLoad    : function () {
                    var index;

                    index = Nenu.event.load();
                    console.log(index);
                    setTimeout(function () {
                        layer.close(index);
                    }, 2000);


                }

            };
        },

        /**
         * 子页面关闭回调函数
         * @private
         **/
        end : function () {
            Nenu.open.end = function (option) {
                var data = Nenu.open.data.parent;
                console.log(option);
                console.log(data);
            };
        },

        bind : function () {
            Nenu.method.bind(['jc-change|change']);
        }

        ,
        /**
         * 业务逻辑 函数
         * @private
         **/
        business : function () {

        }
        ,
        /**
         * 初始化
         * @private
         **/
        init     : function () {
            this.event();
            this.business();
            this.bind();
            this.end();
        }
    };

    nePageLogic.init();

});