define ( function ( require ) {
    'use strict';
    require ( 'jquery' );

    //加载event核心文件
    require ( 'neEvent' );
    var upload = require ( 'neUpload' );

    /**
     * 页面级 页面参数对象
     * @namespace
     *
     *@param {Object}
     */
    var nePageArgum = {};

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {

        /**
         * 全局页面事件对象
         * @private
         **/
        event : function () {
            Nenu.context.event = {

                ease3 : function () {
                    upload.open ();
                }
            };
        }

        ,
        /**
         * 业务逻辑 函数
         * @private
         **/
        business : function () {

        }
        ,
        /**
         * 初始化
         * @private
         **/
        init     : function () {
            this.event ();
            this.business ();
        }
    };

    nePageLogic.init ();

} );