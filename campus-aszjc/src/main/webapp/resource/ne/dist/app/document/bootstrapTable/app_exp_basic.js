define(function (require) {
    var $ = require('jquery');
    require('bootstrap');
    require('jqBootstrapTable');

    var $grid = $('#js_grid');

    var jsonPath = Nenu.CONSTANT.PATH.JSON;

    $grid.neTable({

        //nenu 本系统定义的参数对象
        nenu : {

            query : '#js_grid_search_form',

            toobarBtn : {

                search : {
                    switch   : true,
                    callBack : function () {

                        //可以使用自定义
                       /* $grid.neTable('refresh', {
                            offset : 144
                        });
                        */
                    }
                },

                add : {
                    switch   : true,
                    callBack : function () {
                        console.log('添加按钮');
                    }
                },

                delete : {

                    switch    : true,
                    callBack  : function (e, arrySelectDataByID) {
                        console.log('删除按钮');
                    },
                    checkBack : function (arrySelectDataByID) {

                    }
                },

                edit : {
                    switch   : true,
                    callBack : function () {
                        console.log('修改按钮');
                    }
                }

            },
            rowBtn    : {
                list : [
                    {
                        title    : '添加',
                        text     : '添加',
                        icon     : 'glyphicon glyphicon-plus',
                        cls      : 'add',
                        callBack : function (e, value, row, index) {
                            console.log('添加');
                        }
                    },
                    {
                        title    : '修改按钮喔!',
                        text     : '修改',
                        icon     : 'glyphicon glyphicon-pencil',
                        cls      : 'edit',
                        callBack : function (e, value, row, index) {
                            console.log('修改');
                        }
                    }
                ]
            }

        },

        table : {

            url : jsonPath + "bootstrapTable/base.json", //ajax url

            columns : [
                {
                    checkbox : true,
                    field    : 'state'
                }, {
                    field : 'id',
                    title : 'ID'

                }, {
                    field : 'name',
                    title : '名称'

                }, {
                    field : 'price',
                    title : '价格'
                }
            ]
        }

    });


});