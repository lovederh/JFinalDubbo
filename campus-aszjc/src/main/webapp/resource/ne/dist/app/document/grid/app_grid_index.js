define(function (require) {
    var $ = require('jquery');
    require('jqGrid');


    var $table = $('#js_table');

    $table.jqGrid({
        url: '/json/jqGrid/index.json',
        mtype: "GET",
        styleUI: 'Bootstrap',
        datatype: "json",
        colNames: ['序号', '日期', '客户', '金额',  '备注'],
        colModel: [
            {label: 'OrderID', name: 'OrderID', key: true, width: 100},
            {label: 'Customer ID', name: 'CustomerID', width: 150},
            {label: 'Order Date', name: 'OrderDate', width: 200},
            {label: 'Freight', name: 'Freight', width: 200},
            {label: 'Ship Name', name: 'ShipName', width: 300}
        ],
        shrinkToFit: true,
        viewrecords: true,
        autowidth: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: "#js_table_page",
        caption: "jqGrid 示例2"

    });

    console.log($('.wrapper').outerHeight(true));






    $('.ui-jqgrid-bdiv').height($('.wrapper').outerHeight(true) - $('.ui-jqgrid-hdiv').offset().top - 36 - 39 - 2);

    $(window).bind('resize', function () {

        var width = $('.jc_wrapper').width();

        $('.ui-jqgrid-bdiv').height($('.wrapper').outerHeight(true)- $('.ui-jqgrid-hdiv').offset().top - 36 - 39 - 2);

        $table.setGridWidth(width);
    });


});










