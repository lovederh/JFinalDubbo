define(function (require) {
    require('mConfig');
    require('jqLayerDate');

    laydate({
        elem: '#js_default_form_date_date',
        format: 'YYYY-MM-DD hh:mm:ss',
        min: laydate.now(),
        istime: true,
        start: laydate.now()
    });

});