/**
 * 编辑信息按钮
 */
function editMyInfo(){
    $('#userNameShow').hide();
    $('#userName').val($('#userNameShow').html()).show();

    $('#genderShow').hide();
    $('#radioDiv').show();

    $('#emailShow').hide();
    $('#email').val($('#emailShow').html()).show();

    $('#cardNoShow').hide();
    $('#cardNo').val($('#cardNoShow').html()).show();

    $('#qqShow').hide();
    $('#qq').val($('#qqShow').html()).show();

    $('#wexinShow').hide();
    $('#wexin').val($('#wexinShow').html()).show();

    $('#phoneShow').hide();
    $('#phone').val($('#phoneShow').html()).show();

    $('#orgIdShow').hide();
    $('#orgId').val($('#orgIdShow').html()).show();

    $('#domainShow').hide();
    $('#domain').val($('#domainShow').html()).show();

    $('#saveMyInfoBtn').removeClass('btn-default').addClass('btn-primary').attr('disabled', false);
    $('#editMyInfoDiv').hide();
    $('#cancelDiv').show();

}
/**
 * 保存信息按钮
 */
function saveMyInfo(){
    var errorFlag = false;
    var userName = $("#userName").val();
    var cardNo = $("#cardNo").val();
    var phone = $("#phone").val();
    var orgId = $("#orgId").val();
    var domain = $("#domain").val();

    if(!userName){
        $('#userNameRemind').css('display','');
        errorFlag=true;
    }
    if(!cardNo){
        $('#cardNoRemind').css('display','');
        errorFlag=true;
    }
    if(!phone){
        $('#phoneRemind').css('display','');
        errorFlag=true;
    }
    if(!orgId){
        $('#orgIdRemind').css('display','');
        errorFlag=true;
    }
    if(!domain){
        $('#domainRemind').css('display','');
        errorFlag=true;
    }
    if(errorFlag){
        return;
    }else{
        $('#userNameShow').text(userName).show();
        $('#userName').hide();
        $('#userNameRemind').css('display','none');

        var genderText = '';
        if($('input:radio:checked').val() == '0'){
            genderText = '男';
        }else if($('input:radio:checked').val() == '1'){
            genderText = '女';
        }
        $('#genderShow').text(genderText).show();
        $('#radioDiv').css('display','none');

        $('#emailShow').text($('#email').val()).show();
        $('#email').hide();

        $('#cardNoShow').text($('#cardNo').val()).show();
        $('#cardNo').hide();
        $('#cardNoRemind').css('display','none');

        $('#qqShow').text($('#qq').val()).show();
        $('#qq').hide();

        $('#wexinShow').text($('#wexin').val()).show();
        $('#wexin').hide();

        $('#phoneShow').text(phone).show();
        $('#phone').hide();
        $('#phoneRemind').css('display','none');

        $('#orgIdShow').text(orgId).show();
        $('#orgId').hide();
        $('#orgIdRemind').css('display','none');

        $('#domainShow').text(domain).show();
        $('#domain').hide();
        $('#domainRemind').css('display','none');

        $('#saveMyInfoBtn').removeClass('btn-primary').attr('disabled', true);
        $('#editMyInfoDiv').show();
        $('#cancelDiv').hide();

        var formData = $('#myInfoForm').serializeArray();
        $.ajax({
            type:'POST',
            url:'/user/saveMyInfo',
            async: false,
            data: formData,
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if('200' == status){
                    layer.msg(msg, {
                        icon: 1,
                        btn: ['确定']
                    });
                }else{
                    layer.msg(msg, {
                        icon: 5,
                        time: 2000,//2s后自动关闭
                        btn: ['确定']
                    });
                }
            }
        })
    }
}
/**
 * 取消按钮
 */
function cancel(){
    $('#userNameShow').show();
    $('#userName').hide();
    $('#userNameRemind').css('display','none');
    $('#cardNoRemind').css('display','none');
    $('#phoneRemind').css('display','none');
    $('#orgIdRemind').css('display','none');
    $('#userNameRemind').css('display','none');

    $('#genderShow').show();
    var genderText = $('#genderShow').html();
    if(genderText=='女'){
        $('#gender1').prop("checked",true);
    }else if(genderText=='男'){
        $('#gender0').prop("checked",true);
    }
    $('#radioDiv').hide();

    $('#emailShow').show();
    $('#email').hide();

    $('#cardNoShow').show();
    $('#cardNo').hide();

    $('#qqShow').show();
    $('#qq').hide();

    $('#wexinShow').show();
    $('#wexin').hide();

    $('#phoneShow').show();
    $('#phone').hide();

    $('#orgIdShow').show();
    $('#orgId').hide();

    $('#domainShow').show();
    $('#domain').hide();

    $('#saveMyInfoBtn').removeClass('btn-primary').addClass('btn-default').attr('disabled', true);
    $('#cancelDiv').hide();
    $('#editMyInfoDiv').show();
}