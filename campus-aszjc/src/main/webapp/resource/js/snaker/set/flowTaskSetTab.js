define(function (require) {
    //加载组件
    require('mConfig');

    $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
    $(window).on('resize.neTableHt', function () {
        setTimeout(function () {
            $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
        }, 200);
    });

});