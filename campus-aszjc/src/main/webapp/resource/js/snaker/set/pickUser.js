define(function (require) {
    var $ = require('jquery');
    require("mConfig");
    require('jqZtree');

    require('jqLayout');
    require('neEvent');
    require('neToastr');
    require('jqDualListTable');


    var $dualList;


    //获取后端传入的json
    // var editMemberData = [];

    var allUserData = [];




    $.ajax({
        url: '/flowSet/findLoginUsers',
        dataType: 'json',
        type: "post",
        data: {
            taskSetId: pageParam.taskSetId
        },
        async: false,
        success: function (jsonData) {
            allUserData = jsonData;
        }
    });



    var nePageArgum = {
        option: Nenu.open.data.child
    };

    var nePageLogic = {
        event: function () {
            var newNum = 1;
            Nenu.context.event = {

                // 确定按钮
                fnSure: function () {
                    var data = $dualList.neDualListTable('getData');

                    nePageArgum.option.setParentData(data);
                    nePageArgum.option.close();
                },

                //取消按钮
                fnCancel: function () {
                    nePageArgum.option.close();
                }

            }
        },
        end: function () {
            Nenu.open.end = function () {


            }
        },

        init: function () {
            this.event();
            this.end();
        }
    };

    nePageLogic.init();




    function fnLoad() {

        //初始化neDualListTable
        $dualList = $('#js_dual_list_table').neDualListTable({
            data: allUserData,
            rightData: nePageArgum.option.neData,

            filter    : 'loginId' , //多选功能的过滤条件

            neTable: {
                table: {
                    columns: [
                        //列设置
                        {checkbox: true, field: 'state'},
                        {field: 'userName', title: '姓名', name: 'userName'},
                        {field: 'loginId', title: '登录名', name: 'loginId'},
                        {field: 'cardNo', title: '身份证号', name: 'cardNo'}
                    ],
                    //行数据加载时候的回调 , 这个使用默认即可
                    rowAttributes: function (row, index) {
                        return row.state = false;
                    }

                }
            }
        });



    }

    //初始化layout
    $('body .tab-pane .panel-body ').layout({
        name: 'manLayout',
        west: {size: 250},
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: fnLoad
    });

});