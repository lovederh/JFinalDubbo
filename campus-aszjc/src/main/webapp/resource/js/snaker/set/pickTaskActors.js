define(function (require) {
    var $ = require('jquery');
    require("mConfig");
    require('jqZtree');

    require('jqLayout');
    require('neEvent');
    require('neToastr');

    require('jqDualListTable');
    require('jqBootstrapTableGroupBy');

    var $dualList;

    //获取后端传入的json
    var allUserData = [];

    $.ajax({
        url: '/flowApiData/findPickData',
        data: {
            treeType: 'teacher'
        },
        dataType: 'json',
        type: "post",
        async: false,
        success: function (jsonData) {
            allUserData = jsonData;
        }
    });


    var nePageArgum = {
        option: Nenu.open.data.child
    };

    var nePageLogic = {
        event: function () {
            Nenu.context.event = {
                // 确定按钮
                fnSure: function () {
                    var data = $dualList.neDualListTable('getData');
                    nePageArgum.option.setParentData({
                        pickSureFlag : 'true',
                        actorsData: data
                    });
                    nePageArgum.option.close();
                },
                //取消按钮
                fnCancel: function () {
                    nePageArgum.option.close();
                }
            }
        },
        end: function () {
            Nenu.open.end = function () {

            }
        },
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    function fnLoad() {
        //初始化neDualListTable
        $dualList = $('#js_dual_list_table').neDualListTable({
            data: allUserData,
            rightData: nePageArgum.option.neData,
            filter    : 'id' , //多选功能的过滤条件
            neTable: {
                table: {
                    columns: [
                        //列设置
                        {checkbox: true, field: 'state'},
                        {field: 'actorDisplay', title: '姓名', name: 'actorDisplay'},
                        {field: 'actorValue', title: '登录名', name: 'actorValue'},
                        {field: 'cardNo', title: '身份证号', name: 'cardNo'},
                        {field: 'id', name: 'id', visible: false},
                        {field: 'actorType', name: 'actorType', visible: false}
                    ],
                    //行数据加载时候的回调 , 这个使用默认即可
                    rowAttributes: function (row, index) {
                        return row.state = false;
                    }
                }
            }
        });
    }

    //初始化layout
    $('body .tab-pane .panel-body ').layout({
        name: 'actorsLayout',
        west: {
            size: 250
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
        },
        onload_end: fnLoad
    });
});