/**
 * 共通化的工单处理方法
 * 注: 通过新建工单标识确定该页面展示方式, 新建派发情况下则不涉及弹窗,
 *     其它处理环节为弹窗展示, 则注意调用关闭弹窗的方法
 * 2016/12/24
 */
define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //校验组件
    require('jqValidate');
    //日期组件
    require('neDate');

    require('jqSlimscroll');
    require('jqLayout');


    var upload = require('neUpload');


    var pageOpenOption;
    //新建工单情况下, 表单是嵌到iframe中的, 其父页面为弹窗对象
    pageOpenOption = window.parent.Nenu.open.data.child;

    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        //取自身的父界面属性作为弹窗属性
        option  : pageOpenOption
    };

    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },

        event: function () {
            Nenu.context.event = {
                saveDraftBtn : saveDraftFunc,
                saveTemplateBtn : saveTemplateFunc,
                useTemplateBtn : useTemplateFunc,
                goBackBtn : function(){//返回至流程类型选择
                    history.go(-1);
                },
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                },
                //添加文件
                uploadFileBtn: uploadFileFunc,
                //删除文件方法
                delFileBtn: delFileFunc
            };
        },
        end : function () {
            Nenu.open.end = function () {

            };
        },
        init: function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    //任务表单验证并提交
    var $form = $('#js_dealTaskForm');
    $form.neValidate({
        ajaxBtn : {
            btn : $('#js_submitBtn'),//按钮对应的dom元素
            type : 'post',
            url : '/flowDeal/dealTaskSaveBusiness',
            success : function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    util_alertOkDone(msg, function(){

                        nePageArgum.option.setParentData({
                            dealSuccess : 'true'
                        });
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                } else {
                    Nenu.toastr.error(msg);
                }
            }
        }
    });

    //存为草稿
    function saveDraftFunc(){
        //保存草稿不校验表单
        var $saveDraftBtn = $('#js_saveDraftBtn');
        $saveDraftBtn.attr('disabled', true);
        // 序列化表单数据后提交
        var formData = $('#js_dealTaskForm').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/flowDeal/saveDraftData',
            async: false,
            data: formData,
            dataType: 'json',
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    util_alertOkDone(msg, function(){

                        nePageArgum.option.setParentData({
                            dealSuccess : 'true'
                        });
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                } else {
                    Nenu.toastr.error(msg);
                    $saveDraftBtn.attr('disabled', false);
                }
            }
        });
    }

    //存为模板
    function saveTemplateFunc(){

    }

    //引用模板
    function useTemplateFunc(){

    }



    /**
     * 上传附件
     */
    function uploadFileFunc(){
        //判断是否已经上传过(只支持一个附件)
        var $uploadFile = $('#js_uploadFile');
        if($uploadFile.val()){
            Nenu.toastr.error('只能上传一个附件!');
            return;
        }

        upload.open({
            nenu: {
                finishBack: function (jsonData) {
                    if(jsonData.length != 1){
                        return;
                    }
                    var fileData = JSON.parse(jsonData[0].resData);
                    $('#js_fileDiv').append(
                        '<div class="glyphicon glyphicon-trash jc-btn" id="uploadFile_' + fileData.id + '" ' +
                        'data-btn="delFileBtn" data-file="' + fileData.id + '" > ' + jsonData[0].name + '<div>'
                    );

                    $uploadFile.val(fileData.id);
                }
            },
            config: {
                server: '/file/uploadifyFileList',
                fileNumLimit: 1
            }
        });
    }

    /**
     * 删除附件
     */
    function delFileFunc(){
        var fileId = $(this).data().file;
        $.ajax({
            type: 'POST',
            url: "/file/delFile",
            dataType: 'json',
            async: false,
            data: {
                id: fileId
            },
            success: function (jsonData) {
                Nenu.toastr.success(jsonData.msg);

                $("#uploadFile_" + fileId).remove();
                // 移除文件之后, 隐藏域清空
                $('#js_uploadFile').val('');
            }
        });
    }
});
