define(function (require) {
    require('mConfig');
    //校验组件
    require('jqValidate');
    //通知组件
    require('neToastr');
    //加载event核心文件
    require('neEvent');

    require('jqSlimscroll');
    require('jqLayout');

    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        option  : Nenu.open.data.child
    };

    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },
        event: function () {
            Nenu.context.event = {
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                }
            };
        },
        end : function () {
            Nenu.open.end = function () {

            };
        },
        init : function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //表单验证
    var $form = $('#js_customFlowForm');
    $form.neValidate({
        ajaxBtn : {
            btn : $('#js_submitBtn'),//按钮对应的dom元素
            type : 'post',
            url : '/flowCustom/saveCustomFlow',
            success : function (jsonData) {
                var status = jsonData.status;
                var msg = jsonData.msg;
                if ('200' == status) {
                    util_alertOkDone(msg, function(){
                        nePageArgum.option.close();//弹出iframe层关闭
                    });
                } else {
                    Nenu.toastr.error(msg);
                }
            }
        }
    });
});