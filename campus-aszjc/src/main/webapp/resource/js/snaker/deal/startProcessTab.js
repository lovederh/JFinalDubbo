define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');


    //流程图是否已经加载过标识
    var initPictureFlag = false;

    $("#js_pictureLi").click(function(){
        if(initPictureFlag){
            return;
        }
        document.getElementById('js_pictureFrame').src = '/flowQuery/toFlowPicture?' + $.param(pageParam);
        initPictureFlag = true;
    });

    //使用按钮事件组件
    var nePageLogic = {
        event: function () {
            Nenu.context.event = {

            };
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {

            };
        },
        //初始化
        init: function () {
            this.event();
            this.end();
        }
    };
    nePageLogic.init();
});