define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqSlimscroll');
    require('jqLayout');


    var nePageArgum = {
        $body   : $('body'),
        $scroll : $('.jc-layout-scroll'),
        option  : Nenu.open.data.child
    };

    var nePageLogic = {
        pSlimScroll : function () {
            nePageArgum.$scroll.slimScroll({
                height        : 'auto',
                railOpacity   : 0.9,
                alwaysVisible : false
            });
        },
        pLayout : function () {
            nePageArgum.$body.layout({
                applyDemoStyles : false,
                closable        : false,
                onload_end      : this.pSlimScroll,
                onresize_end    : this.pSlimScroll
            });
        },
        event : function () {
            Nenu.context.event = {
                closeBtn : function () {//关闭弹窗
                    nePageArgum.option.close();
                }
            };
        },
        end : function () {
            Nenu.open.end = function () {

            };
        },
        init : function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };

    nePageLogic.init();

    //流程图片绑定单击派单事件
    $(document).ready(function () {
        $(".js_processImg").click(function() {
            var processData = $(this).attr('processData');
            if(processData){
                //请求选中流程信息, 进行派单处理
                location.href = '/flowDeal/instanceProcessTab?processId=' + processData;
            }
        });
    });
});
