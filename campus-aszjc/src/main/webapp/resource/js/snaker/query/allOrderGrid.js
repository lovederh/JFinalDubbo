define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //日期组件
    require('neDate');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['标题','申请类型','发起时间','发起人','状态','操作','processId','orderId'];
    var colModel = [
        {name: 'orderTitle', index: 'orderTitle', width: 200, align: "center"},
        {name: 'processDisplay', index: 'processDisplay', width: 100, align: "center"},
        {name: 'createTime', index: 'createTime', width: 100, align: "center"},
        {name: 'creatorDisplay', index: 'creatorDisplay', width: 100, align: "center"},
        {name: 'orderState', index: 'orderState', width: 100, align: "center", formatter: function (cellValue, options, rowObject) {
            return util_displayGridOrderState(cellValue, true);
        }},
        {name: 'operate', width: 100, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn =
                '<button class="btn btn-primary btn-outline btn-sm" ' +
                '   onclick="util_viewOrder(\''+ rowObject.processId +'\',\''+ rowObject.orderId +'\')" >' +
                '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;查看</button>';
            return operateBtn;
        }},
        {name: 'processId', hidden: true},
        {name: 'orderId', hidden: true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: '/flowQuery/allOrderGridData',
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "10",
                rowList: [5,10,20,50,100],
                pager: '#js_gridPager',
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: false,
                jsonReader: {
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                searchGridDataBtn: searchGridDataFunc,
                deletePhysicalOrderBtn: deletePhysicalOrderFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {

            };
        },
        init: function () {
            Nenu.date.default(util_initSearchDateDom('searchCreateTimeStart'));
            Nenu.date.default(util_initSearchDateDom('searchCreateTimeEnd'));
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //查询按钮
    function searchGridDataFunc(){
        //确认时间框选择的值是否有效
        var searchCreateTimeStart = $('#searchCreateTimeStart').val();
        var searchCreateTimeEnd = $('#searchCreateTimeEnd').val();
        if(searchCreateTimeEnd < searchCreateTimeStart){
            Nenu.toastr.warning('发起时间查询: 截至时间不能在起始时间之前!');
            return;
        }
        $grid.jqGrid('setGridParam',{
            //发送查询条数据
            postData: $('#js_gridSearchBar').serializeArray(),
            page: 1
        }).trigger("reloadGrid");
    }

    //彻底删除
    function deletePhysicalOrderFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个已归档工单进行删除!');
            return;
        }
        var rowData = $grid.jqGrid('getRowData', rows[0]);
        if(1 == rowData.orderState){
            Nenu.toastr.warning('请选择一个已归档工单进行删除!');
            return;
        }
        util_deletePhysicalOrder(rowData.orderId, reloadGrid);
    }

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});


