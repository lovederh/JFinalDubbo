define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //日期组件
    require('neDate');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['标题','申请类型','发起时间','发起人','任务名称','工单状态','操作','processId','orderId','taskId','taskName','isCcOrderFlag'];
    var colModel = [
        {name: 'orderTitle', index: 'orderTitle', width: 200, align: "center"},
        {name: 'processDisplay', index: 'processDisplay', width: 100, align: "center"},
        {name: 'createTime', index: 'createTime', width: 100, align: "center"},
        {name: 'creatorDisplay', index: 'creatorDisplay', width: 100, align: "center"},
        {name: 'taskDisplay', index: 'taskDisplay', width: 100, align: "center", formatter: function (cellValue, options, rowObject) {
            var displayColor = 'true'==rowObject.isCcOrderFlag? 'green': 'red';
            var taskDisplay = "<label style='color:" + displayColor + ";'>" + cellValue + "</label>";
            return taskDisplay;
        }},
        {name: 'orderState', index: 'orderState', width: 100, align: "center", formatter: function (cellValue, options, rowObject) {
            return util_displayGridOrderState(cellValue, false);
        }},
        {name: 'operate', width: 100, align: "center", formatter: function(cellValue, options, rowObject) {
            var operateBtn;
            if('true'==rowObject.isCcOrderFlag){
                operateBtn =
                    '<button class="btn btn-primary btn-outline btn-sm" ' +
                    '   onclick="util_viewOrder(\''+ rowObject.processId +'\',\''+ rowObject.orderId +'\',true)" >' +
                    '<i class="glyphicon glyphicon-eye-open"></i>&nbsp;查看</button>';
            }else{
                operateBtn =
                    '<button class="btn btn-primary btn-outline btn-sm" ' +
                    '   onclick="util_dealTask(\''+ rowObject.processId +'\',\''+ rowObject.orderId +'\',\''+ rowObject.taskId +'\')" >' +
                    '<i class="glyphicon glyphicon-send"></i>&nbsp;处理</button>';
            }
            return operateBtn;
        }},
        {name: 'processId', hidden: true},
        {name: 'orderId', hidden: true},
        {name: 'taskId', hidden: true},
        {name: 'taskName', hidden: true},
        {name: 'isCcOrderFlag', hidden: true},
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: '/flowQuery/myDealGridData',
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "10",
                rowList: [5,10,20,50,100],
                pager: '#js_gridPager',
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: false,
                jsonReader: {
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                searchGridDataBtn: searchGridDataFunc,
                ccOrdersReadBtn: ccOrdersReadFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();
            };
        },
        init: function () {
            Nenu.date.default(util_initSearchDateDom('searchCreateTimeStart'));
            Nenu.date.default(util_initSearchDateDom('searchCreateTimeEnd'));
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //查询按钮
    function searchGridDataFunc(){
        //确认时间框选择的值是否有效
        var searchCreateTimeStart = $('#searchCreateTimeStart').val();
        var searchCreateTimeEnd = $('#searchCreateTimeEnd').val();
        if(searchCreateTimeEnd < searchCreateTimeStart){
            Nenu.toastr.warning('发起时间查询: 截至时间不能在起始时间之前!');
            return;
        }
        $grid.jqGrid('setGridParam',{
            //发送查询条数据
            postData: $('#js_gridSearchBar').serializeArray(),
            page: 1
        }).trigger("reloadGrid");
    }


    // 标记已阅, 支持批量标记(智能跳过无需标记工单)
    function ccOrdersReadFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择要标记已阅的抄送工单!', true);
            return;
        }
        // 选择的工单中, 所有可抄送工单ID集合
        var readOrders = [];
        $.each(rows, function(i, item){
            var rowData = $grid.jqGrid('getRowData', rows[i]);
            if(rowData.isCcOrderFlag == 'true'){
                readOrders.push(rowData.orderId);
            }
        });
        if(readOrders.length <= 0){
            Nenu.toastr.warning('请选择要标记已阅的抄送工单!');
            return;
        }
        util_ccOrdersRead(readOrders.join(','), reloadGrid);
    }

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});

