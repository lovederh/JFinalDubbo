define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['企业名称','登录名','单位地址','联系电话','单位法人','id'];
    var colModel = [
        {name: 'companyName', index: 'companyName', width: 200, align: "center"},
        {name: 'loginID', index: 'loginID', width: 100, align: "center"},
        {name: 'companyAdd', index: 'companyAdd', width: 300, align: "center"},
        {name: 'tel', index: 'tel', width: 100, align: "center"},
        {name: 'corporation', index: 'corporation', width: 100, align: "center"},
        {name: 'id', hidden: true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: "/flowQuery/sysFlowDomainsData?" + $.param(pageParam),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "-1",//显示全部记录
                pager: false,
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: true,
                jsonReader: {
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                batchDeploySysFlowBtn: batchDeploySysFlowFunc,
                batchUndeployFlowBtn: batchUndeployFlowFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();
            };
        },
        init: function () {
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();


    function initSelectedDomains(){
        //遍历确认选择的所有企业域
        var domains = [];//选择的所有企业域
        var domainNames = [];
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(rows.length){
            $.each(rows, function(i, item){
                var rowData = $grid.jqGrid('getRowData', rows[i]);
                domains.push(rowData.id);
                domainNames.push(rowData.companyName);
            });
        }
        return {
            'domains': domains.join(','),
            'domainNames': domainNames.join(', ')
        };
    }

    //为企业域批量安装
    function batchDeploySysFlowFunc(){
        var domainsData = initSelectedDomains();
        return {
            title: '企业域安装确认',
            url: '/flowSet/toDeployInputCommonSet',
            neData: {
                'fileResource': pageParam.fileResource,
                'domains': domainsData.domains,
                'domainNames': domainsData.domainNames
            },
            width: '800px',
            height: '350px'
        };
    }


    //为企业域批量禁用(完成卸载操作)
    function batchUndeployFlowFunc(){
        Nenu.event.confirm({
            title : '将执行所选企业域(不选视为全选)的流程卸载, 是否确认?',
            isColse: false,
            execute : function (close){
                var domainsData = initSelectedDomains();
                batchUndeployFlow(pageParam.fileResource, domainsData.domains);
            }
        });
    }

    function batchUndeployFlow(fileResource, domains){
        $.ajax({
            type: 'POST',
            url: '/flowSet/batchUndeployFlow',
            dataType: 'json',
            async: false,
            data: {
                fileResource : fileResource,
                domains : domains
            },
            success: function(jsonData){
                var status = jsonData.status;
                var msg = jsonData.msg;
                if('200' == status){
                    Nenu.toastr.success(msg);
                    reloadGrid();
                }else{
                    Nenu.toastr.error(msg);
                }
            }
        });
    }

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});

