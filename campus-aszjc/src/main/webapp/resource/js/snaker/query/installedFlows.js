define(function (require) {
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    //日期组件
    require('neDate');

    require('jqGrid');


    var $grid = $('#js_jqGrid');
    $.jgrid.defaults.styleUI = 'Bootstrap';

    var colNames = ['申请类型','流程版本号','安装时间','安装人','定制类型','processId','processName','processSetId'];
    var colModel = [
        {name: 'processDisplay', index: 'processDisplay', width: 200, align: "center"},
        {name: 'version', index: 'version', width: 100, align: "center"},
        {name: 'createTime', index: 'createTime', width: 200, align: "center"},
        {name: 'creatorName', index: 'creatorName', width: 100, align: "center"},
        {name: 'customType', index: 'customType', width: 200, align: "center"},
        {name: 'processId', hidden: true},
        {name: 'processName', hidden: true},
        {name: 'processSetId', hidden: true}
    ];

    var nePageLogic = {
        fnTable : function () {
            $grid.jqGrid({
                url: '/flowQuery/installedFlowsData',
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowNum : "10",
                rowList: [5,10,20,50,100],
                pager: '#js_gridPager',
                height: "auto",
                autowidth: true,
                shrinkToFit:true,
                viewrecords : true,
                multiselect: true,
                jsonReader: {
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                }
            });
        },
        event: function () {
            Nenu.context.event = {
                searchGridDataBtn: searchGridDataFunc,
                undeployFlowBtn: undeployFlowFunc,
                editFlowSetBtn: editFlowSetFunc,
                flowTaskSetBtn: flowTaskSetFunc
            }
        },
        //子页面关闭回调函数
        end: function () {
            Nenu.open.end = function () {
                reloadGrid();//当前页列表刷新
            };
        },
        init: function () {
            this.fnTable();
            this.event();
            this.end();
        }
    };
    nePageLogic.init();

    //查询按钮
    function searchGridDataFunc(){
        $grid.jqGrid('setGridParam',{
            //发送查询条数据
            postData: $('#js_gridSearchBar').serializeArray(),
            page: 1
        }).trigger("reloadGrid");
    }

    //卸载流程
    function undeployFlowFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行卸载!', true);
            return;
        }
        var processId = $grid.jqGrid('getRowData', rows[0]).processId;
        Nenu.event.confirm({
            title : '是否确认卸载选中流程?',
            execute : function (close){
                close();
                // 关闭后发送卸载请求
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: '/flowSet/undeployFlow',
                    data: {
                        processId : processId
                    },
                    success:function(jsonData){
                        Nenu.toastr.success(jsonData.msg);
                        reloadGrid();
                    }
                });
            }
        });
    }

    //修改流程整体配置
    function editFlowSetFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行配置!', true);
            return;
        }
        var processId = $grid.jqGrid('getRowData', rows[0]).processId;
        return {
            title: '流程整体配置',// 窗口标题
            url: '/flowSet/openFlowSetForm?processId=' + processId,
            height: '500px',
            width: '800px'
        };
    }

    //流程环节配置
    function flowTaskSetFunc(){
        var rows = $grid.jqGrid('getGridParam','selarrrow');
        if(!rows.length || rows.length>1){
            Nenu.toastr.warning('请选择一个流程进行配置!', true);
            return;
        }
        var processSetId = $grid.jqGrid('getRowData', rows[0]).processSetId;
        var processId = $grid.jqGrid('getRowData', rows[0]).processId;
        return {
            title: '流程环节配置',
            url: '/flowSet/openFlowTaskSetTab?processSetId=' + processSetId + '&processId=' + processId,
            height: '1500px',
            width: '2000px'
        };
    }

    //重新加载当前表格
    function reloadGrid(){
        $grid.trigger('reloadGrid');
    }
});
