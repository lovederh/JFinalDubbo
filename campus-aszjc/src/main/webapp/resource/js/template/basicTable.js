/**
 * 通用表列表，通用增删改查，通用弹出表单
 * Created by dsideal-lee on 2016/11/21.
 * json:{
 *
 *  columns:[{field:"", title:""},{}]
 * }
 *
 */
define(function (require) {
    require('mConfig');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    require('neToastr');

    var fnGrid = require("/resource/js/common/event_grid.js");


    var selRowData;
    var $grid = $('#js_grid').neTable({
        //nenu 本系统定义的参数对象
        nenu: {
            query: '#js_query_form',
            toobarBtn: {
                search: {
                    switch: true,
                    sel: '#js_query_form_btn_search'
                }
            },
            rowBtn: {}
        },
        table: {
            url: "/page/getListInitData?dtoName=" + formObj.dtoName + "&domain=" + formObj.domain, //ajax url
            method: "get",//请求方式
            queryParams: function (params) {
                return {limit: params.limit, offset: params.offset}
            },
            dataField: "rows",//服务端返回数据键值 就是说记录放的键值是rows，分页时使用总记录数的键值为total
            pageNumber: 1,
            pageSize: 10,
            dataType: "json",//期待返回数据类型
            pagination: true,//是否分页
            sidePagination: "server",
            pageList: [10, 20, 50, 100, 200, 500],
            striped: true,
            idField: "id",  //标识哪个字段为id主键
            toolbar: '#js_grid_toolbar',
            columns: JSON.parse(booTableConf.columns)
        }
    });

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        /**
         * 全局页面事件对象
         * @private
         **/
        event: function () {
            fnGrid({grid: $grid});
        },
        /**
         * 子页面关闭回调函数
         * @private
         **/
        end: function () {
            Nenu.open.end = function () {
                var data = Nenu.open.data.parent;
                //父页面表格刷新
                $('#js_grid').bootstrapTable("refresh");
            };
        },

        /**
         * 业务逻辑 函数
         * @private
         **/
        business: function () {
            selRowData = undefined;
        },
        refreshTable: function () {
            $grid.bootstrapTable("refresh");
        }
        ,
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
            this.business();
            this.end();
        }
    };
    nePageLogic.init();
});

