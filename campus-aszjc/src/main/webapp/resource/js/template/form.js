define(function (require) {
    require('mConfig');
    //校验组件
    require('jqValidate');

    //城市三级联动组件
    require('jqCitys');
    //日期组件
    require('neDate');
    //通知组件
    require('neToastr');
    //加载event核心文件
    require('neEvent');
    //滚动条插件
    require('jqSlimscroll');
    require('jqLayout');

    //全局JSON
    var gJSON = require('mJson');

    //获取dom
    var $form = $('#js_info_form');
    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll')
    };

    /**
     * 页面级 工具对象
     * @namespace
     */
    var nePageLogic = {
        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        }
        ,

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        /**
         * 全局页面事件对象
         * @private
         **/
        event: function () {
            var option = Nenu.open.data.child;
            var formObj = option.neData;
            Nenu.context.event = {
                save: function () {
                    var isValidate;
                    // //校验
                    // $form.neValidate({
                    //     ajaxBtn: {
                    //         type: 'get',
                    //         url: '/resource/ne/dist/json/ajax.json',
                    //         success: function (data) {
                    //             isValidate = data.success;
                    //         }
                    //     }
                    // });
                    //提交前处理checkbox未选中赋值为false
                    $("input[type='checkbox']").each(function () {
                        $("#js_info_form").append("<input type='hidden' value='false' name='" + $(this).attr('name') + "'>");
                    });


                    var formData = $("#js_info_form").serialize();
                    $.ajax({
                        url: '/page/save?' + $.param(formObj) + "&" + formData,
                        type: "POST",
                        async: false,
                        dataType: "json",
                        success: function (jsonData) {
                            Nenu.event.confirm({
                                isColse: false,
                                title: jsonData.msg,
                                execute: function (close) {
                                    option.close();
                                    close();
                                }
                            });
                        }
                    });
                },
                resetForm: function () {
                    $form.neResetForm();
                },
                fnClose: function () {
                    option.close();
                }
            };
        },

        /**
         * 子页面关闭回调函数
         * @private
         **/
        end: function () {
            Nenu.open.end = function () {
            };
        },
        business: function () {
            //日期控件
            getDateIdList(extraObj.gDateIdList);
            function getDateIdList(dataIdListStr) {
                if (dataIdListStr != "") {
                    var gDateIdList = dataIdListStr.split(",");
                    for (var i = 0; i < gDateIdList.length - 1; i++) {
                        Nenu.date.default({elem: '#' + gDateIdList[i]});
                    }
                }
            }

            var option = Nenu.open.data.child;
            if (option.neData.paraOpt == 'edit'
                || option.neData.formWrapper == 'addPeer' //增加树结点
                || option.neData.formWrapper == 'addChild') {
                this.loadData(JSON.parse(extraObj.formData));
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.pLayout();
            this.event();
            this.business();
            this.end();
        },
        loadData: function (obj) {
            var key, value, tagName, type, arr;
            for (x in obj) {
                key = x;
                value = obj[x];
                $("[name='" + key + "'],[name='" + key + "[]']").each(function () {
                    tagName = $(this)[0].tagName;
                    type = $(this).attr('type');
                    if (tagName == 'INPUT') {
                        if (type == 'radio') {
                            $(this).attr('checked', $(this).val() == value);
                        } else if (type == 'checkbox') {
                            arr = value.split(',');
                            for (var i = 0; i < arr.length; i++) {
                                if ($(this).val() == arr[i]) {
                                    $(this).attr('checked', true);
                                    break;
                                }
                            }
                        } else {
                            $(this).val(value);
                        }
                    } else if (tagName == 'SELECT' || tagName == 'TEXTAREA') {
                        $(this).val(value);
                    }

                });
            }
        }
    };
    nePageLogic.init();

});