define(function (require) {
    var $ = require('jquery');

    require('jqBootstrapTable');

    var columns = [{
        field: "aa",
        title: "kbkb",
        align: "left",
        sortable: true,
        order: "asc"
    }, {
        field: "bb",
        title: "kbkb",
        align: "left",
        sortable: true,
        order: "asc"
    }];
    var data = [{aa: "3333", bb: "5555"}, {"aa": "2222", bb: "4444"}];
    $('#js_table').neTable({
        table: {
            // url: "/grid/getGridData",
            data: data,
            columns: columns,
            search: true,
            pagination: true,
            showRefresh: true,
            showToggle: true,
            showColumns: true,
            iconSize: 'outline',
            toolbar: '#exampleTableEventsToolbar',
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            }
        }
    });
});