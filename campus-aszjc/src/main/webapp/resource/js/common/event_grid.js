define(function (require) {
    'use strict';
    if (!formObj) {
        $.error('没有定义formObj,请检查!!!');
    }

    require('jquery');
    require('jqBootstrapTable');
    require('neEvent');
    require('neToastr');
    require('jqZtree');

    /**
     * options
     *   isExtend boolean 是否合并event对象  默认 true
     *
     *   grid    jqObj   jquery对象
     */
    return function (options) {
        var isExtend = options.isExtend || true;
        var $grid = options.grid;
        var oEvent = {
            add     : function () {
                formObj.paraOpt = "add";
                return {
                    title: "新增信息",
                    url    : '/page/showForm?' + $.param(formObj),
                    neData : formObj,
                    height : '750px',
                    width  : '800px'
                };
            },
            edit    : function () {
                formObj.paraOpt = "edit";
                var selRows = $grid.bootstrapTable('getSelections');
                if (selRows.length > 0) {
                    if (selRows.length > 1) {
                        Nenu.toastr.info('编辑时，只允许选择一条数据！！');
                        return;
                    }
                    return {
                        title: "编辑信息",
                        url    : '/page/showForm?id=' + selRows[0].id + '&' + $.param(formObj),
                        neData : formObj,
                        height : '750px',
                        width  : '800px'
                    }
                } else {
                    Nenu.toastr.info('请选择一行数据！');
                    return {};
                }
            },
            delData : function () {
                formObj.paraOpt = "del";
                var selRows = $grid.bootstrapTable('getSelections');
                if (selRows.length > 0) {
                    Nenu.event.confirm({
                        isClose : false,             // 是否自动关闭窗口
                        title   : '确定要删除所选的记录吗？',        // 窗口标题
                        execute : function (close) {  // 确定按钮执行的事件 参数 close 传入的关闭函数
                            var id = new Array();
                            for (var i = 0; i < selRows.length; i++) {
                                id.push(selRows[i].id);
                            }
                            formObj.ids = id.toString();
                            $.ajax({
                                type     : 'POST',
                                url      : "/page/save",
                                dataType : 'json',
                                async    : false,
                                data     : formObj,
                                success  : function (jsonData) {
                                    $grid.bootstrapTable('refresh');
                                }
                            });
                            close();
                        }
                    });
                } else {
                    Nenu.toastr.info('请选择一行数据！');
                    return {};
                }
            },
            grantRole : function(){
                var selRows = $grid.bootstrapTable('getSelections');
                if (selRows.length > 0) {
                    if (selRows.length > 1) {
                        Nenu.toastr.info('编辑时，只允许选择一条数据！！');
                        return;
                    }
                    formObj.roleId = selRows[0].id;
                    return {
                        title  : "授权界面",
                        url    : '/permission/grantRole?id=' + selRows[0].id + '&' + $.param(formObj),
                        neData : formObj,
                        height : '750px',
                        width  : '800px'
                    }
                } else {
                    Nenu.toastr.info('请选择一行数据！');
                    return {};
                }
            },

            // 组织机构--机构维护--部门调整按钮方法
            deptAdjust : function () {
                var selRows = $grid.bootstrapTable('getSelections');
                if(selRows.length > 0){
                    if(selRows[0].pId == '0'){
                        Nenu.toastr.warning("您选择的为该部门的根节点,不可以进行调整,请选择该部门下的子部门！");
                        return;
                    }
                    if(selRows.length > 1){
                        Nenu.toastr.warning("请选择一条数据！");
                        return;
                    }
                    return{
                        url: '/dept/deptAdjustInfo?orgId='+selRows[0].orgId+'&'+$.param(formObj),
                        height: '600px',
                        width: '600px'
                    }
                }else{
                    Nenu.toastr.warning("请选择一行数据！");
                    return;
                }
            },

            //  人事管理--人员信息维护--人员调动按钮
            personMove : function () {
                var selRows = $grid.bootstrapTable('getSelections');
                if(selRows.length == 0){
                    Nenu.toastr.warning("请选择至少一条数据!");
                    return;
                }
                var id = [];
                for (var i = 0; i < selRows.length; i++) {
                    id.push(selRows[i].id);
                }
                return {
                    title: '人员调动信息',
                    url    : '/emp/personMove?ids='+ id.join(',')+"&businessTable="+extraObj.businessTable,
                    neData : formObj,
                    height : '750px',
                    width  : '800px'
                }
            },

            //  人事管理--人员信息维护--添加人员按钮
            addEmp : function () {
                var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();
                var selectNode = nodes[0];
                if(! nodes.length){
                    Nenu.toastr.warning("请选择具体部门添加人员!");
                    return;
                }
                if(selectNode.isLeaf == '0'){
                    Nenu.toastr.warning("请把人员添加到具体的叶子节点的组织机构!");
                    return;
                }
                var dtoObj = {
                    dtoName: formObj.dtoName,
                    domain: formObj.domain,
                    paraOpt: "add",
                    orgId: selectNode.id
                }
                return {
                    title: '添加人员',
                    url    : '/page/showForm?' + $.param(dtoObj),
                    neData : dtoObj,
                    height : '750px',
                    width  : '800px'
                }
            },


            //  人事管理--人员信息维护--人员状态变更按钮
            empStatusChange : function () {
                var selRows = $grid.bootstrapTable('getSelections');
                if(selRows.length == 0){
                    Nenu.toastr.warning("请选择至少一条数据!");
                    return;
                }
                var id = [];
                for (var i = 0; i < selRows.length; i++) {
                    id.push(selRows[i].id);
                }
                return {
                    title: '人员状态变更',
                    url    : '/emp/empStatusChange?ids='+ id.join(','),
                    height : '500px',
                    width  : '500px'
                }
            },

            // 人事管理--人员信息维护--人员附表按钮
            empMeter : function () {
                var selRows = $grid.bootstrapTable('getSelections');
                if (selRows.length > 0) {
                    if (selRows.length > 1) {
                        Nenu.toastr.info('请选择一条数据！！');
                        return;
                    }
                    return {
                        title: "人员附表",
                        url    : '/emp/empMeter?empId=' + selRows[0].id,
                        neData : formObj,
                        height : '750px',
                        width  : '800px'
                    }
                } else {
                    Nenu.toastr.info('请选择一行数据！');
                    return {};
                }
            },


            //  楼宇管理--信息维护--添加人员按钮
            addBuilding : function () {
                var nodes = $.fn.zTree.getZTreeObj('js_tree_building').getSelectedNodes();
                var selectNode = nodes[0];
                if(! nodes.length){
                    Nenu.toastr.warning("请选择具体楼宇添加房间!");
                    return;
                }
                if(selectNode.isLeaf == '0'){
                    Nenu.toastr.warning("请把房间信息添加到具体的叶子节点!");
                    return;
                }
                formObj.paraOpt = "add";
                formObj.buildId=selectNode.dataId;
                return {
                    title: '添加房间',
                    url    : '/page/showForm?' + $.param(formObj),
                    neData : formObj,
                    height : '750px',
                    width  : '800px'
                }
            },

            addTreeGridNote : function () {
            }
        };

        Nenu.context.event = isExtend ? $.extend({}, Nenu.context.event, oEvent) : oEvent;
    };
});