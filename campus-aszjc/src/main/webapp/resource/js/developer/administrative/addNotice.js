define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqGrid');
    //加载event核心文件
    require('neEvent');
    var upload = require('neUpload');
    //通知组件
    require('neToastr');
    require('jqSlimscroll');
    require('jqLayout');

    require('nvUEditor');

    // 文件ID数组
    var fileArray = new Array();

    var pickUsersStr = '';
    var initContent = $('#initContent').val();
    initContent = initContent ? initContent : '';

    var userData = [];

    var abstractEditor = UE.getEditor('abstractEditor', {
        initialFrameWidth: '95%',
        initialFrameHeight: 300,
        serverUrl: formObj.path + '/ueditor/server',
        UEDITOR_HOME_URL: "/resource/third/ueditor/",
        autoWidth: true,
        elementPathEnabled: false,
        scaleEnabled: true,
        initialContent: initContent
    });


    $('#pickStaffBtn').click(function () {
        Nenu.event.ifreame({
            title: '选择人员',
            url: '/notice/selectMaintainStaff?pickUsersStr=' + pickUsersStr + '&' + $.param(formObj),
            height: '1000px',
            width: '1200px',
            neData: userData
        });
    });

    /**
     *  页面级 页面参数对象
     *  @namespace
     *  @param {Object} $body  jq-body 对象
     *  @param {Object} $scroll  jq-scroll 对象 需要添加滚动条的dom对象
     *  @param {Object} data  父页面传递过来的数据对象
     */
    var nePageArgum = {
        $body: $('body'),
        $scroll: $('.jc-layout-scroll'),
        //取自身的父界面属性作为弹窗属性
        option: Nenu.open.data.child
    };

    $('#scopeFlag').click(function () {
        if ($(this).prop("checked") == true) {
            $('#scopeFlag').val('2');
            $('#partScope').css('display', '');
        } else {
            $('#scopeFlag').val('1');
            $('#partScope').css('display', 'none');
        }
    });

    var nePageLogic = {
        /**
         * 滚动条插件 , 弹窗内容超出滚动条
         * @private
         **/
        pSlimScroll: function () {
            nePageArgum.$scroll.slimScroll({
                height: 'auto',
                railOpacity: 0.9,
                alwaysVisible: false
            });
        },

        /**
         * 布局插件
         * @private
         **/
        pLayout: function () {
            nePageArgum.$body.layout({
                applyDemoStyles: false,
                closable: false,
                onload_end: this.pSlimScroll,
                onresize_end: this.pSlimScroll
            });
        },

        event: function () {
            Nenu.context.event = {
                fnSure: function () {
                    $.ajax({
                        type: 'POST',
                        url: "/notice/saveNotice",
                        dataType: 'json',
                        async: false,
                        data: {
                            titile: $('#titile').val(),
                            scopeFlag: $('#scopeFlag').val(),
                            pickUsersStr: pickUsersStr,
                            content: $('#ueditor_textarea_editorValue').val(),
                            fileArrayIds: fileArray.toString()
                        },
                        success: function (jsonData) {
                            if (jsonData.status == '200') {
                                Nenu.toastr.success("恭喜你,添加成功！");
                                nePageArgum.option.close();
                            }
                        }
                    });
                },
                //重置
                fnReset: function () {
                    $('#titile').val('');
                    $('#scopeFlag').attr('checked', false).val('1');
                    $('#partScope').css('display', 'none');
                    $('#abstractContent').val('');
                },
                //添加文件
                addFile: function () {
                    upload.open({
                        nenu: {
                            finishBack: function (jsonData) {
                                for (var i = 0; i < jsonData.length; i++) {
                                    $('#fileDiv').append('<div id="' + JSON.parse(jsonData[i].resData).id + '" class="glyphicon glyphicon-trash jc-btn" data-btn="delFile" data-fileId="' + JSON.parse(jsonData[i].resData).id + '" >' + jsonData[i].name + '<div>');
                                    fileArray[i] = JSON.parse(jsonData[i].resData).id;
                                }
                            }
                        },
                        config: {
                            server: '/file/uploadifyFileList'
                        }
                    });
                },
                //删除文件方法
                delFile: function () {
                    var id = $(this).data().fileid;
                    $.ajax({
                        type: 'POST',
                        url: "/file/delFile",
                        dataType: 'json',
                        async: false,
                        data: {id: id},
                        success: function (jsonData) {
                            Nenu.toastr.success(jsonData.msg);
                            $("#" + id).remove();
                            // 移除文件之后,文件数组中也要移除其文件ID
                            $.each(fileArray, function (index, item) {
                                // index是索引值（即下标）   item是每次遍历得到的值；
                                if (item == id) {
                                    fileArray.splice(index, 1);
                                }
                            });
                        }
                    });
                }
            }
        },

        end: function () {
            Nenu.open.end = function () {
                userData = Nenu.open.data.parent;   //子页面往父页面返回的数据
                var userName = [];
                var $scope = $('#personNameScope');
                $scope.empty();

                if (!userData.length) {
                    return;
                }
                pickUsersStr = [];

                for (var i = 0, length = userData.length; i < length; i++) {
                    var user = userData[i];
                    pickUsersStr.push(user.id);
                    userName.push(user.userName);
                }

                $scope.html(userName.join(','));
                pickUsersStr = pickUsersStr.join(',');

            };
        },

        init: function () {
            this.pLayout();
            this.event();
            this.end();
        }
    };

    nePageLogic.init();

});
