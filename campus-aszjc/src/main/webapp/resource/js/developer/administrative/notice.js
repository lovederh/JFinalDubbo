define(function (require) {
    // 'use strict';
    //加载组件
    require('mConfig');
    //加载event核心文件
    require('neEvent');
    //日期组件
    require('neDate');
    //通知组件
    require('neToastr');

    require('jqGrid');

    var $grid = $('#js_grid');

    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['主键ID', '标题', '内容', '发布时间', '发布人', '已读', '未读', '附件', '范围', '操作'];
    var colModel = [
        {name: 'id', hidden: true},
        {name: 'titile', index: 'titile', width: 100, align: "center"},
        {name: 'content', index: 'content', width: 100, align: "center"},
        {name: 'publishTime', index: 'publishTime', width: 100, align: "center"},
        {name: 'initiator', index: 'initiator', width: 100, align: "center"},
        {name: 'readNum', index: 'readNum', width: 100, align: "center"},
        {name: 'unReadNum', index: 'unReadNum', width: 100, align: "center"},
        {name: 'file', hidden: true},
        {name: 'scope', hidden: true},
        {name: 'operate', index: 'operate', width: 100, align: "center"}
    ];

    var nePageLogic = {
        fnTable: function () {
            $("#js_grid").jqGrid({
                url: '/notice/queryNoticePager?' + $.param(formObj),
                datatype: 'json',
                colNames: colNames,//列名
                colModel: colModel,//列字段配置
                rowList: [10, 20, 30],
                pager: '#js_pager_grid',//pager在使用树表格时禁用
                height: "auto",
                rowNum: "10",//显示全部记录
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                sortorder: "desc",
                multiselect: true,
                jsonReader: {                                   //解析的jsonReader
                    root: "list",
                    page: "pageNumber",
                    total: "totalPage",
                    repeatitems: false
                },
                gridComplete: function () {         // 数据加载完成后 每行添加 查看按钮
                    var ids = $grid.jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var id = ids[i];
                        var checkBtn = '<button class="btn btn-primary btn-outline btn-sm jc-btn" data-btn="viewNotice" data-noticeId="' + id + '" ><i class="glyphicon glyphicon-eye-open"></i> 查看</button>';
                        $grid.jqGrid('setRowData', ids[i], {operate: checkBtn});
                    }
                }

            });
        },
        event: function () {
            Nenu.context.event = {
                //  新增通知公告按钮
                addNotice: function () {
                    return {
                        title: "行政管理>通知公告>新增",
                        url: '/notice/addNotice?' + $.param(formObj),
                        neData: formObj,
                        height: '1500px',
                        width: '1200px'
                    }
                },

                //  批量删除按钮
                delNotice: function () {
                    var selRows = $grid.jqGrid('getGridParam', 'selarrrow');
                    if (!selRows.length) {
                        Nenu.toastr.warning("请选择需要删除的记录！");
                        return;
                    }
                    if (selRows.length > 0) {
                        Nenu.event.confirm({
                            isClose: false,             // 是否自动关闭窗口
                            title: '确定要删除所选的记录吗？',        // 窗口标题
                            execute: function (close) {  // 确定按钮执行的事件 参数 close 传入的关闭函数
                                var ids = "";
                                for (var i = 0; i < selRows.length; i++) {
                                    if (ids != "") {
                                        ids = ids + "," + selRows[i];
                                    } else {
                                        ids = selRows[i];
                                    }
                                }
                                $.ajax({
                                    type: 'POST',
                                    url: "/notice/delNotice",
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                        ids: ids
                                    },
                                    success: function (jsonData) {
                                        Nenu.toastr.success(jsonData.msg);
                                        $grid.trigger('reloadGrid');
                                    }
                                });
                                close();
                            }
                        });
                    } else {
                        Nenu.toastr.warning("请选择需要删除的记录！");
                        return;
                    }
                },

                // 搜索按钮
                searchNotice: function () {
                    // 序列化表单数据后提交
                    var formObj = $('#js_query_form').serializeArray();
                    $("#js_grid").jqGrid('setGridParam', {
                        url: "/notice/queryNoticePager",
                        postData: formObj, //发送数据
                        page: 1
                    }).trigger("reloadGrid");
                },

                //查看公告信息
                viewNotice: function () {
                    var noticeId = $(this).data().noticeid;
                    //接收通知公告人员是否人数处理
                    $.ajax({
                        type: 'POST',
                        url: "/notice/receiveNoticeUserNum",
                        dataType: 'json',
                        async: false,
                        data: {
                            noticeId: noticeId
                        },
                        success: function (jsonData) {
                            window.open('/notice/viewNotice?id=' + noticeId);
                        }
                    });
                }
            }
        },

        end: function () {
            Nenu.open.end = function () {
                $('#js_grid').trigger("reloadGrid");
            }
        },

        init: function () {
            Nenu.date.default({elem: '#publishTimeBefore'});
            Nenu.date.default({elem: '#publishTimeAfter'});
            this.fnTable();
            this.end();
            this.event();
        }
    };

    nePageLogic.init();

});


