/**
 * 删除选中菜单
 */
function operateDelete(menuId){
    //询问框
    layer.confirm('将删除当前菜单及其子菜单, 是否确认?', {
        btn: ['确定', '取消']
    }, function(){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/menu/deleteMenuDefine',
            data: {
                menuId: menuId
            },
            success:function(data){
                layer.msg(data.msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //关闭后的操作
                    reloadAndCloseWin();
                });
            }
        });
    });
}

/**
 * 修改保存当前菜单
 */
function operateSave(){
    // 保存按钮不再可用, 防止重复提交
    var $saveBtn = $('#saveBtn');
    $saveBtn.attr('disabled', true);
    // 序列化表单数据后提交
    var formData = $('#menuDefineForm').serializeArray();
    $.ajax({
        type: 'POST',
        url: '/menu/saveMenuDefine',
        dataType: 'json',
        async: false,
        data: formData,
        success: function(jsonData){
            var status = jsonData.status;
            var msg = jsonData.msg;
            if('200' == status){
                layer.msg(msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //保存成功后执行关闭
                    reloadAndCloseWin();
                });
            }else{
                layer.msg(msg, {
                    icon: 5,
                    time: 2000,//2s后自动关闭
                    btn: ['确定']
                });
            }
        }
    });
    $saveBtn.attr('disabled', false);
}

/**
 * 新增子菜单
 */
function operateInsert(parentMenuId, parentLevel){
    var url = '/menu/insertMenuDialog';
    if(parentMenuId && parentLevel){
        url += '?parentMenuId=' + parentMenuId + '&parentLevel=' + parentLevel;
    }
    location.href = url;
}

/**
 * 刷新父页面树表格, 关闭弹窗
 */
function reloadAndCloseWin(){
    // 先刷新父菜单树表格
    parent.reloadGrid();
    // 再关闭弹出层
    operateClose();
}

/**
 * 关闭(iframe页面关闭自身)
 */
function operateClose(){
    var index = parent.layer.getFrameIndex(window.name);//先得到当前iframe层的索引
    parent.layer.close(index);//再执行关闭
}