// 全局树对象
var ztreeObj;

$(document).ready(function () {
    // 初始构造图表树
    initEChartsTree();
});

// 请求后台构造图表树
function initEChartsTree(){
    // zTree属性设置
    var treeSetting = {
        view: {
            selectedMulti: false//不再支持多选
        },
        callback: {
            // 单击事件(显示当前字典配置信息)
            onClick: function(event, treeId, treeNode) {
                var echartId = treeNode.id; // 获取选择节点ID
                var isRoot = checkIsRootNode(treeNode); // 判断是否为根节点,返回true或false
                if(!isRoot){
                    document.getElementById('echartsFrame').src = '/echarts/showEchartForm?type=edit&echartId=' + echartId;
                }else{
                    document.getElementById('echartsFrame').src = '';
                }
            }
        }
    };
    // 请求数据初始化树
    $.ajax({
        type : 'post',
        url : '/echarts/initEchartsTree',
        dataType : 'json',
        success : function(jsonData){
            // 构建图表树
            $.fn.zTree.init($('#echartsTree'), treeSetting, jsonData);
            // 全局对象赋值
            ztreeObj = $.fn.zTree.getZTreeObj('echartsTree');
            // 默认选中根节点
            defaultSelectRoot();
        }
    });
}

/******************************************新增OR删除按钮操作功能START****************************************************/

/**
 * 点击新增按钮方法, 新增一条图表信息
 */
function operateInsert(){
    var nodes = ztreeObj.getSelectedNodes();
    var pEchartsNode = nodes[0];
    var echartId = pEchartsNode.id;//当前选中的节点ID作为父图表ID
    if(echartId =='0'){
        document.getElementById('echartsFrame').src = '/echarts/showEchartForm?type=add&echartId=' + echartId;
    }else{
        alertErrorLayer('请选择根节点添加图表!');
        return;
    }
}

/**
 * 删除选中字典及其子字典
 */
function operateDelete(){
    // 获取当前选中的图表节点
    var nodes = ztreeObj.getSelectedNodes();
    if(! nodes.length){
        alertErrorLayer('请选择要删除的图表!');
        return;
    }
    var selectNode = nodes[0];
    if(checkIsRootNode(selectNode)){
        alertErrorLayer('不能删除根节点!');
        return;
    }
    //询问框
    layer.confirm('将删除当前图表, 是否确认?', {
        btn: ['确定', '取消']
    }, function(){
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/echarts/deleteEcharts',
            data: { echartId: selectNode.id },
            success:function(data){
                layer.msg(data.msg, {
                    icon: 1,
                    btn: ['确定']
                }, function(){
                    //关闭后的操作
                    ztreeObj.removeNode(selectNode);//移除所选节点
                    defaultSelectRoot();//选中根节点
                });
            }
        });
    });
}

/******************************************新增OR删除按钮操作功能END****************************************************/

/**
 * 检验是否为根节点
 */
function checkIsRootNode(node){
    var nodeId = node.id;
    return '0' == nodeId;
}

/**
 * 默认选中根节点
 */
function defaultSelectRoot(){
    var nodes = ztreeObj.getNodes();
    if(nodes.length){
        ztreeObj.selectNode(nodes[0]);
    }
}

/**
 * 弹出错误警告框
 */
function alertErrorLayer(msg){
    layer.msg(msg, {
        icon: 5,
        time: 2000,//2s后自动关闭
        btn: ['确定']
    });
}