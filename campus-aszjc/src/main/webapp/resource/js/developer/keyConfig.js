/**
 * Created by dsideal-lee on 2016/10/29.
 */
$(document).ready(function () {

    $.jgrid.defaults.styleUI = 'Bootstrap';
    var colNames = ['序号', '规则名称', '规则类型', '开头字母', '流水号位数', '是否启用', '所属业务系统'];
    var colModel = [

        {
            name: 'id',
            index: 'id',
            editable: false,
            width: 60,
            sorttype: "int",
            search: true
        },
        {
            name: 'ruleName',
            index: 'ruleName',
            editable: true,
            width: 90,
            edittype: "text"
        },
        {
            name: 'ruleType',
            index: 'ruleType',
            editable: true,
            width: 100,
            edittype: "select",
            formatter: "select", //格式化显示为下拉列表的值
            editoptions: {value: "0:系统默认;1:UUID;2:树形结构;3:日期流水号;4:字母开头义流水号"}
        },
        {
            name: 'beginWith',
            index: 'beginWith',
            editable: true,
            width: 80,
            align: "right"
        },
        {
            name: 'flowNumWidth',
            index: 'flowNumWidth',
            editable: true,
            width: 80,
            align: "right"
        },
        {
            name: 'isUsed',
            index: 'isUsed',
            editable: true,
            width: 80,
            align: "center",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox"
        },
        {
            name: 'domain',
            index: 'domain',
            editable: true,
            width: 100,
            sortable: false
        }
    ];
    // Configuration for jqGrid Example 2
    $("#grid_list").jqGrid({
        url: "/meta/model",
        postData: formObj,
        datatype: "json",
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: colNames,
        colModel: colModel,
        pager: "#pager_list",
        editurl: "/meta/save",
        viewrecords: true,
        caption: "主键规则设置",
        jsonReader: {                                   //解析的jsonReader
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: false
        },
        hidegrid: false
    });
    // Setup buttons
    $("#grid_list").jqGrid('navGrid', '#pager_list',
        // the buttons to appear on the toolbar of the grid
        {
            edit: true,
            add: true,
            del: true,
            search: false,
            refresh: false,
            view: false,
            position: "left",
            cloneToTop: false
        },
        // options for the Edit Dialog
        {
            editCaption: "编辑框",
            recreateForm: true,
            checkOnUpdate: true,
            checkOnSubmit: false, //是否弹出数据改变确认框
            closeAfterEdit: true,
            editData: formObj,
            reloadAfterSubmit: true,  //保存后更新表格数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Add Dialog
        {
            closeAfterAdd: true,
            recreateForm: true,
            editData: formObj,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        // options for the Delete Dailog
        {
            editData: formObj,
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper').width();
        $('#grid_list').setGridWidth(width);
    });
});