define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');

    $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
    $(window).on('resize.neTableHt', function () {
        setTimeout(function () {
            $('.tab-content').height($('.tab-content').parent().height() - $('.tab-content').position().top);
        }, 200);
    });
});