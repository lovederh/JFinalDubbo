define(function (require) {
    'use strict';
    //加载组件
    require('mConfig');
    require('jqLayout');
    require('jqZtree');
    require('jqLayer');
    require('jqBootstrapTable');
    //加载event核心文件
    require('neEvent');
    //通知组件
    require('neToastr');
    var fnTable = function () {
        var deptId = -1;
        // 生成组织机构树
        var $tree = $('#js_tree').neTree({
            // ztree的配置参数 setting 完全按照ztree原生组件的参数设置即可
            setting: {
                async: {
                    enable: true,
                    url: '/emp/initDeptTree?businessTable=' + extraObj.businessTable
                },
                //回调函数参数对象
                callback: {
                    onClick: function (event, treeId, treeNode) {
                        $('#deptName').html(treeNode.name);
                    }
                }
            }
        });
    };

    $('body').layout({
        name: 'manLayout',
        west: {
            size: 250,
            children: {
                name: 'treeLayout'

            }
        },
        applyDemoStyles: false,
        closable: false,
        spacing_open: 15,
        onresize_end: function () {
            console.log(11111);
        },
        onload_end: fnTable
    });

    var nePageLogic = {
        event: function () {
            var option = Nenu.open.data.child;
            Nenu.context.event = {
                save: function () {
                    var nodes = $.fn.zTree.getZTreeObj('js_tree').getSelectedNodes();
                    var selectNode = nodes[0];
                    if (!nodes.length) {
                        Nenu.toastr.warning("请选择要调整到的部门!");
                        return;
                    }
                    // 所选择部门的id
                    var moveToNewDataId = selectNode.dataId;
                    if (selectNode.isLeaf == '0') {
                        Nenu.toastr.warning("该机构下还有子机构,请把人员调动到具体的组织机构!");
                        return;
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/emp/empMoveSave',
                        dataType: 'json',
                        async: false,
                        data: {
                            empId: extraObj.empId,
                            moveToNewDataId: moveToNewDataId
                        },
                        success: function (jsonData) {
                            Nenu.toastr.success(jsonData.msg);
                        }
                    })
                },
                fnClose: function () {
                    option.close();
                }
            }
        },
        /**
         * 初始化
         * @private
         **/
        init: function () {
            this.event();
        }
    };
    nePageLogic.init();
});

