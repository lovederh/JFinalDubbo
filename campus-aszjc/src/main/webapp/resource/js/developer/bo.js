$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    //主grid
    var colNameMaster = ['主键', '所属业务系统', '业务表名', '显示名', '基础表名', '业务类型', '拦截器', '自定义js路径', '数据过滤条件', '默认排序规则'];
    var colModelMaster = [                                    //列配置
        {
            name: 'id',
            index: 'id',
            editable: false,
            width: 60,
            sorttype: "int",
            search: true,
            hidden: true
        },
        {
            name: 'domain',
            index: 'domain',
            editable: true,
            hidden: false,
            hidedlg: true,
            width: 100
        },
        {
            name: 'boName',
            index: 'boName',
            editable: true,
            hidden: false,
            width: 100
        },
        {
            name: 'showName',
            index: 'showName',
            editable: true,
            hidden: false,
            width: 90
        },
        {
            name: 'tableName',
            index: 'tableName',
            editable: true,
            width: 80,
            hidden: false,
            hidedlg: true,
            align: "right",
            edittype: "select",
            formatter: "select",
            editoptions:{value:selectObj.tableNameValue}

        },
        {
            name: 'boType',
            index: 'boType',
            editable: true,
            width: 80,
            hidden: false,
            align: "right",
            edittype: "select",
            formatter: "select",
            editoptions: {value:"single:单表;tree:树结构;"}
        },
        {
            name: 'doAop',
            index: 'doAop',
            editable: true,
            width: 80,
            hidden: false,
            align: "right"
        },
        {
            name: 'jsPath',
            index: 'jsPath',
            editable: true,
            width: 80,
            hidden: false,
            align: "right"
        },
        {
            name: 'dataFilter',
            index: 'dataFilter',
            editable: true,
            width: 180,
            hidden: false,
            align: "right"
        },
        {
            name: 'defaultOrd',
            index: 'defaultOrd',
            editable: true,
            width: 180,
            hidden: false,
            align: "right"
        }

    ];
    $("#grid_master").jqGrid({
        url: '/meta/model',                            //从服务端获取数据的url
        datatype: "json",                              //返回数据的解析格式
        height: 150,                                   //grid的高度
        postData: formObjMaster,                       //向服务端传递的数据
        autowidth: true,                               //自动宽度
        shrinkToFit: true,                             //宽度自动调整
        rowNum: 20,                                     //每页条数
        rowList: [10, 20, 30],                          //每页条数列表
        colNames: colNameMaster, //列名
        colModel: colModelMaster,                            //列配置
        pager: "#pager_master",                        //工具栏的元素
        jsonReader: {                                   //解析的jsonReader
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: false
        },
        viewrecords: true,                             //查看数据
        caption: "表信息",                             // 表格的名称信息
        hidegrid: false,                               //是否允许隐藏
        editurl: "/bo/save",                          //增 删 改提交时的url
        onSelectRow: function (ids) {                  //选中某行触发事件
            if (ids == null) {
                layer.msg("请先选择一下表信息再进行编辑！", 1, ["确定"]);
                return;
                // if (jQuery("#grid_slave").jqGrid('getGridParam', 'records') > 0) {
                //     jQuery("#grid_slave").jqGrid('setGridParam', {url: "/meta/fields?pId=" + ids, page: 1});
                //     jQuery("#grid_slave").jqGrid('setCaption', "字段表: " + ids)
                //         .trigger('reloadGrid');
                // }
            } else {
                jQuery("#grid_slave").jqGrid('setGridParam', {url: "/meta/fields?pId=" + ids, page: 1});
                jQuery("#grid_slave").jqGrid('setCaption', "字段表: " + ids).trigger('reloadGrid');
            }
        }
    });
    // Setup buttons
    $("#grid_master").jqGrid('navGrid', '#pager_master', {
            edit: true,
            add: true,
            del: true,
            search: true,
            refresh: false
        }, {                                                        //设置编辑按钮的配置
            editCaption: "编辑",
            recreateForm: true,
            checkOnUpdate: true,
            checkOnSubmit: false,                                 //是否弹出数据改变确认提示框
            closeAfterEdit: true,
            reloadAfterSubmit: true,
            resize: true,
            editData: formObjMaster,                              //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置新增按钮的配置
            closeAfterAdd: true,
            recreateForm: true,
            editData: formObjMaster,                            //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        },
        {//设置删除按钮的配置
            delData: formObjMaster,                            //设置提交数据时的额外数据
            errorTextFormat: function (data) {
                return 'Error: ' + data.responseText
            }
        });
    //子grid
    var colNameSlave = ['主键', '业务表', '列字段', '列名', '类型', '字段长度', '主键否','父ID', '所属表', '显示宽度','启用', '列表显示',
        '表单显示', '对齐方式', '查看', '查询否', '查询配置', '编辑否', '编辑类型',
        '关联表','关联字典名','字典存储列','字典显示列','关联表过滤条件', '编辑配置', '排序否', '备注', '编辑规则', '排序'];
    var colModelSlave = [
        {name: 'id', index: 'id', editable: false, width: 55, frozen: true},
        {name: 'boName', index: 'boName', editable: false, width: 105, frozen: true},
        {name: 'colName', index: 'colName', editable: false, width: 100, frozen: true},
        {name: 'showName', index: 'showName', editable: true, width: 80, align: "left"},
        {name: 'type', index: 'type', width: 80, editable: false, align: "left"},
        {
            name: 'colLen',
            index: 'colLen',
            editable: false,
            width: 80,
            align: "left",
            edittype: "number",
            sortable: false,
            search: false
        },
        {
            name: 'isKey',
            index: 'isKey',
            editable: false,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'pId',
            index: 'pId',
            editable: false,
            width: 80,
            align: "left",
            edittype: "text",
            sortable: false,
            search: false
        },
        {
            name: 'tableName',
            index: 'tableName',
            editable: false,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },
        {name: 'width', index: 'width', editable: true, width: 50, align: "left", sortable: false, search: false},
        {
            name: 'isUsed',
            index: 'isUsed',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'hidden',
            index: 'hidden',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'hidedlg',
            index: 'hidedlg',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {name: 'align', index: 'align', editable: true, width: 150, align: "left", sortable: false, search: false},
        {
            name: 'viewable',
            index: 'viewable',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'search',
            index: 'search',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'searchoption',
            index: 'searchoption',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },
        {
            name: 'editable',
            index: 'editable',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {
            name: 'edittype',
            index: 'edittype',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false,
            edittype: "select",
            editoptions: {value: "text:text;date:date;select:select;radio:radio;checkbox:checkbox;textarea:textarea;password:password;button:button;image:image;file:file"}
        },
        {
            name: 'refTable',
            index: 'refTable',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },{
            name: 'dictType',
            index: 'dictType',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },{
            name: 'dictId',
            index: 'dictId',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },{
            name: 'dictName',
            index: 'dictName',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },
        {
            name: 'refTableFilter',
            index: 'refTableFilter',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },
        {
            name: 'eidtoptions',
            index: 'eidtoptions',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false
        },
        {
            name: 'sortable',
            index: 'sortable',
            editable: true,
            width: 80,
            align: "left",
            editoptions:{value:"true:false"},
            edittype: "checkbox",
            formatter: "checkbox",
            sortable: false,
            search: false
        },
        {name: 'title', index: 'title', editable: true, width: 150, align: "left", sortable: false, search: false},
        {
            name: 'editrule',
            index: 'editrule',
            editable: true,
            width: 150,
            align: "left",
            sortable: false,
            search: false,
            edittype: "select",
            formatter: "select",
            editoptions:{value:selectObj.requirValue}

        },
        {name: 'ord', index: 'ord', editable: true, width: 80, align: "left", sortable: false, search: false},
    ];
    var lastSel;
    jQuery("#grid_slave").jqGrid({
        url: '/meta/fields?id=0&pId=0',
        datatype: "json",
        postData: formObjSlave,                       //向服务端传递的数据
        colNames: colNameSlave,
        colModel: colModelSlave,
        autowidth: true,
        height: 250,
        shrinkToFit: false,
        forceFit: true,
        //autoScroll: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        pager: '#pager_slave',
        sortname: 'item',
        viewrecords: true,
        sortorder: "asc",
        multiselect: false,
        hidegrid: false,
        editurl: "/bo/save",
        ondblClickRow: function (id) {
            if (id && id !== lastSel) {
                jQuery('#grid_slave').restoreRow(lastSel);
                lastSel = id;
            }
            jQuery('#grid_slave_ilcancel').removeClass("ui-disabled"); //取消编辑按钮可用
            jQuery('#grid_slave_ilsave').removeClass("ui-disabled"); //保存按钮可用
            jQuery('#grid_slave').editRow(id, true, null, null, null, formObjSlave); //编辑行，回车后保存
        },
        jsonReader: {
            root: "list",
            page: "pageNumber",
            total: "totalPage",
            repeatitems: true
        },
        caption: "表字段信息"
    });
    $("#grid_slave").jqGrid('navGrid', '#pager_slave', {
        edit: true,
        add: false,
        del: false,
        view: true,
        search: true,
        refresh: false
    }, {                                                        //设置编辑按钮的配置
        editCaption: "编辑",
        recreateForm: true,
        checkOnUpdate: true,
        checkOnSubmit: false,                                 //是否弹出数据改变确认提示框
        closeAfterEdit: false,
        reloadAfterSubmit: true,
        resize: true,
        editData: formObjSlave,                              //设置提交数据时的额外数据
        errorTextFormat: function (data) {
            return 'Error: ' + data.responseText
        }
    }, {}, {});
    jQuery("#grid_slave").jqGrid('inlineNav', "#pager_slave", {
        add: false,
        edit: false,
        save: false,
        cancel: true
    });
    jQuery("#grid_slave").jqGrid("setFrozenColumns");

    jQuery("#grid_slave").jqGrid("navButtonAdd", "#pager_slave", {
            caption: "keyRule",
            buttonicon: "ui-icon-add",
            onClickButton: function(){
                var gsr = jQuery("#grid_slave").jqGrid('getGridParam','selrow');
                if(gsr){
                    var rowData = jQuery("#grid_slave").jqGrid("getRowData", gsr);
                    //jQuery("#grid_slave").jqGrid('GridToForm',gsr,"#order");
                    if(rowData.isKey !== "true"){
                        layer.alert("请选择主键所在的行！");
                    }else{
                        layer.open({
                            type: 2,
                            title: '主键规则设置',
                            shadeClose: true,
                            shade: false,
                            maxmin: true, //开启最大化最小化按钮
                            area: ['600px', '500px'],
                            content: '/bo/configKeyRule?v=2.0.0&keyCol='+rowData.colName+'&dtoName=t_sys_keyrule&boName='+rowData.boName+'&domain='+formObjSlave.domain,
                            end: function(){
                                console.log("关闭后执行的？");
                            }
                        });
                    }
                } else {
                    layer.alert("请选择主键所在的行！");
                }
            },
            position: "last",
            title: "设置主键规则",
            cursor: "pointer"
        }
    )
    //强制显示滚动条
    $("#grid_slave").closest(".ui-jqgrid-bdiv").css({"overflow-x": "scroll"});
    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper').width();
        $('#grid_master').setGridWidth(width);
        $('#grid_slave').setGridWidth(width);
    });
});