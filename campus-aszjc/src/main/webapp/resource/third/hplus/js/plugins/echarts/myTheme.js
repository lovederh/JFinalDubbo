/**
 * 自定义图表颜色风格
 * zhaoyou 2016-06-30
 */
function Theme() {
	var myTheme = {
		color: [
	        '#FF0000','#00FF00','#FF8000',
	        '#00FFFF','#D200D2','#EAC100',
	        '#5B5B5B','#FF0080','#07a2a4',
	        '#f5994e','#8d98b3','#FF8EFF',
	        '#FCCE10','#c05050','#59678c',
	        '#7eb00a','#c14089','#5ab1ef',
	        '#C6E579','#D2691E','#ffc0ff',
	        '#ff4040','#400080','#c0ff00',
	        '#ff8080','#60c0ff','#ff60ff',
	        '#a04020','#000020','#ffff20',
	        '#c04060','#008040','#a0c0ff',
	        '#E6E6FA','#8600FF','#ADFF2F',
	        '#B8860B','#40E0D0','#FAFAD2'
	    ]
	};
	this.getTheme = function(name) {
		if (name == 'myTheme')
			return myTheme;
	}
}