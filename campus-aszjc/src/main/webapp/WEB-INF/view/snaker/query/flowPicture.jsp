<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">
<head>
<title>流程图</title>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>

<link rel="stylesheet" href="/resource/third/snaker/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="/resource/third/snaker/css/snaker.css" type="text/css" media="all" />
</head>

<body>

    <div id="js_flowPicture" style="width: 100%; height: 100%;" >
    </div>

<script src="/resource/third/snaker/raphael-min.js" type="text/javascript"></script>
<script src="/resource/third/snaker/jquery-ui-1.8.4.custom/js/jquery.min.js" type="text/javascript"></script>
<script src="/resource/third/snaker/jquery-ui-1.8.4.custom/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="/resource/third/snaker/snaker.designer.js" type="text/javascript"></script>
<script src="/resource/third/snaker/snaker.model.js" type="text/javascript"></script>
<script src="/resource/third/snaker/snaker.editors.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    // 发送后台请求, 请求流程图构造数据
    $.ajax({
        type: "get",
        dataType: "json",
        url: "/flowQuery/flowPictureData",
        data: {
            processId: '${processId}',
            orderId: '${orderId}'
        },
        async: false,
        globle:false,
        success: function(data){
            showPicture(data.process, data.state);
        }
    });
});

// 流程图展示方法, 注意路径设置
function showPicture(process, state) {

    $('#js_flowPicture').snakerflow($.extend(true, {
            basePath: "/resource/third/snaker/",
            orderId: '${orderId}',
            restore: eval("(" + process + ")"),
            editable: false
        }, eval("(" + state + ")")
    ));
}
</script>

</body>
</html>
